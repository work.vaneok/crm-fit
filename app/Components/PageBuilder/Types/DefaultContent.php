<?php
namespace App\Components\PageBuilder\Types;

use App\Components\PageBuilder\Column;
use App\Components\PageBuilder\Content;
use App\Components\PageBuilder\Row;

class DefaultContent extends BaseContent
{

    public function dashboard()
    {
        return $this->content->row(function (Row $row) {

            $row->column(12, function (Column $column) {
                $column->append(view('_content.dashboard.index')->render());
            });
        });
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Lucky
 * Date: 11.03.2020
 * Time: 12:18
 */

namespace App\Repositories;


use App\Components\CrmHelper;
use App\Models\Company;
use App\Models\Order;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class OrderRepository
{

    protected $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @param $paymentSource
     * @return Collection
     */
    public function getSubscriptionsForCash($paymentSource)
    {
        $subsTable = (new Subscription())->getTable();
        $orderTable = (new Order())->getTable();

        return Order::select([$subsTable . '.validity', DB::raw('COUNT(*) as `count`'), DB::raw('SUM(' . $orderTable . '.price) as `price`')])
            ->where($orderTable . '.company_id', $this->company->id)
            ->join($subsTable, 'order_item_id', '=', $subsTable . '.id')
            ->payed()
            ->subscriptions()
            ->paymentSource($paymentSource)
            ->groupBy($subsTable . '.validity')
            ->get();

    }

    /**
     * @param $paymentSource
     * @param $countDays
     * @return Collection
     */
    public function getSubscriptionsForCashPerDays($paymentSource, $countDays)
    {
        $subsTable = (new Subscription())->getTable();
        $orderTable = (new Order())->getTable();

        $dateFrom = Carbon::now()->subDays($countDays)->startOfDay();
        $dateEnd = Carbon::now()->endOfDay();
        return Order::select([$subsTable . '.validity', DB::raw('COUNT(*) as `count`'), DB::raw('SUM(' . $orderTable . '.price) as `price`')])
            ->where($orderTable . '.company_id', $this->company->id)
            ->whereBetween($orderTable . '.created_at', [$dateFrom->toDateTimeString(), $dateEnd->toDateTimeString()])
            ->join($subsTable, 'order_item_id', '=', $subsTable . '.id')
            ->payed()
            ->subscriptions()
            ->paymentSource($paymentSource)
            ->groupBy($subsTable . '.validity')
            ->get();
    }
}

<?php
namespace App\Http\Requests\Staffs;

use App\Constants\CompanyStaffRoles;
use App\Repositories\CompanyRolesRepository;
use App\Rules\PhoneFormatRule;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class UpdateCompanyStaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roles = app(CompanyRolesRepository::class)->getForSelectBoxForStaff()->keys();
        return [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'phone' => ['required', new PhoneFormatRule(), Rule::unique('users')->ignore($this->staff->user_id, 'id')],
            'role_in_company' => ['required', Rule::in($roles)],
            'birthday' => ['nullable', 'date_format:Y-m-d'],
            'trainer_price' => ['nullable', 'required_if:role_in_company,' . CompanyStaffRoles::TRAINER, 'numeric']
        ];
    }
}

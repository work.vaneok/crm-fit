<?php
namespace App\Http\Requests\Staffs;

use App\Components\CrmHelper;
use App\Constants\CompanyStaffRoles;
use App\Http\Requests\Traits\DefaultCompanyIdInRequest;
use App\Repositories\CompanyRolesRepository;
use App\Repositories\ValidityRepository;
use App\Rules\PhoneFormatRule;
use App\Rules\UniqueStaffPhoneRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateCompanyStaffRequest extends FormRequest
{
    use DefaultCompanyIdInRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roles = app(CompanyRolesRepository::class)->getForSelectBoxForStaff()->keys();
        return [
            'company_id' => ['required'],
            'first_name' => ['required'],
            'last_name' => ['required'],
            'phone' => ['required', new PhoneFormatRule(), /*Rule::unique('users')*/new UniqueStaffPhoneRule($this->get('company_id'))],
            'role_in_company' => ['required', Rule::in($roles)],
            'birthday' => ['nullable', 'date_format:Y-m-d'],
            'trainer_price' => ['nullable', 'required_if:role_in_company,' . CompanyStaffRoles::TRAINER, 'numeric']
//            'email' => ['nullable', 'email'],
        ];
    }

}

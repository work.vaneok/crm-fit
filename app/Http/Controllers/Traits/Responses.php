<?php
namespace App\Http\Controllers\Traits;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

trait Responses
{
    public function response($data, $code = 200)
    {
        return response()->json($data, $code);
    }
}

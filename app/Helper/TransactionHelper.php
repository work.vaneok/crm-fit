<?php

namespace App\Helper;

class TransactionHelper
{
    const TRANSACTION_REFILL_TYPE_ID = 1;
    const TRANSACTION_REFILL_NAME = 'refill';
    const TRANSACTION_REFILL_DESCRIPTION = 'Пополнение';

    const TRANSACTION_WRITEOFF_TYPE_ID = 2;
    const TRANSACTION_WRITEOFF_NAME = 'writeoff';
    const TRANSACTION_WRITEOFF_DESCRIPTION = 'Списание';

    const TRANSACTION_BUY_TYPE_ID = 3;
    const TRANSACTION_BUY_NAME = 'buy';
    const TRANSACTION_BUY_DESCRIPTION = 'Покупка';

    const TRANSACTION_CLOSE_PAYMENT_TYPE_ID = 4;
    const TRANSACTION_CLOSE_PAYMENT_NAME = 'buy';
    const TRANSACTION_CLOSE_PAYMENT_DESCRIPTION = 'Закрытие платежа';

}

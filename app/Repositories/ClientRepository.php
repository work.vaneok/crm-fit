<?php
namespace App\Repositories;

use App\Models\Client;
use App\Models\Company;
use App\Models\CompanySetting;

/**
 * Посетители все
 *
 * Class ClientRepository
 * @package App\Repositories
 */
class ClientRepository
{


    /**
     * @return Client|null
     */
    public function findByPhone($phone)
    {
        return Client::firstWhere('phone', $phone);
    }

}

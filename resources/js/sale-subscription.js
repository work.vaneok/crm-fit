
// import Vue from 'vue'
import vSelect from 'vue-select'
import SaleSubscription from './components/SaleSubscription'

Vue.component('v-select', vSelect)

const app = new Vue({
    el: '#sale-form',
    components: {
        SaleSubscription
    }
});

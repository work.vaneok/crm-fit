<div class="row">
    <div class="card" >
        <div class="card-body">
            <a href="{{ route('sale.subscription') }}" class="btn btn-primary">{{ __('Продать абонемент') }}</a>
        </div>
    </div>

    <div class="card" >
        <div class="card-body">
            <a href="{{ route('client-visit.check') }}" class="btn btn-success">{{ __('Посещение') }}</a>
        </div>
    </div>

</div>

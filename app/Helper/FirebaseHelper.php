<?php

namespace App\Helper;


class FirebaseHelper
{
    const FIREBASE_DEFAULT_TOPIC = 1;
    const FIREBASE_NEWS_TOPIC = 2;

    public static function getFirebaseTopics()
    {
        return [
            self::FIREBASE_DEFAULT_TOPIC => 'Общий',
            self::FIREBASE_NEWS_TOPIC => 'Новости',

        ];
    }

}

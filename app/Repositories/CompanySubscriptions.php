<?php
namespace App\Repositories;

use App\Models\Client;
use App\Models\Company;
use App\Models\CompanyClient;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CompanySubscriptions
{
    /**
     * @var Company
     */
    private $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Client|null
     */
    public function listForMobile()
    {
        return $this->company->subscriptions()->active()->forMobile()->get();
    }
}

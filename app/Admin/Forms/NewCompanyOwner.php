<?php

namespace App\Admin\Forms;

use App\Actions\CompanyOwner\CreateCompanyOwnerAction;
use App\Repositories\TariffRepository;
use App\Rules\PhoneFormatRule;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;

class NewCompanyOwner extends Form
{
    /**
     * The form title.
     *
     * @var string
     */
    public $title = 'Company Owner';

    /**
     * Handle the form request.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request)
    {
        (new CreateCompanyOwnerAction())->execute($request);

        admin_success('Processed successfully.');

        return redirect(route('admin.companies.index'));
    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $tariffs = app(TariffRepository::class);

        $this->text('title', 'Название компании')->rules('required');
        $this->datetime('available_from', 'Доступна с')->rules('required');
        $this->datetime('available_to', 'Доступна до')->rules('required');
        $this->text('first_name', 'Имя собственника')->rules('required');
        $this->text('last_name', 'Фамилия собственника')->rules('required');
        $this->text('phone', 'Телефон')->rules(['required', 'unique:users', 'numeric', new PhoneFormatRule()], [
            'regex' => 'Номер должен начинаться с 380 и иметь формат 380123456789'
        ])->placeholder('380********');
        $this->select('tariff_id', 'Тариф')->rules('required')->options($tariffs->getForSelectBoxForAdmin());
    }

    /**
     * The data of the form.
     *
     * @return array $data
     */
    public function data()
    {
        return [
            'available_from' => now(),
            'available_to' => now()->addYear(),
        ];
    }
}

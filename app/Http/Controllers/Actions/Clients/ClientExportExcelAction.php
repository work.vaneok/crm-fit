<?php

namespace App\Http\Controllers\Actions\Clients;

use App\Export\ClientParameterHistoryExport;
use App\Http\Controllers\Actions\BaseAction;
use App\Models\Client;
use App\Models\CompanyClientParameterHistory;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ClientExportExcelAction extends BaseAction
{

    public $client_id;
    public $company_id;

    public function rules(): array
    {
        return [
            'client_id' => ['integer', 'required'],
            'company_id' => ['integer', 'required'],
        ];
    }

    public function action()
    {

        $items = CompanyClientParameterHistory::query()->where([
            'company_id' => $this->company_id,
            'client_id' => $this->client_id
        ])->orderBy('id', 'desc')
            ->get();
        $response = [];
        $counter = 0;

        $response[$counter][CompanyClientParameterHistory::C_DATE] = 'Дата';
        $response[$counter][CompanyClientParameterHistory::C_WEIGHT] = 'Вес';
        $response[$counter][CompanyClientParameterHistory::C_SHOULDER_VOLUME] = 'Объем плеч	';
        $response[$counter][CompanyClientParameterHistory::C_WAIST] = 'Талия';
        $response[$counter][CompanyClientParameterHistory::C_HIPS] = 'Бедра';
        $response[$counter][CompanyClientParameterHistory::C_BICEPS_VOLUME] = 'Объем бицепса';
        $response[$counter][CompanyClientParameterHistory::C_BREAST_VOLUME] = 'Объем груди';
        $response[$counter][CompanyClientParameterHistory::C_LEG_VOLUME] = 'Объем ноги';
        $response[$counter][CompanyClientParameterHistory::C_CAVIAR_VOLUME] = 'Объем икр';
        $response[$counter][CompanyClientParameterHistory::C_COMMENT] = 'Комментарий';

        $counter++;
        /** @var CompanyClientParameterHistory $item */
        foreach ($items as $item) {
            $response[$counter][CompanyClientParameterHistory::C_DATE] = $item->formatDate(CompanyClientParameterHistory::C_DATE);
            $response[$counter][CompanyClientParameterHistory::C_WEIGHT] = $item[CompanyClientParameterHistory::C_WEIGHT];
            $response[$counter][CompanyClientParameterHistory::C_SHOULDER_VOLUME] = $item[CompanyClientParameterHistory::C_SHOULDER_VOLUME];
            $response[$counter][CompanyClientParameterHistory::C_WAIST] = $item[CompanyClientParameterHistory::C_WAIST];
            $response[$counter][CompanyClientParameterHistory::C_HIPS] = $item[CompanyClientParameterHistory::C_HIPS];
            $response[$counter][CompanyClientParameterHistory::C_BICEPS_VOLUME] = $item[CompanyClientParameterHistory::C_BICEPS_VOLUME];
            $response[$counter][CompanyClientParameterHistory::C_BREAST_VOLUME] = $item[CompanyClientParameterHistory::C_BREAST_VOLUME];
            $response[$counter][CompanyClientParameterHistory::C_LEG_VOLUME] = $item[CompanyClientParameterHistory::C_LEG_VOLUME];
            $response[$counter][CompanyClientParameterHistory::C_CAVIAR_VOLUME] = $item[CompanyClientParameterHistory::C_CAVIAR_VOLUME];
            $response[$counter][CompanyClientParameterHistory::C_COMMENT] = $item[CompanyClientParameterHistory::C_COMMENT];
            $counter++;
        }
        $client = Client::findOrFail($this->client_id);
        $filename = 'Параметры-' . $client->first_name . '-' . $client->last_name . '-' . $client->phone . '-' . Carbon::now()->toDateString() . '.xlsx';

        $export = new ClientParameterHistoryExport($response);
        return Excel::download($export, $filename);

    }

}

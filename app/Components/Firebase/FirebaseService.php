<?php

namespace App\Components\Firebase;

use App\Forms\FirebaseNotificationForm;
use App\Models\ClientDevice;
use App\Models\FirebaseNotification;
use App\Models\FirebaseNotificationReceiver;
use Illuminate\Support\Facades\DB;
use Kreait\Firebase\Messaging\CloudMessage;

class FirebaseService
{
    private $firebase;

    public function __construct()
    {
        $this->firebase = app('firebase.messaging');
    }

    public static function getFirestoreDB()
    {
        return app('firebase.firestore')->database();
    }

    public function send(FirebaseNotificationForm $form)
    {
        try {
            DB::beginTransaction();
        } catch (\Exception $exception) {
            throw  new \Exception($exception->getMessage());
        }

        try {

            $clientDevices = ClientDevice::whereIn('id', $form->getDevices())->get();
            $notification = [];
            $notification['title'] = $form->title;

            $firebase = new FirebaseNotification();
            $firebase->title = $form->getTitle();

            if ($form->description) {
                $notification['body'] = $form->title;
                $firebase->descriptions = $form->description;
            }

            if ($form->media_url) {
                $notification['image'] = $form->title;
                $firebase->image_url = $form->media_url;
            }

            $firebase->topic_id = $form->getTopicId();
            $firebase->company_id = $form->getCompanyId();
            $firebase->data = $form->getJSONData();
            $firebase->save();

            $message = CloudMessage::fromArray([
                'notification' => $notification,
                'data' => $form->getData()
            ]);

            /** @var ClientDevice $clientDevice */
            foreach ($clientDevices as $clientDevice) {
                $firebaseStatus = new FirebaseNotificationReceiver();
                $firebaseStatus->notification_id = $firebase->id;
                $firebaseStatus->client_id = $clientDevice->client_id;
                $firebaseStatus->company_id = $clientDevice->company_id;
                $firebaseStatus->device_id = $clientDevice->id;
                $firebaseStatus->topic_id = $form->getTopicId();
                $firebaseStatus->status = 1;

                $report = $this->firebase->sendMulticast($message, [$clientDevice->fcm_id]);
                if ($report->hasFailures()) {
                    foreach ($report->failures()->getItems() as $failure) {
                        $firebaseStatus->data .= $failure->error()->getMessage();
                    }
                    $firebaseStatus->status = 0;
                }
                //  $tokenToDelete = $report->unknownTokens() + $report->invalidTokens();
                $firebaseStatus->save();
            }
            DB::commit();
            return true;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw  new \Exception($exception->getMessage());

        }

    }
    /*public function sendMulticast($device_identifiers, $title, $description, $image_url = null, $topic_id = GlobalHelper::FIREBASE_DEFAULT_TOPIC)
    {
        try {
            DB::beginTransaction();
        } catch (\Exception $exception) {
            throw  new \Exception($exception->getMessage());
        }
        try {
            $clientDevices = ClientDevice::whereIn('id', $device_identifiers)->get();

            $firebase = new FirebaseNotification();
            $firebase->title = $title;
            $firebase->descriptions = $description;
            $firebase->image_url = $image_url;
            $firebase->topic_id = $topic_id;
            $firebase->save();

            $message = CloudMessage::fromArray([
                'notification' => [
                    'title' => $title,
                    'body' => $description,
                    'image' => $image_url
                ],
            ]);

            foreach ($clientDevices as $clientDevice) {
                $firebaseStatus = new FirebaseNotificationReceiver();
                $firebaseStatus->notification_id = $firebase->id;
                $firebaseStatus->client_id = $clientDevice->client_id;
                $firebaseStatus->company_id = $clientDevice->company_id;
                $firebaseStatus->device_id = $clientDevice->id;
                $firebaseStatus->topic_id = $topic_id;
                $firebaseStatus->status = 1;

                $report = $this->firebase->sendMulticast($message, [$clientDevice->fcm_id]);
                if ($report->hasFailures()) {
                    foreach ($report->failures()->getItems() as $failure) {
                        $firebaseStatus->data .= $failure->error()->getMessage();
                    }
                    $firebaseStatus->status = 0;
                }
                //  $tokenToDelete = $report->unknownTokens() + $report->invalidTokens();
                $firebaseStatus->save();
            }
            DB::commit();

        } catch (\Exception $exception) {
            DB::rollBack();
            throw  new \Exception($exception->getMessage());

        }

    }*/
}

<?php

namespace App\Models\Traits;

trait QuantityOfOccupationText
{

    public function getQuantityOfOccupationTextAttribute()
    {
        return ($this->quantity_of_occupation == -1) ? __('Безлим') : $this->quantity_of_occupation;
    }
}

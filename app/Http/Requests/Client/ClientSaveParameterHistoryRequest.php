<?php

namespace App\Http\Requests\Client;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ClientSaveParameterHistoryRequest
 * @package App\Http\Requests\Client
 * @property string $weight
 * @property string $shoulder_volume
 * @property string $waist
 * @property string $hips
 * @property string $biceps_volume
 * @property string $breast_volume
 * @property string $leg_volume
 * @property string $caviar_volume
 * @property string $comment
 */
class ClientSaveParameterHistoryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'date' => [
                'required',
                'string'
            ],
            'weight' => [
                'required',
                'string'
            ],
            'shoulder_volume' => [
                'required',
                'string'
            ],
            'waist' => [
                'required',
                'string'
            ],
            'hips' => [
                'required',
                'string'
            ],
            'biceps_volume' => [
                'required',
                'string'
            ],
            'breast_volume' => [
                'required',
                'string'
            ],
            'leg_volume' => [
                'required',
                'string'
            ],
            'caviar_volume' => [
                'required',
                'string'
            ],
            'comment' => [
                'string'
            ],
        ];
    }


}

<?php

namespace App\Http\Requests\Chat;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ClientGlobalRequest
 * @package App\Http\Requests\Chat
 * @property int $client_id
 */
class ClientGlobalRequest extends FormRequest
{

    public function rules()
    {
        return [
            'client_id' => ['required'],
        ];
    }


}

@extends('layouts.dashboard')

@section('title')
    {{ __('Абонементы') }}
@endsection

@push('scripts')
    <script src="{{ asset('js/subscriptions/staff-clients.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".js-example-basic-single").select2();
        });
    </script>
@endpush

@section('content')
    <div class="header__filter" style=" margin-bottom: 15px;">
        <div class="row">
            <div class="col-1">
                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#filter"
                        style="margin-left: 0;">{{ __('Фильтр') }}</button>
            </div>
            <div class="col-10">
            </div>
            {{--<div class="col-1">
                <form action="{{ route('subscriptions.export') }}" accept-charset="UTF-8">
                    <input style="display: none" type="text" value="{{get_filter_value('client_id')}}"
                           name="filter[client_id]" placeholder="Id">
                    <input style="display: none" type="text" value="{{get_filter_value('date_from')}}"
                           name="filter[date_from]" placeholder="Id">
                    <input style="display: none" type="text" value="{{get_filter_value('date_to')}}"
                           name="filter[date_to]" placeholder="Id">
                    <div class="col-auto">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-file-export"></i>Excel</button>
                    </div>
                </form>
            </div>--}}
            <div id="filter" class="collapse col-12" style="margin-top: 15px;">
                <form action="" class="grid-filter" method="get" id="filterForm">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="filter[client_id]"><b>{{ __('Клиент') }}</b></label><br>
                            @search_select2(['name' => 'client_id', 'options' => $clients, 'label' => __('Клиент')])
                        </div>

                        <div class="form-group col-md-2">
                            <label for="filter[client_id]"><b>{{ __('Дата') }}</b></label>
                            @search_date_range(['name' => 'date'])
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary "><i
                                    class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                        </div>
                        <div class="form-group">
                            <a href="/{{ Route::current()->uri }}" class="btn btn-danger "><i
                                    class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">{{ __('Об Клиенте') }}</th>
                <th scope="col">{{ __('Абонемент') }}</th>
                <th scope="col">{{ __('Действия') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>
                        {{$item['client']['first_name'].' '. $item['client']['last_name']}} <br>
                        <a href="tel:+{{ $item['client']['phone'] }}"> {{ $item['client']['phone'] }} </a>

                    </td>
                    {{--    <td>
                            <a href="{{route('clients.profile',['client' => $item['client']['id']])}}">  {{$item['client']['first_name'].' '. $item['client']['last_name']}}
                                <i class="fa fa-user" aria-hidden="true"></i></a> <br>
                            <a href="tel:+{{ $item['client']['phone'] }}"> {{ $item['client']['phone'] }} </a>
                        </td>--}}
                    <td>
                        <span><b>№</b>: {{  $item['id'] }}</span>
                        <span><b>{{__('Название')}}</b>: {{  $item['subscription']['title'] }}</span> <br>
                        <span><b>{{__('Действителен')}}</b>: {{  $item['available_date'] }}</span> <br>
                        <span><b>{{__('Занятий')}}</b>: {{  $item['quantity_of_occupation'] }}</span> <br>

                        <?php
                        if ($item['deleted_at']) {
                        ?>
                        <span class="badge badge-danger"><?=__('Удален')?></span>
                        <?php
                        } elseif ($item['active']) {
                        ?>
                        <span class="badge badge-success"><?=__('Активный')?></span>
                        <?
                        }else {
                        ?>
                        <span class="badge badge-secondary"><?=__('Не активный')?></span>
                        <?
                        }

                        ?>
                    </td>
                    <td>
                        <div class="btn-group btn-group-toggle" data-toggle="buttons" style="margin-bottom: 5px;">
                            <a href="<?= route('chat.client-global-chat', ['client_id' => $item['client']['id']])?>"
                               class="btn btn btn-sm btn-primary"
                               role="button" aria-pressed="true">{{__('ЧАТ')}}</a>
                        </div>
                        <br>
                        <div class="btn-group btn-group-toggle" data-toggle="buttons" style="margin-bottom: 5px;">
                            <a href="<?= route('clients.parameters-page', ['client_id' => $item['client']['id']])?>"
                               class="btn btn btn-sm btn-primary"
                               role="button" aria-pressed="true"> {{__('Параметры')}} </a>
                        </div>
                        <br>
                        <div class="btn-group btn-group-toggle" data-toggle="buttons" style="margin-bottom: 5px;">
                            <a href="<?= route('clients.history-page', ['client_id' => $item['client']['id']])?>"
                               class="btn btn btn-sm btn-primary"
                               role="button" aria-pressed="true"> {{__('Снимки')}} </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $clientSubsModel ? $clientSubsModel->appends(request()->only('filter'))->links() : '' }}

    </div>
    @include('inc.ajax-form-modal.form')
@endsection

@section('actions')

@endsection

<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Components\Sms\Models{
/**
 * App\Components\Sms\Models\SentSms
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $internal_id
 * @property string|null $phone
 * @property int $status
 * @property string $content
 * @property string|null $sms_id
 * @property string|null $response
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms whereInternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms whereResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms whereSmsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Components\Sms\Models\SentSms whereUpdatedAt($value)
 */
	class SentSms extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Client
 *
 * @property int $id
 * @property int|null $created_by_user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string|null $email
 * @property \Illuminate\Support\Carbon|null $birthday
 * @property string $password
 * @property string|null $created_by
 * @property int $sex 1 - М, 2 - Ж
 * @property int $status_id
 * @property string|null $remember_token
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ClientSubscription[] $activeSubscriptions
 * @property-read int|null $active_subscriptions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Company[] $companies
 * @property-read int|null $companies_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client byCompany($company_id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereUpdatedAt($value)
 */
	class Client extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ClientComment
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $client_id
 * @property int $created_by
 * @property string $content
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientComment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientComment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientComment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientComment whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientComment whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientComment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientComment whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientComment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientComment whereUpdatedAt($value)
 */
	class ClientComment extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ClientSubscription
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $company_id
 * @property int $company_location_id
 * @property int $client_id
 * @property int $subscription_id
 * @property \Illuminate\Support\Carbon|null $available_from Начало действия абонемента
 * @property \Illuminate\Support\Carbon|null $available_to Конец действия абонемента
 * @property \Illuminate\Support\Carbon $available_to_before_begin_of_use Конечная дата действия абонемента до начала активности
 * @property int $quantity_of_occupation Количество занятий
 * @property int $validity Период дейсвия, 1 день 1 месяц, 1 год
 * @property int $has_trainer
 * @property int $can_share Можно делиться
 * @property string|null $created_by
 * @property int $order_id
 * @property int $start_of_activity Начало действия абонемента: С момента продажи | С первого посещения
 * @property int $created_by_user_id
 * @property int $is_current
 * @property int $is_active
 * @property string|null $inactive_reason Причина, по которое перестал был активным
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ClientVisit[] $allVisits
 * @property-read int|null $all_visits_count
 * @property-read mixed $active
 * @property-read mixed $is_unlimited
 * @property-read mixed $quantity_of_occupation_text
 * @property-read mixed $validity_text
 * @property-read \App\Models\Order $order
 * @property-read \App\Models\Subscription $subscription
 * @property-read \App\Models\Client $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ClientVisit[] $visits
 * @property-read int|null $visits_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription active($is_active = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription byCompany($company_id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription current($is_current = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereAvailableFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereAvailableTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereAvailableToBeforeBeginOfUse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereCanShare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereCompanyLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereHasTrainer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereInactiveReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereIsCurrent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereQuantityOfOccupation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereStartOfActivity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientSubscription whereValidity($value)
 */
	class ClientSubscription extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ClientVisit
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $client_subscription_id
 * @property int $client_id
 * @property int $company_id
 * @property int $company_location_id
 * @property string|null $created_by
 * @property int $created_by_user_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $deleted_by
 * @property string|null $delete_reason
 * @property int|null $key_number
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\ClientSubscription $clientSubscription
 * @property-read \App\Models\Company $company
 * @property-read \App\User $createdBy
 * @property-read \App\User|null $deletedBy
 * @property-read mixed $client_name
 * @property-read \App\Models\CompanyLocation $location
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit byCreatedAt($date)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit byCreatedAtFrom($date)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit byCreatedAtTo($date)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit byPhone($phone)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ClientVisit onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereClientSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereCompanyLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereDeleteReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereKeyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientVisit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ClientVisit withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ClientVisit withoutTrashed()
 */
	class ClientVisit extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Company
 *
 * @property int $id
 * @property string $uuid
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon $available_from
 * @property \Illuminate\Support\Carbon $available_to
 * @property int $owner_id
 * @property string $title
 * @property string|null $description
 * @property int $tariff_id
 * @property string|null $created_by
 * @property string|null $image
 * @property-read \App\Models\CompanyAdminSettings $adminSettings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanyLocation[] $locations
 * @property-read int|null $locations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanySetting[] $settings
 * @property-read int|null $settings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanyStaff[] $staffs
 * @property-read int|null $staffs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @property-read \App\Models\Tariff $tariff
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereAvailableFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereAvailableTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereTariffId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereUuid($value)
 */
	class Company extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanyAdminSettings
 *
 * @property int $company_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $domain
 * @property array $settings
 * @property-read \App\Models\Company $company
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyAdminSettings byCompany($company_id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyAdminSettings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyAdminSettings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyAdminSettings query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyAdminSettings whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyAdminSettings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyAdminSettings whereDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyAdminSettings whereSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyAdminSettings whereUpdatedAt($value)
 */
	class CompanyAdminSettings extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanyClient
 *
 * @property int $client_id
 * @property int $company_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $barcode
 * @property string|null $created_by
 * @property int|null $created_by_user_id
 * @property string|null $api_token
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ClientSubscription[] $activeSubscriptions
 * @property-read int|null $active_subscriptions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ClientVisit[] $allVisits
 * @property-read int|null $all_visits_count
 * @property-read \App\Models\Client $client
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Company[] $company
 * @property-read int|null $company_count
 * @property-read \App\User|null $createdBy
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient byCreatedAt($date)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient byCreatedAtFrom($date)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient byCreatedAtTo($date)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient byPhone($phone)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient whereBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyClient withLastVisitDate()
 */
	class CompanyClient extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanyLocation
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $company_id
 * @property string $title
 * @property string|null $description
 * @property string|null $created_by
 * @property string|null $address
 * @property string|null $email
 * @property array|null $phones
 * @property string|null $image
 * @property-read \App\Models\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation wherePhones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyLocation whereUpdatedAt($value)
 */
	class CompanyLocation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanyNews
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string $title
 * @property string|null $description
 * @property string|null $image
 * @property string|null $content
 * @property int $company_id
 * @property int $created_by_user_id
 * @property int|null $deleted_by
 * @property int $is_show_in_mobile_slider
 * @property-read \App\Models\Company $company
 * @property-read \App\User $createdBy
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews mobileSlider()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereIsShowInMobileSlider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyNews whereUpdatedAt($value)
 */
	class CompanyNews extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanySetting
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $company_id
 * @property string $settings_name
 * @property string $settings_value
 * @property-read \App\Models\Company $company
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySetting byCompany($company_id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySetting whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySetting whereSettingsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySetting whereSettingsValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySetting whereUpdatedAt($value)
 */
	class CompanySetting extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanyStaff
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $created_by
 * @property int $user_id
 * @property int $company_id
 * @property int|null $current_location_id
 * @property int $role_in_company 1 - владелец, 2 - администратор, 3 - тренер
 * @property int $trainer_price
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $deleted_by
 * @property string|null $delete_reason
 * @property-read \App\Models\Company $company
 * @property-read \App\Models\CompanyLocation|null $currentLocation
 * @property-read mixed $birthday
 * @property-read mixed $first_name
 * @property-read mixed $last_name
 * @property-read mixed $phone
 * @property-read mixed $username
 * @property-read \App\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyStaff onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereCurrentLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereDeleteReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereRoleInCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereTrainerPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyStaff whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyStaff withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyStaff withoutTrashed()
 */
	class CompanyStaff extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\File
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $file_size
 * @property string $content_type
 * @property string $resource_type
 * @property string $path
 * @property string $file_name
 * @property string $original_name
 * @property string $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereContentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereFileSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereOriginalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereResourceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereUpdatedAt($value)
 */
	class File extends \Eloquent {}
}

namespace App\Models\Logs{
/**
 * \App\Models\Logs\LogClientVisits
 *
 * @property int $id
 * @property int $visit_id
 * @property int $client_subscription_id
 * @property int $company_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $created_by
 * @property int|null $created_by_user_id
 * @property string $action_name
 * @property-read \App\User|null $createdBy
 * @property-read \App\Models\ClientVisit $visit
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereActionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereClientSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereVisitId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\ClientSubscription $clientSubscription
 */
	class LogClientVisits extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Order
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $order_item_type
 * @property int $order_item_id
 * @property int $company_id
 * @property int $company_location_id
 * @property int $client_id
 * @property int $price
 * @property string|null $payed_at
 * @property int|null $payed_by
 * @property int $payment_source Наличный, безналичный
 * @property string $created_by
 * @property array|null $order_item_info
 * @property int $created_by_user_id
 * @property string|null $bank_response
 * @property int $status 0 - created, 1 - ok, 2 - error
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\Company $company
 * @property-read \App\Models\CompanyLocation $location
 * @property-read \App\User $soldBy
 * @property-read \App\Models\Subscription $subscription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order byCreatedAt($date)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order byCreatedAtFrom($date)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order byCreatedAtTo($date)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order byPhone($phone)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order payed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order paymentSource($pay_source)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order subscriptions()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereBankResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCompanyLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereOrderItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereOrderItemInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereOrderItemType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePayedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePayedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePaymentSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUpdatedAt($value)
 */
	class Order extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Subscription
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $company_id
 * @property \Illuminate\Support\Carbon|null $available_from
 * @property \Illuminate\Support\Carbon|null $available_to
 * @property int $is_active
 * @property string|null $created_by
 * @property int $can_be_selected_by_staff Может быть выбран сотрудником
 * @property int $price
 * @property int $old_price
 * @property int $quantity_of_occupation Количество занятий
 * @property int $validity Период дейсвия, 1 день 1 месяц, 1 год
 * @property int $has_trainer
 * @property int $can_share Можно делиться
 * @property string $title
 * @property string|null $image
 * @property int $is_available_in_mobile_app
 * @property int $available_to_before_begin_of_use
 * @property-read \App\Models\Company $company
 * @property-read mixed $is_unlimited
 * @property-read mixed $quantity_of_occupation_text
 * @property-read mixed $validity_text
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription byCompany($company_id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription forMobile()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription forOwner()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription forStaff()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereAvailableFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereAvailableTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereAvailableToBeforeBeginOfUse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereCanBeSelectedByStaff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereCanShare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereHasTrainer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereIsAvailableInMobileApp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereOldPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereQuantityOfOccupation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereValidity($value)
 */
	class Subscription extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Tariff
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $title
 * @property string|null $created_by
 * @property int $price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tariff newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tariff newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tariff query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tariff whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tariff whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tariff whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tariff wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tariff whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tariff whereUpdatedAt($value)
 */
	class Tariff extends \Eloquent {}
}

namespace App{
/**
 * \App\User
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string|null $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $password
 * @property string|null $created_by
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $birthday
 * @property-read mixed $name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class User extends \Eloquent {}
}


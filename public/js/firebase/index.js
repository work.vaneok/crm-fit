$(document).ready(function () {
    var ajaxModal = new AjaxFormModal();
    var notificationCreate = new AjaxButton('.notification-create');
    var firebaseDetail = new AjaxButton('.firebase-detail-view').setTypeView();
    ajaxModal.addOpenButton(notificationCreate);
    ajaxModal.addOpenButton(firebaseDetail);
    ajaxModal.build();
});

define({ "api": [
  {
    "group": "CompanyGroup",
    "version": "0.1.0",
    "name": "CompanyLocationsRequest",
    "type": "get",
    "url": "/locations",
    "title": "Список локаций",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.title",
            "description": "<p>Наименование</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.description",
            "description": "<p>Описание</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.image",
            "description": "<p>Изображение</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.address",
            "description": "<p>Адрес</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data.phones",
            "description": "<p>Номера телефонов</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"data\":\n         \"locations\": [{\n               \"id\": 1,\n               \"title\": \"Наименование\",\n               \"description\": \"Описание\",\n               \"email\": null,\n               \"image\": \"http://fitness.org/storage/custom-files/3/858/c/8587503c787a1c689fc0c12e363cb9f1-thumb.jpg\",\n               \"address\": \"addresss\",\n               \"phones\": [\n                   \"122\",\n                   \"233\",\n                   \"344\"\n               ]\n           }],\n         \"social\": {\n              \"fb\":\"<link>\",\n              \"insta\":\"<link>\",\n              \"telegram\":\"<link>\",\n              \"viber\":\"<link>\",\n              \"vk\":\"<link>\",\n         }\n }",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/CompanyController.php",
    "groupTitle": "Компания",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\"",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА: Не выполнена авторизация",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"message\": \"Unauthenticated.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "CompanyGroup",
    "version": "0.1.0",
    "name": "CompanyPolicyRequest",
    "type": "get",
    "url": "/policy",
    "title": "Политика",
    "success": {
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"data\": {\n         \"link\": \"<link>\"\n     }\n }",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/CompanyController.php",
    "groupTitle": "Компания",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\"",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "CompanyGroup",
    "version": "0.1.0",
    "name": "CompanySubscriptionsRequest",
    "type": "get",
    "url": "/subscriptions",
    "title": "Список абонементов",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.title",
            "description": "<p>Наименование</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.price",
            "description": "<p>Актуальная цена</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.price_old",
            "description": "<p>Старая цена</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.quantity_of_occupation",
            "description": "<p>Количество посещений. -1 значит безлим</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.image",
            "description": "<p>Изображение</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"data\": [{\n         \"id\": 1,\n         \"title\": \"тест\",\n         \"price\": 11,\n         \"price_old\": null,\n         \"quantity_of_occupation\": 15,\n         \"image\": \"http://fitness.org/storage/custom-files/3/98c/c/98c0f4e8b83599414e79d3be9312bccf-thumb.jpg\"\n     }]\n }",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/CompanyController.php",
    "groupTitle": "Компания",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\"",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА: Не выполнена авторизация",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"message\": \"Unauthenticated.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "InitGroup",
    "version": "0.1.0",
    "name": "InitRequest",
    "type": "get",
    "url": "/init",
    "title": "Инициализация приложения",
    "success": {
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"news\": СТРУКРУТА как у новостей,\n     \"visits\": аналогично к посещениям,\n     \"subscriptions\": подписки,\n     \"locations\": локации,\n     \"slider\": слайдер\n }",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/InitController.php",
    "groupTitle": "Инициализация",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\"",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА: Не выполнена авторизация",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"message\": \"Unauthenticated.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "LoginGroup",
    "version": "0.1.0",
    "name": "RegisterRequest",
    "type": "post",
    "url": "/register",
    "title": "Регистрация",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Номер телефона в формате 380681234567</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>CODE</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>Имя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Фамилия</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА 422: Указанные данные были неверными.",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n      \"message\": \"Указанные данные были неверными.\",\n      \"errors\": {\n          \"phone\": [\n              \"Номер телефона обязательно\"\n          ],\n         \"first_name\": [\n              \"Имя обязательно\"\n          ],\n         \"last_name\": [\n              \"Фамилия обязательно\"\n          ],\n         \"code\": [\n              \"Код неверный\"\n          ]\n      }\n  }",
          "type": "json"
        },
        {
          "title": "ОШИБКА 429: слишком много запросов",
          "content": "HTTP/1.1 429 Too Many Attempts",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/LoginController.php",
    "groupTitle": "Авторизация и регистрация",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\"",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.token",
            "description": "<p>ТОКЕН</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.first_name",
            "description": "<p>Имя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.last_name",
            "description": "<p>Фамилия</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.birthday",
            "description": "<p>Дата рождения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.phone",
            "description": "<p>Номер телефона</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.barcode",
            "description": "<p>Штрих-код</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.sex",
            "description": "<p>Пол, -1, 1, 2</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data.active_subscriptions",
            "description": "<p>список активных абонементов</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"data\": {\n         \"first_name\": \"Имя АПИ\",\n         \"last_name\": \"Фамилия АПИ\",\n         \"token\": \"pkJWVAPj7I7zjdZZrXoCgNtcAksWzou1\",\n         \"email\": \"email\",\n         \"phone\": \"380688504700\",\n         \"barcode\": \"30000009\",\n         \"sex\": 2,\n         \"active_subscriptions\": []\n     }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "LoginGroup",
    "version": "0.1.0",
    "name": "SmsCodeRequest",
    "type": "post",
    "url": "/sms/request",
    "title": "Отправить запрос на получение СМС кода",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Номер телефона в формате 380681234567</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Текстовый ответ</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "seconds",
            "description": "<p>Сколько секунд живет код</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"message\": \"Смс с кодом отправлено успешно!\",\n     \"seconds\": 180\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА 422: Указанные данные были неверными.",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n      \"message\": \"Указанные данные были неверными.\",\n      \"errors\": {\n          \"phone\": [\n              \"Номер телефона обязательно\"\n          ]\n      }\n  }",
          "type": "json"
        },
        {
          "title": "ОШИБКА 429: слишком много запросов",
          "content": "HTTP/1.1 429 Too Many Attempts",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/LoginController.php",
    "groupTitle": "Авторизация и регистрация",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\"",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "LoginGroup",
    "version": "0.1.0",
    "name": "TokenRequest",
    "type": "post",
    "url": "/login",
    "title": "Запрос на получение токена",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Номер телефона в формате 380681234567</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>CODE</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА 422: Указанные данные были неверными.",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n      \"message\": \"Указанные данные были неверными.\",\n      \"errors\": {\n          \"phone\": [\n              \"Номер телефона обязательно\"\n          ],\n         \"code\": [\n              \"Код неверный\"\n          ]\n      }\n  }",
          "type": "json"
        },
        {
          "title": "ОШИБКА 404: Клиент не найден",
          "content": "HTTP/1.1 404 Not Found\n {\n      \"code\": \"client_not_found\"\n  }",
          "type": "json"
        },
        {
          "title": "ОШИБКА 429: слишком много запросов",
          "content": "HTTP/1.1 429 Too Many Attempts",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/LoginController.php",
    "groupTitle": "Авторизация и регистрация",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\"",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.token",
            "description": "<p>ТОКЕН</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.first_name",
            "description": "<p>Имя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.last_name",
            "description": "<p>Фамилия</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.birthday",
            "description": "<p>Дата рождения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.phone",
            "description": "<p>Номер телефона</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.barcode",
            "description": "<p>Штрих-код</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.sex",
            "description": "<p>Пол, -1, 1, 2</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data.active_subscriptions",
            "description": "<p>список активных абонементов</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"data\": {\n         \"first_name\": \"Имя АПИ\",\n         \"last_name\": \"Фамилия АПИ\",\n         \"token\": \"pkJWVAPj7I7zjdZZrXoCgNtcAksWzou1\",\n         \"email\": \"email\",\n         \"phone\": \"380688504700\",\n         \"barcode\": \"30000009\",\n         \"sex\": 2,\n         \"active_subscriptions\": []\n     }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "MyGroup",
    "version": "0.1.0",
    "name": "ActiveSubscriptionsRequest",
    "type": "get",
    "url": "/my/active-subscriptions",
    "title": "Активные абонементы",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.id",
            "description": "<p>ИД абонемента</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.title",
            "description": "<p>Наименование абонемента</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.active_from",
            "description": "<p>Доступен с</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.active_to",
            "description": "<p>Доступен по</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.auto_available_from",
            "description": "<p>Доступен с, если еще не был активирован</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.image",
            "description": "<p>Картинка абонемента</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.occupations",
            "description": "<p>Занятий осталось</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"data\": [{\n         \"id\": 1,\n         \"title\": \"title\",\n         \"active_from\": \"2020-02-02\",\n         \"active_to\": \"2020-02-02\",\n         \"auto_available_from\": \"2020-02-02\",\n         \"image\": \"LINK\",\n         \"occupations\": 13,\n     }]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/ClientController.php",
    "groupTitle": "Моя информация",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer Authorization</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\",\n\"Authorization\": \"Bearer OLraGtiL3uc01uF6JtRIlt96LcerGyE1\",",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА: Не выполнена авторизация",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"message\": \"Unauthenticated.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "MyGroup",
    "version": "0.1.0",
    "name": "CompanyVisits",
    "type": "get",
    "url": "/my/visits",
    "title": "Получить список посещений по компании",
    "success": {
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"data\": [\n          {\n              \"date\": \"2020-01-24 11:05:27\",\n              \"location\": \"Наименование локации\"\n          }\n     ],\n     \"links\": {\n          \"first\": \"https://<domain>/api/v1/my/company-visits?page=1\",\n          \"last\": \"https://<domain>/api/v1/my/company-visits?page=6\",\n          \"prev\": null,\n          \"next\": \"https://<domain>/api/v1/my/company-visits?page=2\"\n     },\n     \"meta\": {\n          \"current_page\": 1,\n          \"from\": 1,\n          \"last_page\": 6,\n          \"path\": \"https://<domain>/api/v1/my/company-visits\",\n          \"per_page\": 1,\n          \"to\": 1,\n          \"total\": 6\n     }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/ClientController.php",
    "groupTitle": "Моя информация",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer Authorization</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\",\n\"Authorization\": \"Bearer OLraGtiL3uc01uF6JtRIlt96LcerGyE1\",",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА: Не выполнена авторизация",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"message\": \"Unauthenticated.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "MyGroup",
    "version": "0.1.0",
    "name": "MyInfoRequest",
    "type": "get",
    "url": "/me",
    "title": "Информация обо мне",
    "filename": "../app/Http/Controllers/Api/V1/ClientController.php",
    "groupTitle": "Моя информация",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer Authorization</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\",\n\"Authorization\": \"Bearer OLraGtiL3uc01uF6JtRIlt96LcerGyE1\",",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.token",
            "description": "<p>ТОКЕН</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.first_name",
            "description": "<p>Имя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.last_name",
            "description": "<p>Фамилия</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.birthday",
            "description": "<p>Дата рождения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.phone",
            "description": "<p>Номер телефона</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.barcode",
            "description": "<p>Штрих-код</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.sex",
            "description": "<p>Пол, -1, 1, 2</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data.active_subscriptions",
            "description": "<p>список активных абонементов</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"data\": {\n         \"first_name\": \"Имя АПИ\",\n         \"last_name\": \"Фамилия АПИ\",\n         \"token\": \"pkJWVAPj7I7zjdZZrXoCgNtcAksWzou1\",\n         \"email\": \"email\",\n         \"phone\": \"380688504700\",\n         \"barcode\": \"30000009\",\n         \"sex\": 2,\n         \"active_subscriptions\": []\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА: Не выполнена авторизация",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"message\": \"Unauthenticated.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "MyGroup",
    "version": "0.1.0",
    "name": "MyInfoUpdateRequest",
    "type": "put",
    "url": "/me",
    "title": "Обновить информацию обо мне",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>Имя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Фамилия</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "birthday",
            "description": "<p>Дата рождения</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "sex",
            "description": "<p>Пол</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"message\": \"Данные обновлены\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/ClientController.php",
    "groupTitle": "Моя информация",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer Authorization</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\",\n\"Authorization\": \"Bearer OLraGtiL3uc01uF6JtRIlt96LcerGyE1\",",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА: Не выполнена авторизация",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"message\": \"Unauthenticated.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "NewsGroup",
    "version": "0.1.0",
    "name": "NewsDetailRequest",
    "type": "post",
    "url": "/news/2",
    "title": "Новость детально",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.created_at",
            "description": "<p>Дата создания</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.title",
            "description": "<p>Заголовок</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.description",
            "description": "<p>Описание</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.content",
            "description": "<p>Содержание новости</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.image",
            "description": "<p>Изображение</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"data\": {\n         \"id\": \"2\",\n         \"created_at\": \"06.02.2020\",\n         \"title\": \"Заголовок\",\n         \"description\": \"Описание\",\n         \"content\": \"Content\",\n         \"image\": \"<URL>\"\n     }\n }",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/NewsController.php",
    "groupTitle": "Новости",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\"",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА: Не выполнена авторизация",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"message\": \"Unauthenticated.\"\n}",
          "type": "json"
        },
        {
          "title": "ОШИБКА: Не найдено",
          "content": "HTTP/1.1 404 Not found",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "NewsGroup",
    "version": "0.1.0",
    "name": "NewsRequest",
    "type": "post",
    "url": "/news",
    "title": "Последние новости",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.created_at",
            "description": "<p>Дата создания</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.title",
            "description": "<p>Заголовок</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.description",
            "description": "<p>Описание</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.image",
            "description": "<p>Изображение</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "links",
            "description": "<p>Ссылки для постраничной навигации</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "meta",
            "description": "<p>Информация о текущей странице</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"data\": [{\n         \"id\": \"2\",\n         \"created_at\": \"06.02.2020\",\n         \"title\": \"Заголовок\",\n         \"description\": \"Описание\",\n         \"image\": \"<URL>\"\n     }],\n     \"links\": {\n         \"first\": \"http://fitness.org/api/v1/news?page=1\",\n         \"last\": \"http://fitness.org/api/v1/news?page=1\",\n         \"prev\": null,\n         \"next\": null\n     },\n     \"meta\": {\n         \"current_page\": 1,\n         \"from\": 1,\n         \"last_page\": 1,\n         \"path\": \"http://fitness.org/api/v1/news\",\n         \"per_page\": 15,\n         \"to\": 1,\n         \"total\": 1\n     }\n }",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/NewsController.php",
    "groupTitle": "Новости",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\"",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА: Не выполнена авторизация",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"message\": \"Unauthenticated.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "SliderGroup",
    "version": "0.1.0",
    "name": "SliderRequest",
    "type": "get",
    "url": "/slider",
    "title": "Элементы слайдера",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.title",
            "description": "<p>Заголовок</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.description",
            "description": "<p>Описание</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.image",
            "description": "<p>Изображение</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "links",
            "description": "<p>Ссылки для постраничной навигации</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "meta",
            "description": "<p>Информация о текущей странице</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный ответ",
          "content": "HTTP/1.1 200 OK\n{\n     \"data\": [{\n         \"id\": 1,\n         \"title\": \"Заголовок\",\n         \"created_at\": \"06.02.2020\",\n         \"description\": \"Описание\",\n         \"image\": \"<URL>\"\n     }],\n     \"links\": {\n         \"first\": \"http://<domain>/api/v1/slider?page=1\",\n         \"last\": \"http://<domain>/api/v1/slider?page=1\",\n         \"prev\": null,\n         \"next\": null\n     },\n     \"meta\": {\n         \"current_page\": 1,\n         \"from\": 1,\n         \"last_page\": 1,\n         \"path\": \"http://<domain>/api/v1/slider\",\n         \"per_page\": 15,\n         \"to\": 1,\n         \"total\": 1\n     }\n }",
          "type": "json"
        }
      ]
    },
    "filename": "../app/Http/Controllers/Api/V1/SliderController.php",
    "groupTitle": "Слайдер",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Формат отправленных данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Формат запрашиваемых данных, только <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p>Текстовый идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример заголовка",
          "content": "\"Accept\": \"application/json\",\n\"Content-Type\": \"application/json\",\n\"company\": \"e97db65f-9fc3-4ba8-983f-b14a3a2f553d\"",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "ОШИБКА: Не выполнена авторизация",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"message\": \"Unauthenticated.\"\n}",
          "type": "json"
        }
      ]
    }
  }
] });

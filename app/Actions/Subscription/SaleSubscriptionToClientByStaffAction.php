<?php

namespace App\Actions\Subscription;

use App\Components\CrmHelper;
use App\Components\SubscriptionAvailable;
use App\Constants\CompanyStaffRoles;
use App\Constants\StartOfActivity;
use App\Models\ClientSubscription;
use App\Models\Order;
use App\Models\Subscription;
use App\Repositories\SubscriptionCancellationRepository;
use Carbon\Carbon;
use DB;
use Auth;

class SaleSubscriptionToClientByStaffAction
{
    public function execute($data)
    {

        /** @var Subscription $subscription */
        $subscription = Subscription::findOrFail($data['subscription_id']);


        $clientSubscription = null;

        DB::transaction(function () use ($subscription, $data, &$clientSubscription) {
            // Создать заказ
            /** @var Order $order */
            $order = (new MakeOrderAction())->execute('subscription', $data['company_id'], $data['company_location_id'], $data['client_id'], $subscription->price, $subscription->id, $subscription->toArray(), $data['payment_source']);

            // С момента продажи
            if ($data['start_of_activity'] == StartOfActivity::FROM_SALE_DATE) {
                [$available_from, $available_to] = SubscriptionAvailable::calculate($subscription->validity, Carbon::now());
                $data['available_from'] = $available_from;
                $data['available_to'] = $available_to;
            }


            $trainer_id = null;
            $nutritionist_id = null;
            try {
                if (CrmHelper::getRoleInCompany() == CompanyStaffRoles::TRAINER) {
                    $trainer_id = Auth::id();
                }
                if (CrmHelper::getRoleInCompany() == CompanyStaffRoles::NUTRITIONIST) {
                    $nutritionist_id = Auth::id();
                }

            } catch (\Exception $exception) {

            }

            // Оплатить абонемент
            (new ApproveOrderAction())->execute($order->id, Carbon::now(), \Auth::id());

            // Создать абонемент клиента
            $clientSubscription = (new CreateClientSubscriptionAction())->execute(
                $data['client_id'],
                $subscription->company_id,
                $data['company_location_id'],
                $subscription->id,
                $order->id,
                $subscription->validity,
                $subscription->quantity_of_occupation,
                $data['start_of_activity'],
                $subscription->available_to_before_begin_of_use,
                $data['available_from'] ?? null,
                $data['available_to'] ?? null,
                $subscription->can_share,
                $subscription->has_trainer,
                $subscription->has_nutritionist,
                $trainer_id,
                $nutritionist_id
            );
        });

        return $clientSubscription;
    }
}

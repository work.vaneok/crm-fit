<?php
namespace App\Components\Sms\Events;

class SmsBeforeSendEvent
{
    private $phone;
    private $msg;
    private $smsInternalId;

    public function __construct($smsInternalId, $phone, $msg)
    {
        $this->phone = $phone;
        $this->msg = $msg;
        $this->smsInternalId = $smsInternalId;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @return mixed
     */
    public function getSmsInternalId()
    {
        return $this->smsInternalId;
    }

}

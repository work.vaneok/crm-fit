<?php

namespace App\Rules;

use App\Models\Client;
use App\Models\Subscription;
use App\Repositories\ConfirmSmsCodeRepository;
use Illuminate\Contracts\Validation\Rule;

class SubscriptionIsAvailableForMobileSaleRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Subscription::where(['id' => $value])->active()->forMobile()->first() ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Абонемент невозможно приобрести из мобильного приложения');
    }
}

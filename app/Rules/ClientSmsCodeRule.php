<?php
namespace App\Rules;

use App\Repositories\LoginSmsCodeRepository;
use Illuminate\Contracts\Validation\Rule;

class ClientSmsCodeRule implements Rule
{
    private $phone;
    private $cache_key;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($phone, $cache_key)
    {
        $this->phone = $phone;
        $this->cache_key = $cache_key;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $demoMode = in_array($this->phone, config('auth.demo_phones')) || app()->environment('local');

        return ($demoMode && $value == config('app.test_sms_code'))
            ||
            ($value == app(LoginSmsCodeRepository::class)->get($this->cache_key, $this->phone));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Код неверный');
    }
}

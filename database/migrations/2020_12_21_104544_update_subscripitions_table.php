<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSubscripitionsTable extends Migration
{
    private $table = 'subscriptions';

    //nutritionist
    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->boolean('has_nutritionist')->default(0);
        });
    }

    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropColumn('has_nutritionist');
        });
    }
}

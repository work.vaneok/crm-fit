<?php

namespace App\Http\Controllers\Auth;

use App\Actions\User\UserResetPasswordAction;
use App\Components\CrmHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Providers\RouteServiceProvider;
use App\Rules\PhoneFormatRule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use View;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'phone';
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Validate the user login request.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => ['required', 'string', new PhoneFormatRule()],
            'password' => 'required|string',
        ]);
    }

    public function resetPasswordView()
    {
        try {
            return $this->createSuccessResponse(
                [
                    'title' => __("Сброс пароля"),
                    'content' => View::make('auth.reset-password')->render()
                ]
            );
        } catch (\Exception $exception) {
            return $this->createErrorResponse('');
        }
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        try {
            (new UserResetPasswordAction())->executeByPhone($request->phone);
            return $this->createSuccessResponse([
                'message' => __('Смс с новым паролем отправлено успешно!')
            ]);
        } catch (ModelNotFoundException $e) {
            return $this->createErrorResponse(__('Номер телефона не найден в базе'));
        }catch (\Exception $exception) {
            Log::channel('telegram')->error($exception);
            return $this->createErrorResponse(__('На данный момент сервер не может обработать ваш запрос'));
        }
    }

    public function sms(Request $request)
    {
        $validated = $this->validate($request, [
            'phone' => ['required', 'string', new PhoneFormatRule()]
        ]);

        try {
            (new UserResetPasswordAction())->executeByPhone($validated['phone']);
        } catch (ModelNotFoundException $e) {
            Log::error(__FILE__ . ':' . __LINE__ . ' ' . $e->getMessage());
        } catch (\Exception $e) {
            Log::error(__FILE__ . ':' . __LINE__ . ' ' . $e->getMessage());
        }
        return response()->json(['message' => __('Смс с новым паролем отправлено успешно!')]);
    }

    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            //\Auth::guard('admin')->logout();
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function test()
    {

    }
}

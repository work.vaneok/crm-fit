const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
    processCssUrls: false,
});

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/sale-subscription.js', 'public/js')
    .js('resources/js/check-visit.js', 'public/js')
    .js('resources/js/company-login.js', 'public/js')
    .js('resources/js/create-client-page.js', 'public/js')
    .js('resources/js/scripts.js', 'public/js')
    .js('resources/js/chat.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();

mix.copy('node_modules/bootstrap-datepicker/dist/', 'public/assets/bootstrap-datepicker', false);

//
if (mix.inProduction()) {
    mix.version();
}

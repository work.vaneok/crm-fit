<?php

namespace App\Http\Controllers\Actions\CompanyNews;


use App\Actions\BaseAction;
use App\Models\CompanyNews;

class CompanyNewUpdateAction extends BaseAction
{
    public $title;
    public $news_id;
    public $description;
    public $content;
    public $is_show_in_mobile_slider;

    public function rules(): array
    {
        return [
            'news_id' => ['required'],
            'title' => ['required'],
            'description' => ['nullable'],
            'content' => ['required'],
            'is_show_in_mobile_slider' => ['nullable'],
        ];
    }

    public function action()
    {
        $companyNewModel = CompanyNews::findOrFail($this->news_id);


        if ($this->title) {
            $companyNewModel->title = $this->title;
        } else {
            $companyNewModel->title = null;
        }


        if ($this->description) {
            $companyNewModel->description = $this->description;
        } else {
            $companyNewModel->description = null;
        }

        if ($this->content) {
            $companyNewModel->content = $this->content;
        } else {
            $companyNewModel->content = null;
        }

        if ($this->is_show_in_mobile_slider) {
            $companyNewModel->is_show_in_mobile_slider = 1;
        } else {
            $companyNewModel->is_show_in_mobile_slider = 0;
        }

        if (!$companyNewModel->save()) {
            throw new \Exception('CompanyNewStoreAction');
        }


    }
}

@push('css')
    <link href="{{ asset('css/ajax-form-modal/styles.css') }}" rel="stylesheet"/>
@endpush
@push('scripts')
    <script src="{{ asset('js/ajax-form-modal.js') }}"></script>
@endpush

<!-- Modal FOR FORM -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalFormTitle">Modal title</h5>
                {{--    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>--}}
            </div>
            <div id="modalFormBody" class="modal-body">

            </div>
            <div class="modal-footer">
                <div class="type-view hidden">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"
                            id="modalFormClose">{{__('Ok')}}
                    </button>
                </div>
                <div class="type-submit hidden">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                            id="modalFormClose">{{__('Отменить')}}
                    </button>
                    <button type="button" class="btn btn-primary" id="modalFormSubmit">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                          style="display: none"></span>
                        {{__('Сохранить')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal FOR FORM END -->
<!-- Modal FOR CONFIRM  -->
<div id="modalConfirm" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box">
                    <i class="material-icons">&#xE876;</i>
                </div>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p class="text-center"></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal FOR CONFIRM END -->
<!-- Modal FOR ERROR  -->
<div id="modalError" class="modal fade">
    <div class="modal-dialog modal-error">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box">
                    <i class="material-icons">&#xE5CD;</i>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body text-center">
                <h4 class="title"></h4>
                <p class="description"></p>
                <button class="btn btn-success" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal FOR ERROR END  -->

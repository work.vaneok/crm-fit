<?php
namespace App\Constants;

class Permissions
{
    const RETURN_VISIT_ALWAYS = 'returnVisitAlways';
    const RETURN_VISIT_TEMPORARY = 'returnVisitTemporary';
    const SALE_SUBSCRIPTIONS = 'sale_subscriptions';

}

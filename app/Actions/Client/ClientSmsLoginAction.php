<?php
namespace App\Actions\Client;

use App\Constants\CacheKeysPrefixes;
use App\Repositories\LoginSmsCodeRepository;
use Illuminate\Support\Facades\Log;


class ClientSmsLoginAction
{
    public function execute($phone)
    {
        $demoMode = in_array($phone, config('auth.demo_phones')) || app()->environment('local');

        if (!$demoMode) {
            $code = app(LoginSmsCodeRepository::class)->get(CacheKeysPrefixes::CLIENT_LOGIN, $phone);
            if (!$code) {
                $code = random_int(111111, 999999);
            }
        } else {
            $code = config('app.test_sms_code');
        }

        $seconds = app(LoginSmsCodeRepository::class)->save(CacheKeysPrefixes::CLIENT_LOGIN, $phone, $code);
        if (!$demoMode) {
            \Sms::send($phone, __("Ваш код :code", ['code' => $code]));
        }
        return $seconds;
    }
}

<?php
namespace App\Components\Sms\Events;

class SmsAfterSendEvent
{
    private $phone;
    private $msg;
    private $result;
    private $smsInternalId;

    public function __construct($smsInternalId, $phone, $msg, $result)
    {
        $this->phone = $phone;
        $this->msg = $msg;
        $this->result = $result;
        $this->smsInternalId = $smsInternalId;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return mixed
     */
    public function getSmsInternalId()
    {
        return $this->smsInternalId;
    }

}

<?php
namespace App\Admin\Extensions\Tools;

use Encore\Admin\Actions\RowAction;

class CompanySettingsButton extends RowAction
{

    /**
     * Render Export button.
     *
     * @return string
     */
    public function render()
    {
        return "<a href='" . route('admin.company.settings', ['company' => $this->row->getKey()]) . "' class='' ><i class='fa fa-gears '></i> Настройки</a>";
    }

}

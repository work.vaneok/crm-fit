<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyStaffCompanyLocation extends Model
{
    protected $table = 'company_staff_company_locations';
    protected $guarded = ['id'];

    public function companyLocation()
    {
        return $this->belongsTo(CompanyLocation::class, 'company_location_id', 'id');
    }

    public function companyStaff()
    {
        return $this->belongsTo(CompanyStaff::class, 'company_staff_id', 'id');
    }
}

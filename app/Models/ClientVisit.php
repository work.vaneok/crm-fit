<?php

namespace App\Models;

use App\Models\Traits\DisplayDateFormat;
use App\Models\Traits\RequestFiltersScopes;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientVisit extends Model
{
    use DisplayDateFormat, SoftDeletes, RequestFiltersScopes;

    protected $table = 'client_visits';
    protected $guarded = ['id'];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function location()
    {
        return $this->belongsTo(CompanyLocation::class, 'company_location_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function clientSubscription()
    {
        return $this->belongsTo(ClientSubscription::class, 'client_subscription_id', 'id');
    }

    public function getClientNameAttribute()
    {
        return $this->client->getUserName();
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by_user_id', 'id');
    }

    public function deletedBy()
    {
        return $this->belongsTo(User::class, 'deleted_by', 'id');
    }

}

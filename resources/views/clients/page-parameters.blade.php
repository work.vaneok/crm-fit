@extends('layouts.dashboard')

@section('title')
    {{ __('Страница параметров') }}
@endsection

@section('content')
    <div class="container emp-profile">
        <div class="table-responsive">
            <table id="grid-table" class="table  table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col">{{ __('Дата') }}</th>
                    <th scope="col">{{ __('Вес') }}</th>
                    <th scope="col">{{ __('Объем плеч') }}</th>
                    <th scope="col">{{ __('Талия') }}</th>
                    <th scope="col">{{ __('Бедра') }}</th>
                    <th scope="col">{{ __('Объем бицепса') }}</th>
                    <th scope="col">{{ __('Объем груди') }}</th>
                    <th scope="col">{{ __('Объем ноги') }}</th>
                    <th scope="col">{{ __('Объем икр') }}</th>
                    <th scope="col">{{ __('Коментрий') }}</th>
                    <th scope="col">{{ __('Действия') }}</th>

                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>{{$item['date']}}</td>
                        <td>{{$item['weight']}}</td>
                        <td>{{$item['shoulder_volume']}}</td>
                        <td>{{$item['waist']}}</td>
                        <td>{{$item['hips']}}</td>
                        <td>{{$item['biceps_volume']}}</td>
                        <td>{{$item['breast_volume']}}</td>
                        <td>{{$item['leg_volume']}}</td>
                        <td>{{$item['caviar_volume']}}</td>
                        <td>{{$item['comment']}}</td>
                        <td>
                            <button type="button" class="btn btn-sm btn-primary ajax-view"
                                    data-toggle="modal"
                                    data-form-url="{{route('clients.profile.parameter-photo-history',
                            [
                                'parameter_id' => $item['id'],

                            ])}}">
                                {{__('Просмотреть фото')}}
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                                      style="display: none"></span>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @include('inc.ajax-form-modal.form')

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var ajaxModal = new AjaxFormModal();
            var ajaxView = new AjaxButton('.ajax-view').setTypeView();
            ajaxModal.addOpenButton(ajaxView);
            ajaxModal.build();
        });

    </script>
@endpush

@section('actions')

@endsection

@push('css')
    {{--
        <link href="{{ asset('css/clients/profile.css') }}" rel="stylesheet"/>
    --}}
@endpush

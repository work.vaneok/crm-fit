window._ = require('lodash');
import Swal from 'sweetalert2'
window.Swal = Swal;
window.moment = require('moment');
window.queryString = require('query-string');

const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
});

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');

} catch (e) {
    console.log(e);
}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');
window.qs = require('qs');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

$(document).ready(() => {
    $('form').attr('autocomplete', "off");
    // $('.dropdown-toggle').dropdown();
    $('.set-location-item').on('click', function () {
        axios.post($(this).data('href'), qs.stringify({'location': $(this).data('id')})).then(() => {
            location.reload();
        }).catch(ajaxError);
    });

    $('.delete-btn').on('click', function (e) {
        if (!$(this).closest('form').data('ajax')) {
            return;
        }
        e.preventDefault();
        confirmDeleteModal($(this).closest('form'));
    });
});



window.ajaxError = function(e) {
    let errors = [];
    if (e.response.data.message) {
        errors.push(e.response.data.message);
    }
    for (let errorField in e.response.data.errors) {
        errors.push(e.response.data.errors[errorField].join(', '));
    }
    Swal.fire({
        icon: 'error',
        // title: e.response.data.message,
        text: errors.join('; <br>'),
    })
}

window.ajaxSuccess = function(message) {

    Swal.fire({
        icon: 'success',
        // title: e.response.data.message,
        text: message,
    })
}


window.confirmDeleteModal = async function($form) {


    const { value: text, dismiss: dismiss } = await swalWithBootstrapButtons.fire({
        input: 'textarea',
        title: trans('app.Выполнить действие?'),
        icon: 'warning',
        inputValidator: (result) => {
            return !result && trans('app.Вы должны указать причину данного действия')
        },
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: trans('app.Подтвердить выполнение!'),
        cancelButtonText: trans('app.Отмена'),
        inputPlaceholder: trans('app.Опишите причину действия'),
        showCancelButton: true
    });

    if (dismiss) {
        return;
    }

    let formData = new FormData($form[0]);

    if (text) {
        formData.append('reason', text);
    }

    axios.post($form.attr('action'), formData).then((response) => {
        location.reload();
    }).catch(ajaxError);
}


window.trans = function (key, replace = {})
{
    let translation = key.split('.').reduce((t, i) => t[i] || key, window.translations);

    for (var placeholder in replace) {
        translation = translation.replace(`:${placeholder}`, replace[placeholder]);
    }

    return translation;
};

window.trans_choice = function (key, count = 1, replace = {})
{
    let translation = key.split('.').reduce((t, i) => t[i] || key, window.translations).split('|');

    translation = count > 1 ? translation[1] : translation[0];

    for (var placeholder in replace) {
        translation = translation.replace(`:${placeholder}`, replace[placeholder]);
    }

    return translation;
};


window.routes = function () {
    var args = Array.prototype.slice.call(arguments);
    var name = args.shift();
    if (Laravel.routes[name] === undefined) {
        console.error('Route not found ', name);
    } else {
        return window.Laravel.baseUrl + '/' + Laravel.routes[name]
            .split('/')
            .map(s => s[0] == '{' ? args.shift() : s)
            .join('/');
    }
};

window.toFormData = function (ob) {
    let formData = new FormData();
    for(let field in ob) {
        formData.append(field, ob[field]);
    }

    return formData;
};

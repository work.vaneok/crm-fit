<?php

namespace App\Rules;

use App\Models\Client;
use App\Models\ClientSubscription;
use App\Models\Subscription;
use App\Repositories\ConfirmSmsCodeRepository;
use App\Rules\Traits\SetCompanyIdTrait;
use Illuminate\Contracts\Validation\Rule;

class ClientSubscriptionOfCompanyRule implements Rule
{
    use SetCompanyIdTrait;
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return ClientSubscription::byCompany($this->company_id)->where(['id' => $value])->first() ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Абонемент не относится к этой компании');
    }
}

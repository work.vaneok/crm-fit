<?php

namespace App\Http\Requests\Firebase;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FirebaseIndexRequest
 * @package App\Http\Requests\Auth
 */
class FirebaseDetailViewRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => ['integer', 'required'],
        ];
    }


}

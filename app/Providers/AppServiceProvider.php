<?php

namespace App\Providers;

use App\Actions\Company\SaveCompanySettingsAction;
use App\Classes\Fondy;
use App\Components\CrmHelper;
use App\Components\Firebase\FirebaseService;
use App\Components\Payments\Facades\WayForPayCredentialsFacade;
use App\Components\PriceFormatter;
use App\Components\Sms\SmsService;
use App\Constants\CompanySettings;
use App\Constants\Roles;
use App\Models\Client;
use App\Models\ClientSubscription;
use App\Models\ClientVisit;
use App\Models\Company;
use App\Models\CompanyClient;
use App\Models\CompanyLocation;
use App\Models\CompanyNews;
use App\Models\CompanyStaff;
use App\Models\Logs\LogClientVisits;
use App\Models\Order;
use App\Models\Tariff;
use App\Observers\ApiTokenObserver;
use App\Observers\CompanyIdObserver;
use App\Observers\CreatedByObserver;
use App\Observers\CreatedByUserIdObserver;
use App\Observers\SetDeletedByUserIdObserver;
use App\Observers\UuidObserver;
use App\Repositories\ClientSubscriptionRepository;
use App\Repositories\CompanyClientBuysRepository;
use App\Repositories\CompanyClientRepository;
use App\Repositories\CompanySettingsRepository;
use App\Repositories\CompanySubscriptions;
use App\Repositories\DependedOnRole\SubscriptionsCompanyOwnerRepository;
use App\Repositories\DependedOnRole\SubscriptionsCompanyStaffRepository;
use App\Repositories\DependedOnRole\SubscriptionsRepository;
use App\Repositories\OrderRepository;
use App\User;
use Auth;
use Blade;
use Illuminate\Support\ServiceProvider;
use LiqPay;
use Spatie\MediaLibrary\Models\Media;
use WayForPay\SDK\Credential\AccountSecretCredential;
use WayForPay\SDK\Credential\AccountSecretTestCredential;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Sven\ArtisanView\ServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerObservers();
        $this->registerBladeTemplates();
        $this->registerBindings();
    }

    private function registerObservers()
    {
        User::observe(CreatedByObserver::class);
        Company::observe([UuidObserver::class, CreatedByObserver::class]);
        CompanyLocation::observe(CreatedByObserver::class);
        Tariff::observe(CreatedByObserver::class);
        CompanyStaff::observe([CreatedByObserver::class, SetDeletedByUserIdObserver::class]);
        Client::observe(CreatedByObserver::class);
        CompanyClient::observe(ApiTokenObserver::class);

        ClientSubscription::observe([CreatedByObserver::class, CreatedByUserIdObserver::class]);
        Order::observe([CreatedByObserver::class, CreatedByUserIdObserver::class]);
        LogClientVisits::observe([CreatedByObserver::class, CreatedByUserIdObserver::class]);

        ClientVisit::observe([CreatedByObserver::class, CreatedByUserIdObserver::class, SetDeletedByUserIdObserver::class]);
        CompanyNews::observe([CreatedByUserIdObserver::class, SetDeletedByUserIdObserver::class]);

        Media::observe([CompanyIdObserver::class]);
    }

    private function registerBladeTemplates()
    {
        Blade::include('inc.actions.delete', 'delete');
        Blade::include('inc.actions.add', 'add');
        Blade::include('inc.actions.edit', 'edit');
        Blade::include('inc.actions.button', 'button');

        Blade::include('inc.display.boolean', 'boolean');
        Blade::include('inc.display.dl', 'dl');
        Blade::include('inc.display.image_media', 'image_media');
        Blade::include('inc.display.return_visit', 'return_visit');
        Blade::include('inc.display.fire_employee', 'fire_employee');
        Blade::include('inc.display.grid_btn', 'grid_btn');

        Blade::include('inc.menu.item', 'menu_item');

        Blade::include('inc.fields.search_date', 'search_date');
        Blade::include('inc.fields.search_select', 'search_select');
        Blade::include('inc.fields.search_select2', 'search_select2');
        Blade::include('inc.fields.search_date_range', 'search_date_range');

        Blade::include('inc.forms.filter_form', 'filter_form');

        //Sidebar
        Blade::include('inc.sidebar.title', 'sidebar_title');
        Blade::include('inc.sidebar.item', 'sidebar_item');
        Blade::include('inc.sidebar.dropdown', 'sidebar_dropdown');

        //Cash
        Blade::include('inc.cash.card', 'cash_card');
    }

    private function registerBindings()
    {
        $this->app->singleton('fbm', function ($app) {
            return new FirebaseService();
        });

        $this->app->singleton('sms', function ($app) {
            return new SmsService(CrmHelper::getSmsProviders());
        });

        $this->app->singleton('price-formatter', function ($app) {
            return new PriceFormatter();
        });

        /** REPOSITORIES */
        $this->app->bind(CompanySettingsRepository::class, function ($app) {
            return new CompanySettingsRepository(CrmHelper::company());
        });

        $this->app->bind(CompanyClientRepository::class, function ($app) {
            return new CompanyClientRepository(CrmHelper::company());
        });

        $this->app->bind(CompanyClientBuysRepository::class, function ($app) {
            return new CompanyClientBuysRepository(CrmHelper::company());
        });

        $this->app->bind(ClientSubscriptionRepository::class, function ($app) {
            return new ClientSubscriptionRepository(CrmHelper::company());
        });

        $this->app->bind(CompanySubscriptions::class, function ($app) {
            return new CompanySubscriptions(CrmHelper::company());
        });

        $this->app->bind(OrderRepository::class, function ($app) {
            return new OrderRepository(CrmHelper::company());
        });


        // I JUST CHECKED HOW IT CAN USE AS PATTERN "FACTORY"
        $this->app->bind(SubscriptionsRepository::class, function ($app) {
            if (Auth::user()->hasRole(Roles::COMPANY_OWNER)) {
                return new SubscriptionsCompanyOwnerRepository(Auth::user(), CrmHelper::company());
            }

            if (Auth::user()->hasRole(Roles::COMPANY_STAFF)) {
                return new SubscriptionsCompanyStaffRepository(Auth::user(), CrmHelper::company());
            }
        });
        /** END REPOSITORIES */

        $this->app->bind(SaveCompanySettingsAction::class, function ($app) {
            return (new SaveCompanySettingsAction(CrmHelper::company()));
        });

        $this->app->bind('LiqPay', function ($app) {
            return new LiqPay(config('payments.liqpay_pub_key'), config('payments.liqpay_private_key'));
        });


        $this->app->bind(WayForPayCredentialsFacade::class, function ($app) {

            if (config('payments.mode') === 'dev') {
                return new AccountSecretTestCredential();
            }

            if (empty(CompanySettings::WAY_FOR_PAY_LOGIN) || empty(CompanySettings::WAY_FOR_PAY_SECRET)) {
                throw new \Exception('Way for pay merchant data are empty');
            }
            return new AccountSecretCredential(CrmHelper::settings(CompanySettings::WAY_FOR_PAY_LOGIN), CrmHelper::settings(CompanySettings::WAY_FOR_PAY_SECRET));
        });

        /*$this->app->bind(Fondy::class, function($app) {
            return new Fondy($app['config']['fondy']);
        });*/
    }
}

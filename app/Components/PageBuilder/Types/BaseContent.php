<?php
namespace App\Components\PageBuilder\Types  ;


use App\Components\PageBuilder\Column;
use App\Components\PageBuilder\Content;
use App\Components\PageBuilder\Row;

abstract class BaseContent
{

    /**
     * @var Content
     */
    protected $content;

    function __construct()
    {
        $this->content = new Content();
    }

    public function dashboard()
    {
        return '';
    }
}

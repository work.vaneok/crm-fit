@extends('layouts.dashboard')

@section('title')
    {{ __('Продажа абонемента') }}
@endsection

@section('content')
    <div class="content-m-t row mb-12">
        <div id="sale-form" class="col-lg-12">
            <sale-subscription phone="{{ $phone }}" :subscriptions="{{ $subscriptions->toJson() }}" :cancelPeriods="{{ $cancelablePeriods->toJson() }}"  ></sale-subscription>
        </div>
    </div>
@endsection

@section('actions')

@endsection


@push('scripts')
    <script src="{{ mix('js/sale-subscription.js') }}"></script>
@endpush



<?php

namespace App\Policies;

use App\Components\CrmHelper;
use App\Models\CompanyNews;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyNewsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any company news.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the company news.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyNews  $companyNews
     * @return mixed
     */
    public function view(User $user, CompanyNews $companyNews)
    {
        return true;
    }

    /**
     * Determine whether the user can create company news.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the company news.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyNews  $companyNews
     * @return mixed
     */
    public function update(User $user, CompanyNews $companyNews)
    {
        return $companyNews->company_id == CrmHelper::companyId();
    }

    /**
     * Determine whether the user can delete the company news.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyNews  $companyNews
     * @return mixed
     */
    public function delete(User $user, CompanyNews $companyNews)
    {
        return $companyNews->company_id == CrmHelper::companyId();
    }

    /**
     * Determine whether the user can restore the company news.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyNews  $companyNews
     * @return mixed
     */
    public function restore(User $user, CompanyNews $companyNews)
    {
        return $companyNews->company_id == CrmHelper::companyId();
    }

    /**
     * Determine whether the user can permanently delete the company news.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyNews  $companyNews
     * @return mixed
     */
    public function forceDelete(User $user, CompanyNews $companyNews)
    {
        return $companyNews->company_id == CrmHelper::companyId();
    }
}

@extends('layouts.dashboard')

@section('title')
    {{ __('Абонемент') }}
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                {{ __('Наименование') }}
            </dt>
            <dd class="col-sm-10">
                {{ $subscription->title }}
            </dd>
        </dl>


    </div>
@endsection

@section('actions')
    @if (Auth::user()->can('update', $subscription))
        <li>
            @edit(['route' => route('subscriptions.edit',['subscription' => $subscription->id])])&nbsp;
        </li>
    @endif
@endsection

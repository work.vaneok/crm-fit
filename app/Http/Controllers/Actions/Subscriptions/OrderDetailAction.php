<?php

namespace App\Http\Controllers\Actions\Subscriptions;

use App\Http\Controllers\Actions\BaseAction;
use App\Models\ClientSubscription;
use App\Models\CompanyClient;
use App\Models\Order;
use View;

class OrderDetailAction extends BaseAction
{
    public $order_id;

    public $response;

    public function rules(): array
    {
        return [
            'order_id' => ['integer', 'required'],
        ];
    }

    public function action()
    {
        $order = Order::findOrFail($this->order_id);

        $this->response = [
            'title' => __("Детальная страница заказа"),
            'content' => View::make('subscriptions.order-detail', [
                    'order' => $order->toArray(),
                ]
            )->render()
        ];
    }

}

<?php

namespace App\Repositories;

use App\Models\Tariff;

class TariffRepository
{
    /**
     * @return Tariff[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function getForSelectBoxForAdmin()
    {
        return Tariff::all()->keyBy('id')->map(function ($item) {
            return $item->title;
        });
    }
}

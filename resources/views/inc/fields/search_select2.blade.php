<select   style="" name="filter[{{ $name }}]" id="filter-{{ $name }}" placeholder="{{ $label }}" class="js-example-basic-single search-select2">
    @foreach($options as $id => $value)
        <option value="{{ $id }}"
                @if(!is_null(get_filter_value($name)) && $id == get_filter_value($name)) selected @endif >{{ $value }}</option>
    @endforeach
</select>


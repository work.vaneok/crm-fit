@if($boolean)
    <span class="badge badge-success">{{ __('Да') }}</span>
@else
    <span class="badge badge-secondary">{{ __('Нет') }}</span>
@endif

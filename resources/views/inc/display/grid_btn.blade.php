<a href="{{ $route }}">
    <i class="fa fa-{{ $class ?? '' }}"></i>
    {{ $label }}
</a>

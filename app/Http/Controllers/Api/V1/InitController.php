<?php

namespace App\Http\Controllers\Api\V1;

use App\Components\CrmHelper;
use App\DependedGetters\CompanyNewsGetter;
use App\DependedGetters\MobileSliderGetter;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClientVisitsApiCollection;
use App\Http\Resources\CompanyLocationsApiCollection;
use App\Http\Resources\CompanySubscriptionsApiCollection;
use App\Http\Resources\NewsApiCollection;
use App\Http\Resources\SliderApiCollection;
use App\Repositories\CompanyClientRepository;
use App\Repositories\CompanySubscriptions;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class InitController extends Controller
{
    /**
     * @apiGroup InitGroup
     * @apiVersion 0.1.0
     * @apiName InitRequest
     * @apiUse RequestHeader
     *
     * @api {get} /init Инициализация приложения
     *
     *
     * @apiSuccessExample {json} Успешный ответ
     *     HTTP/1.1 200 OK
     *     {
     *          "news": СТРУКРУТА как у новостей,
     *          "visits": аналогично к посещениям,
     *          "subscriptions": подписки,
     *          "locations": локации,
     *          "slider": слайдер
     *      }
     *
     * @apiUse UnauthorizedError
     *
     */
    public function __invoke(Request $request)
    {
        return [
            'news' => (new NewsApiCollection(CompanyNewsGetter::getList()->setPath(route('api.news'))))->response()->getData(true),
            'visits' => (new ClientVisitsApiCollection(app(CompanyClientRepository::class)->visits($request->user())->setPath(route('api.my.visits'))))->response()->getData(true),
            'subscriptions' => (new CompanySubscriptionsApiCollection(app(CompanySubscriptions::class)->listForMobile()))->response()->getData(true),
            'locations' => (new CompanyLocationsApiCollection(CrmHelper::company()->locations))->response()->getData(true),
            'slider' => (new NewsApiCollection(MobileSliderGetter::getList()->setPath(route('api.slider'))))->response()->getData(true),
        ];
    }

}

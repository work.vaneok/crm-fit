@extends('layouts.dashboard')

@section('title')
    {{ __('Создание клиента') }}
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->multipart()->route('clients.store-from-exists')->post()->fill($client) !!}
        {!! Form::text('phone', 'Номер телефона')->disabled() !!}
        {!! Form::text('first_name', 'Имя')->disabled() !!}
        {!! Form::hidden('client_id', $client->id) !!}

        {!! Form::hidden('back', $backUrl) !!}

        <div class="form-group row submit-btn-wrap">
            <div class="offset-2 col-sm-10">
                {!! Form::submit(__('Сохранить изменения')) !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection





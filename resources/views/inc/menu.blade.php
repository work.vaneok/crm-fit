<ul class="nav flex-column">

    @menu_item(['link' => route('home'), 'name' => __('Главная панель'), 'segment' => ''])
    @menu_item(['link' => route('profile.index'), 'name' => __('Профиль'), 'segment' => 'profile'])

    @menu_item(['link' => route('client-visit.index'), 'name' => __('Посещения'), 'segment' => 'client-visits'])
    @menu_item(['link' => route('sold-subscriptions.index'), 'name' => __('Проданные абонементы'), 'segment' => 'sold-subscriptions'])
    @menu_item(['link' => route('company-news.index'), 'name' => __('Новости компании'), 'segment' => 'company-news'])
    @menu_item(['link' => route('firebase.index'), 'name' => __('Firebase'), 'segment' => 'firebase'])

    @role(\App\Constants\Roles::COMPANY_OWNER)
        @menu_item(['link' => route('clients.index'), 'name' => __('Клиенты компании'), 'segment' => 'clients'])
        @menu_item(['link' => route('subscriptions.index'), 'name' => __('Абонементы компании'), 'segment' => 'subscriptions'])
        @menu_item(['link' => route('locations.index'), 'name' => __('Локации'), 'segment' => 'locations'])
        @menu_item(['link' => route('staffs.index'), 'name' => __('Сотрудники'), 'segment' => 'staffs'])
        @menu_item(['link' => route('company-settings.index'), 'name' => __('Настройки компании'), 'segment' => 'company-settings'])
    @endrole

</ul>

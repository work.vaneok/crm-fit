<div>
    {!! Form::open()->multipart()->route('clients.buy')->post() !!}
    {!! Form::hidden('company_id', $company_id) !!}
    {!! Form::hidden('client_id', $client->id) !!}
    {!! Form::text('phone',  __('Телефон'),$client->phone)->disabled(true)->required() !!}
    {!! Form::text('first_name', __('Имя'),$client->first_name)->disabled(true)->required() !!}
    {!! Form::text('last_name', __('Фамилия'),$client->last_name)->disabled(true)->required() !!}
    {!! Form::text('amount',  __('Сумма платежа'))->required() !!}
    {!! Form::text('comment',  __('Коментарий'))->required() !!}
    {!!  Form::close() !!}

</div>

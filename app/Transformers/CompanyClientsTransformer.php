<?php
namespace App\Transformers;

use App\Constants\VisitActions;
use App\Models\ClientVisit;
use App\Models\CompanyClient;
use App\Models\Logs\LogClientVisits;

class CompanyClientsTransformer
{
    public function __invoke(CompanyClient $item)
    {
        return [
            'client_id' => $item->client_id,
            'created_at' => $item->formatDateTime('created_at'),
            'created_by' => $item->createdBy ? $item->createdBy->name : '',
            'username' => $item->client->getUserName(false),
            'phone' => $item->client->phone,
            'last_visited_at' => $item->formatDateTime('last_visited_at'),
        ];
    }
}

<?php

namespace App\Http\Controllers\Actions\Home;

use App\Components\CrmHelper;
use App\Constants\CompanyStaffRoles;
use App\Http\Controllers\Actions\BaseAction;
use App\Models\ClientSubscription;
use Carbon\Carbon;

class HomeDashboardAction extends BaseAction
{
    public $user_id;
    public $company_id;
    public $company_location_id;

    public $r_items_cash;
    public $r_items_cashless;

    public function rules(): array
    {
        return [
            'user_id' => ['integer', 'required'],
            'company_id' => ['integer', 'required'],
            'company_location_id' => ['integer', 'required'],
        ];
    }

    public function action()
    {
        $dateFrom = Carbon::now()->startOfDay();
        $dateEnd = Carbon::now()->endOfDay();

        $clientsSubs = ClientSubscription::query()
            ->where(['company_id' => $this->company_id])
            ->where(['company_location_id' => $this->company_location_id])
            ->whereBetween('created_at', [$dateFrom->toDateTimeString(), $dateEnd->toDateTimeString()]);

        $clientsSubs->with(['order', 'subscription']);

        if (CrmHelper::getRoleInCompany() == CompanyStaffRoles::TRAINER) {
            $clientsSubs->where(['trainer_id' => $this->user_id]);
        }

        if (CrmHelper::getRoleInCompany() == CompanyStaffRoles::NUTRITIONIST) {
            $clientsSubs->where(['nutritionist_id' => $this->user_id]);
        }

        $items = $clientsSubs->get();

        $itemsCash = [];
        $itemsCash['items'] = [];
        $itemsCash['total_price'] = 0;
        $itemsCashless = [];
        $itemsCashless['items'] = [];
        $itemsCashless['total_price'] = 0;

        /** @var ClientSubscription $item */
        foreach ($items as $item) {
            if (!$item->order) {
                continue;
            }

            //Наличка
            if ($item->order->payment_source == 1) {
                $itemsCash['items'][$item->subscription->id]['title'] = $item->subscription->title;

                if (isset($itemsCash['items'][$item->subscription->id]['price'])) {
                    $itemsCash['items'][$item->subscription->id]['price'] += $item->order->price;
                } else {
                    $itemsCash['items'][$item->subscription->id]['price'] = $item->order->price;
                }

                if (isset($itemsCash['items'][$item->subscription->id]['count'])) {
                    $itemsCash['items'][$item->subscription->id]['count'] += 1;
                } else {
                    $itemsCash['items'][$item->subscription->id]['count'] = 1;
                }

                if (isset($itemsCash['total_price'])) {
                    $itemsCash['total_price'] += $item->order->price;
                } else {
                    $itemsCash['total_price'] = $item->order->price;
                }
            }

            //БезНаличка
            if ($item->order->payment_source == 2) {
                $itemsCashless['items'][$item->subscription->id]['title'] = $item->subscription->title;

                if (isset($itemsCashless['items'][$item->subscription->id]['price'])) {
                    $itemsCashless['items'][$item->subscription->id]['price'] += $item->order->price;
                } else {
                    $itemsCashless['items'][$item->subscription->id]['price'] = $item->order->price;
                }

                if (isset($itemsCashless['items'][$item->subscription->id]['count'])) {
                    $itemsCashless['items'][$item->subscription->id]['count'] += 1;
                } else {
                    $itemsCashless['items'][$item->subscription->id]['count'] = 1;
                }

                if (isset($itemsCashless['total_price'])) {
                    $itemsCashless['total_price'] += $item->order->price;
                } else {
                    $itemsCashless['total_price'] = $item->order->price;
                }
            }
        }
        $this->r_items_cash = $itemsCash;
        $this->r_items_cashless = $itemsCashless;
    }
}

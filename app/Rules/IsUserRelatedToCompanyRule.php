<?php

namespace App\Rules;

use App\Models\Company;
use App\Models\CompanyStaff;
use App\Models\Subscription;
use App\Rules\Traits\SetCompanyIdTrait;
use Auth;
use Illuminate\Contracts\Validation\Rule;

class IsUserRelatedToCompanyRule implements Rule
{
    use SetCompanyIdTrait;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return
            (
                Company::firstWhere(['owner_id' => Auth::id(), 'id' => $this->company_id])
                    ||
                CompanyStaff::where([
                    'user_id' => Auth::id(),
                    'company_id' => $this->company_id
                ])->first()
            ) ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Вы не имеете отношения к компании');
    }
}

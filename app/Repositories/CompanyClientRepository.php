<?php

namespace App\Repositories;

use App\Components\CrmHelper;
use App\Models\Client;
use App\Models\ClientSubscription;
use App\Models\ClientVisit;
use App\Models\Company;
use App\Models\CompanyClient;
use App\Models\CompanyClientParameterHistory;
use App\Models\CompanyClientPhotoHistory;
use App\Models\Logs\LogClientVisits;
use App\Models\Order;
use App\Transformers\ClientVisitsHistoryTransformer;
use App\Transformers\ProfileSoldSubscriptionsTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Посетители компании
 *
 * Class CompanyClientRepository
 * @package App\Repositories
 */
class CompanyClientRepository
{
    /**
     * @var Company
     */
    private $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Client|null
     */
    public function findByPhone($phone)
    {
        return $this->company->clients()->where(['phone' => $phone])->first();
    }

    /**
     * @return Client|null
     */
    public function findByBarcode($number)
    {


        //TODO: Вынести может куда-то?
        if (isPhoneNumber($number)) { // This is phone number
            return $this->company->clients()->where(['phone' => $number])->with(['activeSubscriptionsV1' => function (HasMany $q) {
                $q->where('company_id', $this->company->id);
            }])->first();
        }

        // This is barcode
        return $this->company->clients()->where(['barcode' => $number])->with(['activeSubscriptionsV1' => function (HasMany $q) {
            $q->where('company_id', $this->company->id);
        }])->first();
    }

    /**
     * @param $client_id
     * @return CompanyClient|null
     */
    public function findByClientId($client_id)
    {
        return CompanyClient::where([
            'company_id' => $this->company->id,
            'client_id' => $client_id
        ])->with([
            'client',
            'client.activeSubscriptions' => function (HasMany $q) {
                $q->where('company_id', $this->company->id);
            }
        ])->first();
    }

    public function findById($client_id)
    {
        return CompanyClient::where([
            'company_id' => $this->company->id,
            'client_id' => $client_id
        ])->first();
    }

    public function activeSubscriptionsByClient(CompanyClient $client)
    {
        return $client->activeSubscriptions()->where('company_id', $this->company->id)->get();
    }

    public function activeSubscriptionsByClientV1(CompanyClient $client)
    {

        $carbon = Carbon::now()->toDateTimeString();
        return $client->activeSubscriptions()
            ->where('company_id', $this->company->id)
            ->where('deleted_at', null)
            ->where('quantity_of_occupation', '<>', 0)
            ->where(function ($query) use ($carbon) {
                $query->where('available_to', '=', null)
                    ->orWhere('available_to', '>', $carbon);
            })->get();
    }

    /**
     * @param CompanyClient $client
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function visits(CompanyClient $client)
    {
        return $client->companyVisits($client->company_id)->with(['location'])->orderBy('id', 'DESC')->paginate();
    }


    public function visitsHistory(CompanyClient $client)
    {

        $items = QueryBuilder::for(LogClientVisits::class)
            ->join('client_visits', 'client_visits.id', '=', 'log_client_visits.visit_id')
            ->with(['visit', 'createdBy', 'clientSubscription.subscription'])
            ->where([
                'client_visits.company_id' => CrmHelper::companyId(),
                'client_visits.client_id' => $client->client_id,
            ])
            ->allowedFilters([
                AllowedFilter::exact('client_subscription', 'log_client_visits.client_subscription_id'),
            ])
            ->orderBy('client_visits.id', 'DESC')
            ->limit(15)->get();
        $items->transform(new ClientVisitsHistoryTransformer());
        return $items;

        return ClientVisit::where([
            'client_id' => $client->client_id,
            'company_id' => $client->company_id
        ])->with([
            'company', 'location'
        ])->get();
    }

    public function orders(CompanyClient $client)
    {

        $items = QueryBuilder::for(Order::class)
            ->where([
                'company_id' => $client->company_id,
                'order_item_type' => 'subscription',
                'client_id' => $client->client_id
            ])
            ->with(['client', 'soldBy', 'subscription'])
            ->orderBy('id', 'DESC')
            ->limit(15)->get();

        $items->transform(new ProfileSoldSubscriptionsTransformer());
        return $items;
    }

    public function parameters(CompanyClient $client)
    {
        $items = CompanyClientParameterHistory::query()->where([
            'company_id' => $client->company_id,
            'client_id' => $client->client_id
        ])->orderBy('id', 'desc')
            ->get();
        $response = [];
        $counter = 0;
        /** @var CompanyClientParameterHistory $item */
        foreach ($items as $item) {
            $response[$counter][CompanyClientParameterHistory::C_DATE] = $item->formatDate(CompanyClientParameterHistory::C_DATE);
            $response[$counter][CompanyClientParameterHistory::C_WEIGHT] = $item[CompanyClientParameterHistory::C_WEIGHT];
            $response[$counter][CompanyClientParameterHistory::C_SHOULDER_VOLUME] = $item[CompanyClientParameterHistory::C_SHOULDER_VOLUME];
            $response[$counter][CompanyClientParameterHistory::C_WAIST] = $item[CompanyClientParameterHistory::C_WAIST];
            $response[$counter][CompanyClientParameterHistory::C_HIPS] = $item[CompanyClientParameterHistory::C_HIPS];
            $response[$counter][CompanyClientParameterHistory::C_BICEPS_VOLUME] = $item[CompanyClientParameterHistory::C_BICEPS_VOLUME];
            $response[$counter][CompanyClientParameterHistory::C_BREAST_VOLUME] = $item[CompanyClientParameterHistory::C_BREAST_VOLUME];
            $response[$counter][CompanyClientParameterHistory::C_LEG_VOLUME] = $item[CompanyClientParameterHistory::C_LEG_VOLUME];
            $response[$counter][CompanyClientParameterHistory::C_CAVIAR_VOLUME] = $item[CompanyClientParameterHistory::C_CAVIAR_VOLUME];
            $response[$counter][CompanyClientParameterHistory::C_COMMENT] = $item[CompanyClientParameterHistory::C_COMMENT];
            $response[$counter][CompanyClientParameterHistory::C_ID] = $item[CompanyClientParameterHistory::C_ID];
            $counter++;
        }


        return $response;
    }

    public function photos(CompanyClient $client)
    {
        $response = [];

        $history = CompanyClientPhotoHistory::query()
            ->where([
                'client_id' => $client->client_id,
                'company_id' => $client->company_id,
                'deleted_at' => null
            ])
            ->orderBy('id', 'desc')
            ->get();

        /** @var CompanyClientPhotoHistory $item */

        foreach ($history as $item) {
            $date = Carbon::parse($item[CompanyClientPhotoHistory::C_DATE]);
            $date = $date->toDateString();
            $medias = $item->getMedia();
            foreach ($medias as $media) {
                $response[$date]['images'] [] = $media->getFullUrl();
            }
        }
        return $response;
    }

    public function subscriptions(CompanyClient $client)
    {
        $response = [];

        $subscriptions = ClientSubscription::query()->where([
            'client_id' => $client->client_id,
            'company_id' => $client->company_id,
        ])
            ->with([
                'subscription'
            ])
            ->orderBy('id', 'desc')
            ->get();

        $carbon = Carbon::now()->toDateTimeString();
        foreach ($subscriptions as $subscription) {
            $response[$subscription->id]['subscription_id'] = $subscription->id;
            $response[$subscription->id]['client_id'] = $subscription->client_id;
            $response[$subscription->id]['company_id'] = $subscription->company_id;
            $response[$subscription->id]['available_to'] = $subscription->available_to ? $subscription->formatDateAsDB('available_to') : '-';
            $response[$subscription->id]['available_from'] = $subscription->available_from ? $subscription->formatDateAsDB('available_from') : '-';
            $response[$subscription->id]['quantity_of_occupation'] = $subscription->quantity_of_occupation;
            $response[$subscription->id]['order_id'] = $subscription->order_id;
            $response[$subscription->id]['subscription']['title'] = $subscription->subscription->title;
            $response[$subscription->id]['deleted_at'] = $subscription->deleted_at;
            $response[$subscription->id]['active'] = ($subscription->available_to > $carbon && $subscription->quantity_of_occupation != 0 || $subscription->available_to == null && $subscription->quantity_of_occupation != 0) ? true : false;
        }

        return $response;
    }

    public function photosByDate($client_id, $company_id, $date)
    {
        $response = [];
        $carbonDate = Carbon::parse($date);
        $history = CompanyClientPhotoHistory::query()
            ->where([
                'client_id' => $client_id,
                'company_id' => $company_id,
            ])->whereBetween('date', [$carbonDate->startOfDay()->toDateTimeString(), $carbonDate->endOfDay()->toDateTimeString()])
            ->orderBy('id', 'desc')
            ->get();

        /** @var CompanyClientPhotoHistory $item */
        foreach ($history as $item) {
            $medias = $item->getMedia();
            foreach ($medias as $media) {
                $response['images'] [] = $media->getFullUrl();
            }
        }
        return $response;
    }

    public function parameterPhotosById($parameter_id)
    {
        $response = [];


        $history = CompanyClientParameterHistory::query()
            ->where([
                'id' => $parameter_id
            ])
            ->limit(1)
            ->first();


        /** @var CompanyClientPhotoHistory $item */

        $medias = $history->getMedia();
        foreach ($medias as $media) {
            $response['images'] [] = $media->getFullUrl();
        }

        return $response;
    }

}

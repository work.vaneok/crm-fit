<?php

namespace App\Models;

use App\Models\Interfaces\CompanyClientParameterHistoryInterface;
use App\Models\Traits\DisplayDateFormat;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class CompanyClientParameterHistory
 * @package App\Models
 */
class CompanyClientParameterHistory extends Model implements CompanyClientParameterHistoryInterface, HasMedia
{
    use HasMediaTrait;

    protected $table = self::TABLE_NAME;
    protected $guarded = ['id'];
    public $timestamps = false;
    use DisplayDateFormat;
}

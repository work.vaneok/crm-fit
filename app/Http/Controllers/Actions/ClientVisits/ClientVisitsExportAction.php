<?php

namespace App\Http\Controllers\Actions\ClientVisits;

use App\Components\CrmHelper;
use App\Export\ClientParameterHistoryExport;
use App\Http\Controllers\Actions\BaseAction;
use App\Models\ClientVisit;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ClientVisitsExportAction extends BaseAction
{
    public $company_id;
    public $company_location_id;
    public $filter_client_id;
    public $filter_location_id;
    public $filter_date_from;
    public $filter_date_to;
    public $filter_staff_id;
    public $filter_group_type;

    public $provider;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'company_location_id' => ['integer', 'required'],
            'filter_group_type' => ['string', 'required'],
            'filter_client_id' => ['nullable'],
            'filter_subscription_id' => ['nullable'],
            /*'filter_location_id' => ['nullable'],*/
        ];
    }

    public function action()
    {
        $subsItem = [];
        $countRows = 0;

        $this->provider = ClientVisit::query()
            ->where(['company_id' => $this->company_id])
            ->where(['company_location_id' => $this->company_location_id])
            ->with(['location', 'clientSubscription.subscription', 'client', 'createdBy']);

        if ($this->filter_client_id) {
            $this->provider->where([
                'client_id' => $this->filter_client_id
            ]);
        }

        if ($this->filter_date_from && $this->filter_date_to) {
            $this->provider->whereBetween('created_at', [
                Carbon::parse($this->filter_date_from)->startOfDay()->toDateTimeString(),
                Carbon::parse($this->filter_date_to)->endOfDay()->toDateTimeString()
            ]);
        }
        if ($this->filter_location_id) {
            $this->provider->where([
                'company_location_id' => $this->filter_location_id
            ]);
        }

        if ($this->filter_staff_id) {
            $this->provider->where([
                'created_by_user_id' => $this->filter_staff_id
            ]);
        }


        if ($this->filter_group_type == 'group-excel') {
            $this->provider->orderBy('created_by_user_id')->orderBy('client_id');
            $items = $this->provider->get();

            $subsItem[0]['staff'] = 'Сотрудник';
            $subsItem[0]['client'] = 'Клиент';
            $subsItem[0]['subscription'] = 'Абонемент';
            $subsItem[0]['count_uses'] = 'Использовано';
            $rowsLinks = [];
            $currentRow = 1;

            foreach ($items as $item) {
                $key = $item->createdBy->id . $item->client->id . $item->clientSubscription->id;
                if (isset($rowsLinks[$key])) {
                    $row = $rowsLinks[$key];
                } else {
                    $row = $currentRow;
                    $currentRow++;
                    $rowsLinks[$key] = $row;
                }

                $subsItem[$row]['staff'] = $item->createdBy->first_name . ' ' . $item->createdBy->last_name . ' (' . $item->createdBy->phone . ')';
                $subsItem[$row]['client'] = $item->client->first_name . ' ' . $item->client->last_name . ' (' . $item->client->phone . ')';
                $subsItem[$row]['subscription'] = '№' . $item->clientSubscription->id . ' ' . $item->clientSubscription->subscription->title;

                if (!isset($subsItem[$row]['count_uses'])) {
                    $subsItem[$row]['count_uses'] = 1;
                } else {
                    $subsItem[$row]['count_uses'] += 1;
                }
            }
            $export = new ClientParameterHistoryExport($subsItem);
            $filename = 'Сгруппированные_посещения_' . Carbon::now()->toDateString() . '.xlsx';
            return Excel::download($export, $filename);
        } else {
            $items = $this->provider->orderByDesc('id')->get();

            $subsItem[$countRows]['id'] = 'ID';
            $subsItem[$countRows]['name'] = 'Имя';
            $subsItem[$countRows]['fio'] = 'Фамилия';
            $subsItem[$countRows]['phone'] = 'Номер';
            $subsItem[$countRows]['location'] = 'Локация';
            $subsItem[$countRows]['subscription'] = 'Абонемент';
            $subsItem[$countRows]['staff'] = 'Сотрудник';
            $subsItem[$countRows]['created_at'] = 'Создан в';

            $countRows = 1;
            foreach ($items as $item) {
                $subsItem[$countRows]['id'] = $item->id;
                $subsItem[$countRows]['first_name'] = $item->client->first_name;
                $subsItem[$countRows]['last_name'] = $item->client->last_name;
                $subsItem[$countRows]['phone'] = $item->client->phone . ' ';
                $subsItem[$countRows]['location'] = $item->location->title;
                $subsItem[$countRows]['subscription'] = $item->clientSubscription->subscription->title;
                $subsItem[$countRows]['staff'] = $item->createdBy->first_name . ' ' . $item->createdBy->last_name;
                $subsItem[$countRows]['created_at'] = $item->created_at;
                $countRows++;
            }

            $export = new ClientParameterHistoryExport($subsItem);
            $filename = 'Посещения_' . Carbon::now()->toDateString() . '.xlsx';
            return Excel::download($export, $filename);
        }
    }
}

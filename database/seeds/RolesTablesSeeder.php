<?php

use App\Constants\Permissions;
use App\Constants\Roles;
use Illuminate\Database\Seeder;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;
use Spatie\Permission\Exceptions\RoleDoesNotExist;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            'roles' => [Roles::COMPANY_OWNER => null, Roles::COMPANY_STAFF => null],
            'permissions' => [Permissions::RETURN_VISIT_ALWAYS => null, Permissions::RETURN_VISIT_TEMPORARY => null, Permissions::SALE_SUBSCRIPTIONS => null],
        ];
        foreach ($arr['roles'] as $name => $role) {
            try {
                $arr['roles'][$name] = Role::findByName($name);
            } catch (RoleDoesNotExist $e) {
                $arr['roles'][$name] = Role::create(['name' => $name]);
            }
        }

        foreach ($arr['permissions'] as $name => $permission) {
            try {
                $arr['permissions'][$name] = Permission::findByName($name);
            } catch (PermissionDoesNotExist $e) {
                $arr['permissions'][$name] = Permission::create(['name' => $name]);
            }
        }

        $arr['permissions'][Permissions::RETURN_VISIT_ALWAYS]->assignRole($arr['roles'][Roles::COMPANY_OWNER]);
        $arr['permissions'][Permissions::RETURN_VISIT_TEMPORARY]->assignRole($arr['roles'][Roles::COMPANY_STAFF]);
        $arr['permissions'][Permissions::SALE_SUBSCRIPTIONS]->assignRole([$arr['roles'][Roles::COMPANY_OWNER], $arr['roles'][Roles::COMPANY_STAFF]]);
    }
}

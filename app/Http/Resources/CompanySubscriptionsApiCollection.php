<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanySubscriptionsApiCollection extends ResourceCollection
{
    public $collects = CompanySubscriptionsApiResource::class;

}

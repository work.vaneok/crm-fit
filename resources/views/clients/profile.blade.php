@extends('layouts.dashboard')

@section('title')
    {{ __('Создание клиента') }}
@endsection

@section('content')
    <div class="container emp-profile">
        <form method="post">
            <div class="row">

                <div class="col-lg-3">
                    <div class="profile-head">
                        <h5 style="font-weight: bold;">
                            {{$client->first_name.' '. $client->last_name}}
                        </h5>
                        <h5 style="font-weight: bold;">
                            {{$client->phone}}
                        </h5>
                        <p  style="font-weight: bold;" class="proile-rating">{{__('Баланс')}} :
                            <span>{{PriceFormatter::withCurrency($companyClient->balance)}}</span>
                        </p>
                        <?php
                        if($total_debt){
                        ?>
                        <p style="font-weight: bold;" class="proile-rating">{{__('Долг')}} :
                            <span>{{PriceFormatter::withCurrency($total_debt)}}</span></p>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <button type="button" class="btn btn-primary ajax-refill-button" data-toggle="modal"
                                    data-form-url="{{route('clients.refill-view',['client' => $client->id])}}">
                                {{__('Пополнить счет')}}
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                                      style="display: none"></span>
                            </button>
                            <button type="button" class="btn btn-primary ajax-buy-button" data-toggle="modal"
                                    data-form-url="{{route('clients.buy-view',['client' => $client->id])}}">
                                {{__('Создать платеж')}}
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                                      style="display: none"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="profile-img">
                        <img
                            src="{{asset('images/default-avatar.png')}}" alt=""/>
                        <div class="file btn btn-lg btn-primary">
                            Change Photo
                            <input type="file" name="file"/>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12">
                {{--<ul role="tablist" class="nav nav-tabs bs-adaptive-tabs" id="myTab">--}}
                    <ul role="tablist" class="nav nav-tabs nav-justified justify-content-center justify-content-sm-start" id="myTab">
                    <li class="tab-list-li" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="tab7-tab" href="#tab7"><i class="fas fa-images"></i></a></li>
                    <li class="tab-list-li" role="presentation"><a aria-expanded="false" aria-controls="home" data-toggle="tab" role="tab" id="tab1-tab" href="#tab1"><i class="fa fa-home"></i></a></li>
                    <li class="tab-list-li" role="presentation"><a aria-expanded="false" aria-controls="home" data-toggle="tab" role="tab" id="tab2-tab" href="#tab2"><i class="fa fa-archive"></i> </a></li>
                    <li class="tab-list-li" role="presentation"><a aria-expanded="false" aria-controls="home" data-toggle="tab" role="tab" id="tab3-tab" href="#tab3"><i class="fa fa-calendar"></i> </a></li>
                    <li class="tab-list-li" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="tab4-tab" href="#tab4"><i class="fa fa-cog"></i> </a></li>
                    <li class="tab-list-li" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="tab5-tab" href="#tab5"><i class="fas fa-history"></i> </a></li>
                    <li class="tab-list-li" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="tab6-tab" href="#tab6"><i class="fas fa-images"></i></a></li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div aria-labelledby="tab1-tab" id="tab1" class="tab-pane fade" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="text-align: center">
                                <h3>{{__('Профиль')}}</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>ID</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$client->id}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>{{__('Имя')}}</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$client->first_name}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>{{__('Фамилия')}}</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$client->last_name}}</p>
                            </div>
                        </div>
                        <?php
                        if ($client->email) {
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Email</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$client->email}}</p>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                        <?php
                        if ($client->birthday) {
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <label>{{__('Дата рождения')}}</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$client->formatDate('birthday')}}</p>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <label>{{__('Номер телефона')}}</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$client->phone}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>{{__('Дата регистрации')}}</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$client->formatDate('created_at')}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>{{__('Дата последнего обновления профиля')}}</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$client->formatDate('updated_at')}}</p>
                            </div>
                        </div>                </div>
                    <div aria-labelledby="tab2-tab" id="tab2" class="tab-pane" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="text-align: center">
                                <h3>{{__('Посещения')}}</h3>
                            </div>
                        </div>
                        {{ View::make('clients.history', ['items' => $visits]) }}
                    </div>
                    <div aria-labelledby="tab3-tab" id="tab3" class="tab-pane " role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="text-align: center">
                                <h3>{{__('Продажи')}}</h3>
                            </div>
                        </div>
                        {{ View::make('clients.orders', ['items' => $orders]) }}
                    </div>
                    <div aria-labelledby="tab7-tab" id="tab7" class="tab-pane active" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="text-align: center">
                                <h3>{{__('Абонементы')}}</h3>
                            </div>
                        </div>
                        {{ View::make('clients.subcriptions', ['items' => $subscriptions]) }}
                    </div>
                    <div aria-labelledby="tab4-tab" id="tab4" class="tab-pane  in" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="text-align: center">
                                <h3>{{__('Платежи')}}</h3>
                            </div>
                        </div>
                        {{ View::make('clients.buys', ['items' => $buys,'companyClient' => $companyClient]) }}
                    </div>
                    <div aria-labelledby="tab5-tab" id="tab5" class="tab-pane" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="text-align: center">
                                <div class="row">
                                    <div class="col-md-6">
                                                <h3>{{__('История параметров')}}</h3>
                                    </div>
                                    <div class="col-md-6">
                                        <form action="{{ route('subscriptions.export') }}" accept-charset="UTF-8">
                                            <input style="display: none" type="text" value="{{get_filter_value('client_id')}}"
                                                   name="filter[client_id]" placeholder="Id">
                                        </form>

                                        <form action="{{ route('clients.route-parameter-history-export') }}" accept-charset="UTF-8">
                                            <input style="display: none" type="text" value="{{$companyClient->client_id}}"
                                                   name="client_id" placeholder="Id">
                                            <div class="col-auto">
                                                <button type="submit" class="btn btn-primary"><i class="fas fa-file-export"></i>Excel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{ View::make('clients.parameters-history', ['items' => $parameters,'companyClient' => $companyClient]) }}

                    </div>
                    <div aria-labelledby="tab6-tab" id="tab6" class="tab-pane" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="text-align: center">
                                <h3>{{__('Фото-отчеты')}}</h3>
                            </div>
                        </div>
                        {{ View::make('clients.photo-history', ['items' => $photos,'companyClient' => $companyClient]) }}
                    </div>
                </div>
                </div>

            </div>


        </form>
    </div>
    @include('inc.ajax-form-modal.form')

@endsection

@push('scripts')
    <script src="{{ asset('js/create-client-page.js') }}"></script>
    <script src="{{ asset('js/clients/profile.js') }}"></script>
@endpush

@section('actions')

@endsection

@push('css')
    <link href="{{ asset('css/clients/profile.css') }}" rel="stylesheet"/>
@endpush

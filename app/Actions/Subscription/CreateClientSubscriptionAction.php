<?php

namespace App\Actions\Subscription;

use App\Constants\StartOfActivity;
use App\Models\ClientSubscription;
use App\Models\Order;
use App\Repositories\SubscriptionCancellationRepository;
use Carbon\Carbon;

class CreateClientSubscriptionAction
{
    public function execute($client_id, $company_id, $location_id, $subscription_id, $order_id, $validity, $quantity_of_occupation, $start_of_activity, $available_to_before_begin_of_use, $available_from = null, $available_to = null, $can_share = 0, $has_trainer = 0, $has_nutritionist = 0, $trainer_id = null, $nutritionist_id = null)
    {
        /** @var SubscriptionCancellationRepository $cancelableRep */
        $cancelableRep = app(SubscriptionCancellationRepository::class);

        $available_to_before_begin_of_use = ($start_of_activity == StartOfActivity::FROM_FIRST_VISIT) ? ($cancelableRep->getById($available_to_before_begin_of_use)['nextDateFromCurrent'] ?? null) : Carbon::now();

        $clientSubscription = ClientSubscription::create([
            'client_id' => $client_id,
            'company_id' => $company_id,
            'company_location_id' => $location_id,
            'subscription_id' => $subscription_id,
            'order_id' => $order_id,
            'can_share' => $can_share,
            'validity' => $validity,
            'quantity_of_occupation' => $quantity_of_occupation,
            'start_of_activity' => $start_of_activity,
            'available_to_before_begin_of_use' => $available_to_before_begin_of_use,
            'available_from' => $available_from,
            'available_to' => $available_to,
            'has_trainer' => $has_trainer,
            'has_nutritionist' => $has_nutritionist,
            'trainer_id' => $trainer_id,
            'nutritionist_id' => $nutritionist_id,

            'is_current' => 1, // ОЧЕНЬ ВАЖНЫЙ ПАРАМЕТР, который уже не надо. -Как сообсетвенно и весь написаний код...
        ]);

        return $clientSubscription;
    }
}

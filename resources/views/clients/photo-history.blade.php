<div class="table-responsive">
    <table id="grid-table" class="table  table-hover table-striped">
        <thead>
        <tr>
            <th scope="col">{{ __('Дата') }}</th>
            <th scope="col">{{ __('Количество фото') }}</th>
            <th scope="col">{{ __('Действия') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $key => $value)
            <tr>
                <td>{{$key}}</td>
                <td>{{count($value['images'])}}</td>
                <td>
                    <button type="button" class="btn btn-sm btn-primary photo-history"
                            data-toggle="modal"
                            data-form-url="{{route('clients.profile.photo-history',
                            [
                                'client_id' => $companyClient->client_id,
                                'company_id' => $companyClient->company_id,
                                'date' => $key
                            ])}}">
                        <i class="fas fa-info"></i>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                              style="display: none"></span>
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

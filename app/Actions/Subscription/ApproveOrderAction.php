<?php
namespace App\Actions\Subscription;

use App\Constants\OrderStatuses;
use App\Exceptions\Actions\Subscription\SubscriptionAlreadyPayed;
use App\Models\Order;
use Carbon\Carbon;

class ApproveOrderAction
{
    /**
     * @param $order_id
     * @param $payed_at
     * @param null $payed_by
     * @return Order|bool
     * @throws SubscriptionAlreadyPayed|\Throwable|\Exception
     */
    public function execute($order_id, $payed_at, $payed_by = null)
    {
        /** @var Order $order */
        $order = Order::find($order_id);
        if (!$order) {
            \Log::error("Order: {$order->id} not found");
            throw new \Exception("Order: {$order->id} not found");
        }

        throw_if($order->isPayed(), new SubscriptionAlreadyPayed());

        $order->update([
            'payed_at' => $payed_at,
            'payed_by' => $payed_by,
            'status' => OrderStatuses::OK,
        ]);

        return $order;
    }
}

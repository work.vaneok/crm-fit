<?php

namespace App\Actions\Client;

use App\Constants\CacheKeysPrefixes;
use App\Models\Client;
use App\Models\ClientDevice;
use App\Repositories\LoginSmsCodeRepository;
use paragraph1\phpFCM\Recipient\Device;

class ClientSaveDeviceAction
{
    public static function execute($client_id, $company_id, $device_id, $fcm_id, $device_os, $device_os_version)
    {
        $device = ClientDevice::where([
            ClientDevice::COLUMN_COMPANY_ID => $company_id,
            ClientDevice::COLUMN_CLIENT_ID => $client_id,
            ClientDevice::COLUMN_DEVICE_ID => $device_id,
            ClientDevice::COLUMN_FCM_ID => $fcm_id
        ])->first();
        if ($device) {
            return true;
        }
        $device = new ClientDevice();
        $device->fcm_id = $fcm_id;
        $device->company_id = $company_id;
        $device->client_id = $client_id;
        $device->device_id = $device_id;
        $device->device_os = $device_os;
        $device->device_os_version = $device_os_version;
        if(!$device->save()){
            \Log::error($device->errors());
        }
    }
}

<?php

namespace App\Models\Interfaces;

interface CompanyClientPhotoHistoryInterface
{
    const C_COMPANY_ID = 'company_id';
    const C_ID = 'id';
    const C_CLIENT_ID = 'client_id';
    const C_DATE = 'date';
    const TABLE_NAME = 'company_client_photo_history';
}

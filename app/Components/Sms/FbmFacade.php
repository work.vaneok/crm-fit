<?php
namespace App\Components\Sms;

use Illuminate\Support\Facades\Facade;

class FbmFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'fbm';
    }
}

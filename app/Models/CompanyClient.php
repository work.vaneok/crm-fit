<?php

namespace App\Models;

use App\Components\CrmHelper;
use App\Models\Traits\DisplayDateFormat;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\RequestFiltersScopes;
use App\User;
use DB;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;

/**
 * Class CompanyClient
 * @package App\Models
 * @property string $balance
 */
class CompanyClient extends Model
{
    use HasCompositePrimaryKey, DisplayDateFormat, RequestFiltersScopes;
    const TableName = 'company_clients';
    const COLUMN_CLIENT_ID = 'client_id';
    const COLUMN_COMPANY_ID = 'company_id';


    protected $guarded = ['created_at', 'updated_at'];

    protected $primaryKey = ['client_id', 'company_id'];

    public function getKey()
    {
        $attributes = [];
        foreach ($this->getKeyName() as $key) {
            $attributes[$key] = $this->getAttribute($key);
        }

        return $attributes;
    }

    public function getAuthIdentifier()
    {
        return $this->client_id . '-' . $this->company_id;
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function company()
    {
        return $this->hasMany(Company::class, 'company_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by_user_id', 'id');
    }

    function scopeWithLastVisitDate($query)
    {
        $query->addSelect(['last_visited_at' => function (Builder $q) {
            $q->select('created_at')
                ->from('client_visits')
                ->whereColumn('company_clients.client_id', 'client_visits.client_id')
                ->whereColumn('company_clients.company_id', 'client_visits.company_id')
                ->orderBy('client_visits.id', 'desc')
                ->limit(1);
        }]);
    }

    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where('client_id', $value)->where('company_id', CrmHelper::companyId())->first() ?? abort(404);
    }

    public function refreshToken()
    {
        $this->api_token = Str::random(32);
        CompanyClient::where(['client_id' => $this->client_id, 'company_id' => $this->company_id])->update(['api_token' => $this->api_token]);
    }

    public function getToken()
    {
        return $this->api_token;
    }

    public function activeSubscriptions()
    {
        return $this->hasMany(ClientSubscription::class, 'client_id', 'client_id')->active()->with(['subscription' => function ($q) {
            // $q->withTrashed();
        }, 'subscription.media']);
    }

    public function allVisits()
    {
        return $this->hasMany(ClientVisit::class, 'client_id', 'client_id');
    }

    public function companyVisits($company_id)
    {
        return $this->hasMany(ClientVisit::class, 'client_id', 'client_id')
            ->where('company_id', $company_id);
    }

    public function getBalanceString()
    {
        return number_format($this->balance, 0);
    }

}

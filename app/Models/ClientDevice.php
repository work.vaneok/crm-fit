<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientDevice
 * @package App\Models
 * @property int $client_id
 * @property int $id
 * @property int $company_id
 * @property string $device_id
 * @property string $fcm_id
 * @property string $device_os
 * @property string $device_os_version
 */
class ClientDevice extends Model
{
    const TableName = 'client_devices';
    protected $table = self::TableName;
    public $timestamps = false;
    const COLUMN_ID = 'id';
    const COLUMN_CLIENT_ID = 'client_id';
    const COLUMN_COMPANY_ID = 'company_id';
    const COLUMN_DEVICE_ID = 'device_id';
    const COLUMN_FCM_ID = 'fcm_id';
    const COLUMN_DEVICE_OS = 'device_os';
    const COLUMN_DEVICE_OS_VERSION = 'device_os_version';


}

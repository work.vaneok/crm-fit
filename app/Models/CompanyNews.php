<?php

namespace App\Models;

use App\Models\Traits\DisplayDateFormat;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class CompanyNews extends Model implements HasMedia
{
    use DisplayDateFormat, HasMediaTrait;

    protected $table = 'company_news';
    protected $guarded = ['id'];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by_integer', 'id');
    }


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_MAX, 450, 450);
    }

    public function scopeMobileSlider($q)
    {
        $q->where(['is_show_in_mobile_slider' => 1]);
    }
}

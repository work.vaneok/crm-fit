<?php

return [
    //'provider' => 'LiqPay',
    'mode' => env('PAYMENTS_MODE', 'dev'),
    'order_prefix' => env('ORDER_PREFIX', ''),
    'provider' => 'WayForPay',

    'liqpay_pub_key' => env('LIQ_PAY_PUB_KEY', ''),
    'liqpay_private_key' => env('LIQ_PAY_PRIVATE_KEY', ''),

    'way_for_pay_account' => env('WAY_FOR_PAY_ACCOUNT', ''),
    'way_for_pay_secret' => env('WAY_FOR_PAY_SECRET', ''),
];

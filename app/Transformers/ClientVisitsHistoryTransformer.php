<?php
namespace App\Transformers;

use App\Constants\VisitActions;
use App\Models\ClientVisit;
use App\Models\Logs\LogClientVisits;

class ClientVisitsHistoryTransformer
{
    public function __invoke(LogClientVisits $item)
    {
        return [
            'created_at' => $item->formatDateTime('created_at'),
            'actionName' => __($item->action_name),
            'action_name' => $item->action_name,
            'createdBy' => $item->createdBy->name,
            'deleted_reason' => $item->visit->delete_reason,
            'subscription' => $item->clientSubscription->subscription,
        ];
    }
}

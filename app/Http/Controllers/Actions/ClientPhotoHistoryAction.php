<?php

namespace App\Http\Controllers\Actions;

use App\Models\CompanyClientParameterHistory;
use App\Models\CompanyClientPhotoHistory;
use Carbon\Carbon;

class ClientPhotoHistoryAction extends BaseAction
{
    public $company_id;
    public $client_id;
    public $per_page = 15;
    public $page = 1;


    public $response;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'client_id' => ['integer', 'required'],
            'per_page' => ['integer'],
            'page' => ['integer']
        ];
    }

    public function action()
    {
        $response = [];

        $history = CompanyClientPhotoHistory::query()->where([
            'client_id' => $this->client_id,
            'company_id' => $this->company_id,
            'deleted_at' => null
        ])
            ->orderBy('id', 'desc')
            ->get();


        $response['history']['items'] = [];
        /** @var CompanyClientPhotoHistory $item */
        foreach ($history as $item) {
            $date = Carbon::parse($item[CompanyClientParameterHistory::C_DATE]);
            $date = $date->toDateString();
            $medias = $item->getMedia();
            foreach ($medias as $media) {
                $response['history']['items'][$date]['images'] [] = $media->getFullUrl();
            }
        }
        $this->response = $response;
    }

}

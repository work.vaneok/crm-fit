<?php

namespace App\Http\Requests\Subscriptions;


use App\Http\Requests\Traits\DefaultCompanyLocationIdInRequest;
use Illuminate\Foundation\Http\FormRequest;

class StaffSubsRequest extends FormRequest
{
    use DefaultCompanyLocationIdInRequest;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
        ];
    }

}

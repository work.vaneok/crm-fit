<?php

namespace App\Policies;

use App\Components\CrmHelper;
use App\Constants\CompanySettings;
use App\Constants\Permissions;
use App\Models\ClientVisit;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientVisitPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any client visits.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the client visit.
     *
     * @param  \App\User  $user
     * @param  \App\Models\ClientVisit  $clientVisit
     * @return mixed
     */
    public function view(User $user, ClientVisit $clientVisit)
    {
        //
    }

    /**
     * Determine whether the user can create client visits.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the client visit.
     *
     * @param  \App\User  $user
     * @param  \App\Models\ClientVisit  $clientVisit
     * @return mixed
     */
    public function update(User $user, ClientVisit $clientVisit)
    {
        //
    }

    /**
     * Determine whether the user can delete the client visit.
     *
     * @param  \App\User  $user
     * @param  \App\Models\ClientVisit  $clientVisit
     * @return mixed
     */
    public function delete(User $user, ClientVisit $clientVisit)
    {
        //
    }

    /**
     * Determine whether the user can restore the client visit.
     *
     * @param  \App\User  $user
     * @param  \App\Models\ClientVisit  $clientVisit
     * @return mixed
     */
    public function restore(User $user, ClientVisit $clientVisit)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the client visit.
     *
     * @param  \App\User  $user
     * @param  \App\Models\ClientVisit  $clientVisit
     * @return mixed
     */
    public function forceDelete(User $user, ClientVisit $clientVisit)
    {
        //
    }

    public function returnVisitPolicy(User $user, ClientVisit $clientVisit)
    {
        if ($clientVisit->company_id != CrmHelper::companyId()) {
            return false;
        }

        if ($user->can(Permissions::RETURN_VISIT_ALWAYS)) {
            return true;
        }

        if ($user->can(Permissions::RETURN_VISIT_TEMPORARY)) {
            $time = CrmHelper::settings(CompanySettings::TIME_TO_RETURN_VISIT, 15);
            return Carbon::now()->diffInMinutes($clientVisit->created_at) < $time;
        }

        return false;
    }

}

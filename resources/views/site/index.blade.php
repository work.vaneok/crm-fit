@extends('layouts.site')

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $pageConfig['title'] }}</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Style css -->
    <link rel="stylesheet" href="{{ asset('site' ) }}/css/style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="{{ asset('site' ) }}/css/responsive.css">
</head>
<body>
<div class="preloader-main">
    <div class="preloader-wapper">
        <svg class="preloader" xmlns="http://www.w3.org/2000/svg" version="1.1" width="600" height="200">
            <defs>
                <filter id="goo" x="-40%" y="-40%" height="200%" width="400%">
                    <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -8" result="goo" />
                </filter>
            </defs>
            <g filter="url(#goo)">
                <circle class="dot" cx="50" cy="50" r="25" fill="#8731E8" />
                <circle class="dot" cx="50" cy="50" r="25" fill="#8731E8" />
            </g>
        </svg>
        <div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
    </div>
</div>
<div id="scrollUp" title="Scroll To Top">
    <i class="fas fa-arrow-up"></i>
</div>

<div class="main">
    <!-- ***** Header Start ***** -->
    <header class="navbar navbar-sticky navbar-expand-lg navbar-dark">
        <div class="container position-relative">
            <a class="navbar-brand" href="/">
                <img class="navbar-brand-regular" src="{{ asset('site' ) }}/img/logo/logo-white.png" alt="brand-logo">
            </a>
            <!--<button class="navbar-toggler d-lg-none" type="button" data-toggle="navbarToggler" aria-label="Toggle navigation">
                 <span class="navbar-toggler-icon"></span>
             </button>-->

            <div class="navbar-inner ptb_50">
                <!--  Mobile Menu Toggler -->
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="navbarToggler" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </header>
    <!-- ***** Header End ***** -->

<!-- ***** Welcome Area Start ***** -->
    <section id="home" class="section welcome-area bg-overlay overflow-hidden d-flex align-items-center">
        <div class="container">
            <div class="row align-items-center">
                <!-- Welcome Intro Start -->
                <div class="col-12 col-md-7 col-lg-6">
                    <div class="welcome-intro">
                        <h1 class="text-white">{{ $pageConfig['title'] }}</h1>
                        <p class="text-white my-4">{{ $pageConfig['description'] }}</p>
                        <!-- Store Buttons -->
                        <div class="button-group store-buttons d-flex">
                            <a href="{{ $pageConfig['google_play'] }}">
                                <img src="{{ asset('site') }}/img/icon/google-play.png" alt="">
                            </a>
                            <a href="{{ $pageConfig['apple_store'] }}">
                                <img src="{{ asset('site') }}/img/icon/app-store.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 col-lg-6">
                    <!-- Welcome Thumb -->
                    <div class="welcome-thumb mx-auto" data-aos="fade-left" data-aos-delay="500" data-aos-duration="1000">
                        <img src="{{ asset('site') }}/img/welcome/welcome-mockup.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!-- Shape Bottom -->
        <div class="shape-bottom">
            <svg viewBox="0 0 1920 310" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg replaced-svg">
                <defs></defs>
                <g id="sApp-Landing-Page" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="sApp-v1.0" transform="translate(0.000000, -554.000000)" fill="#FFFFFF">
                        <path d="M-3,551 C186.257589,757.321118 319.044414,856.322454 395.360475,848.004007 C509.834566,835.526337 561.525143,796.329212 637.731734,765.961549 C713.938325,735.593886 816.980646,681.910577 1035.72208,733.065469 C1254.46351,784.220361 1511.54925,678.92359 1539.40808,662.398665 C1567.2669,645.87374 1660.9143,591.478574 1773.19378,597.641868 C1848.04677,601.75073 1901.75645,588.357675 1934.32284,557.462704 L1934.32284,863.183395 L-3,863.183395" id="sApp-v1.0"></path>
                    </g>
                </g>
            </svg>
        </div>

    </section>
    <!-- ***** Welcome Area End ***** -->

    <!-- Footer Bottom -->
    <footer>
        <div class="footer-bottom ptb_30">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- Copyright Area -->
                        <div class=" d-flex flex-wrap justify-content-center justify-content-sm-between text-center py-4">
                            <!-- Copyright Left -->
                            <div class="copyright-left">&copy; Copyrights 2020 POWER GYM</div>
                            <!-- Copyright Right -->
                            <div class="copyright-right"><a href="{{ $pageConfig['offer'] }}">Публічна оферта</a></div>
                            <!-- Copyright Right -->
                            <div class="copyright-right"><a href="{{ $pageConfig['policy'] }}">Політика конфіденційності</a></div>
                            <!-- Copyright Right -->
                            <div class="copyright-right">Powered by <a href="#">WebStick</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--====== Footer Area End ======-->
</div>


<!-- ***** All jQuery Plugins ***** -->

<!-- jQuery(necessary for all JavaScript plugins) -->
<script src="{{ asset('site' ) }}/js/jquery/jquery-3.3.1.min.js"></script>

<!-- Plugins js -->
<script src="{{ asset('site' ) }}/js/plugins/plugins.min.js"></script>

<!-- Active js -->
<script src="{{ asset('site' ) }}/js/active.js"></script>
</body>
</html>

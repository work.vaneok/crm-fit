<?php

namespace App\Components\Chat;


use App\Components\Firebase\FirebaseFirestoreService;
use App\Components\Firebase\FirebaseService;
use App\Constants\CompanyStaffRoles;
use App\Models\Client;
use App\Models\CompanyStaff;
use App\User;
use Carbon\Carbon;
use Firebase\JWT\JWT;

class ChatComponent
{

    const COLLECTION_ROOMS = 'rooms';

    public static function generateCompanyUserId($id)
    {
        return 'user-' . $id;
    }

    public static function generateCompanyClientId($id)
    {
        return 'client-' . $id;
    }

    /** Генерация id комнаты для ГЛОБАЛЬНОГО ЧАТА КОМПАНИЯ - КЛИЕНТ
     * @param $company_id
     * @param $client_id
     */
    public static function generateRoomIdForGlobalChat($company_id, $client_id)
    {
        $room_id = json_encode([
            'prefix' => (string)config('chat.chat_room_prefix'),
            'action' => (string)'company-client-global-chat',
            'data' => [
                'client_id' => (int)$client_id,
                'company_id' => (int)$company_id
            ]
        ]);
        return $room_id;
    }


    public static function addMessageLikeClient($room_id, $client_id, $message)
    {
        $carbon = Carbon::now()->timestamp;
        $firestore = new FirebaseFirestoreService();
        $db = $firestore->getFirestoreDb();
        $client = Client::query()->where(['id' => $client_id])->first();
        $db->collection(self::COLLECTION_ROOMS)->document($room_id)->set([
            'room_id' => $room_id,
            'last_update' => $carbon
        ]);

        $db->collection(self::COLLECTION_ROOMS)->document($room_id)->collection('messages')->document($carbon)
            ->set([
                'by_id' => self::generateCompanyClientId($client_id),
                'by_name' => $client->first_name . ' ' . $client->last_name,
                'message' => $message,
                'time' => $carbon
            ]);
    }

    public static function addMessageLikeUser($room_id, $user_id, $message, $company_id)
    {
        $carbon = Carbon::now()->timestamp;
        $firestore = new FirebaseFirestoreService();
        $db = $firestore->getFirestoreDb();
        $user = User::query()->where(['id' => $user_id])->first();
        $by_name = $user->first_name . ' ' . $user->last_name;

        $companyStaff = CompanyStaff::query()->where([
            'user_id' => $user_id,
            'company_id' => $company_id
        ])->limit(1)->first();

        if ($companyStaff) {
            $staffRole = $companyStaff->role_in_company;

            if ($staffRole == CompanyStaffRoles::TRAINER) {
                $by_name .= ' (тренер)';
            }

            if ($staffRole == CompanyStaffRoles::NUTRITIONIST) {
                $by_name .= ' (диетолог)';
            }
        }


        $db->collection(self::COLLECTION_ROOMS)->document($room_id)->set([
            'room_id' => $room_id,
            'last_update' => $carbon
        ]);

        $db->collection(self::COLLECTION_ROOMS)->document($room_id)->collection('messages')->document($carbon)
            ->set([
                'by_id' => self::generateCompanyUserId($user_id),
                'by_name' => $by_name,
                'message' => $message,
                'time' => $carbon
            ]);


    }
}

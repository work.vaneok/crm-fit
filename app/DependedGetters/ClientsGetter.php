<?php
namespace App\DependedGetters;

use App\Components\CrmHelper;
use App\Filters\FiltersClientUserName;
use App\Models\Client;
use App\Models\ClientSubscription;
use App\Models\ClientVisit;
use App\Models\CompanyClient;
use App\Models\CompanyLocation;
use App\Models\Logs\LogClientVisits;
use Carbon\Carbon;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ClientsGetter
{
    static function getList()
    {
        $items = QueryBuilder::for(CompanyClient::class)
            ->where([
                'company_id' => CrmHelper::companyId()
            ])
            ->scopes('withLastVisitDate')
            ->with(['client'])
            ->allowedFilters([
                AllowedFilter::custom('username', new FiltersClientUserName()),
                AllowedFilter::scope('date_from', 'byCreatedAtFrom'),
                AllowedFilter::scope('date_to', 'byCreatedAtTo'),
                AllowedFilter::scope('phone', 'byPhone'),
            ])
            ->orderBy('last_visited_at', 'DESC')
            ->paginate(15);

        return $items;
    }

    static function getListOrderedByName()
    {
        $items = QueryBuilder::for(CompanyClient::class)
            ->where([
                'company_id' => CrmHelper::companyId()
            ])
            ->with(['client'])
            ->join('clients', 'clients.id', '=', 'company_clients.client_id')
            ->allowedFilters([
                AllowedFilter::custom('username', new FiltersClientUserName()),
                AllowedFilter::scope('date_from', 'withClientsByCreatedAtFrom'),
                AllowedFilter::scope('date_to', 'withClientsByCreatedAtTo'),
                AllowedFilter::scope('phone', 'byPhone'),
            ])
            ->orderByRaw('first_name, last_name')
            ->paginate(15);
        return $items;
    }
}

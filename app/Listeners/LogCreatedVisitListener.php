<?php
namespace App\Listeners;

use App\Events\ClientVisitCreatedEvent;
use App\Models\ClientVisit;
use App\Models\Logs\LogClientVisits;

class LogCreatedVisitListener
{
    public function handle(ClientVisitCreatedEvent $event)
    {
        /** @var ClientVisit $visit */
        $visit = ClientVisit::withTrashed()->firstWhere('id', $event->visit_id);
        if (!$visit) {
            \Log::error('Не получил визит для обновления посещений');
            return false;
        }

        LogClientVisits::add($visit);
    }
}

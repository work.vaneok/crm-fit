<?php
namespace App\Repositories;

use App\Components\Globals\ValidityValues;

/**
 * Период действия абонемента
 *
 * Class ValidityRepository
 * @package App\Repositories
 */
class ValidityRepository
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getForSelectBox()
    {



        return collect(
            ValidityValues::get()
        );
    }
}

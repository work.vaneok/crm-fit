<?php

namespace App\Http\Controllers\Api\V1;

use App\Components\CrmHelper;
use App\Http\Controllers\Actions\Chat\SaveMessageToChatAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Chat\SaveMessageRequest;


class ChatController extends Controller
{

    public function actionSaveMessage(SaveMessageRequest $request)
    {
        try {
            $action = new SaveMessageToChatAction([
                'message' => $request->input('message', null),
                'client_id' => $request->user()->client_id,
                'company_id' => CrmHelper::companyId()
            ]);
            $action->execute();
            return $this->createSuccessResponse([

            ]);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . ' ' . $exception->getTraceAsString());
            echo '</pre>';
            die;

        }
    }


}

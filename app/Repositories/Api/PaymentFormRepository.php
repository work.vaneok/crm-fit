<?php
namespace App\Repositories\Api;

use Cache;
use Carbon\Carbon;

class PaymentFormRepository
{
    private function getKey($key)
    {
        return 'payment_form_' . $key;
    }

    public function save($key, $html)
    {
        $this->clear($this->getKey($key));

        Cache::set($this->getKey($key), $html, $time = Carbon::now()->addMinutes(60));
    }

    public function get($key)
    {
        return Cache::get($this->getKey($key), null);
    }

    public function clear($key)
    {
        return Cache::forget($this->getKey($key));
    }
}

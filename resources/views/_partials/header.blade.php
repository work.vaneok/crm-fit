<nav class="header">
    <ul class="nav-left list-unstyled">
        <li>
            <div id="sidebarToggle"><i class="icon-menu"></i></div>
        </li>
    </ul>
    <ul class="nav-right list-unstyled">
        @yield('actions')
        @if($locations)
            <li>
                <a href="#" class="header__dropdown-button" role="button" id="locationDropdownLink"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="icon-location"></i>
                    <span>{{$currentLocationName}}</span>
                    <i class="icon-down"></i>
                </a>

                <div class="dropdown-menu shadow" style="right: -38px!important;"
                     aria-labelledby="locationsDropdownLink">
                    @foreach($locations as $id => $location)
                        @if($location)
                            @if(count(App\Components\CrmHelper::companyStaffCompanyLocations()) > 0)
                                @foreach(App\Components\CrmHelper::companyStaffCompanyLocations() as $cscl)
                                    @if($id === $cscl->company_location_id)
                                        <div class="cur-p dropdown-item set-location-item"
                                             data-href="{{ route('profile.set-current-location') }}"
                                             data-id="{{ $id }}">{{ $location }}</div>
                                    @endif
                                @endforeach
                            @else
                                <div class="cur-p dropdown-item set-location-item"
                                     data-href="{{ route('profile.set-current-location') }}"
                                     data-id="{{ $id }}">{{ $location }}
                                </div>
                            @endif
                        @endif
                    @endforeach
                </div>
            </li>
        @endif

        <li>
            <a class="header__dropdown-button" href="#" role="button" id="userDropdownLink" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="icon-user"></i>
                <span>{{Auth::user()->name}}</span>
                <i class="icon-more"></i>
            </a>

            <div class="dropdown-menu user-dropdown shadow" aria-labelledby="userDropdownLink">
                <a class="dropdown-item" href="{{route('profile.index')}}"><i class="icon-user"></i>Профиль</a>
                {{--<a class="dropdown-item" href="#"><i class="icon-case"></i>Мои компании</a>--}}
                @if(\App\Components\CrmHelper::getRoleInCompany() == \App\Constants\CompanyStaffRoles::OWNER ||\App\Components\CrmHelper::getRoleInCompany() == \App\Constants\CompanyStaffRoles::ADMINISTRATOR )
                    <a class="dropdown-item" href="{{route('locations.index')}}">
                        <i class="icon-location"></i>Мои локации
                    </a>
                @endif
                <div class="divider"></div>
                {{--<a class="dropdown-item" href="{{ route('logout') }}"><i class="icon-power"></i>Выход</a>--}}
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="icon-power"></i>Выход
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </a>
            </div>
        </li>
    </ul>
</nav>

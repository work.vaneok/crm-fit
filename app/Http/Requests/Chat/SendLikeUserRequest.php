<?php

namespace App\Http\Requests\Chat;


use Illuminate\Foundation\Http\FormRequest;


class SendLikeUserRequest extends FormRequest
{

    public function rules()
    {
        return [
            'message' => ['required'],
            'chat_id' => ['required'],
        ];
    }


}

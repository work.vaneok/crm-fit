<?php
namespace App\Routes;

use App\Models\Subscription;
use Illuminate\Routing\Router;

class SubscriptionRoutes extends Router
{
    public function registerRouters()
    {
        $this->get('/subscriptions', 'SubscriptionsController@index')->middleware('can:viewAny, ' . Subscription::class)->name('subscriptions.index');

        $this->get('/subscriptions/create', 'SubscriptionsController@create')->middleware('can:create, ' . Subscription::class)->name('subscriptions.create');
        $this->get('/subscriptions/edit/{subscription}', 'SubscriptionsController@edit')->middleware('can:update,subscription')->name('subscriptions.edit');
        $this->get('/subscriptions/show/{subscription}', 'SubscriptionsController@show')->middleware('can:view,subscription')->name('subscriptions.show');;

        $this->post('/subscriptions/create', 'SubscriptionsController@store')->middleware('can:create, ' . Subscription::class)->name('subscriptions.store');
        $this->put('/subscriptions/edit/{subscription}', 'SubscriptionsController@update')->middleware('can:update,subscription')->name('subscriptions.update');
    }
}

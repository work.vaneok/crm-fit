<?php
return [
    'company:timeToReturnVisit' => 'Время для возврата посещения (в минутах)',

    'liqpay:public_key' => 'LIQPAY Public Key',
    'liqpay:private_key' => 'LIQPAY Private Key',

    'wayforpay:login' => 'WayForPay MERCHANT LOGIN',
    'wayforpay:secret_key' => 'WayForPay MERCHANT SECRET KEY',

    'sms_turbo:login' => 'TurboSMS Login',
    'sms_turbo:password' => 'TurboSMS Password',
    'sms_turbo:sender' => 'TurboSMS Sender',

    'social:fb' => 'Facebook',
    'social:instagram' => 'Instagram',
    'social:telergam' => 'Telegram',
    'social:viber' => 'Viber',
    'social:vk' => 'VK',
];

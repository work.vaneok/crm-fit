<?php

namespace App\Http\Controllers;

use App\Actions\Subscription\CreateClientSubscriptionAction;
use App\Actions\Subscription\ApproveOrderAction;
use App\Components\CrmHelper;
use App\Components\Payments\Facades\WayForPayCredentialsFacade;
use App\Constants\OrderStatuses;
use App\Constants\StartOfActivity;
use App\Exceptions\Actions\Subscription\SubscriptionAlreadyPayed;
use App\Models\Order;
use App\Repositories\Api\PaymentFormRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Log;
use WayForPay\SDK\Exception\WayForPaySDKException;
use WayForPay\SDK\Handler\ServiceUrlHandler;

class BuySubscriptionController extends Controller
{
    public function form(Order $order)
    {
        /** @var PaymentFormRepository $payRep */
        $payRep = app(PaymentFormRepository::class);
        $html = $payRep->get($order->id);
        $payRep->clear($order->id);
        return $html ?? abort(404);
    }

    public function result()
    {
        /**
         * "merchantAccount" => "test_merch_n1"
         * "merchantSignature" => "80e64454d8a801ce9766cb1a828a0a27"
         * "orderReference" => "abomenent_49"
         * "amount" => "111"
         * "currency" => "UAH"
         * "authCode" => null
         * "email" => "pashkinz92@gmail.com"
         * "phone" => "380688504706"
         * "createdDate" => "1582189412"
         * "processingDate" => "1582189526"
         * "cardPan" => "51****6541"
         * "cardType" => "MasterCard"
         * "issuerBankCountry" => "Ukraine"
         * "issuerBankName" => "COMMERCIAL BANK PRIVATBANK"
         * "transactionStatus" => "Declined"
         * "reason" => "Three Ds Fail"
         * "reasonCode" => "1108"
         * "fee" => "0"
         * "paymentSystem" => "card"
         */

        // TODO: Сделать фабрику
        $json = file_get_contents('php://input');
        $request = $_REQUEST;


        $order_id = parseOrderId($request['orderReference']);

        $clientSubscription = null;
        $orderModel = Order::query()->where(['id' => $order_id])->limit(1)->first();
        // $orderModel = Order::whereId($order_id);
        if (!$orderModel) {
            throw new \Exception('Order not found ' . var_export([$order_id, $request['orderReference'], $request], true));
        }
        CrmHelper::loadCompanyById($orderModel->company_id);

        $orderModel->update(['bank_response' => $json]);

        try {
            try {
                $handler = new ServiceUrlHandler(app(WayForPayCredentialsFacade::class));
            } catch (\Exception $e) {
                if (request('transactionStatus') == 'Declined') {
                    $orderModel->update(['status' => OrderStatuses::DECLINED]);
                    Log::channel('telegram')->error("Возможно не заполнены данные получателя для платежа");
                    \Log::error("Возможно не заполнены данные получателя для платежа");
                    return view('mobile.order.decline');
                }

            }

            $response = $handler->parseRequestFromGlobals();

            if ($response->getReason()->isOK()) {
                try {
                    DB::transaction(function () use ($orderModel, $order_id, &$clientSubscription) {
                        $order = (new ApproveOrderAction())->execute($order_id, Carbon::now());
                        if ($order) {
                            $orderItemInfo = $order->order_item_info;
                            // Создать абонемент клиента
                            $clientSubscription = (new CreateClientSubscriptionAction())->execute(
                                $order->client_id,
                                $order->company_id,
                                $order->company_location_id,
                                $order->order_item_id,
                                $order->id,
                                $orderItemInfo['validity'],
                                $orderItemInfo['quantity_of_occupation'],
                                StartOfActivity::FROM_FIRST_VISIT,
                                $orderItemInfo['available_to_before_begin_of_use']
                            );
                        }
                        Log::channel('telegram')->error('Subscription was bought, order id ' . $orderModel->id . ', subs id ' . $clientSubscription->id);

                        \Log::info('Subscription was bought, order id ' . $orderModel->id . ', subs id ' . $clientSubscription->id);
                    });
                } catch (SubscriptionAlreadyPayed $exception) {
                    Log::channel('telegram')->error("SubscriptionAlreadyPayed");
                    return view('mobile.order.success');
                } catch (\Exception $exception) {
                    $orderModel->update(['status' => OrderStatuses::ERROR]);
                    \Log::error($exception->getMessage());
                    Log::channel('telegram')->error($exception->getMessage());

                    return view('mobile.order.error');
                }
                if (!$clientSubscription) {
                    $orderModel->update(['status' => OrderStatuses::ERROR]);
                    Log::channel('telegram')->error('Subscription was not created for order ' . $orderModel->id . '; from mobile application');
                    \Log::error('Subscription was not created for order ' . $orderModel->id . '; from mobile application');
                    return view('mobile.order.error');
                }

                //TODO: Event about bought client subscription
                return view('mobile.order.success');
            } else {
                if (request('transactionStatus') == 'Declined') {
                    $orderModel->update(['status' => OrderStatuses::DECLINED]);
                    return view('mobile.order.decline');
                } else {
                    $orderModel->update(['status' => OrderStatuses::ERROR]);
                    return view('mobile.order.error');
                }
            }
        } catch (WayForPaySDKException $e) {
            \Log::error("WayForPay SDK exception: " . $e->getMessage());
            Log::channel('telegram')->error("WayForPay SDK exception: " . $e->getMessage());

            if (request('transactionStatus') == 'Declined') {
                $orderModel->update(['status' => OrderStatuses::DECLINED]);
                return view('mobile.order.decline');
            } else {
                $orderModel->update(['status' => OrderStatuses::ERROR]);
                return view('mobile.order.error');
            }
        }
    }


}

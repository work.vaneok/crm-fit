<?php
namespace App\Repositories;

use App\Components\Globals\SubscriptionCancellationPeriods;

/**
 * Период аннуляции абонемента
 *
 * Class SubscriptionCancellationRepository
 * @package App\Repositories
 */
class SubscriptionCancellationRepository
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function getForSelectBox()
    {

        return collect(
            SubscriptionCancellationPeriods::get()
        )->map(function ($item) {
            return $item['label'];
        });
    }

    public function getList()
    {
        return collect(
            SubscriptionCancellationPeriods::get()
        );
    }

    public function getById($id)
    {
        return SubscriptionCancellationPeriods::getByKey($id);
    }
}

<?php

namespace App\Components\MediaLibrary;

use App\Components\CrmHelper;
use App\Models\Company;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\PathGenerator\PathGenerator;

class MediaLibraryGenerator implements PathGenerator
{

    public function getPath(Media $media): string
    {
        $company_id = $media->company_id ? $media->company_id : 'common';
        return 'custom-files/' . implode('/', str_split($company_id)) . '/' . substr($media->file_name, 0, 3) . '/';
    }

    public function getPathForConversions(Media $media): string
    {
        return $this->getPath($media) . 'c/';
    }

    public function getPathForResponsiveImages(Media $media): string
    {
        return $this->getPath($media) . '/cri/';
    }
}

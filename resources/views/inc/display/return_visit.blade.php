
<form action="{{ $route }}" method="POST" style="display: inline" data-ajax="true">
    @method('PATCH')
    @csrf

    <button style="margin-left: 5px" type="submit" class="btn btn-sm  {{ $btn ?? '' }} btn-warning delete-btn">  <i class="fa fa-undo"></i>  {{'Возврат'}}</button>
</form>

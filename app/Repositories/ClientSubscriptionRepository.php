<?php
namespace App\Repositories;

use App\Models\Client;
use App\Models\ClientSubscription;
use App\Models\Company;
use App\Models\CompanySetting;

/**
 * Абонементы пользователя по компании
 *
 * Class ClientSubscriptionRepository
 * @package App\Repositories
 */
class ClientSubscriptionRepository
{
    /**
     * @var Company
     */
    private $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return ClientSubscription[]
     */
    public function findActive($client_id)
    {
        return ClientSubscription::active()->byCompany($this->company->id)->where(['client_id' => $client_id])->with(['user'])->get();
    }

}

<?php
namespace App\Components\Sms;

use App\Components\Sms\Events\SmsAfterSendEvent;
use App\Components\Sms\Events\SmsBeforeSendEvent;
use App\Components\Sms\Events\SmsErrorSendEvent;
use Str;

class SmsService
{
    private $providers;

    public function __construct($providers)
    {
        $this->providers = $providers;
    }

    public function send($phone, $msg, $serviceName = null/*'turbosms'*/)
    {
        $smsInternalId = Str::uuid();
        event(new SmsBeforeSendEvent($smsInternalId, $phone, $msg));

        $msg = remove_emoji($msg);

        if ($serviceName) {
            $result = $this->get($serviceName)->sendSms($phone, $msg);
            event(new SmsAfterSendEvent($smsInternalId, $phone, $msg, $result));
            return $result;
        }

        try {
            foreach ($this->providers as $key => $v) {
                if ($this->providers[$key]['active']) {
                    $rez = $this->get($key)->sendSms($phone, $msg);

                    event(new SmsAfterSendEvent($smsInternalId, $phone, $msg, $rez));

                    if ($rez['status']) {
                        return $rez;
                    }
                    continue;
                }
            }
        } catch (\Exception $e) {
            event(new SmsErrorSendEvent($smsInternalId, $e->getMessage()));
            throw  new \Exception($e->getMessage());
        }

        //throw new \Exception('Нет активных Сервисов');
        return [
            'status' => false,
            'msg' => 'Нет активных Сервисов'
        ];
    }

    private function get($name)
    {
        if (!isset($smsProviders[$name])) {
            $smsProviders[$name] = SmsProviderFactory::create($name, $this->providers[$name]);
        }
        return $smsProviders[$name];
    }
}

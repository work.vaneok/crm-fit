$(document).ready(function () {
    var ajaxModal = new AjaxFormModal();
    var ajaxBtnForm = new AjaxButton('.ajax-btn-form');
    var ajaxBtnView = new AjaxButton('.ajax-btn-view').setTypeView();
    ajaxModal.addOpenButton(ajaxBtnForm);
    ajaxModal.addOpenButton(ajaxBtnView);
    ajaxModal.build();
});

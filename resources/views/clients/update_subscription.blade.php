<div>

    {!! Form::open()->multipart()->route('clients.profile.update-subscription')->post() !!}
    {!! Form::hidden('subscription_id', $model['id']) !!}
    {!!
        Form::text('available_from',
        'Доступен с',
        $model['available_from']?\Carbon\Carbon::parse($model['available_from'])->format('d.m.Y'):'')
        ->placeholder(config('app.form_date_mask'))
        ->help(__('Дату вводите в формате',['?'=>config('app.form_date_mask')]))
        ->required()
         !!}
    {!! Form::text('available_to',
        'Доступен по',
        $model['available_to']?\Carbon\Carbon::parse($model['available_to'])->format('d.m.Y'):'')
       ->placeholder(config('app.form_date_mask'))
       ->help(__('Дату вводите в формате',['?'=>config('app.form_date_mask')]))
       ->required() !!}
    {!! Form::text('quantity_of_occupation', __('Кол-во занятий'),$model['quantity_of_occupation'])->type('numeric')->help('@-1@ - Безлимитное количетво занятий ') !!}
    <div class="form-group row">
        <div class="col-sm-2"></div>
        <div class="col-sm-6">
            <div class="form-check"><input type="checkbox" name="has_trainer" id="inp-has_trainer" value="1"
                                           {{$model['has_trainer']?'checked':''}}
                                           class="form-check-input"><label for="inp-has_trainer"
                                                                           class="form-check-label">Тренер
                    включен</label></div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-2"></div>
        <div class="col-sm-6">
            <div class="form-check"><input type="checkbox" name="has_nutritionist" id="inp-has_nutritionist" value="1"
                                           {{$model['has_nutritionist']?'checked':''}} class="form-check-input"><label for="inp-has_nutritionist"
                                                                                      class="form-check-label">Диетолог
                    включен</label></div>
        </div>
    </div>

    {{--{!! Form::checkbox('has_trainer',__('Тренер включен'),1,$model['has_trainer']) !!}
    {!! Form::checkbox('has_nutritionist',__('Диетолог включен'),1,$model['has_trainer']) !!}--}}

    @if($model['has_trainer'])
        {!! Form::select('trainer_id',__('Тренер'),$trainers,$model['trainer_id']) !!}
    @endif

    @if($model['has_nutritionist'])
        {!! Form::select('nutritionist_id',__('Диетолог'),$nutritionists,$model['nutritionist_id']) !!}
    @endif

    {!!  Form::close() !!}
</div>


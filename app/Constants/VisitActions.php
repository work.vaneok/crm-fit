<?php
namespace App\Constants;

class VisitActions
{
    const VISIT = 'visit';
    const RETURN = 'return';
}

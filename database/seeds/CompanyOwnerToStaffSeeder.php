<?php

use App\Constants\CompanyStaffRoles;
use App\Constants\Roles;
use App\Models\Company;
use App\Models\CompanyStaff;
use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class CompanyOwnerToStaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::findByName(Roles::COMPANY_OWNER);
        foreach ($role->users as $user) {
            $company = Company::whereOwnerId($user->id)->first();
            if (!$company) {
                continue;
            }

            CompanyStaff::firstOrCreate([
                'company_id' => $company->id,
                'user_id' => $user->id,
                'role_in_company' => CompanyStaffRoles::OWNER,
            ]);
        }

    }
}

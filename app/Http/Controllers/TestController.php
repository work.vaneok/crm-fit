<?php

namespace App\Http\Controllers;

use App\Actions\Client\ClientConfirmSmsAction;
use App\Actions\Client\CreateCompanyClientAction;
use App\Http\Requests\Client\CreateClientRequest;
use App\Http\Requests\Client\FindClientByPhoneRequest;
use App\Http\Resources\CompanyClient;
use App\Models\Client;
use App\Repositories\CompanyClientRepository;
use App\Repositories\CompanySettingsRepository;
use App\Repositories\SexRepository;
use App\Rules\PhoneFormatRule;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Log;

class TestController extends Controller
{
    public function index()
    {
        dd(Carbon::now()->startOfDay()->addYear()->format('d.m.Y H:i:s'));
    }
}

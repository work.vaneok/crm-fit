@extends('layouts.dashboard')

@section('title')
    {{ __('Редактирование абонемента') }}
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->multipart()->route('subscriptions.update', ['subscription' => $subscription->id])->put()->fill($subscription) !!}

        {!! Form::text('title', 'Наименование') !!}
        {!! Form::date('available_from', 'Доступен с', $subscription->available_from ? $subscription->available_from->toDateString() : \Carbon\Carbon::now()->toDateString()) !!}
        {!! Form::date('available_to', 'Доступен по', $subscription->available_to ? $subscription->available_to->toDateString() : \Carbon\Carbon::now()->addYear()->toDateString()) !!}
        {!! Form::file('image', __('Изображение')) !!}
        @image_media(['model' => $subscription, 'form' => true])
        {!! Form::text('price', __('Цена'))->type('numeric') !!}
        {!! Form::text('old_price', __('Старая цена'))->type('numeric') !!}
        {!! Form::select('validity', __('Срок действия'))->options($validity_values) !!}
        {!! Form::select('available_to_before_begin_of_use', __('Период аннуляции'))->options($cancellation_value)->required() !!}
        {!! Form::text('quantity_of_occupation', __('Кол-во занятий'))->help(__('Оставьте пустым, если безлим'))->type('numeric') !!}
        {!! Form::checkbox('can_be_selected_by_staff', __('Доступен сотрудникам')) !!}
        {!! Form::checkbox('can_share', __('Возможно делиться')) !!}
        {!! Form::checkbox('has_trainer', __('Тренер включен')) !!}
        {!! Form::checkbox('has_nutritionist', __('Диетолог включен')) !!}
        {!! Form::checkbox('is_available_in_mobile_app', __('Доступен в мобильном приложении')) !!}
        {!! Form::checkbox('can_buy_online', __('Доступен для покупки online')) !!}

        <br>
        {!! Form::checkbox('is_active', __('Активный')) !!}

        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit(__('Сохранить изменения')) !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection

@push('scripts')
    @include('inc.ckeditor')
@endpush


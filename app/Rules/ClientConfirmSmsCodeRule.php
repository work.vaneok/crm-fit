<?php

namespace App\Rules;

use App\Repositories\ConfirmSmsCodeRepository;
use Illuminate\Contracts\Validation\Rule;

class ClientConfirmSmsCodeRule implements Rule
{
    private $phone;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /** @var ConfirmSmsCodeRepository $smsConfirmRep */
        $smsConfirmRep = app(ConfirmSmsCodeRepository::class);
        return $smsConfirmRep->get($this->phone) === (int)$value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Код из смс не совпадает');
    }
}

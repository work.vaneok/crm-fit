<?php

namespace App\Http\Requests\Auth;


use App\Rules\PhoneFormatRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ResetPasswordRequest
 * @package App\Http\Requests\Auth
 * @property string $phone
 */
class ResetPasswordRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'phone' => [
                'required',
                'string',
                new PhoneFormatRule()
            ],
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => __('Поле обязательно'),
        ];
    }
}

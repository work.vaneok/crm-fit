<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NewsApiCollection extends ResourceCollection
{
    public $collects = NewsApiResource::class;

}

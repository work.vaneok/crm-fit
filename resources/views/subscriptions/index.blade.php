@extends('layouts.dashboard')

@section('title')
    {{ __('Абонементы') }}
@endsection

@section('content')

    <div class="header__filter" style=" margin-bottom: 15px;">
        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#filter"
                style="margin-left: 0;">{{ __('Фильтр') }}</button>
        <div id="filter" class="collapse col-lg-12" style="margin-top: 15px; padding-left: 0px;">
            <form action="" class="grid-filter" method="get">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="filter[id]">ID</label>
                        <input class="form-control" type="text" value="{{get_filter_value('id')}}"
                               name="filter[id]" placeholder="Id">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="filter[title]">{{ __('Наименование') }}</label>
                        <input class="form-control " type="text" value="{{get_filter_value('title')}}"
                               name="filter[title]" placeholder="{{ __('Наименование') }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary "><i class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                    </div>
                    <div class="form-group">
                        <a href="/{{ Route::current()->uri }}" class="btn btn-danger "><i class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">{{ __('Наименование') }}</th>
                <th scope="col">{{ __('Период действия') }}</th>
                <th scope="col">{{ __('Цена') }}</th>
                <th scope="col">{{ __('Кол-во занятий') }}</th>
{{--                <th scope="col">{{ __('Тренер') }}</th>--}}
                <th scope="col">{{ __('Период активности') }}</th>
                <th scope="col">{{ __('Активный') }}</th>
                <th scope="col">{{ __('Действия') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($subscriptions as $item)
                <tr url="{{route('subscriptions.show',['subscription' => $item->id])}}">
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->validityText }}</td>
                    <td>{{ $item->price }}</td>
                    <td>{{ $item->quantity_of_occupation_text }}</td>
                    <td>{{ $item->formatDate('available_from') }} - {{ $item->formatDate('available_to') }}</td>
{{--                    <td>@boolean(['boolean' => $item->has_trainer])</td>--}}
                    <td>@boolean(['boolean' => $item->is_active])</td>
                    <td>@edit(['route' => route('subscriptions.edit', ['subscription' => $item->id, 'backUrl' => url()->full()])])</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $subscriptions ? $subscriptions->appends(request()->only('filter'))->links() : '' }}
    </div>
@endsection

@section('actions')
    @if (Auth::user()->can('create', \App\Models\Subscription::class))
        {{--<div class="btn-toolbar mb-2 mb-md-0">--}}
        <li>
            @add(['route' => route('subscriptions.create')])
        </li>
        {{--</div>--}}
    @endif
@endsection

@push('css')

@endpush

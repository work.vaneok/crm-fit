<?php

namespace App\Http\Requests\Firebase;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FirebaseIndexRequest
 * @package App\Http\Requests\Auth
 */
class FirebaseCreateViewRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'topic_id' => ['integer'],
            'title' => ['string'],
            'descriptions' => ['string'],
            'image_url' => ['string'],
        ];
    }


}

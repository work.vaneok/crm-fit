<?php
namespace App\Actions\CompanyStaff;

use App\Models\CompanyStaff;
use App\Models\CompanyStaffCompanyLocation;

class SetAvailableLocationsAction
{
    public function execute($staff_id, $locations_id)
    {
        CompanyStaffCompanyLocation::where('company_staff_id', $staff_id)->delete();
        if ($locations_id) {
            foreach ($locations_id as $location_id) {
                CompanyStaffCompanyLocation::create(['company_staff_id' => $staff_id, 'company_location_id' => $location_id]);
            }
            if (!array_search(CompanyStaff::find($staff_id)->current_location_id, $locations_id)) {
                CompanyStaff::find($staff_id)->update(['current_location_id' => $locations_id[0]]);
            }
        }
    }
}

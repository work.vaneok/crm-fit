@extends('layouts.dashboard')

@section('title')
    {{ __('Локация') }}
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                {{ __('Наименование') }}
            </dt>
            <dd class="col-sm-10">
                {{$location->title}}
            </dd>
        </dl>


    </div>
@endsection

@section('actions')
    @if (Auth::user()->can('update', $location))
        <li>
            @edit(['route' => route('locations.edit',['location' => $location->id, 'backUrl' => url()->previous()])])&nbsp;
        </li>
{{--    @delete(['route' => route('locations.destroy',['location' => $location->id])])--}}
    @endif
@endsection

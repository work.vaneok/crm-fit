<?php
namespace App\Exceptions\Actions\Subscription;

class SubscriptionAlreadyPayed extends \Exception
{
    protected $message = 'Subscription already payed';
}

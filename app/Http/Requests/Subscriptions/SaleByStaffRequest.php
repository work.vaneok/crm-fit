<?php
namespace App\Http\Requests\Subscriptions;

use App\Components\CrmHelper;
use App\Http\Requests\Traits\DefaultCompanyIdInRequest;
use App\Http\Requests\Traits\DefaultCompanyLocationIdInRequest;
use App\Repositories\ValidityRepository;
use App\Rules\ClientOfCompanyRule;
use App\Rules\PhoneFormatRule;
use App\Rules\SubscriptionOfCompanyRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SaleByStaffRequest extends FormRequest
{
    use DefaultCompanyLocationIdInRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required'],
            'company_location_id' => ['required'],
            'client_id' => ['required', new ClientOfCompanyRule($this->get('company_id'))],
            'subscription_id' => ['required', new SubscriptionOfCompanyRule($this->get('company_id'))],
           // 'phone' => ['required', new PhoneFormatRule()],
         //   'phone' => ['required'],
            'payment_source' => ['required', Rule::in(['1', '2'])],
            'start_of_activity' => ['required', Rule::in(['1', '2'])],
        ];
    }

}

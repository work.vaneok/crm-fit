<?php

namespace App\Policies;

use App\Components\CrmHelper;
use App\Constants\Roles;
use App\Models\CompanyStaff;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyStaffPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any company staff.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasRole(Roles::COMPANY_OWNER);
    }

    /**
     * Determine whether the user can view the company staff.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyStaff  $companyStaff
     * @return mixed
     */
    public function view(User $user, CompanyStaff $companyStaff)
    {
        return $user->hasRole(Roles::COMPANY_OWNER);
    }

    /**
     * Determine whether the user can create company staff.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole(Roles::COMPANY_OWNER);
    }

    /**
     * Determine whether the user can update the company staff.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyStaff  $companyStaff
     * @return mixed
     */
    public function update(User $user, CompanyStaff $companyStaff)
    {
        return $companyStaff->company_id == CrmHelper::companyId() && $user->hasRole(Roles::COMPANY_OWNER);
    }

    /**
     * Determine whether the user can delete the company staff.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyStaff  $companyStaff
     * @return mixed
     */
    public function delete(User $user, CompanyStaff $companyStaff)
    {
        return $companyStaff->company_id == CrmHelper::companyId() && $user->hasRole(Roles::COMPANY_OWNER);
    }

    /**
     * Determine whether the user can restore the company staff.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyStaff  $companyStaff
     * @return mixed
     */
    public function restore(User $user, CompanyStaff $companyStaff)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the company staff.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyStaff  $companyStaff
     * @return mixed
     */
    public function forceDelete(User $user, CompanyStaff $companyStaff)
    {
        //
    }

    public function fireEmployee(User $user, CompanyStaff $companyStaff)
    {
        return $companyStaff->user_id != $user->id && $companyStaff->company_id == CrmHelper::companyId() && $user->hasRole(Roles::COMPANY_OWNER);
    }
}

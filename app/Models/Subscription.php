<?php

namespace App\Models;

use App\Models\Traits\ByCompanyScopeTrait;
use App\Models\Traits\DisplayDateFormat;
use App\Models\Traits\QuantityOfOccupationText;
use App\Models\Traits\ValidityText;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Subscription extends Model implements HasMedia
{
    use DisplayDateFormat, HasMediaTrait, ValidityText, ByCompanyScopeTrait, QuantityOfOccupationText;

    protected $table = 'subscriptions';
    protected $guarded = ['id'];

    protected $dates = ['available_from', 'available_to'];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_MAX, 600, 600);
    }

    public function getIsUnlimitedAttribute()
    {
        return $this->quantity_of_occupation == -1;
    }

    public function scopeForStaff($q)
    {
        $q->where(['can_be_selected_by_staff' => 1]);
    }

    public function scopeForOwner($q)
    {

    }

    public function scopeActive(Builder $q)
    {
        $q->where(['is_active' => 1])
            ->where('available_from', '<=', Carbon::now()->toDateString())
            ->where('available_to', '>=', Carbon::now()->toDateString());
    }

    public function scopeForMobile(Builder $q)
    {
        $q->where(['is_available_in_mobile_app' => 1]);
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}

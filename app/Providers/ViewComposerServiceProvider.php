<?php

namespace App\Providers;

use App\Actions\Company\SaveCompanySettingsAction;
use App\Components\CrmHelper;
use App\Components\PriceFormatter;
use App\Components\Sms\SmsService;
use App\Constants\Roles;
use App\Models\Client;
use App\Models\ClientSubscription;
use App\Models\ClientVisit;
use App\Models\Company;
use App\Models\CompanyLocation;
use App\Models\CompanyStaff;
use App\Models\Order;
use App\Models\Tariff;
use App\Observers\CreatedByObserver;
use App\Observers\CreatedByUserIdObserver;
use App\Repositories\ClientSubscriptionRepository;
use App\Repositories\CompanyClientRepository;
use App\Repositories\CompanySettingsRepository;
use App\Repositories\DependedOnRole\SubscriptionsCompanyOwnerRepository;
use App\Repositories\DependedOnRole\SubscriptionsCompanyStaffRepository;
use App\Repositories\DependedOnRole\SubscriptionsRepository;
use App\User;
use Auth;
use Blade;
use Illuminate\Support\ServiceProvider;
use View;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer([
            'layouts.dashboard',
            'layouts.default',
            'layouts.chat'
        ], function ($view) {
            if (isset($view->getData()['currentLocationName'])) {
                $data['currentCompanyName'] = $view->getData()['currentCompanyName'];
                $data['currentLocationName'] = $view->getData()['currentLocationName'];
                $data['locations'] = $view->getData()['locations'];
            } else {
                //dd(CrmHelper::company());
                $data['currentCompanyName'] = CrmHelper::company()->title;
                $data['currentLocationName'] = CrmHelper::companyLocation() ? CrmHelper::companyLocation()->title : false;
                $data['locations'] = CrmHelper::company()->locations->mapWithKeys(function ($item) {
                    return [$item->id => $item->title];
                });
            }

            if (isset($view->getData()['companyImage']) && isset($view->getData()['companyImageBack'])) {
                $data['companyImage'] = $view->getData()['companyImage'];
            } else {
                $data['companyImageBack'] = CrmHelper::company()->getFirstMedia('image_back') ? CrmHelper::company()->getFirstMediaUrl('image_back', 'thumb') : null;
                $data['companyImage'] = CrmHelper::company()->getFirstMedia('image') ? CrmHelper::company()->getFirstMediaUrl('image', 'thumb') : null;
                //$data['companyImage'] = CrmHelper::company()->getFirstMedia() ? '<div class="company-logo" style="background-image: url(' . asset(CrmHelper::company()->getFirstMediaUrl('default', 'thumb')) . ')" ></div>' : '';
            }

            $view->with($data);
        });
    }
}

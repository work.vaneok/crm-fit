<?php
namespace App\Http\Requests\Client;


use App\Rules\PhoneFormatRule;
use Illuminate\Foundation\Http\FormRequest;

class FindClientByBarcodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'number' => ['required', 'numeric'],
        ];
    }

    public function messages()
    {
        return [
            'number.required' => __('Поле обязательно'),
            'number.numeric' => __('Поле должно быть числом'),
        ];
    }
}

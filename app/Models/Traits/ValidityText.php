<?php

namespace App\Models\Traits;

use App\Components\Globals\ValidityValues;

trait ValidityText
{

    public function getValidityTextAttribute()
    {
        return ValidityValues::get()[$this->validity];
    }
}

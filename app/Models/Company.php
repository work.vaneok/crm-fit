<?php

namespace App\Models;

use App\Models\Traits\DisplayDateFormat;
use App\Repositories\CompanySubscriptions;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Company extends Model implements HasMedia
{
    const TableName = 'companies';


    use DisplayDateFormat, HasMediaTrait;

    protected $casts = [
        'available_from' => 'datetime',
        'available_to' => 'datetime',
    ];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function tariff()
    {
        return $this->belongsTo(Tariff::class, 'tariff_id', 'id');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_MAX, 250, 250);
    }

    public function settings()
    {
        return $this->hasMany(CompanySetting::class, 'company_id', 'id');
    }

    public function locations()
    {
        return $this->hasMany(CompanyLocation::class, 'company_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clients()
    {
        return $this->belongsToMany(Client::class, 'company_clients', 'company_id', 'client_id')->withTimestamps()->withPivot(['created_by_user_id', 'api_token']);
    }

    public function staffs()
    {
        return $this->hasMany(CompanyStaff::class, 'company_id', 'id');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'company_id', 'id');
    }

    public function adminSettings()
    {
        return $this->belongsTo(CompanyAdminSettings::class, 'id', 'company_id');
    }
}

<?php
namespace App\DependedGetters;

use App\Components\CrmHelper;
use App\Models\ClientSubscription;
use App\Models\ClientVisit;
use App\Models\CompanyClient;
use App\Models\CompanyLocation;
use App\Models\Logs\LogClientVisits;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ClientVisitsGetter
{
    static function getList()
    {
        $items = QueryBuilder::for(ClientVisit::class)
            ->where([
                'company_id' => CrmHelper::companyId()
            ])
            ->allowedFilters([
                AllowedFilter::partial('username', 'client.first_name'),
                AllowedFilter::partial('subscription', 'client_subscription.subscription.title'),
                AllowedFilter::scope('date', 'byCreatedAt'),
                AllowedFilter::scope('date_from', 'byCreatedAtFrom'),
                AllowedFilter::scope('date_to', 'byCreatedAtTo'),
                AllowedFilter::scope('phone', 'byPhone'),
            ])
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return $items;
    }

    static function getHistory(CompanyClient $companyClient)
    {
        $items = QueryBuilder::for(LogClientVisits::class)
            ->join('client_visits', 'client_visits.id', '=', 'log_client_visits.visit_id')
            ->with(['visit', 'createdBy', 'clientSubscription.subscription'])
            ->where([
                'client_visits.company_id' => CrmHelper::companyId(),
                'client_visits.client_id' => $companyClient->client_id,
            ])
            ->allowedFilters([
                AllowedFilter::exact('client_subscription', 'log_client_visits.client_subscription_id'),
            ])
            ->orderBy('client_visits.id', 'DESC')
            ->paginate(15);

        return $items;
    }

    static function getCountToday()
    {
        return QueryBuilder::for(ClientVisit::class)
            ->where(['company_id' => CrmHelper::companyId()])
            ->where(['company_location_id' => CrmHelper::companyLocationId()])
            ->whereDate('created_at', today())
            ->count();
    }
}

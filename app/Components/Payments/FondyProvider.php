<?php
namespace App\Components\Payments;


use App\Classes\Fondy;
use App\Classes\Tariffs\Models\TariffBill;
use App\Classes\Tariffs\Models\TariffOrder;
use App\Components\Payments\Models\PaymentModel;
use App\Facades\LiqPayFacade;

class FondyProvider implements IPaymentProvider
{
    public function getForm(PaymentModel $model)
    {
        /** @var Fondy $fondy */
        /*$fondy = app(Fondy::class);

        $data = [
            'order_desc' => $bill->getBillDescription(),
            'order_id' => __generate_payment_order_id($order),
            'currency' => $bill->company->region->currency->iso,
            'amount' => $bill->finalPrice * 100,
            'response_url' => route('tariff.pay-success', [], true),
            'server_callback_url' => route('tariff.pay-success', [], true),
            'lang' => 'ru',
        ];*/

        return $fondy->form($data);
    }

    public function successPay($request)
    {
        /*
        [
          "rrn" => null,
          "masked_card" => "444455XXXXXX1111",
          "sender_cell_phone" => null,
          "response_signature_string" => "**********|92000|UAH|92000|478450|444455|VISA|UAH|5|444455XXXXXX1111|1396424|crm1top_65|approved|23.01.2020 12:16:32|191938152|card|success|0|no@no.no|0|purchase",
          "response_status" => "success",
          "sender_account" => null,
          "fee" => null,
          "rectoken_lifetime" => null,
          "reversal_amount" => "0",
          "settlement_amount" => "0",
          "actual_amount" => "92000",
          "order_status" => "approved",
          "response_description" => null,
          "verification_status" => null,
          "order_time" => "23.01.2020 12:16:32",
          "actual_currency" => "UAH",
          "order_id" => "crm1top_65",
          "parent_order_id" => null,
          "merchant_data" => null,
          "tran_type" => "purchase",
          "eci" => "5",
          "settlement_date" => null,
          "payment_system" => "card",
          "rectoken" => null,
          "approval_code" => "478450",
          "merchant_id" => "1396424",
          "settlement_currency" => null,
          "payment_id" => "191938152",
          "product_id" => null,
          "currency" => "UAH",
          "card_bin" => "444455",
          "response_code" => null,
          "card_type" => "VISA",
          "amount" => "92000",
          "sender_email" => "no@no.no",
          "signature" => "853dfc04c153776a8926ee98989e8019684be7c0"
        ]
        */

        /** @var Fondy $fondy */
        $fondy = app(Fondy::class);

        if (!$fondy->isPaymentValid($request)) {
            abort(403, 'Payment is not valid');
        }


        if ($fondy->isPaymentApproved($request)) {
            return [__parse_payment_order_id($request['order_id'])[1], $request, in_array($request['response_status'], ['success'])];
        } else {
            throw new \Exception('Payment is not approved');
        }
    }
}

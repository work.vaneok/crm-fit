<?php

namespace App\Http\Controllers\Actions\Chat;

use App\Components\Chat\ChatComponent;
use App\Http\Controllers\Actions\BaseAction;

class SaveMessageToChatAction extends BaseAction
{


    public $message;
    public $client_id;
    public $company_id;

    public function rules(): array
    {

        return [
            'message' => ['required'],
            'client_id' => ['required'],
            'company_id' => ['required'],
        ];
    }

    public function action()
    {
        $room_id = ChatComponent::generateRoomIdForGlobalChat($this->company_id, $this->client_id);
        ChatComponent::addMessageLikeClient($room_id, $this->client_id, $this->message);
    }

}

<?php

namespace App\Actions\Firebase;


use App\Actions\BaseAction;
use App\Models\Client;
use App\Models\FirebaseNotification;
use App\Models\FirebaseNotificationReceiver;

class FirebaseIndexAction extends BaseAction
{

    public $company_id;
    public $page;
    public $per_page;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'page' => ['nullable']
        ];
    }

    public function action()
    {
        $current_page = $this->page ? $this->page : BaseAction::DEFAULT_PAGE;
        $current_per_page = $this->per_page ? $this->per_page : BaseAction::DEFAULT_PER_PAGE;

        $query = FirebaseNotification::where(
            [
                FirebaseNotification::COLUMN_COMPANY_ID => $this->company_id
            ]
        )
            ->with([
                'receivers'
            ])->orderBy(FirebaseNotification::COLUMN_ID, 'desc');


        $dataProvider = $query->paginate($current_per_page, ['*'], 'page', $current_page);

        return view('firebase.index', [
            'items' => $dataProvider,
            'backUrl' => \request('back'),
        ]);
    }


}

<?php

namespace App\Http\Controllers\Traits;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

trait MediaAssetTrait
{
    /**
     * @param Request $request
     * @param Model $model
     * @throws \Spatie\MediaLibrary\Exceptions\MediaCannotBeDeleted
     */
    public function addMediaAssets(Request $request, Model $model)
    {
        if ($request->has('image')) {
            if ($model->hasMedia()) {
                $model->deleteMedia($model->getFirstMedia()->id);
            }
            $imageName = md5(microtime()) . '.' . request()->image->getClientOriginalExtension();
            $model->addMediaFromRequest('image')->usingFileName($imageName)/*->withResponsiveImages()*/ ->toMediaCollection();
        }
    }

    /**
     * @param Request $request
     * @param Model $model
     * @throws \Spatie\MediaLibrary\Exceptions\MediaCannotBeDeleted
     */
    public function addMediaAssetsKey($key, Request $request, Model $model)
    {
        if ($request->has($key)) {
            if ($model->hasMedia()) {
                $model->deleteMedia($model->getFirstMedia()->id);
            }
            $imageName = md5(microtime()) . '.' . request()->$key->getClientOriginalExtension();
            $model->addMediaFromRequest($key)->usingFileName($imageName)/*->withResponsiveImages()*/ ->toMediaCollection($key);
        }
    }
}

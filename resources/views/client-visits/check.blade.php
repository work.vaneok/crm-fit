@extends('layouts.dashboard')

@section('title')
    {{ __('Добавить посещение') }}
@endsection

@section('content')
    <div class="content-m-t">
        <div id="check-visit-form">
            <check-visit number="{{ $number }}" staffs='{!! json_encode($staffs) !!}' staff_id="{{$staff_id}}"></check-visit>
        </div>
    </div>
@endsection

@section('actions')

@endsection


@push('scripts')
    <script src="{{ mix('js/check-visit.js') }}"></script>
@endpush



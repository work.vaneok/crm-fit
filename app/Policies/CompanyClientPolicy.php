<?php

namespace App\Policies;

use App\Models\CompanyClient;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyClientPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the company client.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyClient  $companyClient
     * @return mixed
     */
    public function viewHistory(User $user, CompanyClient $companyClient)
    {
        return true;
    }
}

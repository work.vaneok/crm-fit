<div class="row">
    <div class="col-md-12">

        <div id="mdb-lightbox-ui"></div>

        <div class="mdb-lightbox no-margin">

            <?php
            foreach ($photos['images'] as $photo) {
            ?>
            <figure class="col-md-12">
                <a href="<?=$photo?>"
                   data-size="1600x1067">
                    <img alt="picture" src="<?=$photo?>"
                         class="img-fluid">
                </a>
            </figure>
            <?php
            }
            ?>
        </div>
    </div>
</div>

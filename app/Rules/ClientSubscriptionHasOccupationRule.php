<?php

namespace App\Rules;

use App\Models\Client;
use App\Models\ClientSubscription;
use App\Models\Subscription;
use App\Repositories\ConfirmSmsCodeRepository;
use App\Rules\Traits\SetCompanyIdTrait;
use Illuminate\Contracts\Validation\Rule;

/**
 *
 * Проверка на налицие занятий
 *
 * Class ClientSubscriptionHasOccupationRule
 * @package App\Rules
 */
class ClientSubscriptionHasOccupationRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /** @var ClientSubscription $clientSubscription */
        $clientSubscription = ClientSubscription::where(['id' => $value])->first();
        return $clientSubscription && ($clientSubscription->is_unlimited || $clientSubscription->quantity_of_occupation > 0);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Закончились занятия!');
    }
}

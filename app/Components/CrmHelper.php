<?php

namespace App\Components;

use App\Constants\Roles;
use App\Models\Company;
use App\Models\CompanyLocation;
use App\Models\CompanyStaff;
use App\Models\CompanyStaffCompanyLocation;
use Auth;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class CrmHelper
{
    /** @var Company */
    private static $_company;

    /** @var CompanyLocation */
    private static $_companyLocation;

    /** @var CompanyStaffCompanyLocation */
    private static $_companyStaffCompanyLocation;

    /** @var Collection */
    private static $_companySettings;


    public static function getRoleInCompany()
    {
        $company_staff = CompanyStaff::query()->where([
            'company_id' => self::companyId(),
            'user_id' => Auth::id()
        ])->first();
        return $company_staff ? $company_staff->role_in_company : null;
    }

    /**
     * @return integer
     */
    static function companyId()
    {
        if (Auth::check() && !static::$_company) {
            static::$_company = static::company();
        }
        return static::$_company->id;
    }

    /**
     * @return Company
     */
    public static function company()
    {
        if (Auth::check() && !static::$_company) {
            if (Auth::user()->hasRole(Roles::COMPANY_OWNER)) {
                static::$_company = Company::where(['owner_id' => Auth::id()])->first();
            } else if (Auth::user()->hasRole(Roles::COMPANY_STAFF)) {

                $staff = CompanyStaff::where(['user_id' => Auth::id()])->firstOrFail();
                static::$_company = $staff->company;
            }

        }
        return static::$_company;
    }

    /**
     * @return CompanyStaffCompanyLocation
     */
    static function companyStaffCompanyLocations()
    {
        if (Auth::check() && !static::$_companyStaffCompanyLocation) {
            static::$_companyStaffCompanyLocation = CompanyStaffCompanyLocation::where('company_staff_id', (CompanyStaff::where('user_id', auth()->user()->id)->first('id')->id))->get();
        }
        return static::$_companyStaffCompanyLocation;
    }

    /**
     * @return integer
     */
    static function companyLocationId()
    {
        if (Auth::check() && !static::$_companyLocation) {
            static::$_companyLocation = static::companyLocation();
        }
        return static::$_companyLocation ? static::$_companyLocation->id : null;
    }

    /**
     * @return CompanyLocation
     */
    public static function companyLocation()
    {
        if (Auth::check() && !static::$_companyLocation) {
            /** @var CompanyStaff $staff */
            $staff = CompanyStaff::where(['user_id' => Auth::id(), 'company_id' => self::companyId()])->first();
            if (!$staff) {
                return static::$_companyLocation = static::company()->locations()->first();
            }

            if (!$staff->current_location_id) {
                static::$_companyLocation = static::company()->locations()->first();
                if (static::$_companyLocation) {
                    $staff->update(['current_location_id' => static::$_companyLocation->id]);
                }
            } else {
                return static::$_companyLocation = $staff->currentLocation;
            }


        }
        return static::$_companyLocation;
    }

    public static function getSmsProviders(): array
    {


        if (self::$_company) {
            $login = self::settings('sms_turbo:login', null);
            $password = self::settings('sms_turbo:password', null);
            $sender = self::settings('sms_turbo:sender', null);

            if ($login && $password && $sender) {
                $currentCompany = [
                    'turbosms' => [
                        'active' => true,
                        'login' => $login,
                        'password' => $password,
                        'sender' => $sender,
                    ]
                ];
                Log::info($currentCompany);
                return $currentCompany;

            }

        }
        return config('sms.providers');

    }

    public static function settings($key, $default = null)
    {
        if (!static::$_companySettings) {
            static::$_companySettings = static::company()->settings->mapWithKeys(function ($item) {
                return [$item->settings_name => $item->settings_value];
            });

            if (static::company()->adminSettings) {
                static::$_companySettings = static::$_companySettings->toBase()->merge(static::company()->adminSettings->settings);
                static::$_companySettings = static::$_companySettings->merge([
                    'domain' => static::company()->adminSettings->domain
                ]);
            }
        }

        return static::$_companySettings[$key] ?? $default;
    }

    public static function loadCompanyByUuid(?string $company_uuid)
    {
        static::$_company = Company::where(['uuid' => $company_uuid])->first();

        if (!static::$_company) {
            throw new BadRequestHttpException(__('Компания задана неверно'));
        }

        return static::$_company;
    }

    public static function loadCompanyById(string $id)
    {
        static::$_company = Company::where(['id' => $id])->first();

        if (!static::$_company) {
            throw new BadRequestHttpException(__('Компания задана неверно'));
        }

        return static::$_company;
    }
}

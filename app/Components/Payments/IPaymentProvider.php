<?php
namespace App\Components\Payments;


use App\Classes\Tariffs\Models\TariffBill;
use App\Classes\Tariffs\Models\TariffOrder;
use App\Components\Payments\Models\PaymentModel;
use App\Facades\LiqPayFacade;

interface IPaymentProvider
{
    public function getForm(PaymentModel $model);
    public function successPay($request);
}

<?php

namespace App\Components\Sms\Models;

use Illuminate\Database\Eloquent\Model;

class SentSms extends Model
{
    protected $table = 'sent_sms';

    protected $guarded = ['id'];
}

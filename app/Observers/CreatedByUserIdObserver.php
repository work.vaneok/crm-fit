<?php
namespace App\Observers;

class CreatedByUserIdObserver
{
    public function creating($model)
    {
        if(!$model->created_by_user_id){
            $model->created_by_user_id = !is_string(\Auth::id()) && !is_null(\Auth::id()) ? \Auth::id() : 0;
        }
    }
}


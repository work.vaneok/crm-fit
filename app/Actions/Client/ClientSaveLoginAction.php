<?php

namespace App\Actions\Client;

use App\Constants\CacheKeysPrefixes;
use App\Repositories\LoginSmsCodeRepository;

class ClientSaveLoginAction
{
    public function execute($phone, $code)
    {
        app(LoginSmsCodeRepository::class)->save(CacheKeysPrefixes::CLIENT_REGISTER, $phone, $code, 60 * 24 * 30);
    }
}

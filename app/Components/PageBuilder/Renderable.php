<?php
namespace App\Components\PageBuilder;

interface Renderable
{
    public function render();
}

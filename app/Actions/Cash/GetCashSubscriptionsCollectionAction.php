<?php

namespace App\Actions\Cash;

use App\Constants\PaymentSources;
use App\Repositories\OrderRepository;
use App\Transformers\CashSubscriptionsTransformer;

class GetCashSubscriptionsCollectionAction
{
    /**
     * @return array
     */
    public function execute()
    {
        /** @var OrderRepository $orderRep */
        $orderRep = app(OrderRepository::class);

        //$collection = $orderRep->getSubscriptionsForCash(PaymentSources::CASH);
        $collection = $orderRep->getSubscriptionsForCashPerDays(PaymentSources::CASH,0);

        return [
            'items' => $collection->transform(new CashSubscriptionsTransformer()),
            'total_price' => $collection->sum('price')
        ];
    }
}

<?php

namespace App\Http\Requests\Client;


use App\Helper\ValidatorHelper;
use App\Models\CompanyClient;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ClientParametersHistoryExportRequest
 * @package App\Http\Requests\Client
 * @property int $company_id
 * @property int $client_id
 */
class ClientParametersHistoryExportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'client_id' => [
                'required',
                'integer',
                ValidatorHelper::createExists(CompanyClient::TableName, CompanyClient::COLUMN_CLIENT_ID)
            ],

        ];
    }


}

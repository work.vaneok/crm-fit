<?php

namespace App\Forms;


class RefillClientBalanceForm
{
    private $company_id;
    private $client_id;
    private $comment;
    private $need_amount;
    private $created_by;

    public function __construct()
    {

    }

    public function setCreatedBy($created_by_id)
    {
        $this->created_by = $created_by_id;
    }

    public function getCreatedBy()
    {
        return $this->created_by;
    }

    public function setCompanyId(int $company_id)
    {
        $this->company_id = $company_id;
    }

    public function getCompanyId()
    {
        return $this->company_id;
    }

    public function setClientId(int $client_id)
    {
        $this->client_id = $client_id;
    }

    public function getClientId()
    {
        return $this->client_id;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function setNeedAmount($need_amount)
    {
        $this->need_amount = $need_amount;
    }

    public function getNeedAmount()
    {
        return $this->need_amount;
    }

}

<?php
namespace App\DependedGetters;

use App\Components\CrmHelper;
use App\Models\Subscription;
use Spatie\QueryBuilder\QueryBuilder;

class SubscriptionsGetter
{
    static function getList()
    {
        return QueryBuilder::for(Subscription::class)
            ->allowedFilters(['title', 'id'/*, AllowedFilter::scope('category')*/])
            ->where([
                'company_id' => CrmHelper::companyId()
            ])
            ->orderBy('id', 'DESC')
            ->paginate(15);
    }
}

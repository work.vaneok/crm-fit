<?php

namespace App\Http\Requests\Subscriptions;


use App\Http\Requests\Traits\DefaultCompanyLocationIdInRequest;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SubscriptionDetailRequest
 * @package App\Http\Requests\Subscriptions
 * @property  int $subscription_id
 */
class SubscriptionDetailRequest extends FormRequest
{
    use DefaultCompanyLocationIdInRequest;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subscription_id' => ['required']
        ];
    }

}

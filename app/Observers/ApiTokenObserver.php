<?php
namespace App\Observers;

use Illuminate\Support\Str;

class ApiTokenObserver
{
    public function creating($model)
    {
        $model->api_token = Str::random(32);
    }
}

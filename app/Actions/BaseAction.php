<?php

namespace App\Actions;


use Validator;

abstract class BaseAction
{
    protected $validator;

    public const DEFAULT_PAGE = 1;
    public const DEFAULT_PER_PAGE = 15;
    public const DEFAULT_SORT = 'created_at';
    public const DEFAULT_DIRECTION = 'desc';

    public function rules(): array
    {
        return [];
    }

    public function __construct($data)
    {
        $class_vars = get_object_vars($this);
        foreach ($class_vars as $key => $var) {
            if (isset($data[$key])) {
                $this->$key = $data[$key];
            }
        }
        $class_vars = get_object_vars($this);
        $this->validator = Validator::make($class_vars, $this->rules());
    }

    public function validate()
    {
        if ($this->validator->fails()) {
            return $this->validator->errors();
        }
        return null;
    }

    public abstract function action();

    public function execute()
    {
        if ($errors = $this->validate()) {
            echo '<pre>';
            var_dump($errors);
            echo '</pre>';
            die;

            throw new \Exception('Validate Error');
        }
        return $this->action();
    }
}

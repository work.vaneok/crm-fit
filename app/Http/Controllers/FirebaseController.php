<?php

namespace App\Http\Controllers;

use App\Actions\Firebase\FirebaseCreateAction;
use App\Actions\Firebase\FirebaseCreateViewAction;
use App\Actions\Firebase\FirebaseDetailViewAction;
use App\Actions\Firebase\FirebaseIndexAction;
use App\Components\CrmHelper;
use App\Http\Requests\Firebase\FirebaseCreateRequest;
use App\Http\Requests\Firebase\FirebaseCreateViewRequest;
use App\Http\Requests\Firebase\FirebaseDetailViewRequest;
use App\Http\Requests\Firebase\FirebaseIndexRequest;


class FirebaseController extends Controller
{

    public function __construct()
    {

    }

    public function index(FirebaseIndexRequest $request)
    {
        try {
            $action = new FirebaseIndexAction(array_merge($request->all(), [
                'company_id' => CrmHelper::companyId(),
            ]));
            $response = $action->execute();
            return $response;
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage());
            echo '</pre>';
            die;
        }
    }

    public function detailView(FirebaseDetailViewRequest $request)
    {
        try {
            $action = new FirebaseDetailViewAction(array_merge($request->all(), [
                'company_id' => CrmHelper::companyId(),
            ]));
            $response = $action->execute();
            return $this->createSuccessResponse($response);
        } catch (\Exception $exception) {
            return $this->createErrorResponse('Ошибка на сервере');
        }
    }


    public function createView(FirebaseCreateViewRequest $request)
    {
        try {
            $action = new FirebaseCreateViewAction(array_merge($request->all(), [
                'company_id' => CrmHelper::companyId(),
            ]));
            $response = $action->execute();
            return $this->createSuccessResponse($response);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage());
            echo '</pre>';
            die;

            return $this->createErrorResponse('Ошибка на сервере');
        }
    }

    public function create(FirebaseCreateRequest $request)
    {
        try {
            $action = new FirebaseCreateAction(array_merge($request->all(), [

            ]));
            $status = $action->execute();
            if ($status) {
                return $this->createSuccessResponse([
                    'message' => __('Уведомление отправлено')
                ]);
            } else {
                return $this->createErrorResponse('Ошибка на сервере');
            }
        } catch (\Exception $exception) {
            return $this->createErrorResponse('Ошибка на сервере');

        }
    }
}

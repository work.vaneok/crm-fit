<?php

use App\Models\Subscription;
Route::group(['middleware' => 'role:' . \App\Constants\Roles::COMPANY_OWNER.'|'.\App\Constants\Roles::COMPANY_STAFF], function () {
    Route::get('/subscriptions', 'SubscriptionsController@index')->middleware('can:viewAny, ' . Subscription::class)->name('subscriptions.index');
    Route::get('/subscriptions/clients', 'SubscriptionsController@clientSubscriptions')->middleware('can:viewAny, ' . Subscription::class)->name('subscriptions.clients');
    Route::get('/subscriptions/order-detail', 'SubscriptionsController@orderDetail')->middleware('can:viewAny, ' . Subscription::class)->name('subscriptions.order-detail');
    Route::get('/subscriptions/subscription-detail', 'SubscriptionsController@subscriptionDetail')->middleware('can:viewAny, ' . Subscription::class)->name('subscriptions.subscription-detail');
    Route::get('/subscriptions/export', 'SubscriptionsController@export')->middleware('can:viewAny, ' . Subscription::class)->name('subscriptions.export');

    Route::get('/subscriptions/create', 'SubscriptionsController@create')->middleware('can:create, ' . Subscription::class)->name('subscriptions.create');
    Route::get('/subscriptions/edit/{subscription}', 'SubscriptionsController@edit')->middleware('can:update,subscription')->name('subscriptions.edit');
    Route::get('/subscriptions/show/{subscription}', 'SubscriptionsController@show')->middleware('can:view,subscription')->name('subscriptions.show');;

    Route::post('/subscriptions/create', 'SubscriptionsController@store')->middleware('can:create, ' . Subscription::class)->name('subscriptions.store');
    Route::put('/subscriptions/edit/{subscription}', 'SubscriptionsController@update')->middleware('can:update,subscription')->name('subscriptions.update');

    Route::get('/subscriptions/staff-subs', 'SubscriptionsController@actionStaffSubs')->middleware('can:viewAny, ' . Subscription::class)->name('subscriptions.staff-subs');

});

Route::group(['middleware' => 'permission:' . \App\Constants\Permissions::SALE_SUBSCRIPTIONS], function () {
    Route::post('/subscriptions/sale-by-staff', 'SubscriptionsController@saleByStaff')->name('subscriptions.sale-by-staff');
});

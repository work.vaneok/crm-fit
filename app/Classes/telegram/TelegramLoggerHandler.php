<?php

namespace App\Classes\Telegram;

use Illuminate\Support\Facades\App;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

/**
 * Class TelegramHandler
 *
 * @package Rlaurindo\TelegramLogger
 */
class TelegramLoggerHandler extends AbstractProcessingHandler
{

    /**
     * Application name
     *
     * @var string
     */
    private $applicationName;

    /**
     * Application environment
     *
     * @var string
     */
    private $applicationEnvioronment;

    /**
     * Instance of TelegramService
     *
     * @var TelegramService
     */
    private $telegramService;

    /**
     * TelegramHandler constructor.
     *
     * @param string $logLevel
     */
    public function __construct(string $logLevel)
    {
        $monologLevel = Logger::toMonologLevel($logLevel);
        parent::__construct($monologLevel, true);

        $this->applicationName = config('app.name');
        $this->applicationEnvioronment = config('app.env');

        $this->telegramService = new TelegramService(config('telegram-logger.bot_token'), config('telegram-logger.chat_id'));
    }

    public function getIp()
    {
        $keys = [
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'REMOTE_ADDR'
        ];
        foreach ($keys as $key) {
            if (!empty($_SERVER[$key])) {
                $ip = trim(end(explode(',', $_SERVER[$key])));
                if (filter_var($ip, FILTER_VALIDATE_IP)) {
                    return $ip;
                }
            }
        }
        return '---';
    }

    public  function sendNotification($message)
    {

    }

    /**
     * Send log text to Telegram
     *
     * @param array $record
     * @return void
     */
    protected function write(array $record): void
    {
        $this->telegramService->sendMessage($this->formatLogText($record));
    }

    /**
     * Formart log text to send
     *
     * @return string
     * @var array $log
     */
    protected function formatLogText(array $log): string
    {
        $url = null;
        $ip = null;
        if (!App::runningInConsole()) {
            $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $ip = $_SERVER['REMOTE_ADDR'];
        }


        $logText = '<b>Application:</b> ' . $this->applicationName . PHP_EOL;
        $logText .= '<b>Envioronment:</b> ' . $this->applicationEnvioronment . PHP_EOL;
        $logText .= '<b>Log Level:</b> <code>' . $log['level_name'] . '</code>' . PHP_EOL;
        if ($url) {
            $logText .= '<b>URL:</b>' . PHP_EOL . '<pre>' . $url . '</pre>';

        }

        if ($ip) {
            $logText .= '<b>IP:</b>' . PHP_EOL . '<pre>' . $ip . '</pre>';
        }

        if (!empty($log['extra'])) {
            $logText .= '<b>Extra:</b>' . PHP_EOL . '<code>' . json_encode($log['extra']) . '</code>' . PHP_EOL;
        }

        if (!empty($log['context'])) {
            $logText .= '<b>Context:</b>' . PHP_EOL . '<code>' . json_encode($log['context']) . '</code>' . PHP_EOL;
        }

        $logText .= '<b>Message:</b>' . PHP_EOL . '<pre>' . substr($log['message'], 0, 200) . ' ... </pre>';
        $logText .= '<b>$_POST:</b>' . PHP_EOL . '<pre>' . json_encode($_POST) . '</pre>';
        $logText .= '<b>$_GET:</b>' . PHP_EOL . '<pre>' . json_encode($_GET) . '</pre>';
        $logText .= '<b>$_BODY:</b>' . PHP_EOL . '<pre>' . json_encode(file_get_contents('php://input'), true) . '</pre>';

        return $logText;
    }
}

<?php
namespace App\Actions\Company;

use App\Models\Company;

class CreateCompanyAction
{

    /**
     * @param $title
     * @param $available_from
     * @param $available_to
     * @param $owner_id
     * @param $tariff_id
     * @return Company|\Illuminate\Database\Eloquent\Model
     */
    public function execute($title, $description, $available_from, $available_to, $owner_id, $tariff_id)
    {
        return Company::create([
            'title' => $title,
            'description' => $description,
            'available_from' => $available_from,
            'available_to' => $available_to,
            'owner_id' => $owner_id,
            'tariff_id' => $tariff_id,
        ]);
    }
}

<?php

namespace App\Http\Requests\Client;


use App\Helper\ValidatorHelper;
use App\Models\ClientSubscription;
use Illuminate\Foundation\Http\FormRequest;


class ClientUpdateSubscriptionRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'subscription_id' => [
                'required',
                'integer',
                ValidatorHelper::createExists(ClientSubscription::TableName, 'id')
            ],
            'available_from' => [
                'nullable',
                'string',
            ],
            'available_to' => [
                'nullable',
                'string',
            ],
            'quantity_of_occupation' => [
                'nullable',
                'integer',
            ],
        ];
    }

}

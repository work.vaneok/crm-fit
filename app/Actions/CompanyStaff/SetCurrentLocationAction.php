<?php
namespace App\Actions\CompanyStaff;

use App\Actions\Company\CreateCompanyAction;
use App\Actions\User\CreateUserAction;
use App\Actions\User\UserResetPasswordAction;
use App\Constants\CompanyStaffRoles;
use App\Constants\Roles;
use App\Models\CompanyStaff;
use DB;
use Illuminate\Http\Request;
use Log;

class SetCurrentLocationAction
{
    public function execute($user_id, $company_id, $location_id)
    {
        CompanyStaff::where([
            'user_id' => $user_id,
            'company_id' => $company_id
        ])->update([
            'current_location_id' => $location_id
        ]);
    }
}

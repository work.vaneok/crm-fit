@extends('layouts.dashboard')

@section('title')
    {{ __('Страница фото отчетов') }}
@endsection

@section('content')
    <div class="container emp-profile">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">{{ __('Дата') }}</th>
                <th scope="col">{{ __('Действия') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $key => $value)
                <tr>
                    <td>{{$key}}</td>
                    <td>
                        <button type="button" class="btn btn-sm btn-primary photo-history"
                                data-toggle="modal"
                                data-form-url="{{route('clients.profile.photo-history',
                            [
                                'client_id' => $client_id,
                                'company_id' => $company_id,
                                'date' => $key
                            ])}}">
                            {{__('Просмотреть фото')}}
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                                  style="display: none"></span>
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $provider ? $provider->appends(request()->only('filter'))->links() : '' }}

    </div>
    @include('inc.ajax-form-modal.form')

@endsection

@push('scripts')

        <script src="{{ asset('js/clients/profile.js') }}"></script>

@endpush

@section('actions')

@endsection

@push('css')
    {{--
        <link href="{{ asset('css/clients/profile.css') }}" rel="stylesheet"/>
    --}}
@endpush

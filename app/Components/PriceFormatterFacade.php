<?php

namespace App\Components;

use Illuminate\Support\Facades\Facade;

class PriceFormatterFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'price-formatter';
    }

}

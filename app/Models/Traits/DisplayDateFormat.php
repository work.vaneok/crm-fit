<?php
namespace App\Models\Traits;

use Carbon\Carbon;

trait DisplayDateFormat
{
    function formatDateTime($field)
    {
        if (!$this->$field) {
            return '';
        }
        if (is_string($this->$field)) {
            $this->$field = Carbon::createFromFormat('Y-m-d H:i:s', $this->$field);
        }
       // return $this->$field ? $this->$field->addHours(config('app.time_offset'))->format(config('app.dateTimeFormat')) : '';
        return $this->$field ? $this->$field->addHours(config('app.time_offset'))->format(config('app.dateTimeFormat')) : '';
    }

    function formatDate($field)
    {
        if (!$this->$field) {
            return '';
        }
        if (is_string($this->$field)) {
            $this->$field = Carbon::createFromFormat('Y-m-d H:i:s', $this->$field);
        }
        return $this->$field ? $this->$field->addHours(config('app.time_offset'))->format(config('app.dateFormat')) : '';
    }

    function formatDateAsDB($field)
    {
        return $this->$field ? $this->$field->toDateString() : '';
//        return $this->$field->format(config('admin.dateFormat_as_db'));
    }

    function formatDateTimeAsDB($field)
    {
        return $this->$field ? $this->$field->toDateTimeString() : '';
//        return $this->$field->format(config('admin.dateFormat_as_db'));
    }

}

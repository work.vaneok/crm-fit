<?php
namespace App\Components\Sms;

class SmsProviderFactory
{
    public static function create($type, $options = [])
    {
        $class = '\App\Components\Sms\Providers\\'. ucfirst($type) . 'Provider';
        if (class_exists($class)) {
            return new $class($options);
        }

        throw new \Exception($class . ' is not found');
    }
}


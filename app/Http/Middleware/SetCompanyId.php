<?php

namespace App\Http\Middleware;

use App\Components\CrmHelper;
use Closure;

class SetCompanyId
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $company_uuid = $request->headers->get('company');
        CrmHelper::loadCompanyByUuid($company_uuid);

        return $next($request);
    }
}

<?php

namespace App\Providers;

use App\Models\ClientSubscription;
use App\Models\ClientVisit;
use App\Models\Company;
use App\Models\CompanyClient;
use App\Models\CompanyLocation;
use App\Models\CompanyNews;
use App\Models\CompanyStaff;
use App\Models\Subscription;
use App\Policies\ClientSubscriptionPolicy;
use App\Policies\ClientVisitPolicy;
use App\Policies\CompanyClientPolicy;
use App\Policies\CompanyLocationPolicy;
use App\Policies\CompanyNewsPolicy;
use App\Policies\CompanySettingsPolicy;
use App\Policies\CompanyStaffPolicy;
use App\Policies\SubscriptionPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        CompanyLocation::class => CompanyLocationPolicy::class,
        Subscription::class => SubscriptionPolicy::class,
        CompanyStaff::class => CompanyStaffPolicy::class,
        Company::class => CompanySettingsPolicy::class,
        ClientVisit::class => ClientVisitPolicy::class,
        ClientSubscription::class => ClientSubscriptionPolicy::class,
        CompanyClient::class => CompanyClientPolicy::class,
        CompanyNews::class => CompanyNewsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}

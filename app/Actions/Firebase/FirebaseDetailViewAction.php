<?php

namespace App\Actions\Firebase;


use App\Actions\BaseAction;
use App\Models\FirebaseNotification;
use View;

class FirebaseDetailViewAction extends BaseAction
{

    public $id;

    public function rules(): array
    {
        return [
            'id' => ['integer', 'required'],

        ];
    }

    public function action()
    {
        $notification = FirebaseNotification::where(['id' => $this->id])->first();
        $receivers = $notification->getReceiversInfo();

        return
            [
                'title' => __("Детальная информация"),
                'content' => View::make('firebase.detail', [
                        'notification' => $notification,
                        'items' => $receivers
                    ]
                )->render()
            ];
    }
}

<?php
namespace App\Observers;

use Auth;

class CreatedByObserver
{
    public function creating($model)
    {
        $guard_name = '';
        if (Auth::guard('admin')->check()) {
            $guard_name = "admin";
        } elseif (Auth::guard('company')->check()) {
            $guard_name = "company";
        } elseif (Auth::guard('clients_api')->check()) {
            $guard_name = "clients_api";
        } else {
            return;
        }
        /*
        elseif (Auth::guard('client')->check()) {
            $guard_name = "client";
        } else {
            $guard_name = "no";
        }*/

        $model->created_by = $guard_name . ':' . \Auth::guard($guard_name)->id();
    }
}


<div class="input-group select">
    <select name="filter[{{ $name }}]" id="filter-{{ $name }}" placeholder="{{ $label }}" class="form-control">
        <option value="" >{{ __('Выбрать') }} {{ $label }}</option>
        @foreach($options as $id => $value)
            <option value="{{ $id }}" @if(!is_null(get_filter_value($name)) && $id == get_filter_value($name)) selected @endif >{{ $value }}</option>
        @endforeach
    </select>
</div>


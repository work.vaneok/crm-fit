@extends('layouts.dashboard')

@section('title')
    {{ __('Новость') }}
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                {{ __('Наименование') }}
            </dt>
            <dd class="col-sm-10">
                {{$news->title}}
            </dd>
        </dl>


    </div>
@endsection

@section('actions')
    @if (Auth::user()->can('update', $news))
        <li>
            @edit(['route' => route('company-news.edit',['news' => $news->id, 'backUrl' => url()->previous()])])&nbsp;
        </li>
{{--    @delete(['route' => route('locations.destroy',['location' => $location->id])])--}}
    @endif
@endsection

<?php
namespace App\Http\Requests\Client;

use App\Rules\PhoneFormatRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'birthday' => ['nullable', 'date_format:Y-m-d'],
            'sex' => ['required', Rule::in([1, 2])],
            'email' => ['nullable', 'email'],
        ];
    }

}

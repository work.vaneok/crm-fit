<?php

namespace App\Rules;

use App\Models\CompanyStaff;
use Illuminate\Contracts\Validation\Rule;

class UniqueStaffPhoneRule implements Rule
{
    private $company_id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($company_id)
    {

        $this->company_id = $company_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return 0 == CompanyStaff::/*where(['company_id' => $this->company_id])
            ->*/whereHas('user', function ($q) use ($value) {
                $q->where(['phone' => $value]);
            })
            ->count();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Сотрудник с таким номером телефона недоступен для добавление в Вашу компанию');
    }
}

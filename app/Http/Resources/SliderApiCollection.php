<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SliderApiCollection extends ResourceCollection
{
    public $collects = SliderApiResource::class;

}

@extends('layouts.dashboard')

@section('title')
    {{ __('Создание абонемента') }}
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->multipart()->route('subscriptions.store')->post() !!}
        {!! Form::text('title', 'Наименование')->required() !!}
        {!! Form::text('available_from', 'Доступен с',\Carbon\Carbon::now()->format('d.m.Y'))->placeholder(config('app.form_date_mask'))->help(__('Дату вводите в формате',['?'=>config('app.form_date_mask')]))->required() !!}
        {!! Form::text('available_to', 'Доступен по',\Carbon\Carbon::now()->addYear()->format('d.m.Y'))->placeholder(config('app.form_date_mask'))->help(__('Дату вводите в формате',['?'=>config('app.form_date_mask')]))->required() !!}
       {{-- {!! Form::date('available_from', 'Доступен с', \Carbon\Carbon::now()->toDateString()) !!}
        {!! Form::date('available_to', 'Доступен по', \Carbon\Carbon::now()->addYear()->toDateString()) !!}--}}
        {!! Form::file('image', __('Изображение')) !!}
        {!! Form::text('price', __('Цена'))->type('numeric')->required() !!}
        {!! Form::text('old_price', __('Старая цена'))->type('numeric') !!}
        {!! Form::select('validity', __('Срок действия'))->options($validity_values)->required() !!}
        {!! Form::select('available_to_before_begin_of_use', __('Период аннуляции'))->options($cancellation_value)->required() !!}
        {!! Form::text('quantity_of_occupation', __('Кол-во занятий'))->help(__('Оставьте пустым, если безлим'))->type('numeric') !!}



        {{--{!! Form::checkbox('can_be_selected_by_staff', __('Доступен сотрудникам')) !!}
        {!! Form::checkbox('can_share', __('Возможно делиться')) !!}
        {!! Form::checkbox('has_trainer', __('Тренер включен')) !!}
        {!! Form::checkbox('has_nutritionist', __('Диетолог включен')) !!}
        {!! Form::checkbox('is_available_in_mobile_app', __('Доступен в мобильном приложении')) !!}
        {!! Form::checkbox('can_buy_online', __('Доступен для покупки online')) !!}

        <br>
        {!! Form::checkbox('is_active', __('Активный'), 1) !!}--}}

        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit(__('Сохранить изменения')) !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection


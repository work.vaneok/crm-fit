<?php
namespace App\Components\Sms\Events;

class SmsErrorSendEvent
{
    private $msg;
    private $smsInternalId;

    public function __construct($smsInternalId, $msg)
    {
        $this->msg = $msg;
        $this->smsInternalId = $smsInternalId;
    }

    /**
     * @return mixed
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @return mixed
     */
    public function getSmsInternalId()
    {
        return $this->smsInternalId;
    }

}

<?php
namespace App\Constants;

class OrderStatuses
{
    const CREATED = 0;
    const OK = 1;
    const ERROR = 2;
    const DECLINED = 3;
}

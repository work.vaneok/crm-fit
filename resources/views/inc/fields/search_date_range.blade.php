@include('_partials.datepicker')

<div class="input-group input-daterange" data-provide="datepicker">
    <input name="filter[{{$name}}_from]" type="text" class="form-control  " value="{{ get_filter_value($name . '_from') }}" placeholder="{{ __('Дата от') }}">
    <input name="filter[{{$name}}_to]" type="text" class="form-control " value="{{ get_filter_value($name . '_to') }}" placeholder="{{ __('Дата до') }}">
</div>

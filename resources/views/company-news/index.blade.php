@extends('layouts.dashboard')

@section('title')
    {{ __('Новости компании') }}
@endsection

@section('content')


    <div class="header__filter" style=" margin-bottom: 15px;">
        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#filter"
                style="margin-left: 0;">{{ __('Фильтр') }}</button>
        <div id="filter" class="collapse col-lg-12" style="margin-top: 15px; padding-left: 0px;">
            <form action="" class="grid-filter" method="get" id="filterForm">
                @php($errMessages = [])
                @error('filter.date')
                @php($errMessages[] = $message)
                @enderror
                @error('filter.date_from')
                @php($errMessages[] = $message)
                @enderror
                @error('filter.date_to')
                @php($errMessages[] = $message)
                @enderror

                @if(count($errMessages))
                    <div class="alert alert-danger">{!! implode('<br>', $errMessages) !!}</div>
                @endif
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="filter[title]">{{ __('Наименование') }}</label>
                        <input class="form-control " type="text" value="{{get_filter_value('title')}}" name="filter[title]" placeholder="{{ __('Имя клиента') }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary "><i
                                class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                    </div>
                    <div class="form-group">
                        <a href="/{{ Route::current()->uri }}" class="btn btn-danger "><i
                                class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="table-responsive">
        <table id="grid-table" class="table table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">{{ __('Фото') }}</th>
                <th scope="col">{{ __('Наименование') }}</th>
                <th scope="col">{{ __('Дата создания') }}</th>
                <th scope="col" style="width: 25%">{{ __('Действия') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($news as $item)
                <tr url="{{route('company-news.show',['news' => $item->id])}}">
                    <td>@image_media(['model' => $item])</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->formatDate('created_at') }}</td>
                    <td>
                        @edit(['route' => route('company-news.edit',['news' => $item->id, 'backUrl' => url()->full()])])&nbsp;
                        @delete(['route' => route('company-news.destroy',['news' => $item->id])])
                        {{-- @button(['route' => route('company-news.notification',['news' => $item->id, 'backUrl' => url()->full()]),'label'=>'Уведомление','iclass'=>'fa fa-bell'])--}}
                        <button type="button" class="btn btn-sm btn-primary notification-create" data-toggle="modal"
                                data-form-url="{{route('firebase.create-view',[
    'title' => $item->title,
    'descriptions' => $item->description,
    'image_url' =>$item->getFirstMedia()?$item->getFirstMedia()->getFullUrl('thumb'):null])}}">
                            <i class="fa fa-bell"></i>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                                  style="display: none"></span>
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $news ? $news->appends(request()->only('filter'))->links() : '' }}
    </div>
    @include('inc.ajax-form-modal.form')

@endsection

@section('actions')
    @if (Auth::user()->can('create', \App\Models\CompanyNews::class))
        {{--<div class="btn-toolbar mb-2 mb-md-0">--}}
        <li>
            @add(['route' => route('company-news.create')])

        </li>
        {{--</div>--}}
    @endif
@endsection
@push('scripts')
    <script src="{{ asset('js/company-news/index.js') }}"></script>
@endpush
@push('css')

@endpush

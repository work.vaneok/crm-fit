<?php
namespace App\Http\Controllers\Api\V1;

use App\Actions\Subscription\MakeOrderAction;
use App\Components\Payments\Models\PaymentModel;
use App\Components\Payments\PaymentProviderFactory;
use App\Constants\OrderItemTypes;
use App\Constants\PaymentSources;
use App\Http\Requests\Api\BuySubscriptionRequest;
use App\Repositories\Api\PaymentFormRepository;
use Illuminate\Routing\Controller;

class BuySubscriptionController extends Controller
{
    public function index(BuySubscriptionRequest $request)
    {
        $order = (new MakeOrderAction())->execute(
            OrderItemTypes::SUBSCRIPTION,
            $request->getCompanyId(),
            $request->getLocationId(),
            $request->user()->client_id,
            $request->getSubscription()->price,
            $request->get('subscription_id'),
            $request->getSubscription()->toArray(),
            PaymentSources::CARD
        );

        $paymentModel = new PaymentModel($order->company->title, $order->price, 'UAH', $request->getSubscription()->title, $order->id, route('buy-subscription.return-url'), route('buy-subscription.return-url'));

        $view = view('pay.redirect_form', ['form' => PaymentProviderFactory::load(config('payments.provider'))->getForm($paymentModel)])->render();

        /** @var PaymentFormRepository $payRep */
        $payRep = app(PaymentFormRepository::class);
        $payRep->save($order->id, $view);

        return response()->json(['url' => route('buy-subscription.form', ['order' => $order->id])]);
    }
}

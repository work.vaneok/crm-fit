<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyStaffCompanyLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_staff_company_locations', function (Blueprint $table) {
            $table->unsignedBigInteger('company_staff_id');
            $table->unsignedBigInteger('company_location_id');
            $table->foreign(['company_staff_id'])->on('company_staffs')->references('id');
            $table->foreign(['company_location_id'])->on('company_locations')->references('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_staff_company_locations', function (Blueprint $table) {
            $table->dropForeign(['company_staff_id']);
            $table->dropForeign(['company_location_id']);
            $table->drop();
        });
    }
}

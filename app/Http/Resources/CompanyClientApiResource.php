<?php

namespace App\Http\Resources;

use App\Components\Chat\ChatComponent;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyClientApiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** var CompanyClient $this */
        return [
            'first_name' => $this->client->first_name,
            'last_name' => $this->client->last_name,
            'birthday' => $this->client->formatDate('birthday'),
            'email' => $this->client->email,
            'token' => $this->getToken(),
            'phone' => $this->client->phone,
            'barcode' => (string)$this->barcode,
            'sex' => $this->client->sex,
            'balance' => $this->client->balance,
            'active_subscriptions' => new ActiveSubscriptionsApiCollection($this->client->activeSubscriptions),
            'chat_room_id' => ChatComponent::generateRoomIdForGlobalChat($this->company_id, $this->client_id),
            'chat_your_id' => ChatComponent::generateCompanyClientId($this->client_id)
        ];
    }
}

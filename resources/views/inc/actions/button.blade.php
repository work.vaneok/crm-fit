<a href="{{$route}}" class="btn btn-sm  {{ $btn ?? 'btn-primary' }} "><i class="{{$iclass??''}}" aria-hidden="true"></i> {{__($label)}}</a>

@extends('layouts.dashboard')

@section('title')
    {{ __('Проданные абонементы') }}
@endsection

@section('content')

    <div class="header__filter" style=" margin-bottom: 15px;">
        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#filter"
                style="margin-left: 0;">{{ __('Фильтр') }}</button>
        <div id="filter" class="collapse col-lg-12" style="margin-top: 15px; padding-left: 0px;">
            <form action="" class="grid-filter" method="get" id="filterForm">
                @php($errMessages = [])
                @error('filter.date')
                @php($errMessages[] = $message)
                @enderror
                @error('filter.date_from')
                @php($errMessages[] = $message)
                @enderror
                @error('filter.date_to')
                @php($errMessages[] = $message)
                @enderror

                @if(count($errMessages))
                    <div class="alert alert-danger">{!! implode('<br>', $errMessages) !!}</div>
                @endif
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="filter[title]">{{ __('Статус') }}</label>
                        @search_select(['name' => 'status', 'options' => $statuses, 'label' => __('Статус')])
                    </div>
                    <div class="form-group col-md-2">
                        <label for="filter[username]">{{ __('Имя клиента') }}</label>
                        <input class="form-control " type="text" value="{{get_filter_value('username')}}"
                               name="filter[username]" placeholder="{{ __('Имя клиента') }}">
                    </div>

                    <div class="form-group col-md-2">
                        <label for="filter[phone]">{{  __('Телефон') }}</label>
                        <input class="form-control " type="text" value="{{get_filter_value('phone')}}"
                               name="filter[phone]" placeholder="{{ __('Телефон') }}">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="filter[phone]">{{ __('Абонемент') }}</label>
                        @search_select(['name' => 'subscription', 'options' => $subscriptions, 'label' =>
                        __('Абонемент')])
                    </div>
                    <div class="form-group col-md-2">
                        <label for="filter[phone]">{{ __('по Дате') }}</label>
                        @if($filters['date_range'])
                            @search_date_range(['name' => 'date'])
                        @else
                            @search_date(['value' => (get_filter_value('date') ??
                            \Carbon\Carbon::now()->format('d.m.Y')), 'name' => 'date'])
                        @endif
                    </div>

                </div>
                <div class="form-row">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary "><i
                                class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                    </div>
                    <div class="form-group">
                        <a href="/{{ Route::current()->uri }}" class="btn btn-danger "><i
                                class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>



    {{--<form id="filterForm" action="" class="grid-filter" method="get" >
        @php($errMessages = [])
        @error('filter.date')
            @php($errMessages[] = $message)
        @enderror
        @error('filter.date_from')
            @php($errMessages[] = $message)
        @enderror
        @error('filter.date_to')
            @php($errMessages[] = $message)
        @enderror

        @if(count($errMessages))
            <div class="alert alert-danger">{!! implode('<br>', $errMessages) !!}</div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                @search_select(['name' => 'status', 'options' => $statuses, 'label' => __('Статус')])
            </div>
            <div class="col-auto">
                <input class="form-control form-control-sm" type="text" value="{{get_filter_value('username')}}" name="filter[username]" placeholder="{{ __('Имя клиента') }}">
            </div>

            <div class="col-auto">
                <input class="form-control form-control-sm" type="text" value="{{get_filter_value('phone')}}" name="filter[phone]" placeholder="{{ __('Номер телефона') }}">
            </div>

            <div class="col-auto">
                @search_select(['name' => 'subscription', 'options' => $subscriptions, 'label' => __('Абонемент')])
            </div>

            <div class="col-auto">
                @if($filters['date_range'])
                    @search_date_range(['name' => 'date'])
                @else
                    @search_date(['value' => (get_filter_value('date') ?? \Carbon\Carbon::now()->format('d.m.Y')), 'name' => 'date'])
                @endif
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                <a href="/{{ Route::current()->uri }}" class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
            </div>
        </div>
    </form>--}}

    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">{{ __('Клиент') }}</th>
                <th scope="col">{{ __('Дата') }}</th>
                <th scope="col">{{ __('Абонемент') }}</th>
                <th scope="col">{{ __('Оплата') }}</th>
                <th scope="col">{{ __('Сумма') }}</th>
                <th scope="col">{{ __('Продавец') }}</th>
                <th scope="col">{{ __('Статус') }}</th>
                <th scope="col">{{ __('Действия') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>
                        {{ $item['client_name'] }} /
                        {{ $item['client_phone'] }}
                    </td>
                    <td>
                        {{ $item['created_at'] }}
                    </td>
                    <td>
                        {{ $item['title'] }}
                    </td>
                    <td>
                        {{ $item['pay_source'] }}
                    </td>
                    <td>
                        {{ $item['price_formatted'] }}
                    </td>
                    <td>
                        {{ $item['soldBy'] }}
                    </td>
                    <td>
                        {{ $item['statusText'] }}
                    </td>
                    <td>
                        @if ($item['status'] == \App\Constants\OrderStatuses::OK)
                            @grid_btn(['route' => route('client-visit.history', ['companyClient' => $item['client_id'],
                            'filter[client_subscription]' => $item['client_subscription_id']]),'label' => __('История
                            списания'), 'class' => 'history'])
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4"></td>
                <td colspan="">{{ $totals['price'] }}</td>
                <td colspan="3"></td>
            </tr>
            </tfoot>
        </table>
        {{ $items ? $items->appends(request()->only('filter'))->links() : '' }}

    </div>
@endsection

@section('actions')


@endsection

@push('css')

@endpush

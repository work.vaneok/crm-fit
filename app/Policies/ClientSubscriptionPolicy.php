<?php

namespace App\Policies;

use App\Components\CrmHelper;
use App\Constants\CompanySettings;
use App\Constants\Permissions;
use App\Models\ClientSubscription;
use App\Models\ClientVisit;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientSubscriptionPolicy
{
    use HandlesAuthorization;

    public function viewHistory(User $user, ClientSubscription $clientSubscription)
    {
        if ($clientSubscription->company_id != CrmHelper::companyId()) {
            return false;
        }

        return true;
    }
}

<?php

namespace App\Providers;

use App\Http\Middleware\ForceJsonResponse;
use App\Models\CompanyAdminSettings;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $namespaceApi = 'App\Http\Controllers\Api\V1';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/dashboard';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapCommonRoutes();
        $this->mapApiRoutes();

        $this->mapCompanyRoutes();

        //
    }

    /**
     * Define the "company" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapCompanyRoutes()
    {
        Route::namespace($this->namespace)
            ->middleware(['company', 'guest'])
            ->group(base_path('routes/site.php'));

        Route::middleware('company')
             ->namespace($this->namespace)
             ->group(base_path('routes/company.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api/v1')
             ->middleware(['clients_api'])
             ->namespace($this->namespaceApi)
             ->group(base_path('routes/api/api_v1.php'));
    }

    private function mapCommonRoutes()
    {
        Route::middleware([SubstituteBindings::class])
            ->namespace($this->namespace)
            ->group(base_path('routes/common.php'));
    }
}

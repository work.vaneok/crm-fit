<?php

namespace App\Providers;

use App\Components\Sms\SmsEventSubscriber;
use App\Events\ClientVisitCreatedEvent;
use App\Events\ClientVisitReturnedEvent;
use App\Events\CompanyStaffFiredEvent;
use App\Listeners\LogCreatedVisitListener;
use App\Listeners\LogReturnVisitListener;
use App\Listeners\ResetVisitInClientSubscription;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ClientVisitCreatedEvent::class => [
            LogCreatedVisitListener::class,
        ],
        ClientVisitReturnedEvent::class => [
            ResetVisitInClientSubscription::class,
            LogReturnVisitListener::class,
        ],
        CompanyStaffFiredEvent::class => [

        ]
    ];

    protected $subscribe = [
        SmsEventSubscriber::class,
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

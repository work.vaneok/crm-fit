<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FirebaseNotification
 * @package App\Models
 * @property int $id
 * @property int $company_id
 * @property string $title
 * @property string $descriptions
 * @property string $image_url
 * @property int $topic_id
 * @property string $created_at
 * @property int $updated_at
 * @property string $data
 */
class FirebaseNotification extends Model
{
    const TableName = 'firebase_notifications';
    protected $table = self::TableName;
    protected $guarded = ['id'];
    const COLUMN_ID = 'id';
    const COLUMN_COMPANY_ID = 'company_id';
    const COLUMN_TITLE = 'title';
    const COLUMN_DESCRIPTIONS = 'descriptions';
    const COLUMN_IMAGE_URL = 'image_url';
    const COLUMN_TOPIC_ID = 'topic_id';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';
    const COLUMN_DATA = 'data';

    public function receivers()
    {
        return $this->hasMany(FirebaseNotificationReceiver::class, 'notification_id', 'id')->with(['client', 'device']);
    }

    public function getReceiversInfo()
    {
        $response = [];
        foreach ($this->receivers as $receiver) {
            $response[$receiver->id]['client']['first_name'] = $receiver->client->first_name;
            $response[$receiver->id]['client']['last_name'] = $receiver->client->last_name;
            $response[$receiver->id]['client']['full_name'] = $receiver->client->first_name . ' ' . $receiver->client->last_name;
            $response[$receiver->id]['status'] = $receiver->status;
            $response[$receiver->id]['data'] = $receiver->data;
            $response[$receiver->id]['id'] = $receiver->id;
            $response[$receiver->id]['device_os'] = $receiver->device->device_os;
            $response[$receiver->id]['device_os_version'] = $receiver->device->device_os_version;
        }
        return $response;
    }


}

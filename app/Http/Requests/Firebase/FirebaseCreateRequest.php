<?php

namespace App\Http\Requests\Firebase;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FirebaseIndexRequest
 * @package App\Http\Requests\Auth
 */
class FirebaseCreateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'company_id' => ['integer', 'required'],
            'title' => ['string', 'required'],
            'type' => ['string', 'required'],
            'descriptions' => ['string', 'nullable'],
            'image_url' => ['string', 'nullable'],
        ];
    }


}

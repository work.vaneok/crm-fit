<?php
namespace App\Repositories;

use Cache;
use Carbon\Carbon;

/**
 * ConfirmSmsCodeRepository
 *
 * Class ConfirmSmsCodeRepository
 * @package App\Repositories
 */
class ConfirmSmsCodeRepository
{

    public function save($phone, $code)
    {
        // Сохранить код для подтверждение по смс
        Cache::set('client_confirm:' . $phone, $code, Carbon::now()->addMinutes(3));
    }

    public function get($phone)
    {
        return Cache::get('client_confirm:' . $phone, null);
    }

}

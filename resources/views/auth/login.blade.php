@extends('layouts.app')
@include('_partials.company-login')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Login') }}</div>

                    <div class="card-body">
                        {!! Form::open()->route('login')->post() !!}

                        {!! Form::text('phone', __('Номер телефона'))->help(__('Формат: 380123456789')) !!}

                        {!! Form::text('password', __('Пароль'))->type('password')->wrapperAttrs(['class' => 'submit-row d-none-']) !!}

                        <div class="form-group row submit-row d-none-">
                            <div class="col-sm-4 pull-right">
                                <button type="button" class="btn reset-password" data-toggle="modal"
                                        data-form-url="{{route('login.reset-password-view')}}">
                                    {{ __('Забыл пароль') }}
                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                                          style="display: none"></span>
                                </button>
                            </div>

                            <div class="offset-2 col-sm-4 pull-left">
                                {!! Form::submit(__('Войти')) !!}
                            </div>
                        </div>
                        <div class="form-group row sms-submit-row">
                            <div class="offset-2 col-sm-10">

                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('inc.ajax-form-modal.form')

@endsection
@push('js')
    <script src="{{ asset('js/auth/auth-page.js') }}"></script>
@endpush

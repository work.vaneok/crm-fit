<?php

namespace App\Http\Controllers\Actions\Clients;

use App\Http\Controllers\Actions\BaseAction;
use App\Models\CompanyClientParameterHistory;


class ClientParameterHistoryAction extends BaseAction
{
    public $company_id;
    public $client_id;


    public $per_page = 15;
    public $page = 1;

    public $viewParams;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'client_id' => ['integer', 'required'],
            'per_page' => ['integer'],
            'page' => ['integer'],
        ];
    }

    public function action()
    {

        $provider = CompanyClientParameterHistory::query()->where([
            'company_id' => $this->company_id,
            'client_id' => $this->client_id
        ])->orderBy('id', 'desc');

        $provider = $provider->paginate($this->per_page, ['*'], 'page', $this->page);

        $items = [];
        $counter = 0;
        /** @var CompanyClientParameterHistory $item */
        foreach ($provider->items() as $item) {
            $items[$counter][CompanyClientParameterHistory::C_DATE] = $item->formatDate(CompanyClientParameterHistory::C_DATE);
            $items[$counter][CompanyClientParameterHistory::C_WEIGHT] = $item[CompanyClientParameterHistory::C_WEIGHT];
            $items[$counter][CompanyClientParameterHistory::C_SHOULDER_VOLUME] = $item[CompanyClientParameterHistory::C_SHOULDER_VOLUME];
            $items[$counter][CompanyClientParameterHistory::C_WAIST] = $item[CompanyClientParameterHistory::C_WAIST];
            $items[$counter][CompanyClientParameterHistory::C_HIPS] = $item[CompanyClientParameterHistory::C_HIPS];
            $items[$counter][CompanyClientParameterHistory::C_BICEPS_VOLUME] = $item[CompanyClientParameterHistory::C_BICEPS_VOLUME];
            $items[$counter][CompanyClientParameterHistory::C_BREAST_VOLUME] = $item[CompanyClientParameterHistory::C_BREAST_VOLUME];
            $items[$counter][CompanyClientParameterHistory::C_LEG_VOLUME] = $item[CompanyClientParameterHistory::C_LEG_VOLUME];
            $items[$counter][CompanyClientParameterHistory::C_CAVIAR_VOLUME] = $item[CompanyClientParameterHistory::C_CAVIAR_VOLUME];
            $items[$counter][CompanyClientParameterHistory::C_COMMENT] = $item[CompanyClientParameterHistory::C_COMMENT];
            $items[$counter][CompanyClientParameterHistory::C_ID] = $item[CompanyClientParameterHistory::C_ID];
            $counter++;
        }

        $this->viewParams['items'] = $items;
        $this->viewParams['provider'] = $provider;
    }
}

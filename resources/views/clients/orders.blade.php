<div class="table-responsive">
    <table id="grid-table" class="table  table-hover table-striped">
        <thead>
        <tr>
            <th scope="col">{{ __('Действителен c') }}</th>
            <th scope="col">{{ __('Действителен до') }}</th>
            <th scope="col">{{ __('Абонемент') }}</th>
            <th scope="col">{{ __('Оплата') }}</th>
            <th scope="col">{{ __('Сумма') }}</th>
            <th scope="col">{{ __('Продавец') }}</th>
            <th scope="col">{{ __('Статус') }}</th>
            <th scope="col">{{ __('Действия') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr>
                <td>
                    {{ $item['available_from'] }}
                </td>
                <td>
                    {{ $item['available_to'] }}
                </td>
                <td>
                    {{ $item['title'] .' #'.$item['client_name'] }}
                </td>
                <td>
                    {{ $item['pay_source'] }}
                </td>
                <td>
                    {{ $item['price_formatted'] }}
                </td>
                <td>
                    {{ $item['soldBy'] }}
                </td>
                <td>
                    {{ $item['statusText'] }}
                </td>
                <td>
                    @if ($item['status'] == \App\Constants\OrderStatuses::OK)
                        @grid_btn(['route' => route('client-visit.history', ['companyClient' => $item['client_id'],
                        'filter[client_subscription]' => $item['client_subscription_id']]),'label' => __('История
                        списания'), 'class' => 'history'])
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
       {{-- <tfoot>
        <tr>
            <td colspan="4"></td>
            <td colspan="">{{ $totals['price'] }}</td>
            <td colspan="3"></td>
        </tr>
        </tfoot>--}}
    </table>
</div>

<nav class="sidebar">
    <a class="sidebar__header" href="{{route('home')}}">
        <?php
        if ($companyImage && $companyImageBack) {
            echo '<div  class="company-logo-back" style="background-image: url(' . asset($companyImageBack) . ')"><img src="' . asset($companyImage) . '" class="company-logo"></div>';
        }
        ?>

    </a>
    <ul class="list-unstyled components">
        @sidebar_title(['text' => 'Меню'])

        @if(\App\Components\CrmHelper::getRoleInCompany() == \App\Constants\CompanyStaffRoles::OWNER || \App\Components\CrmHelper::getRoleInCompany() == \App\Constants\CompanyStaffRoles::ADMINISTRATOR )
            @sidebar_item(['url' => route('home'), 'text' => 'Рабочий стол', 'icon' => 'icon-cash'])
            @sidebar_item(['url' => route('subscriptions.index'), 'text' => 'Абонементы', 'icon' => 'icon-subscriptions'])
            @sidebar_item(['url' => route('sold-subscriptions.index'), 'text' => 'Продажи', 'icon' => 'icon-wallet'])
            @sidebar_item(['url' => route('subscriptions.clients'), 'text' => 'Проданные Абонементы', 'icon' => 'icon-wallet'])
            @sidebar_item(['url' => route('clients.index'), 'text' => 'Клиенты', 'icon' => 'icon-clients'])
            @sidebar_item(['url' => route('company-news.index'), 'text' => 'Публикации', 'icon' => 'icon-news'])
            @sidebar_item(['url' => route('firebase.index'), 'text' => 'Уведомления', 'icon' => 'icon-news'])
        @endif

        @if(\App\Components\CrmHelper::getRoleInCompany() == \App\Constants\CompanyStaffRoles::NUTRITIONIST || \App\Components\CrmHelper::getRoleInCompany() == \App\Constants\CompanyStaffRoles::TRAINER )
            @sidebar_item(['url' => route('home'), 'text' => 'Рабочий стол', 'icon' => 'icon-cash'])
            @sidebar_item(['url' => route('subscriptions.staff-subs'), 'text' => 'Мои клиенты', 'icon' => 'icon-news'])
        @endif
        @sidebar_item(['url' => route('client-visit.index'), 'text' => 'Посещения', 'icon' => 'icon-gym'])

        @role(\App\Constants\Roles::COMPANY_OWNER)
        @sidebar_item(['url' => route('staffs.index'), 'text' => 'Сотрудники', 'icon' => 'icon-staffs'])
        @sidebar_item(['url' => route('company-settings.index'), 'text' => 'Настройки <br>компании', 'icon' =>
        'icon-settings'])
        @endrole

    </ul>

    <div class="footer">
        <div class="footer__logo">
            <img src="{{asset('images/abonement_logo.png')}}" alt="">
        </div>
        <div class="footer__text">Copyright © 2010-2020 Abonement</div>
    </div>
</nav>

@extends('layouts.dashboard')

@section('title')
    {{ __('Создание сотрудника') }}
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->multipart()->route('staffs.store')->post() !!}
        {!! Form::text('first_name', 'Имя')->required() !!}
        {!! Form::text('last_name', 'Фамилия')->required() !!}
        {!! Form::text('phone', 'Номер телефона')->help(__('Формат: 380123456789'))->required() !!}
        {!! Form::date('birthday', 'Дата рождения') !!}
        {!! Form::select('role_in_company', 'Роль в компании')->options($roles) !!}
        {!! Form::text('trainer_price', 'Стоимость услуг')->type('number') !!}
        {!! Form::select('locations_id[]', 'Доступные локации', $locations)->multiple()->attrs(['style' => 'height:128px'])
            ->help('Для отмены выбора определённого элемента, либо выбора нескольких одновременно, щёлкните по ним, предварительно зажав клавишу CTRL') !!}

        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit(__('Сохранить изменения')) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection

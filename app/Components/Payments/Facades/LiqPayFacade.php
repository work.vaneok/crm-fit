<?php
namespace App\Components\Payments\Facades;

use Illuminate\Support\Facades\Facade;

class LiqPayFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'LiqPay';
    }
}

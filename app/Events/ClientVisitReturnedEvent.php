<?php
namespace App\Events;

class ClientVisitReturnedEvent
{
    public $visit_id;

    public function __construct($visit_id)
    {
        $this->visit_id = $visit_id;
    }

}

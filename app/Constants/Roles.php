<?php

namespace App\Constants;

use App\Models\CompanyStaff;

class Roles
{
    const COMPANY_OWNER = 'CompanyOwner';
    const COMPANY_STAFF = 'CompanyStaff';


}

@if(isset($fields))
    <form action="" class="grid-filter" method="get">
        <div class="form-row">

            @foreach($fields as $field)
                <div class="col-auto">
                    <input class="form-control form-control-sm" type="text" value="{{get_filter_value($field['name'])}}"
                           name="filter[{{ $field['name'] }}]" placeholder="{{ $field['label'] }}">
                </div>
            @endforeach


            <div class="col-auto">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                <a href="/{{ Route::current()->uri }}" class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
            </div>

        </div>
    </form>
@endif

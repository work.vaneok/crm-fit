<?php

namespace App\Repositories\DependedOnRole;

use App\Components\Globals\ValidityValues;
use App\Models\Client;
use App\Models\Company;
use App\Models\CompanySetting;
use App\Models\Subscription;
use PriceFormatter;

/**
 * Абонементы для владельца компании
 *
 * Class SubscriptionsCompanyOwnerRepository.php
 * @package App\Repositories
 */
class SubscriptionsCompanyOwnerRepository extends SubscriptionsRepository
{
    /**
     * @inheritDoc
     */
    protected function listForSaleByAdmin()
    {
        return Subscription::byCompany($this->company->id)->forOwner()->active()->with(['media'])->orderBy('price')->get();
    }
}

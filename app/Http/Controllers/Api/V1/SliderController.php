<?php

namespace App\Http\Controllers\Api\V1;

use App\DependedGetters\CompanyNewsGetter;
use App\DependedGetters\MobileSliderGetter;
use App\Http\Controllers\Controller;
use App\Http\Resources\NewsApiCollection;
use App\Http\Resources\SliderApiCollection;

class SliderController extends Controller
{
    /**
     * @apiGroup SliderGroup
     * @apiVersion 0.1.0
     * @apiName SliderRequest
     * @apiUse RequestHeader
     *
     * @api {get} /slider Элементы слайдера
     *
     * @apiSuccess {String} data.id ID
     * @apiSuccess {String} data.title Заголовок
     * @apiSuccess {String} data.description Описание
     * @apiSuccess {String} data.image Изображение
     * @apiSuccess {Array} links Ссылки для постраничной навигации
     * @apiSuccess {Array} meta Информация о текущей странице
     *
     * @apiSuccessExample {json} Успешный ответ
     *     HTTP/1.1 200 OK
     *     {
     *          "data": [{
     *              "id": 1,
     *              "title": "Заголовок",
     *              "created_at": "06.02.2020",
     *              "description": "Описание",
     *              "image": "<URL>"
     *          }],
     *          "links": {
     *              "first": "http://<domain>/api/v1/slider?page=1",
     *              "last": "http://<domain>/api/v1/slider?page=1",
     *              "prev": null,
     *              "next": null
     *          },
     *          "meta": {
     *              "current_page": 1,
     *              "from": 1,
     *              "last_page": 1,
     *              "path": "http://<domain>/api/v1/slider",
     *              "per_page": 15,
     *              "to": 1,
     *              "total": 1
     *          }
     *      }
     *
     * @apiUse UnauthorizedError
     *
     */
    public function index()
    {
        return new NewsApiCollection(MobileSliderGetter::getList());
    }

}

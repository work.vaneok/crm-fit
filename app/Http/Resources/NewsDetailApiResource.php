<?php

namespace App\Http\Resources;

use App\Components\Globals\ValidityValues;
use App\Components\PriceFormatter;
use App\Models\Client;
use App\Models\ClientSubscription;
use App\Models\CompanyNews;
use Illuminate\Http\Resources\Json\JsonResource;

class NewsDetailApiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this CompanyNews */
        return [
            'id' => $this->id,
            'created_at' => $this->formatDate('created_at'),
            'title' => $this->title,
            'description' => $this->description,
            'content' => $this->content,
            'image' => $this->getFirstMedia()->getFullUrl('thumb'),
        ];
    }
}

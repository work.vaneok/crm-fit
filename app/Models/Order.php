<?php
namespace App\Models;

use App\Constants\OrderItemTypes;
use App\Models\Traits\DisplayDateFormat;
use App\Models\Traits\RequestFiltersScopes;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use DisplayDateFormat, RequestFiltersScopes;

    protected $table = 'orders';

    protected $guarded = ['id'];

    protected $casts = [
        'order_item_info' => 'array'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function location()
    {
        return $this->belongsTo(CompanyLocation::class, 'company_location_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function soldBy()
    {
        return $this->belongsTo(User::class, 'created_by_user_id', 'id');
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class, 'order_item_id', 'id');
    }

    public function isPayed()
    {
        return !empty($this->payed_at);
    }

    public function scopePayed(Builder $q)
    {
        $q->whereNotNull('payed_at');
    }

    public function scopeSubscriptions(Builder $q)
    {
        $q->where('order_item_type', OrderItemTypes::SUBSCRIPTION);
    }

    public function scopePaymentSource(Builder $q, $pay_source)
    {
        $q->where('payment_source', $pay_source);
    }
}

<?php

namespace App\Http\Controllers\Actions;

use App\Models\CompanyClientParameterHistory;
use App\Models\CompanyClientPhotoHistory;
use Carbon\Carbon;

class ClientSavePhotoHistoryAction extends BaseAction
{
    public $company_id;
    public $client_id;
    public $date;


    public $response;


    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'client_id' => ['integer', 'required'],
            'date' => ['string', 'required'],
        ];
    }

    public function action()
    {
        $model = new CompanyClientPhotoHistory([
            CompanyClientParameterHistory::C_CLIENT_ID => $this->client_id,
            CompanyClientParameterHistory::C_COMPANY_ID => $this->company_id,
            CompanyClientParameterHistory::C_DATE => Carbon::parse($this->date)->toDateTimeString(),
        ]);
        $model->save();

        if (\Request::has('images')) {
            $images = \Request::file('images');
            foreach ($images as $image) {
                $model->addMedia($image)->toMediaCollection();
            }
        }

        $this->response = [
            'model_id' => $model->id
        ];

    }

}

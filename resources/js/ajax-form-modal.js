class AjaxFormModal {
    constructor() {
        console.log('AjaxFormModal');
        this.init();
    }

    init() {
        $('.open-modal-form').on('click', function () {
            var formUrl = $(this).attr('data-form-url');
            $.ajax({
                    type: 'get',
                    url: formUrl,
                }
            ).done(function (data) {
                if (data.SERVER_RESPONSE == 'OK') {

                }
                if (data.success) {
                    if (typeof successCallback == 'function') {
                        successCallback();
                    }
                    self.formModal.modal('hide');
                } else if (data.validation) {
                    // server validation failed
                    $yiiform.yiiActiveForm('updateMessages', data.validation, true); // renders validation messages at appropriate places
                } else {
                    // incorrect server response
                }
            }).fail(function (data) {
                console.log('BAD');
                console.log(data);
            });
        })
    }
}

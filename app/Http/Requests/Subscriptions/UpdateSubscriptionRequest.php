<?php
namespace App\Http\Requests\Subscriptions;

use App\Repositories\SubscriptionCancellationRepository;
use App\Repositories\ValidityRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validity_values = app(ValidityRepository::class)->getForSelectBox()->keys();
        $available_to_before_begin_of_use_values = app(SubscriptionCancellationRepository::class)->getForSelectBox()->keys();
        return [
            'title' => ['required'],
            'price' => ['required', 'numeric'],
            'old_price' => ['nullable', 'numeric', 'gt:price'],
            'quantity_of_occupation' => ['nullable', 'numeric'],
            'validity' => ['required', Rule::in($validity_values),],
            'image' => ['image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
            'can_share' => ['boolean'],
            'can_be_selected_by_staff' => ['boolean'],
            'has_trainer' => ['boolean'],
            'has_nutritionist' => ['boolean'],
            'is_available_in_mobile_app' => ['boolean'],
            'is_active' => ['boolean'],
            'can_buy_online' => ['boolean'],
            'available_to_before_begin_of_use' => ['required', Rule::in($available_to_before_begin_of_use_values)],
            'available_from' => ['required', 'date_format:Y-m-d'],
            'available_to' => ['required', 'date_format:Y-m-d', 'after_or_equal:available_from'],
        ];
    }

}

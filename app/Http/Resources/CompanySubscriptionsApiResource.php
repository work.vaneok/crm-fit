<?php

namespace App\Http\Resources;

use App\Models\Subscription;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanySubscriptionsApiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Subscription $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'price' => $this->price,
            'price_old' => $this->price_old,
            'quantity_of_occupation' => $this->quantity_of_occupation,
            'can_buy_online' => $this->can_buy_online,
            'image' => $this->getFirstMedia() ? $this->getFirstMedia()->getFullUrl('thumb') : '',
        ];
    }
}

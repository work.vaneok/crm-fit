<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClientSubcriptionAddDeleteAtColumn extends Migration
{
    private $table = 'client_subscriptions';

    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->softDeletes('deleted_at')->comment('Deleted At');
        });
    }


    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }
}

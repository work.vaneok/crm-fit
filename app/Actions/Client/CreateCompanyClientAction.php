<?php
namespace App\Actions\Client;

use App\Models\Client;
use App\Models\CompanyClient;
use Illuminate\Support\Str;

class CreateCompanyClientAction
{
    /**
     * @param $data
     * @return Client|\Illuminate\Database\Eloquent\Model
     */
    public function execute($data)
    {
        /** @var Client $client */
        if (isset($data['phone'])) {
            $client = Client::firstWhere('phone', $data['phone']);
        } else if (isset($data['client_id'])) {
            $client = Client::firstWhere('id', $data['client_id']);
        }
        if (!$client) {
            $client = (new CreateClientAction())->execute($data);
        }

        $client->companies()->syncWithoutDetaching([
            $data['company_id'] => [
                'created_by_user_id' => \Auth::id(),
                'barcode' => generateBarcode($data['company_id'], $client->id),
                'api_token' => Str::random(),
            ]
        ]);

        return $client;
    }
}

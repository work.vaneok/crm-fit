@extends('layouts.dashboard')

@section('title')
    {{ __('Сотрудники') }}
@endsection

@section('content')

    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">{{ __('Имя') }}</th>
                <th scope="col">{{ __('Телефон') }}</th>
                <th scope="col">{{ __('Роль в компании') }}</th>
                <th scope="col">{{ __('Действия') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($staffs as $item)
                <tr url="{{route('staffs.show',['staff' => $item->id])}}">
                    <td>{{ $item->username }}</td>
                    <td>{{ $item->phone }}</td>
                    <td>{{ isset($roles[$item->role_in_company])?$roles[$item->role_in_company]:'' }}</td>
                    <td>
                        @can('fireEmployee', $item)
                            @fire_employee(['route' => route('staffs.fire', ['staff' => $item->id])])
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $staffs ? $staffs->appends(request()->only('filter'))->links() : '' }}

    </div>
@endsection

@section('actions')
    @if (Auth::user()->can('create', \App\Models\CompanyStaff::class))
        {{--<div class="btn-toolbar mb-2 mb-md-0">--}}
        <li>
            @add(['route' => route('staffs.create')])

        </li>
        {{--</div>--}}
    @endif
@endsection

@push('css')

@endpush

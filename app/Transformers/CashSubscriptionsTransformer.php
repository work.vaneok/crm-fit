<?php
/**
 * Created by PhpStorm.
 * User: Lucky
 * Date: 11.03.2020
 * Time: 12:42
 */

namespace App\Transformers;


use App\Components\Globals\ValidityValues;
use Illuminate\Support\Collection;

class CashSubscriptionsTransformer
{
    public function __invoke($item)
    {
        $item->validity = ValidityValues::getByKey($item->validity);
        return $item;

    }
}
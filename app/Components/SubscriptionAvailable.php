<?php

namespace App\Components;

use Carbon\Carbon;

class SubscriptionAvailable
{
    static function calculate($validity, Carbon $date_from)
    {
        if ($validity == '1') { // 1 день
            return [$date_from->startOfDay(), $date_from->clone()->endOfDay()];
        }

        if ($validity == '2') { // 1 месяц
            return [$date_from->startOfDay(), $date_from->clone()->addMonth()->subDay()->endOfDay()];
        }

        if ($validity == '3') { // 1 год
            return [$date_from->startOfDay(), $date_from->clone()->addYear()->subDay()->startOfDay()];
        }

        if ($validity == '4') { // 3 месяца
            return [$date_from->startOfDay(), $date_from->clone()->addMonths(3)->subDay()->startOfDay()];
        }

        if ($validity == '5') { // 6 месяца
            return [$date_from->startOfDay(), $date_from->clone()->addMonths(6)->subDay()->startOfDay()];
        }
    }
}

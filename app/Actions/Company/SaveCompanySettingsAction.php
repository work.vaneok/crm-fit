<?php
namespace App\Actions\Company;

use App\Models\Company;
use App\Repositories\CompanySettingsRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Collection;

class SaveCompanySettingsAction
{
    /**
     * @var Company
     */
    private $company;
    /** @var CompanySettingsRepository  */
    private $companySettingsRep;

    public function __construct(Company $company)
    {
        $this->company = $company;
        $this->companySettingsRep = app(CompanySettingsRepository::class);
    }


    public function execute($settingsRaw)
    {
        $settings = collect($settingsRaw)->only($this->companySettingsRep->getListForForm()->keys()->all());

        $settings = $settings->map(function ($item, $key) {
            return [
                'company_id' => $this->company->id,
                'settings_name' => $key,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'settings_value' => $item ?? '',
            ];
        })->values()->all();

        DB::transaction(function () use ($settings) {
            $this->company->settings()->delete();
            $this->company->settings()->insert($settings);
        });
    }
}

<?php

namespace App\Http\Requests\Client;


use App\Helper\ValidatorHelper;
use App\Models\ClientSubscription;
use Illuminate\Foundation\Http\FormRequest;


class ClientUpdateSubscriptionViewRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'subscription_id' => [
                'required',
                'integer',
                ValidatorHelper::createExists(ClientSubscription::TableName, 'id')
            ],
        ];
    }

}

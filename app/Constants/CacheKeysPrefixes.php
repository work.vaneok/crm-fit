<?php
namespace App\Constants;

class CacheKeysPrefixes
{
    const CLIENT_LOGIN = 'client_login';
    const CLIENT_REGISTER = 'client_register';
}

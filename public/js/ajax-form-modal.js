const MODAL_WINDOW_TYPE_VIEW = 'type-view';
const MODAL_WINDOW_TYPE_SUBMIT = 'type-submit';

class AjaxButton {

    constructor(btn) {
        this.btn = btn;
        this.EventBeforeClick = null;
        this.EventAfterClick = null;
        this.EventAfterSubmit = null;
        this.EventBeforeSubmit = null;
        this.typeModal = MODAL_WINDOW_TYPE_SUBMIT;
    }

    setTypeView() {
        this.typeModal = MODAL_WINDOW_TYPE_VIEW;
        return this;
    }

    setTypeSubmit() {
        this.typeModal = MODAL_WINDOW_TYPE_SUBMIT;
        return this;
    }

    setEventBeforeClick(EventBeforeClick) {
        this.EventBeforeClick = EventBeforeClick;
        return this;
    }

    setEventAfterClick(EventAfterClick) {
        this.EventAfterClick = EventAfterClick;
        return this;
    }

    setEventAfterSubmit(EventAfterSubmit) {
        this.EventAfterSubmit = EventAfterSubmit;
        return this;
    }

    setEventBeforeSubmit(EventBeforeSubmit) {
        this.EventBeforeSubmit = EventBeforeSubmit;
        return this;
    }
}

class AjaxFormModal {

    constructor() {
        var modalForm = '#modalForm';
        var modalFormTitle = '#modalFormTitle';
        var modalFormBody = '#modalFormBody';
        var modalConfirm = '#modalConfirm';
        var modalError = '#modalError';
        var modalFormSubmit = '#modalFormSubmit';
        var modalFormClose = '#modalFormClose';
        this.bindDomElements(modalForm, modalFormTitle, modalFormBody, modalConfirm, modalError, modalFormSubmit, modalFormClose);
        this.ajaxButtons = [];
    }

    addOpenButton(ajaxButton) {
        this.ajaxButtons.push(ajaxButton);
    }


    clearModal() {
        this.modalFormBody.html('');
        this.modalFormTitle.html('');
        this.modalForm.find('.type-submit').addClass('hidden');
        this.modalForm.find('.type-view').addClass('hidden');
    }

    buildForm(btn) {

        if (btn.typeModal === MODAL_WINDOW_TYPE_SUBMIT) {
            console.log('111');
            this.modalForm.find('.type-submit').removeClass('hidden');
        }

        if (btn.typeModal === MODAL_WINDOW_TYPE_VIEW) {
            console.log('222');
            this.modalForm.find('.type-view').removeClass('hidden');
        }
    }

    bindAjaxButton(btn) {
        self = this;

        this.modalFormSubmit.addClass(btn.btn + '-submit');

        $("body").on('click', btn.btn, function () {
            if (btn.EventBeforeClick) {
                btn.EventBeforeClick();
            }
            var button = this;
            self.spinnerOn(button);
            self.clearModal();
            var formUrl = $(this).attr('data-form-url');
            $.ajax({
                    type: 'get',
                    url: formUrl,
                }
            ).done(function (data) {
                self.spinnerOff(button);
                if (!self.isset(data.response)) {
                    self.modalForm.modal('hide');
                    self.error();
                    return;
                }
                if (data.response === 'OK') {
                    self.buildForm(btn);
                    self.modalFormBody.html(data.content);
                    self.modalFormTitle.html(data.title);
                    self.modalForm.modal('show');
                    if (btn.EventAfterClick) {
                        btn.EventAfterClick();
                    }
                } else {
                    self.error();
                }

            }).fail(function (data) {
                self.spinnerOff(button);
                self.error();
            });
        });
    }

    build() {

        var self = this;

        this.ajaxButtons.forEach(function (item, index, array) {
            self.bindAjaxButton(item);
        });

        this.modalFormSubmit.on('click', function (e) {
            var button = this;
            self.clearForm();
            e.preventDefault();
            var data;
            var currentForm = self.modalForm.find('form');
            var action = currentForm.attr('action');
            data = currentForm.serializeArray();
            self.spinnerOn(button);

            $.ajax({
                    type: 'post',
                    url: action,
                    data: data,
                }
            ).done(function (response) {
                self.spinnerOff(button);
                if (!self.isset(response.response)) {
                    self.modalForm.modal('hide');
                    self.error();
                    return;
                }

                if (response.response !== 'OK') {
                    self.modalForm.modal('hide');
                    self.error();
                    return;
                }

                var message = (self.isset(response.message) ? response.message : null);

                self.modalForm.modal('hide');
                self.confirm(message);

            }).fail(function (reject) {
                self.spinnerOff(button);
                var response = $.parseJSON(reject.responseText);
                //Ошибка валидации
                if (reject.status === 422) {
                    $.each(response.errors, function (key, val) {
                        var input = currentForm.find('input[name ="' + key + '"]');
                        if (input) {
                            input.addClass('is-invalid');
                            input.after(self.errorTemplate(val[0]));
                        }
                    });
                    return;
                }
                self.modalForm.modal('hide');

                if (!self.isset(response.response) || !self.isset(response.error) || !self.isset(response.error.message)) {
                    self.error();
                    return;
                }
                self.error(response.error.message);
            });
        });

        this.modalConfirm.on('hidden.bs.modal', function () {
            location.reload();
        });

    }

    isset(variable) {
        return typeof (variable) != "undefined" && variable !== null
    }


    clearForm() {
        this.modalForm.find('.form-error').remove();
        this.modalForm.find('input').removeClass('is-invalid');
    }

    errorTemplate(error) {
        return '<span class="invalid-feedback form-error" role="alert"><strong>' + error + '</strong> </span>';
    }

    bindDomElements(modalForm, modalFormTitle, modalFormBody, modalConfirm, modalError, modalFormSubmit, modalFormClose) {
        this.modalForm = $(modalForm);
        this.modalFormTitle = $(modalFormTitle);
        this.modalFormBody = $(modalFormBody);
        this.modalConfirm = $(modalConfirm);
        this.modalError = $(modalError);
        this.modalFormSubmit = $(modalFormSubmit);
        this.modalFormClose = $(modalFormClose);
    }

    confirm(title = 'Успех', desc = null, func = null) {
        this.modalConfirm.find('.modal-title').html(title);
        if (desc != null) {
            this.modalConfirm.find('.modal-body .text-center').html(desc);
        }
        this.modalConfirm.modal('show');
    }

    error(title = 'Ошибка', desc = null) {
        this.modalError.find('.modal-body .title').html(title);
        if (desc != null) {
            this.modalError.find('.modal-body .description').html(desc);
        }
        this.modalError.modal('show');
    }

    spinnerOn(self) {
        $(self).find('.spinner-border').show();
        $(self).prop("disabled", true);
    }

    spinnerOff(self) {
        $(self).find('.spinner-border').hide();
        $(self).prop("disabled", false);
    }
}

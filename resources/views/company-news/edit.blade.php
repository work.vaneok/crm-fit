@extends('layouts.dashboard')

@section('title')
    {{ __('Редактирование новости') }}
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->multipart()->route('company-news.update', ['news' => $news->id])->put()->fill($news) !!}

        {!! Form::text('title', __('Наименование'))->required() !!}
        {!! Form::textarea('description', __('Анонс')) !!}
        {!! Form::textarea('content', __('Текст новости'))->required() !!}
        {!! Form::file('image', __('Изображение')) !!}
        @image_media(['model' => $news, 'form' => true])
        <br>
        <div class="form-group row">
            <div class="col-sm-2"></div>
            <div class="col-sm-6">
                <div class="form-check">
                    <input type="checkbox" name="is_show_in_mobile_slider" id="inp-is_show_in_mobile_slider" {{$news['is_show_in_mobile_slider']?'checked':''}} class="form-check-input">
                    <label for="inp-is_show_in_mobile_slider" class="form-check-label">
                        {{__('Показывать в слайдере мобильного приложения')}}
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit(__('Сохранить изменения')) !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection

@push('scripts')
    @include('inc.ckeditor')
@endpush


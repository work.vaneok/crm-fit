<?php
namespace App\Actions\ClientVisit;

use App\Components\SubscriptionAvailable;
use App\Constants\StartOfActivity;
use App\Events\ClientVisitCreatedEvent;
use App\Models\ClientVisit;
use Carbon\Carbon;
use DB;

class CreateClientVisitAction
{
    /**
     * @param $data
     * @return ClientVisit|null
     * @throws \Throwable
     */
    public function execute($data)
    {
//        $data['company_id'];
//        $data['company_location_id'];
//        $data['client_id'];
//        $data['client_subscription_id'];

        $visit = null;
        DB::transaction(function () use ($data, &$visit) {
            /** @var ClientVisit $visit */
            $visit = ClientVisit::create([
                'client_subscription_id' => $data['client_subscription_id'],
                'client_id' => $data['client_id'],
                'company_id' => $data['company_id'],
                'company_location_id' => $data['company_location_id'],
                'key_number' => $data['key_number'] ?? null,
                'created_by_user_id' => $data['staff_id']
            ]);

            if (!$visit) {
                throw new \Exception('Visit was not created');
            }

            if ($visit->clientSubscription->start_of_activity == StartOfActivity::FROM_FIRST_VISIT && empty($visit->clientSubscription->available_from)) {
                [$available_from, $available_to] = SubscriptionAvailable::calculate($visit->clientSubscription->validity, Carbon::now());
                $visit->clientSubscription->update(['available_from' => $available_from, 'available_to' => $available_to]);
            }

            // Выполнить вручную списание посещение, если не безлим
            if (!$visit->clientSubscription->is_unlimited) {
                $visit->clientSubscription->doReduce();
            }

        });

        event(new ClientVisitCreatedEvent($visit->id));

        return $visit;
    }
}

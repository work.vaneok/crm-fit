<?php

namespace App\Components\PageBuilder;

use App\Components\PageBuilder\Types\BaseContent;
use Str;

class PageContentFactory
{
    /**
     * @param string $role
     * @return BaseContent
     */
    static function load(string $role)
    {
        $roleName = ucfirst(Str::camel($role));

        $contentClass = __NAMESPACE__ . '\\types\\' . $roleName . 'Content';
        if (class_exists($contentClass)) {
            return (new $contentClass());
        }

        $contentClass = __NAMESPACE__ . '\\Types\\DefaultContent';
        return (new $contentClass());
    }
}

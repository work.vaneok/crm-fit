<?php
namespace App\Constants;

class PaymentSources
{
    const CASH = 1;
    const CARD = 2;
}

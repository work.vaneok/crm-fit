<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['guest',]
], function (\Illuminate\Routing\Router $router) {
    $router->get('/policy', 'CompanyController@policy');

    $router->post('/sms/request', 'LoginController@sms')->middleware(['throttle:5,1']);
    $router->post('/login', 'LoginController@login');
    $router->post('/register', 'LoginController@register');

    $router->get('/subscriptions', 'CompanyController@subscriptions')->name('api.subscriptions');
    $router->get('/locations', 'CompanyController@locations');

    $router->get('/news', 'NewsController@index')->name('api.news');
    $router->get('/news/{news}', 'NewsController@show');

    $router->get('/slider', 'SliderController@index')->name('api.slider');
    $router->post('/version', 'LoginController@checkVersion');

});

Route::group(['middleware' => 'auth:clients_api'], function (\Illuminate\Routing\Router $router) {

    $router->get('/init', 'InitController');

    $router->get('/me', 'ClientController@me');
    $router->get('/my-notifications', 'ClientController@myNotifications');
    $router->get('/my-payments', 'ClientController@myPayments');
    $router->put('/me', 'ClientController@update');

    $router->get('/my/active-subscriptions', 'ClientController@activeSubscriptions');
    $router->get('/my/visits', 'ClientController@visits')->name('api.my.visits');

    $router->post('/buy-subscription', 'BuySubscriptionController@index');

    $router->post('parameters', 'ClientController@saveParameterHistory')->name('clients.save-parameters-history');
    $router->get('parameters', 'ClientController@parameterHistory')->name('clients.save-parameters');

    $router->post('photo-history', 'ClientController@savePhotoHistory')->name('clients.save-photo-history');
    $router->get('photo-history', 'ClientController@photoHistory')->name('clients.photo-history');


    /* CHAT */
    Route::group(['prefix' => 'chat'], function () {
        Route::post('save-message', 'ChatController@actionSaveMessage')->name('chat.save-message');


    });
});

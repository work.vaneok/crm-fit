<?php

namespace App\Repositories;


use App\Models\Company;
use App\Models\CompanyClientBuy;


/**
 * Class CompanyClientBuysRepository
 * @package App\Repositories
 */
class CompanyClientBuysRepository
{
    /**
     * @var Company
     */
    private $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    public function getBuysCompanyClient($client_id, $company_id)
    {
        $buys = CompanyClientBuy::where([
            'company_id' => $company_id,
            'client_id' => $client_id
        ])->with(['createTransaction', 'paidTransaction'])->orderBy('id', 'DESC')->get();
        return $buys;
    }

}

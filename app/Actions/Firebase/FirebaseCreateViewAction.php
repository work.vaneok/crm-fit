<?php

namespace App\Actions\Firebase;


use App\Actions\BaseAction;
use App\Models\Client;
use App\Models\ClientDevice;
use App\Models\CompanyClient;
use View;

class FirebaseCreateViewAction extends BaseAction
{

    public $company_id;
    public $title;
    public $descriptions;
    public $image_url;
    public $topic_id;
    public $type;
    const NOTIFICATION_TYPE_SEND_ALL = 'type_send_all';
    const NOTIFICATION_TYPE_SEND_ONE = 'type_send_one';

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'topic_id' => ['integer', 'nullable'],
            'title' => ['string', 'nullable'],
            'descriptions' => ['string', 'nullable'],
            'image_url' => ['string', 'nullable'],
            'type' => ['nullable', 'string'],
        ];
    }

    private function types()
    {
        return [
            self::NOTIFICATION_TYPE_SEND_ALL, self::NOTIFICATION_TYPE_SEND_ONE
        ];
    }

    private function validateType($type)
    {
        return in_array($type, $this->types());
    }

    private function getListClientsByCompanyId($company_id)
    {
        $response = [];
        $members = ClientDevice::where(['company_id' => $company_id])->pluck('client_id')->toArray();
        $clients = Client::whereIn('id', $members)->select(['id', 'first_name', 'last_name', 'phone'])->get();
        foreach ($clients as $client) {
            $response[$client->id] = $client->first_name . ' ' . $client->last_name . '(' . $client->phone . ')';
        }
        return $response;
    }

    public function action()
    {
        $type = self::NOTIFICATION_TYPE_SEND_ALL;
        if ($this->type && $this->validateType($this->type)) {
            $type = $this->type;
        }

        $title = 'TITLE NOT SET';
        $clients = [];
        if ($type == self::NOTIFICATION_TYPE_SEND_ONE) {
            $title = __('Отправка уведомления одному клиенту');
            $clients = $this->getListClientsByCompanyId($this->company_id);
        } else if ($type == self::NOTIFICATION_TYPE_SEND_ALL) {
            $title = __('Отправка уведомления всем клиентам');
        }

        return
            [
                'title' => $title,
                'content' => View::make('firebase.create', [
                        'company_id' => $this->company_id,
                        'title' => $this->title ?? '',
                        'description' => $this->descriptions ?? '',
                        'image_url' => $this->image_url ?? '',
                        'type' => $type,
                        'clients' => $clients
                    ]
                )->render()
            ];
    }
}

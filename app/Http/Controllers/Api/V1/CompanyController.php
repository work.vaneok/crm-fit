<?php
namespace App\Http\Controllers\Api\V1;

use App\Components\CrmHelper;
use App\Constants\CompanySettings;
use App\Http\Resources\CompanyLocationsApiCollection;
use App\Http\Resources\CompanySubscriptionsApiCollection;
use App\Repositories\CompanySubscriptions;

class CompanyController
{
    /**
     * @apiGroup CompanyGroup
     * @apiVersion 0.1.0
     * @apiName CompanyPolicyRequest
     * @apiUse RequestHeader
     *
     * @api {get} /policy Политика
     *
     * @apiSuccessExample {json} Успешный ответ
     *     HTTP/1.1 200 OK
     *     {
     *          "data": {
     *              "link": "<link>"
     *          }
     *      }
     *
     */

    public function policy()
    {
        return ['data' => ['link' => CrmHelper::settings(CompanySettings::POLICY_LINK, '')]];
    }

    /**
     * @apiGroup CompanyGroup
     * @apiVersion 0.1.0
     * @apiName CompanySubscriptionsRequest
     * @apiUse RequestHeader
     *
     * @api {get} /subscriptions Список абонементов
     *
     * @apiSuccess {String} data.id ID
     * @apiSuccess {String} data.title Наименование
     * @apiSuccess {Integer} data.price Актуальная цена
     * @apiSuccess {Integer} data.price_old Старая цена
     * @apiSuccess {Integer} data.quantity_of_occupation Количество посещений. -1 значит безлим
     * @apiSuccess {String} data.image Изображение
     *
     * @apiSuccessExample {json} Успешный ответ
     *     HTTP/1.1 200 OK
     *     {
     *          "data": [{
     *              "id": 1,
     *              "title": "тест",
     *              "price": 11,
     *              "price_old": null,
     *              "quantity_of_occupation": 15,
     *              "image": "http://fitness.org/storage/custom-files/3/98c/c/98c0f4e8b83599414e79d3be9312bccf-thumb.jpg"
     *          }]
     *      }
     *
     * @apiUse UnauthorizedError
     *
     */
    public function subscriptions()
    {
        return new CompanySubscriptionsApiCollection(app(CompanySubscriptions::class)->listForMobile());
    }


    /**
     * @apiGroup CompanyGroup
     * @apiVersion 0.1.0
     * @apiName CompanyLocationsRequest
     * @apiUse RequestHeader
     *
     * @api {get} /locations Список локаций
     *
     * @apiSuccess {Integer} data.id ID
     * @apiSuccess {String} data.title Наименование
     * @apiSuccess {String} data.description Описание
     * @apiSuccess {String} data.email Email
     * @apiSuccess {String} data.image Изображение
     * @apiSuccess {String} data.address Адрес
     * @apiSuccess {Array} data.phones Номера телефонов
     *
     * @apiSuccessExample {json} Успешный ответ
     *     HTTP/1.1 200 OK
     *     {
     *          "data":
     *              "locations": [{
     *                    "id": 1,
     *                    "title": "Наименование",
     *                    "description": "Описание",
     *                    "email": null,
     *                    "image": "http://fitness.org/storage/custom-files/3/858/c/8587503c787a1c689fc0c12e363cb9f1-thumb.jpg",
     *                    "address": "addresss",
     *                    "phones": [
     *                        "122",
     *                        "233",
     *                        "344"
     *                    ]
     *                }],
     *              "social": {
     *                   "fb":"<link>",
     *                   "insta":"<link>",
     *                   "telegram":"<link>",
     *                   "viber":"<link>",
     *                   "vk":"<link>",
     *              }
     *      }
     *
     * @apiUse UnauthorizedError
     *
     */
    public function locations()
    {
        return [
            'locations' => new CompanyLocationsApiCollection(CrmHelper::company()->locations),
            'social' => [
                'fb' => CrmHelper::settings('social:fb', ''),
                'insta' => CrmHelper::settings('social:instagram', ''),
                'telegram' => CrmHelper::settings('social:telergam', ''),
                'viber' => CrmHelper::settings('social:viber', ''),
                'vk' => CrmHelper::settings('social:vk', ''),
            ],
        ];
    }
}

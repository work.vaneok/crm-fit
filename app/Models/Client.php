<?php

namespace App\Models;

use App\Models\Traits\ByCompanyScopeTrait;
use App\Models\Traits\DisplayDateFormat;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use ByCompanyScopeTrait, DisplayDateFormat;
    const TableName = 'clients';

    protected $table = 'clients';
    protected $guarded = ['id'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'birthday' => 'date'
    ];

    public function getUserName($short = true)
    {
        if ($short) {
            return $this->first_name . ' ' . mb_substr($this->last_name, 0, 1) . '.';
        }

        return $this->first_name . ' ' . $this->last_name;
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class, 'company_clients', 'client_id', 'company_id')->withTimestamps();
    }

    public function activeSubscriptions()
    {
        return $this->hasMany(ClientSubscription::class, 'client_id', 'id')->with(['subscription' => function ($q) {
            // $q->withTrashed();
        }]);
    }

    public function activeSubscriptionsV1()
    {

        $carbon = Carbon::now()->toDateTimeString();
        return $this->activeSubscriptions()
            ->where('quantity_of_occupation', '<>', 0)
            ->where('deleted_at', null)
            ->where(function ($query) use ($carbon) {
                $query->where('available_to', '=', null)
                    ->orWhere('available_to', '>', $carbon);
            });

    }
}

<?php

namespace App\Models;

use App\Models\Interfaces\CompanyClientPhotoHistoryInterface;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class CompanyClientPhotoHistory
 * @package App\Models
 */
class CompanyClientPhotoHistory extends Model implements CompanyClientPhotoHistoryInterface, HasMedia
{
    use HasMediaTrait;
    protected $table = self::TABLE_NAME;
    protected $guarded = ['id'];
}

<?php

namespace App\Models;

use App\Helper\CompanyClientBuyStatusHelper;
use App\Models\Traits\ByCompanyScopeTrait;
use App\Models\Traits\DisplayDateFormat;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CompanyClientBuy
 * @package App\Models
 * @property int $id
 * @property int $company_id
 * @property int $client_id
 * @property string $comment
 * @property string $current_amount
 * @property string $need_amount
 * @property int $create_transaction_id
 * @property int $paid_transaction_id
 * @property string $status_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class CompanyClientBuy extends Model
{
    use  DisplayDateFormat;

    const TableName = 'company_client_buys';
    protected $table = self::TableName;
    const COLUMN_ID = 'id';
    const COLUMN_COMPANY_ID = 'company_id';
    const COLUMN_CLIENT_ID = 'client_id';
    const COLUMN_COMMENT = 'comment';
    const COLUMN_CURRENT_AMOUNT = 'current_amount';
    const COLUMN_NEED_AMOUNT = 'need_amount';
    const COLUMN_CREATE_TRANSACTION_ID = 'create_transaction_id';
    const COLUMN_PAID_TRANSACTION_ID = 'paid_transaction_id';
    const COLUMN_STATUS_ID = 'status_id';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';
    const COLUMN_DELETED_AT = 'deleted_at';
    const COLUMN_TYPE_ID = 'type_id';

    protected $guarded = ['id'];

    public function getOperationName()
    {
        return CompanyClientBuyStatusHelper::getTypesNameByID($this->type_id);
    }

    public function needPay()
    {
        return ($this->type_id == CompanyClientBuyStatusHelper::TYPE_BUY_TYPE_ID && $this->status_id != CompanyClientBuyStatusHelper::STATUS_COMPLETE_ID);
    }

    public function getStatusName()
    {
        return CompanyClientBuyStatusHelper::getStatusNameByID($this->status_id);
    }

    public function createTransaction()
    {
        return $this->belongsTo(Transaction::class, 'create_transaction_id', 'id')->with(['user']);
    }

    public function paidTransaction()
    {
        return $this->belongsTo(Transaction::class, 'paid_transaction_id', 'id')->with(['user']);
    }

    public function getNeedAmountString()
    {
        return number_format($this->need_amount, 0);
    }

    public function getCurrentAmountString()
    {
        return number_format($this->current_amount, 0);
    }

}

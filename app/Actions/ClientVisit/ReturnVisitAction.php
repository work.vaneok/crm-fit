<?php
namespace App\Actions\ClientVisit;

use App\Events\ClientVisitReturnedEvent;
use App\Models\ClientVisit;
use DB;

class ReturnVisitAction
{
    public function execute(ClientVisit $visit, $delete_reason = null)
    {
        DB::transaction(function () use ($visit, $delete_reason) {
            $visit->update(['delete_reason' => $delete_reason]);
            $visit->delete();
        });
        event(new ClientVisitReturnedEvent($visit->id));
    }
}

<?php

namespace App\Actions\Firebase;


use App\Actions\BaseAction;
use App\Components\CrmHelper;
use App\Forms\FirebaseNotificationForm;
use App\Helper\FirebaseHelper;
use App\Models\ClientDevice;
use Fbm;

class FirebaseCreateAction extends BaseAction
{

    public $company_id;
    public $title;
    public $descriptions;
    public $image_url;
    public $topic_id;
    public $type;
    public $client_id;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'client_id' => ['integer'],
            'title' => ['string', 'required'],
            'type' => ['string', 'required'],
            'descriptions' => ['string', 'nullable'],
            'image_url' => ['string', 'nullable'],
        ];
    }

    public function action()
    {
        $devices = [];
        if ($this->type == FirebaseCreateViewAction::NOTIFICATION_TYPE_SEND_ALL) {
            $devices = ClientDevice::where([
                ClientDevice::COLUMN_COMPANY_ID => CrmHelper::companyId()
            ])->pluck(ClientDevice::COLUMN_ID)->toArray();

        } elseif ($this->type == FirebaseCreateViewAction::NOTIFICATION_TYPE_SEND_ONE) {
            $devices = ClientDevice::where([
                ClientDevice::COLUMN_COMPANY_ID => CrmHelper::companyId(),
                ClientDevice::COLUMN_CLIENT_ID => $this->client_id
            ])->pluck(ClientDevice::COLUMN_ID)->toArray();
        }

        $form = new FirebaseNotificationForm();
        $form->setTitle($this->title);
        $form->setCompanyId($this->company_id);
        $form->setTopicId(FirebaseHelper::FIREBASE_DEFAULT_TOPIC);
        if ($this->descriptions) {
            $form->setDescription($this->descriptions);
        }
        if ($this->image_url) {
            $form->setMediaUrl($this->image_url);
        }

        $form->setDataByTopicID();
        $form->setDevices($devices);
        $status = Fbm::send($form);
        return $status;
    }
}

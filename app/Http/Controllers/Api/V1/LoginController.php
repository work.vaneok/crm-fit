<?php

namespace App\Http\Controllers\Api\V1;

use App\Actions\Client\ClientSaveDeviceAction;
use App\Actions\Client\ClientSaveLoginAction;
use App\Actions\Client\ClientSmsLoginAction;
use App\Actions\Client\CreateCompanyClientAction;
use App\Components\CrmHelper;
use App\Components\StatusHelper;
use App\Constants\CacheKeysPrefixes;
use App\Constants\HttpErrors;
use App\Helper\RequestHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionCheckRequest;
use App\Http\Resources\CompanyClientApiResource;
use App\Models\Client;
use App\Models\CompanyClient;
use App\Repositories\CompanyClientRepository;
use App\Repositories\LoginSmsCodeRepository;
use App\Rules\ClientSmsCodeRule;
use App\Rules\PhoneFormatRule;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Validator;


class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function username()
    {
        return 'phone';
    }


    /**
     * @apiGroup LoginGroup
     * @apiVersion 0.1.0
     * @apiName SmsCodeRequest
     * @apiUse RequestHeader
     *
     * @api {post} /sms/request Отправить запрос на получение СМС кода
     *
     * @apiParam {String} phone Номер телефона в формате 380681234567
     *
     * @apiSuccess {String} message Текстовый ответ
     * @apiSuccess {String} seconds Сколько секунд живет код
     *
     * @apiSuccessExample {json} Успешный ответ
     *     HTTP/1.1 200 OK
     *     {
     *          "message": "Смс с кодом отправлено успешно!",
     *          "seconds": 180
     *     }
     *
     *
     * @apiErrorExample {json} ОШИБКА 422: Указанные данные были неверными.
     *     HTTP/1.1 422 Unprocessable Entity
     *      {
     *           "message": "Указанные данные были неверными.",
     *           "errors": {
     *               "phone": [
     *                   "Номер телефона обязательно"
     *               ]
     *           }
     *       }
     * @apiErrorExample {json} ОШИБКА 429: слишком много запросов
     *     HTTP/1.1 429 Too Many Attempts
     *
     *
     */
    public function sms(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => ['required', 'string', new PhoneFormatRule()]
        ]);

        $validated = $this->doValidate($validator, $request);

        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            $this->incrementLoginAttempts($request);

            return $this->sendLockoutResponse($request);
        }
        $this->incrementLoginAttempts($request);
        $seconds = (new ClientSmsLoginAction())->execute($validated['phone']);

        return $this->response(['message' => __('Смс с кодом отправлено успешно!'), 'seconds' => $seconds]);
    }

    /**
     * @apiGroup LoginGroup
     * @apiVersion 0.1.0
     * @apiName TokenRequest
     * @apiUse RequestHeader
     *
     * @api {post} /login Запрос на получение токена
     *
     * @apiParam {String} phone Номер телефона в формате 380681234567
     * @apiParam {String} code CODE
     *
     *
     * @apiUse ClientInfo
     *
     *
     * @apiErrorExample {json} ОШИБКА 422: Указанные данные были неверными.
     *     HTTP/1.1 422 Unprocessable Entity
     *      {
     *           "message": "Указанные данные были неверными.",
     *           "errors": {
     *               "phone": [
     *                   "Номер телефона обязательно"
     *               ],
     *              "code": [
     *                   "Код неверный"
     *               ]
     *           }
     *       }
     *
     * @apiErrorExample {json} ОШИБКА 404: Клиент не найден
     *     HTTP/1.1 404 Not Found
     *      {
     *           "code": "client_not_found"
     *       }
     *
     * @apiErrorExample {json} ОШИБКА 429: слишком много запросов
     *     HTTP/1.1 429 Too Many Attempts
     *
     *
     */
    /**
     * @param Request $request
     * @return CompanyClientApiResource|\Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => ['required', 'string', new PhoneFormatRule()],
            'code' => ['required', new ClientSmsCodeRule($request->get('phone'), CacheKeysPrefixes::CLIENT_LOGIN)],
        ]);

        $this->doValidate($validator, $request);

        $validated = $validator->validated();

        $client = Client::wherePhone($validated['phone'])->first();

        // If no client - save phone and code for register new client
        if (!$client) {
            (new ClientSaveLoginAction())->execute($validated['phone'], $validated['code']);
            return $this->response(['code' => HttpErrors::CLIENT_NOT_FOUND], 404);
        }

        /** @var CompanyClient $companyClient */
        $companyClient = app(CompanyClientRepository::class)->findByClientId($client->id);

        if (!$companyClient) {
            $client = (new CreateCompanyClientAction())->execute(['client_id' => $client->id, 'company_id' => CrmHelper::companyId()]);
            $companyClient = app(CompanyClientRepository::class)->findByClientId($client->id);
        }
        $companyClient->refreshToken();

        app(LoginSmsCodeRepository::class)->clear(CacheKeysPrefixes::CLIENT_LOGIN, $validated['phone']);
        if ($this->validateFcmRequest($request)) {
            ClientSaveDeviceAction::execute(
                $client->id,
                $companyClient->company_id,
                $request->get('device_id'),
                $request->get('fcm_id'),
                $request->get('device_os'),
                $request->get('device_os_version')
            );
        }
        return new CompanyClientApiResource($companyClient);
    }

    /**
     * @apiGroup LoginGroup
     * @apiVersion 0.1.0
     * @apiName RegisterRequest
     * @apiUse RequestHeader
     *
     * @api {post} /register Регистрация
     *
     * @apiParam {String} phone Номер телефона в формате 380681234567
     * @apiParam {String} code CODE
     * @apiParam {String} first_name Имя
     * @apiParam {String} last_name Фамилия
     *
     * @apiUse ClientInfo
     *
     * @apiErrorExample {json} ОШИБКА 422: Указанные данные были неверными.
     *     HTTP/1.1 422 Unprocessable Entity
     *      {
     *           "message": "Указанные данные были неверными.",
     *           "errors": {
     *               "phone": [
     *                   "Номер телефона обязательно"
     *               ],
     *              "first_name": [
     *                   "Имя обязательно"
     *               ],
     *              "last_name": [
     *                   "Фамилия обязательно"
     *               ],
     *              "code": [
     *                   "Код неверный"
     *               ]
     *           }
     *       }
     * @apiErrorExample {json} ОШИБКА 429: слишком много запросов
     *     HTTP/1.1 429 Too Many Attempts
     *
     *
     */
    /**
     * @param Request $request
     * @return CompanyClientApiResource
     * @throws ValidationException
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => ['required', 'string', new PhoneFormatRule()],
            'code' => ['required', new ClientSmsCodeRule($request->get('phone'), CacheKeysPrefixes::CLIENT_REGISTER)],
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
        ]);

        $this->doValidate($validator, $request);

        $validated = $validator->validated();
        $validated['company_id'] = CrmHelper::companyId();
        $client = (new CreateCompanyClientAction())->execute($validated);
        /** @var CompanyClient $companyClient */

        $companyClient = app(CompanyClientRepository::class)->findByClientId($client->id);
        $companyClient->refreshToken();
        app(LoginSmsCodeRepository::class)->clear(CacheKeysPrefixes::CLIENT_REGISTER, $validated['phone']);
        if ($this->validateFcmRequest($request)) {
            ClientSaveDeviceAction::execute(
                $client->id,
                $companyClient->company_id,
                $request->get('device_id'),
                $request->get('fcm_id'),
                $request->get('device_os'),
                $request->get('device_os_version')
            );
        }
        return new CompanyClientApiResource($companyClient);
    }

    private function validateFcmRequest(Request $request)
    {
        if ($request->has('fcm_id') && $request->has('device_id') && $request->has('device_os') && $request->has('device_os_version')) {
            return true;
        }
        return false;
    }

    protected function guard()
    {
        return Auth::guard('clients_api');
    }

    /**
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @param Request $request
     * @throws ValidationException
     */
    private function doValidate(\Illuminate\Contracts\Validation\Validator $validator, Request $request)
    {
        if ($validator->fails()) {
            if (method_exists($this, 'hasTooManyLoginAttempts') &&
                $this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);
                $this->incrementLoginAttempts($request);

                return $this->sendLockoutResponse($request);
            }
            $this->incrementLoginAttempts($request);
            throw ValidationException::withMessages($validator->errors()->toArray());
        }

        return $validator->validated();
    }

    public function checkVersion(VersionCheckRequest $request)
    {
        try {
            $supported_version = (int)CrmHelper::settings('app:build_version', null);

            if (!$supported_version) {
                return $this->createSuccessResponseEmpty([
                    RequestHelper::SERVER_RESPONSE => RequestHelper::SERVER_RESPONSE_OK,
                ]);
            }
            $app_version = (int)$request->app_build_number;

            if ($supported_version > $app_version) {
                return $this->createSuccessResponseEmpty([
                    RequestHelper::SERVER_RESPONSE => RequestHelper::SERVER_NEED_UPDATE,
                ]);

            } else {
                return $this->createSuccessResponseEmpty([
                    RequestHelper::SERVER_RESPONSE => RequestHelper::SERVER_RESPONSE_OK,
                ]);
            }
        }catch (\Exception $exception){
            return $this->createSuccessResponseEmpty([
                RequestHelper::SERVER_RESPONSE => RequestHelper::SERVER_RESPONSE_OK,
            ]);
        }

    }
}

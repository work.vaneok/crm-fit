<?php

namespace App\Helper;


class CompanyClientBuyStatusHelper
{
    const TYPE_REFILL_TYPE_ID = 1;
    const TYPE_REFILL_TYPE_TITLE = 'Пополнение';

    const TYPE_WRITEOFF_TYPE_ID = 2;
    const TYPE_WRITEOFF_TYPE_TITLE = 'Списание';

    const TYPE_BUY_TYPE_ID = 3;
    const TYPE_BUY_TYPE_TITLE = 'Покупка';

    const STATUS_NEW_ID = 1;
    const STATUS_NEW_TITLE = 'Новый';

    const STATUS_COMPLETE_ID = 2;
    const STATUS_COMPLETE_TITLE = 'Выполнено';

    public static function getTypesList()
    {
        return [
            self::TYPE_REFILL_TYPE_ID => self:: TYPE_REFILL_TYPE_TITLE,
            self::TYPE_WRITEOFF_TYPE_ID => self:: TYPE_WRITEOFF_TYPE_TITLE,
            self::TYPE_BUY_TYPE_ID => self:: TYPE_BUY_TYPE_TITLE
        ];
    }

    public static function getTypesNameByID($id)
    {
        return isset(self::getTypesList()[$id]) ? self::getTypesList()[$id] : '--';
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_NEW_ID => self:: STATUS_NEW_TITLE,
            self::STATUS_COMPLETE_ID => self:: STATUS_COMPLETE_TITLE,
        ];
    }

    public static function getStatusNameByID($id)
    {
        return isset(self::getStatusList()[$id]) ? self::getStatusList()[$id] : '--';
    }

}

<?php

namespace App\Models;

use App\Models\Traits\DisplayDateFormat;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyStaff extends Model
{
    use DisplayDateFormat, SoftDeletes;

    protected $table = 'company_staffs';
    protected $guarded = ['id'];
    protected $appends = ['username', 'phone', 'first_name', 'last_name', 'birthday'];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function currentLocation()
    {
        return $this->belongsTo(CompanyLocation::class, 'current_location_id', 'id');
    }

    public function companyStaffCompanyLocation()
    {
        return $this->belongsToMany(CompanyStaffCompanyLocation::class, 'id', 'company_staff_id');
    }

    public function getUsernameAttribute()
    {
        return $this->user->name;
    }

    public function getPhoneAttribute()
    {
        return $this->user->phone;
    }

    public function getFirstNameAttribute()
    {
        return $this->user->first_name;
    }

    public function getLastNameAttribute()
    {
        return $this->user->last_name;
    }

    public function getBirthdayAttribute()
    {
        return $this->user->birthday ? $this->user->birthday->format('Y-m-d') : "";
    }

    public function getCompanyStaffCompanyLocationAttribute()
    {
        return $this->companyStaffCompanyLocation->companyStaff;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FirebaseNotificationReceiver
 * @package App\Models
 * @property int $id
 * @property int $notification_id
 * @property int $client_id
 * @property int $company_id
 * @property int $device_id
 * @property int $topic_id
 * @property string $status
 * @property string $data
 * @property string $created_at
 * @property string $updated_at
 */
class FirebaseNotificationReceiver extends Model
{
    const TableName = 'firebase_notification_receivers';
    protected $table = self::TableName;

    const COLUMN_ID = 'id';
    const COLUMN_NOTIFICATION_ID = 'notification_id';
    const COLUMN_CLIENT_ID = 'client_id';
    const COLUMN_COMPANY_ID = 'company_id';
    const COLUMN_DEVICE_ID = 'device_id';
    const COLUMN_TOPIC_ID = 'topic_id';
    const COLUMN_STATUS = 'status';
    const COLUMN_DATA = 'data';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }
    public function device()
    {
        return $this->hasOne(ClientDevice::class, 'id', 'device_id');
    }
}

<?php
namespace App\Repositories;

use Cache;
use Carbon\Carbon;

/**
 * LoginSmsCodeRepository
 *
 * Class LoginSmsCodeRepository
 * @package App\Repositories
 */
class LoginSmsCodeRepository
{
    public function save($key, $phone, $code, $minutes = 3)
    {
        // Сохранить код для подтверждение по смс
        $this->clear($key, $phone);

        Cache::set($key . ':' . $phone, $code, $time = Carbon::now()->addMinutes($minutes));
        return $time->timestamp - Carbon::now()->timestamp;
    }

    public function get($key, $phone)
    {
        return Cache::get($key . ':' . $phone, null);
    }

    public function clear($key, $phone)
    {
        return Cache::set($key . ':' . $phone, null);
    }
}

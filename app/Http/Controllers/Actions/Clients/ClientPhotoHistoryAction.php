<?php

namespace App\Http\Controllers\Actions\Clients;

use App\Http\Controllers\Actions\BaseAction;
use App\Models\CompanyClientPhotoHistory;
use Carbon\Carbon;

class ClientPhotoHistoryAction extends BaseAction
{
    public $company_id;
    public $client_id;

    public $per_page = 15;
    public $page = 1;

    public $viewParams;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'client_id' => ['integer', 'required'],
            'per_page' => ['integer'],
            'page' => ['integer'],
        ];
    }

    public function action()
    {

        $provider = CompanyClientPhotoHistory::query()
            ->where([
                'client_id' => $this->client_id,
                'company_id' => $this->company_id,
                'deleted_at' => null
            ])
            ->orderBy('id', 'desc');

        $provider = $provider->paginate($this->per_page, ['*'], 'page', $this->page);

        /** @var CompanyClientPhotoHistory $item */

        $items = [];
        foreach ($provider->items() as $item) {
            $date = Carbon::parse($item[CompanyClientPhotoHistory::C_DATE]);
            $date = $date->toDateString();
            $medias = $item->getMedia();
            foreach ($medias as $media) {
                $items[$date]['images'] [] = $media->getFullUrl();
            }
        }

        $this->viewParams['items'] = $items;
        $this->viewParams['provider'] = $provider;
        $this->viewParams['company_id'] = $this->company_id;
        $this->viewParams['client_id'] = $this->client_id;
    }
}

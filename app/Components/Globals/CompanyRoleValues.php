<?php
namespace App\Components\Globals;

use App\Constants\CompanyStaffRoles;

class CompanyRoleValues
{
    private static $_values = [];

    public static function get()
    {
        if (empty(static::$_values['all'])) {
            static::$_values['all'] = [
                CompanyStaffRoles::OWNER => __('Владелец'),
                CompanyStaffRoles::ADMINISTRATOR => __('Администратор'),
                CompanyStaffRoles::TRAINER => __('Тренер'),
                CompanyStaffRoles::NUTRITIONIST => __('Диетолог'),
            ];
        }

        return static::$_values['all'];
    }

    /**
     * Роли только для сотрудников
     *
     * @return mixed
     */
    public static function getStaffs()
    {
        if (empty(static::$_values['staffs'])) {
            static::$_values['staffs'] = [
                CompanyStaffRoles::ADMINISTRATOR => __('Администратор'),
                CompanyStaffRoles::TRAINER => __('Тренер'),
                CompanyStaffRoles::NUTRITIONIST => __('Диетолог'),

            ];
        }

        return static::$_values['staffs'];
    }
}

<?php

namespace App\DependedGetters;

use App\Components\CrmHelper;
use App\Models\CompanyStaff;
use Spatie\QueryBuilder\QueryBuilder;

class StaffGetter
{
    static function getList()
    {
        $items = QueryBuilder::for(CompanyStaff::class)
            ->allowedFilters(['title', 'id'])
            ->with(['user'])
            ->leftJoin('company_staff_company_locations', 'company_staffs.id', '=', 'company_staff_company_locations.company_staff_id')
            ->where(['company_staff_company_locations.company_location_id' => null])
            ->orWhere(['company_staff_company_locations.company_location_id' => CrmHelper::companyLocationId()])
            ->where(['company_id' => CrmHelper::companyId()])
            ->orderBy('id', 'DESC')
            ->paginate(15);
        return $items;
    }
}

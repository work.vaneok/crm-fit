<?php
namespace App\Models\Logs;

use App\Models\ClientSubscription;
use App\Models\ClientVisit;
use App\Models\Traits\DisplayDateFormat;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * \App\Models\Logs\LogClientVisits
 *
 * @property int $id
 * @property int $visit_id
 * @property int $client_subscription_id
 * @property int $company_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $created_by
 * @property int|null $created_by_user_id
 * @property string $action_name
 * @property-read \App\User|null $createdBy
 * @property-read \App\Models\ClientVisit $visit
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereActionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereClientSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Logs\LogClientVisits whereVisitId($value)
 * @mixin \Eloquent
 */
class LogClientVisits extends Model
{
    use DisplayDateFormat;
    protected $table = 'log_client_visits';
    protected $guarded = ['id'];


    // TODO: Когда нибудь может быть вынести это дело
    public static function add(ClientVisit $visit)
    {
        self::create([
            'visit_id' => $visit->id,
            'client_subscription_id' => $visit->client_subscription_id,
            'company_id' => $visit->company_id,
            'action_name' => 'visit',
        ]);
    }

    public static function return(ClientVisit $visit)
    {
        self::create([
            'visit_id' => $visit->id,
            'client_subscription_id' => $visit->client_subscription_id,
            'company_id' => $visit->company_id,
            'action_name' => 'return',
        ]);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by_user_id', 'id');
    }

    public function visit()
    {
        return $this->belongsTo(ClientVisit::class, 'visit_id', 'id')->withTrashed();
    }

    public function clientSubscription()
    {
        return $this->belongsTo(ClientSubscription::class, 'client_subscription_id', 'id');
    }
}


// import Vue from 'vue'
import SaleSubscription from './components/SaleSubscription'
import vSelect from 'vue-select'

Vue.component('v-select', vSelect)


const app = new Vue({
    el: '#sale-form',
    components: {
        SaleSubscription
    }
});

<?php

namespace App\Models;

use App\Models\Traits\ByCompanyScopeTrait;
use App\Models\Traits\DisplayDateFormat;
use App\Models\Traits\IsActiveScopeTrait;
use App\Models\Traits\QuantityOfOccupationText;
use App\Models\Traits\ValidityText;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientSubscription extends Model
{
    use DisplayDateFormat, ByCompanyScopeTrait, IsActiveScopeTrait, ValidityText, QuantityOfOccupationText;

    protected $table = 'client_subscriptions';
    const TableName = 'client_subscriptions';
    protected $guarded = ['id'];

    protected $casts = [
        'available_from' => 'date',
        'available_to' => 'date',
        'available_to_before_begin_of_use' => 'date',
    ];

    public function getIsUnlimitedAttribute()
    {
        return $this->quantity_of_occupation == -1;
    }

    public function scopeCurrent($q, $is_current = 1)
    {
        $q->where(['is_current' => $is_current]);
    }


    public function user()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class, 'subscription_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function doReduce()
    {
        $this->update(['quantity_of_occupation' => $this->quantity_of_occupation - 1]);
    }

    public function doIncrease()
    {
        $this->update(['quantity_of_occupation' => $this->quantity_of_occupation + 1]);
    }

    public function visits()
    {
        return $this->hasMany(ClientVisit::class, 'client_subscription_id', 'id');
    }

    public function allVisits()
    {
        return $this->hasMany(ClientVisit::class, 'client_subscription_id', 'id')->withTrashed()->with(['createdBy', 'deletedBy'])->orderBy('id', 'DESC');
    }
}

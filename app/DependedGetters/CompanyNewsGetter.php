<?php
namespace App\DependedGetters;

use App\Components\CrmHelper;
use App\Models\CompanyLocation;
use App\Models\CompanyNews;
use Spatie\QueryBuilder\QueryBuilder;

class CompanyNewsGetter
{
    /**
     * @return \Illuminate\Pagination\Paginator
     */
    static function getList()
    {
        $items = QueryBuilder::for(CompanyNews::class)
            ->select(['id', 'created_at', 'title', 'description'])
            ->with(['media'])
            ->where([
                'company_id' => CrmHelper::companyId()
            ])
            ->allowedFilters(['title', 'id'/*, AllowedFilter::scope('category')*/])
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return $items;
    }
}

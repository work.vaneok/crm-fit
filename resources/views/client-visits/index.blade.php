@extends('layouts.dashboard')

@section('title')
    {{ __('Посещения') }}
    {{--  @if (count($dates))
          {{ __('за') }} {{ implode(' - ', $dates) }}
      @endif--}}
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $(".js-example-basic-single").select2({width: 'resolve'});
        });
    </script>
@endpush
@section('content')
    <div class="header__filter" style=" margin-bottom: 15px;">
        <div class="row">
            <div class="col-1">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#filter"
                            style="margin-left: 0;">{{ __('Фильтр') }}</button>
                    <form action="{{ route('client-visit.export') }}" accept-charset="UTF-8" class="d-flex justify-content-start">
                        @if(Request::has('filter.client_id'))
                            <button type="submit" class="btn btn-primary" data-toggle="tooltip" name="filter[group_type]" value="excel" title="{{__('Сохранить в Excel файл')}}">{{__('Excel')}}</button>
                            <button type="submit" class="btn btn-primary" data-toggle="tooltip" name="filter[group_type]" value="group-excel" title="{{__('Сохранить сгруппированные данные в Excel-файл')}}">{{__('Сгрупп.')}}</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="tooltip"  disabled title="{{__('Чтобы активировать возможность импорта нажмите "Фильтр" потом "Фильтровать"')}}">
                                {{__('Excel')}}
                            </button>
                            <button type="button" style="height:37px;text-overflow:ellipsis;" class="btn btn-secondary" data-toggle="tooltip"  disabled title="{{__('Чтобы активировать возможность импорта нажмите "Фильтр" потом "Фильтровать"')}}">
                                {{__('Групп.')}}
                            </button>
                        @endif
                        <input style="display: none" type="text" value="{{get_filter_value('client_id')}}"
                               name="filter[client_id]" placeholder="Id">
                        <input style="display: none" type="text" value="{{get_filter_value('location_id')}}"
                               name="filter[location_id]" placeholder="Id">
                        <input style="display: none" type="text" value="{{get_filter_value('staff_id')}}"
                               name="filter[staff_id]" placeholder="Id">
                        <input style="display: none" type="text" value="{{get_filter_value('date_from')}}"
                               name="filter[date_from]" placeholder="Id">
                        <input style="display: none" type="text" value="{{get_filter_value('date_to')}}"
                               name="filter[date_to]" placeholder="Id">
                    </form>
                </div>
            </div>


            <div id="filter" class="collapse col-lg-12" style="margin-top: 15px; padding-left: 0px;">
                <form action="" class="grid-filter" method="get" id="filterForm">
                    @php($errMessages = [])
                    @error('filter.date')
                    @php($errMessages[] = $message)
                    @enderror
                    @error('filter.date_from')
                    @php($errMessages[] = $message)
                    @enderror
                    @error('filter.date_to')
                    @php($errMessages[] = $message)
                    @enderror

                    @if(count($errMessages))
                        <div class="alert alert-danger">{!! implode('<br>', $errMessages) !!}</div>
                    @endif

                    <div class="form-row">
                        <div class="form-group col-md-4 col-lg-4 col-xl-3">
                            <label for="filter[client_id]"><b>{{ __('Клиент') }}</b></label><br>
                            @search_select2(['name' => 'client_id', 'options' => $clients, 'label' => __('Клиент')])
                        </div>

                        @if(\App\Components\CrmHelper::getRoleInCompany() == \App\Constants\CompanyStaffRoles::OWNER || \App\Components\CrmHelper::getRoleInCompany() == \App\Constants\CompanyStaffRoles::ADMINISTRATOR )
                            <div class="form-group col-md-4 col-lg-4 col-xl-3">
                                <label for="filter[staff_id]"><b>{{ __('Сотрудник') }}</b></label><br>
                                @search_select2(['name' => 'staff_id', 'options' => $staffs, 'label' =>
                                __('Сотрудник')])
                            </div>
                        @endif

                        {{--<div class="form-group col-md-2">
                             <label for="filter[client_id]"><b>{{ __('Абонемент') }}</b></label><br>
                             @search_select2(['name' => 'subscription_id', 'options' => $subscriptions, 'label' => __('Абонемент')])
                         </div>--}}

                        <div class="form-group col-md-3 col-lg-2 col-xl-2">
                            <label for="filter[client_id]"><b>{{ __('Дата') }}</b></label>
                            @search_date_range(['name' => 'date'])
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary "><i
                                    class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                        </div>
                        <div class="form-group">
                            <a href="/{{ Route::current()->uri }}" class="btn btn-danger "><i
                                    class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    {{--<form action="" class="grid-filter" method="get" >
        @php($errMessages = [])
        @error('filter.date')
            @php($errMessages[] = $message)
        @enderror
        @error('filter.date_from')
            @php($errMessages[] = $message)
        @enderror
        @error('filter.date_to')
            @php($errMessages[] = $message)
        @enderror

        @if(count($errMessages))
            <div class="alert alert-danger">{!! implode('<br>', $errMessages) !!}</div>
        @endif
        <div class="form-row">


            <div class="col-auto">
                <input class="form-control form-control-sm" type="text" value="{{get_filter_value('username')}}" name="filter[username]" placeholder="{{ __('Имя клиента') }}">
            </div>

            <div class="col-auto">
                <input class="form-control form-control-sm" type="text" value="{{get_filter_value('phone')}}" name="filter[phone]" placeholder="{{ __('Номер телефона') }}">
            </div>

            <div class="col-auto">
                @search_select(['name' => 'subscription', 'options' => $subscriptions, 'label' => __('Абонемент')])
            </div>

            <div class="col-auto">
                @if($filters['date_range'])
                    @search_date_range(['name' => 'date'])
                @else
                    @search_date(['value' => (get_filter_value('date') ?? \Carbon\Carbon::now()->format('d.m.Y')), 'name' => 'date'])
                @endif
            </div>

            <div class="col-auto">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                <a href="/{{ Route::current()->uri }}" class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
            </div>
        </div>
    </form>--}}

    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">{{ __('Клиент') }}</th>
                <th scope="col">{{ __('Локация') }}</th>
                <th scope="col">{{ __('Абонемент') }}</th>
                <th scope="col">{{ __('Информация') }}</th>
                <th scope="col">{{ __('Действия') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>
                        <a href="{{route('clients.profile',['client' => $item['client']['id']])}}">  {{$item['client']['first_name'].' '. $item['client']['last_name']}}
                            <i class="fa fa-user" aria-hidden="true"></i></a> <br>
                        <a href="tel:+{{ $item['client']['phone'] }}"> {{ $item['client']['phone'] }} </a>
                    </td>
                    <td>{{ $item['location']['title'] }}</td>
                    <td>{{ $item['clientSubscription']['title'] }}</td>
                    <td>
                        <span><b>{{__('ID')}}</b>:  {{ $item['id'] }} </span> <br>
                        <span><b>{{__('Ключ')}}</b>:  {{ $item['key_number'] }} </span> <br>
                        <span><b>{{__('Дата')}}</b>:  {{ $item['created_at'] }} </span> <br>
                        <span><b>{{__('Сотрудник')}}</b>:  {{ $item['byUser']['first_name'].' '.$item['byUser']['last_name']  }} </span>
                        <br>
                    </td>
                    <td>
                        {{--
                                                @can('returnVisitPolicy', $item)
                        --}}

                        @return_visit(['route' => route('client-visit.return', ['visit' => $item['id']])])
                        {{--
                                                @endcan
                        --}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $provider ? $provider->appends(request()->only('filter'))->links() : '' }}
    </div>
@endsection

@section('actions')

    <li>
        @add(['route' => route('client-visit.check')])
    </li>

    {{--<div class="btn-toolbar mb-2 mb-md-0">--}}
    {{--@add(['route' => route('client-visit.check')])--}}
    {{--</div>--}}
@endsection

@push('css')

@endpush

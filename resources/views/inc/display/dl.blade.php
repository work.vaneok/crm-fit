<dl class="row">
    <dt class="col-6 col-sm-4 col-md-5 col-lg-2 col-xl-2"
        style="text-align: right; margin-bottom: 5px; padding-top: 2px">
        {{$label}}
    </dt>
    <dd class="col-6  col-sm-8 col-md-7  col-lg-10 col-xl-10" {{----style="font-size: 0.95rem"---}}>
        @if($type)
            @if($type == 'boolean')
                @boolean(['boolean' => $value])
            @endif
        @else
            {!!  $value !!}
        @endif
    </dd>
</dl>


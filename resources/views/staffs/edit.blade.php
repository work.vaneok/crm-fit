@extends('layouts.dashboard')

@section('title')
    {{ __('Редактирование сотрудника') }}
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->multipart()->route('staffs.update', ['staff' => $staff->id])->put()->fill($staff) !!}
        {!! Form::text('first_name', 'Имя') !!}
        {!! Form::text('last_name', 'Фамилия') !!}
        {!! Form::text('phone', 'Телефон') !!}
        {!! Form::date('birthday', 'Дата рождения' ) !!}
        {!! Form::select('role_in_company', 'Роль в компании')->options($roles) !!}
        {!! Form::text('trainer_price', 'Стоимость занятия с тренером')->type('number') !!}
        <div class="form-group row">
            <label for="inp-locations_id[]" class="col-sm-2 col-form-label">Доступные локации</label>
            <div class="col-sm-6">
                <select style="height:128px" name="locations_id[]" id="inp-locations_id[]" multiple=""
                        class="form-control" aria-describedby="help-inp-locations_id[]">
                    @foreach($locations as $location)
                        <option value="{{array_search($location, $locations)}}"
                            @foreach($selectedLocations as $selectedLocation)
                                @if($selectedLocation['company_location_id'] == array_search($location, $locations))
                                    selected
                                @endif
                            @endforeach
                        >{{$location}}</option>
                    @endforeach
                </select>
                <small id="help-inp-locations_id[]" class="form-text text-muted">Для отмены выбора определённого
                    элемента, либо выбора нескольких одновременно, щёлкните по ним, предварительно зажав клавишу
                    CTRL</small>
            </div>
        </div>
        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit(__('Сохранить изменения')) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection

@push('scripts')
    @include('inc.ckeditor')
@endpush


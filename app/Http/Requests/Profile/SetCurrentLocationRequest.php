<?php
namespace App\Http\Requests\Profile;

use App\Http\Requests\Traits\DefaultCompanyIdInRequest;
use App\Rules\IsLocationRelatedToCompanyRule;
use App\Rules\IsUserRelatedToCompanyRule;
use Illuminate\Foundation\Http\FormRequest;

class SetCurrentLocationRequest extends FormRequest
{
    use DefaultCompanyIdInRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location' => [
                'required',
                new IsLocationRelatedToCompanyRule($this->get('company_id')),
                new IsUserRelatedToCompanyRule($this->get('company_id'))
            ],
        ];
    }

}

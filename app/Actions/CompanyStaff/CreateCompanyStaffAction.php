<?php
namespace App\Actions\CompanyStaff;

use App\Actions\Company\CreateCompanyAction;
use App\Actions\User\CreateUserAction;
use App\Actions\User\UserResetPasswordAction;
use App\Constants\CompanyStaffRoles;
use App\Constants\Roles;
use App\Models\CompanyStaff;
use App\Models\CompanyStaffCompanyLocation;
use DB;
use Illuminate\Http\Request;
use Log;

class CreateCompanyStaffAction
{
    public function execute(Request $request)
    {
        DB::transaction(function () use ($request) {
            $userModel = (new CreateUserAction())->execute(
                $request->get('phone'),
                $request->get('first_name'),
                $request->get('last_name')
            );

            $companyStaff = CompanyStaff::create([
                'company_id' => $request->get('company_id'),
                'user_id' => $userModel->id,
                'role_in_company' => $request->role_in_company,
            ]);

            foreach ($request->get('locations_id') as $location_id) {
                CompanyStaffCompanyLocation::create([
                    'company_staff_id' => $companyStaff->id,
                    'company_location_id' => $location_id
                ]);
            }

            $userModel->assignRole(Roles::COMPANY_STAFF);

            try {
                (new UserResetPasswordAction())->executeByPhone($userModel->phone);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
            }
        });
    }
}

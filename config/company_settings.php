<?php
return [
    'company:timeToReturnVisit' => '',

    'liqpay:public_key' => '',
    'liqpay:private_key' => '',

    'wayforpay:login' => '',
    'wayforpay:secret_key' => '',

    'sms_turbo:login' => '',
    'sms_turbo:password' => '',
    'sms_turbo:sender' => '',

    'social:fb' => '',
    'social:instagram' => '',
    'social:telergam' => '',
    'social:viber' => '',
    'social:vk' => '',

    'app:build_version' => '',
];

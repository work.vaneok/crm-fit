<?php

namespace App\Actions\Client;


use App\Actions\BaseAction;
use App\Components\CrmHelper;
use App\Forms\FirebaseNotificationForm;
use App\Helper\FirebaseHelper;
use App\Models\ClientDevice;
use App\Models\ClientSubscription;
use Carbon\Carbon;
use Fbm;
use GuzzleHttp\Exception\ClientException;

class ClientUpdateSubscriptionAction extends BaseAction
{

    public $subscription_id;
    public $available_from;
    public $available_to;
    public $quantity_of_occupation;
    public $nutritionist_id;
    public $trainer_id;
    public $has_trainer;
    public $has_nutritionist;

    public $response;


    public function rules(): array
    {
        return [
            'subscription_id' => ['integer', 'required'],
            'available_from' => ['nullable', 'string'],
            'available_to' => ['nullable', 'string'],
            'quantity_of_occupation' => ['nullable', 'integer'],
            'trainer_id' => ['nullable', 'integer'],
            'nutritionist_id' => ['nullable', 'integer'],
            'has_trainer' => ['nullable'],
            'has_nutritionist' => ['nullable'],
        ];
    }

    public function action()
    {
        $clientSubscriptionModel = ClientSubscription::findOrFail($this->subscription_id);

        if ($this->available_from) {
            $clientSubscriptionModel->available_from = Carbon::parse($this->available_from)->startOfDay();
        } else {
            $clientSubscriptionModel->available_from = null;
        }

        if ($this->available_to) {
            $clientSubscriptionModel->available_to = Carbon::parse($this->available_to)->endOfDay();
        } else {
            $clientSubscriptionModel->available_to = null;
        }

        if ($this->quantity_of_occupation) {
            $clientSubscriptionModel->quantity_of_occupation = $this->quantity_of_occupation;
        } else {
            $clientSubscriptionModel->available_to = 0;
        }


        if ($this->trainer_id) {
            $clientSubscriptionModel->trainer_id = $this->trainer_id;
        } else {
            $clientSubscriptionModel->trainer_id = null;
        }

        if ($this->nutritionist_id) {
            $clientSubscriptionModel->nutritionist_id = $this->nutritionist_id;
        } else {
            $clientSubscriptionModel->nutritionist_id = null;
        }

        if ($this->has_trainer) {
            $clientSubscriptionModel->has_trainer = 1;
        } else {
            $clientSubscriptionModel->has_trainer = 0;
        }

        if ($this->has_nutritionist) {
            $clientSubscriptionModel->has_nutritionist = 1;
        } else {
            $clientSubscriptionModel->has_nutritionist = 0;
        }

        if (!$clientSubscriptionModel->save()) {
            throw new \Exception('ClientUpdateSubscriptionAction');
        }

        $this->response = [
            'message' => __('Абонемент успешно обновлен')
        ];

    }
}

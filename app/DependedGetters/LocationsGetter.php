<?php
namespace App\DependedGetters;

use App\Components\CrmHelper;
use App\Models\CompanyLocation;
use Spatie\QueryBuilder\QueryBuilder;

class LocationsGetter
{
    static function getList()
    {
        $items = QueryBuilder::for(CompanyLocation::class)
            ->where([
                'company_id' => CrmHelper::companyId()
            ])
            ->allowedFilters(['title', 'id'/*, AllowedFilter::scope('category')*/])
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return $items;
    }

    static function getListForStaffs()
    {
        $items = QueryBuilder::for(CompanyLocation::class)
            ->where(['company_id' => CrmHelper::companyId()])
            ->get();

        $collection = collect($items);

        $keyed = $collection->mapWithKeys(function ($item) {
            return [$item['id'] => $item['title']];
        });

        return $keyed->all();
    }
}

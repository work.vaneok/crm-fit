<?php
namespace App\Actions\CompanyOwner;

use App\Actions\Company\CreateCompanyAction;
use App\Actions\CompanyStaff\CreateCompanyStaffAction;
use App\Actions\User\CreateUserAction;
use App\Actions\User\UserResetPasswordAction;
use App\Constants\CompanyStaffRoles;
use App\Constants\Roles;
use App\Models\CompanyStaff;
use DB;
use Illuminate\Http\Request;
use Log;

class CreateCompanyOwnerAction
{
    public function execute(Request $request)
    {
        DB::transaction(function () use ($request) {
            $userModel = (new CreateUserAction())->execute(
                $request->get('phone'),
                $request->get('first_name'),
                $request->get('last_name')
            );

            $company =(new CreateCompanyAction())->execute(
                $request->get('title'),
                $request->get('description'),
                $request->get('available_from'),
                $request->get('available_to'),
                $userModel->id,
                $request->get('tariff_id')
            );

            CompanyStaff::create([
                'company_id' => $company->id,
                'user_id' => $userModel->id,
                'role_in_company' => CompanyStaffRoles::OWNER,
            ]);

            $userModel->assignRole(Roles::COMPANY_OWNER);

            try {
                (new UserResetPasswordAction())->executeByPhone($userModel->phone);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
            }

        });
    }
}

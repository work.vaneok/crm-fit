<?php

namespace App\Http\Controllers\Actions;

use App\Models\CompanyClientParameterHistory;

class ClientParameterHistoryAction extends BaseAction
{
    public $company_id;
    public $client_id;
    public $per_page = 15;
    public $page = 1;


    public $response;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'client_id' => ['integer', 'required'],
            'per_page' => ['integer'],
            'page' => ['integer']
        ];
    }

    public function action()
    {
        $response = [];

        $history = CompanyClientParameterHistory::query()->where([
            'client_id' => $this->client_id,
            'company_id' => $this->company_id,
            'deleted_at' => null
        ])
            ->orderBy('id', 'desc')
            ->paginate($this->per_page, ['*'], null, $this->page);

        $response['history']['meta']['total'] = $history->total();
        $response['history']['meta']['per_page'] = $history->perPage();
        $response['history']['meta']['current_page'] = $history->currentPage();
        $response['history']['meta']['last_page'] = $history->lastPage();

        $count = 0;
        $response['history']['items'] = [];
        /** @var CompanyClientParameterHistory $item */
        foreach ($history->items() as $item) {
            $response['history']['items'][$count][CompanyClientParameterHistory::C_ID] = $item[CompanyClientParameterHistory::C_ID];
            $response['history']['items'][$count][CompanyClientParameterHistory::C_DATE] = $item[CompanyClientParameterHistory::C_DATE];
            $response['history']['items'][$count][CompanyClientParameterHistory::C_WEIGHT] = $item[CompanyClientParameterHistory::C_WEIGHT];
            $response['history']['items'][$count][CompanyClientParameterHistory::C_SHOULDER_VOLUME] = $item[CompanyClientParameterHistory::C_SHOULDER_VOLUME];
            $response['history']['items'][$count][CompanyClientParameterHistory::C_WAIST] = $item[CompanyClientParameterHistory::C_WAIST];
            $response['history']['items'][$count][CompanyClientParameterHistory::C_HIPS] = $item[CompanyClientParameterHistory::C_HIPS];
            $response['history']['items'][$count][CompanyClientParameterHistory::C_BICEPS_VOLUME] = $item[CompanyClientParameterHistory::C_BICEPS_VOLUME];
            $response['history']['items'][$count][CompanyClientParameterHistory::C_BREAST_VOLUME] = $item[CompanyClientParameterHistory::C_BREAST_VOLUME];
            $response['history']['items'][$count][CompanyClientParameterHistory::C_LEG_VOLUME] = $item[CompanyClientParameterHistory::C_LEG_VOLUME];
            $response['history']['items'][$count][CompanyClientParameterHistory::C_CAVIAR_VOLUME] = $item[CompanyClientParameterHistory::C_CAVIAR_VOLUME];
            $response['history']['items'][$count][CompanyClientParameterHistory::C_COMMENT] = $item[CompanyClientParameterHistory::C_COMMENT];
            $response['history']['items'][$count]['images'] = [];
            $medias = $item->getMedia();
            foreach ($medias as $media) {
                $response['history']['items'][$count]['images'] [] = $media->getFullUrl();
            }
            $count++;
        }
        $this->response = $response;

    }

}

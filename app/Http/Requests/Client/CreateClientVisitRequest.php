<?php
namespace App\Http\Requests\Client;

use App\Http\Requests\Traits\DefaultCompanyLocationIdInRequest;
use App\Rules\ClientOfCompanyRule;
use App\Rules\ClientSubscriptionAvailableRule;
use App\Rules\ClientSubscriptionHasOccupationRule;
use App\Rules\ClientSubscriptionOfCompanyRule;
use Illuminate\Foundation\Http\FormRequest;

class CreateClientVisitRequest extends FormRequest
{
    use DefaultCompanyLocationIdInRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required'],
            'company_location_id' => ['required'],
            'staff_id' => ['required'],
            'client_id' => ['bail', 'required', new ClientOfCompanyRule($this->get('company_id'))],
            'key_number' => ['nullable'],
            'client_subscription_id' => [
                'bail',
                'required',
                new ClientSubscriptionOfCompanyRule($this->get('company_id')),
                new ClientSubscriptionAvailableRule(),
                new ClientSubscriptionHasOccupationRule(),
            ],
        ];
    }

}

<?php
namespace App\Components\PageBuilder;

interface Buildable
{
    public function build();
}

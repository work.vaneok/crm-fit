<?php

namespace App\Http\Controllers\Api\V1;

use App\Components\CrmHelper;
use App\Helper\CompanyClientBuyStatusHelper;
use App\Http\Controllers\Actions\ClientParameterHistoryAction;
use App\Http\Controllers\Actions\ClientPhotoHistoryAction;
use App\Http\Controllers\Actions\ClientSavePhotoHistoryAction;
use App\Http\Controllers\Actions\SaveParameterHistoryAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Client\ClientParameterHistoryRequest;
use App\Http\Requests\Client\ClientPhotoHistoryRequest;
use App\Http\Requests\Client\ClientSaveParameterHistoryRequest;
use App\Http\Requests\Client\ClientSavePhotoHistoryRequest;
use App\Http\Requests\Client\UpdateProfileRequest;
use App\Http\Resources\ActiveSubscriptionsApiCollection;
use App\Http\Resources\ClientVisitsApiCollection;
use App\Http\Resources\CompanyClientApiResource;
use App\Models\CompanyClient;
use App\Models\CompanyClientBuy;
use App\Models\FirebaseNotification;
use App\Models\FirebaseNotificationReceiver;
use App\Presenters\ClientMyNotificationPresenter;
use App\Repositories\CompanyClientBuysRepository;
use App\Repositories\CompanyClientRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ClientController extends Controller
{
    /**
     * @apiGroup MyGroup
     * @apiVersion 0.1.0
     * @apiName ActiveSubscriptionsRequest
     * @apiUse RequestAuthHeader
     *
     * @api {get} /my/active-subscriptions Активные абонементы
     *
     * @apiSuccess {Integer} data.id ИД абонемента
     * @apiSuccess {String} data.title Наименование абонемента
     * @apiSuccess {String} data.active_from Доступен с
     * @apiSuccess {String} data.active_to Доступен по
     * @apiSuccess {String} data.auto_available_from Доступен с, если еще не был активирован
     * @apiSuccess {String} data.image Картинка абонемента
     * @apiSuccess {Integer} data.occupations Занятий осталось
     *
     * @apiSuccessExample {json} Успешный ответ
     *     HTTP/1.1 200 OK
     *     {
     *          "data": [{
     *              "id": 1,
     *              "title": "title",
     *              "active_from": "2020-02-02",
     *              "active_to": "2020-02-02",
     *              "auto_available_from": "2020-02-02",
     *              "image": "LINK",
     *              "occupations": 13,
     *          }]
     *     }
     *
     * @apiUse UnauthorizedError
     *
     */
    public function activeSubscriptions(Request $request)
    {
        return (new ActiveSubscriptionsApiCollection(app(CompanyClientRepository::class)->activeSubscriptionsByClientV1($request->user())));
    }

    /**
     * @apiGroup MyGroup
     * @apiVersion 0.1.0
     * @apiName MyInfoRequest
     * @apiUse RequestAuthHeader
     *
     * @api {get} /me Информация обо мне
     *
     * @apiUse ClientInfo
     *
     * @apiUse UnauthorizedError
     *
     */
    public function me(Request $request)
    {
        return new CompanyClientApiResource($request->user());
    }

    /**
     * @apiGroup MyGroup
     * @apiVersion 0.1.0
     * @apiName MyInfoUpdateRequest
     * @apiUse RequestAuthHeader
     *
     * @api {put} /me Обновить информацию обо мне
     *
     * @apiParam {String} first_name Имя
     * @apiParam {String} last_name Фамилия
     * @apiParam {String} birthday Дата рождения
     * @apiParam {Integer}хабр sex Пол
     *
     * @apiSuccessExample {json} Успешный ответ
     *     HTTP/1.1 200 OK
     *     {
     *          "message": "Данные обновлены"
     *     }
     *
     * @apiUse UnauthorizedError
     *
     */
    public function update(UpdateProfileRequest $request)
    {
        $request->user()->client->update($request->validated());
        return ['message' => __('Данные обновлены')];
    }

    /**
     * @apiGroup MyGroup
     * @apiVersion 0.1.0
     * @apiName CompanyVisits
     * @apiUse RequestAuthHeader
     *
     * @api {get} /my/visits Получить список посещений по компании
     *
     * @apiSuccessExample {json} Успешный ответ
     *     HTTP/1.1 200 OK
     *     {
     *          "data": [
     *               {
     *                   "date": "2020-01-24 11:05:27",
     *                   "location": "Наименование локации"
     *               }
     *          ],
     *          "links": {
     *               "first": "https://<domain>/api/v1/my/company-visits?page=1",
     *               "last": "https://<domain>/api/v1/my/company-visits?page=6",
     *               "prev": null,
     *               "next": "https://<domain>/api/v1/my/company-visits?page=2"
     *          },
     *          "meta": {
     *               "current_page": 1,
     *               "from": 1,
     *               "last_page": 6,
     *               "path": "https://<domain>/api/v1/my/company-visits",
     *               "per_page": 1,
     *               "to": 1,
     *               "total": 6
     *          }
     *     }
     *
     * @apiUse UnauthorizedError
     *
     */
    public function visits(Request $request)
    {
        return (new ClientVisitsApiCollection(app(CompanyClientRepository::class)->visits($request->user())));
    }

    public function myNotifications(Request $request)
    {
        try {
            $company_id = CrmHelper::companyId();
            $client_id = $request->user()->client_id;
            $notificationsIds = FirebaseNotificationReceiver::distinct('notification_id')->where(['client_id' => $client_id, 'company_id' => $company_id])->pluck('notification_id')->toArray();
            $notifications = FirebaseNotification::whereIn('id', $notificationsIds)->get();

            return $this->createSuccessResponse([
                'notifications' => ClientMyNotificationPresenter::presentCollection($notifications),
            ]);
        } catch (\Exception $exception) {
            return $this->createErrorResponse($exception->getMessage());
        }
    }

    public function myPayments(Request $request)
    {
        try {
            $company_id = CrmHelper::companyId();
            $title = CrmHelper::company()->title;
            $client_id = $request->user()->client_id;
            $buys = app(CompanyClientBuysRepository::class)->getBuysCompanyClient($client_id, $company_id);
            /** @var CompanyClient $companyClient */
            $companyClient = app(CompanyClientRepository::class)->findById($client_id);
            $total_debt = (string)$buys->sum('current_amount');
            $response = [];
            $paymentCounter = 0;
            $payments = [];

            foreach ($buys as $item) {
                /** @var CompanyClientBuy $item */

                $payments[$paymentCounter]['created_at'] = $item->formatDateTime('created_at');
                $payments[$paymentCounter]['type_id'] = $item->type_id;
                $payments[$paymentCounter]['status_id'] = $item->status_id;
                $payments[$paymentCounter]['operation_name'] = $item->getOperationName();
                $payments[$paymentCounter]['need_amount'] = $item->getNeedAmountString();
                $payments[$paymentCounter]['current_amount'] = $item->getCurrentAmountString();
                $payments[$paymentCounter]['comment'] = $item->comment;
                $payments[$paymentCounter]['status'] = $item->getStatusName();
                $payments[$paymentCounter]['company_title'] = $title;
                if ($item->createTransaction) {
                    $payments[$paymentCounter]['created_by'] = $item->createTransaction->user->first_name;
                }

                if ($item->paidTransaction) {
                    $payments[$paymentCounter]['paid_by'] = $item->paidTransaction->user->first_name;
                }
                $paymentCounter++;
            }
            $response['payments'] = $payments;
            $response['balance'] = $companyClient->getBalanceString();
            $response['debt'] = $total_debt;
            return $this->createSuccessResponse($response);
        } catch (\Exception $exception) {
            return $this->createErrorResponse($exception->getMessage());
        }
    }

    public function parameterHistory(ClientParameterHistoryRequest $request)
    {
        try {
            $action = new ClientParameterHistoryAction([
                'company_id' => CrmHelper::companyId(),
                'client_id' => $request->user()->client_id
            ]);
            $action->execute();
            return $this->createSuccessResponse($action->response);
        } catch (\Exception $exception) {
            Log::channel('telegram')->error($exception);
            return $this->createErrorResponse('Error');
        }
    }

    public function saveParameterHistory(ClientSaveParameterHistoryRequest $request)
    {
        try {
            $action = new SaveParameterHistoryAction(array_merge($request->all(), [
                'company_id' => CrmHelper::companyId(),
                'client_id' => $request->user()->client_id
            ]));
            $action->execute();
            return $this->createSuccessResponse([
                'model_id' => $action->model_id
            ]);
        } catch (\Exception $exception) {
            Log::channel('telegram')->error($exception);
            return $this->createErrorResponse('Error');
        }
    }

    public function photoHistory(ClientPhotoHistoryRequest $request)
    {
        try {
            $action = new ClientPhotoHistoryAction([
                'company_id' => CrmHelper::companyId(),
                'client_id' => $request->user()->client_id
            ]);
            $action->execute();
            return $this->createSuccessResponse($action->response);
        } catch (\Exception $exception) {
            Log::channel('telegram')->error($exception);
            return $this->createErrorResponse('Error');
        }
    }

    public function savePhotoHistory(ClientSavePhotoHistoryRequest $request)
    {
        try {
            $action = new ClientSavePhotoHistoryAction(array_merge($request->all(), [
                'company_id' => CrmHelper::companyId(),
                'client_id' => $request->user()->client_id,
            ]));
            $action->execute();
            return $this->createSuccessResponse($action->response);
        } catch (\Exception $exception) {
            Log::channel('telegram')->error($exception);
            return $this->createErrorResponse('Error');
        }
    }


}

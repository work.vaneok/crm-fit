<?php
namespace App\Components\Sms\Providers;

class TurbosmsProvider implements SmsProviderInterface
{
    private $method_sms = 'SQL';
    private $options;
    private $turboSms;

    function __construct($options)
    {
        $this->options = $options;
        $this->turboSms = new TurboSms($options);
    }

    public function changeSmsMethod($method)
    {
        $this->method_sms = $method;
    }

    public function sendSms($phone, $text, $time = false)
    {

        return $this->turboSms->sendSmsUseSoap($phone, $text);

        if ($this->method_sms = 'SOAP') {
            return $this->turboSms->sendSmsUseSoap($phone, $text);
        } else if ($this->method_sms = 'SQL') {
            return $this->turboSms->sendSmsUseSql($phone, $text, $time);
        } else {
            return [
                'status' => false,
            ];
        }
    }

    public function checkStatusSms($id)
    {
        return $this->turboSms->checkStatusSms($id);
    }
}

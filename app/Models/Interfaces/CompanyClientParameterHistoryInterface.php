<?php

namespace App\Models\Interfaces;

interface CompanyClientParameterHistoryInterface
{
    const C_COMPANY_ID = 'company_id';
    const C_ID = 'id';
    const C_CLIENT_ID = 'client_id';
    const C_DATE = 'date';
    const C_WEIGHT = 'weight';
    const C_SHOULDER_VOLUME = 'shoulder_volume';
    const C_WAIST = 'waist';
    const C_HIPS = 'hips';
    const C_BICEPS_VOLUME = 'biceps_volume';
    const C_BREAST_VOLUME = 'breast_volume';
    const C_LEG_VOLUME = 'leg_volume';
    const C_CAVIAR_VOLUME = 'caviar_volume';
    const C_COMMENT = 'comment';
    const TABLE_NAME = 'company_client_parameters_history';
}

<?php

namespace App\Repositories\DependedOnRole;

use App\Components\Globals\ValidityValues;
use App\Models\Client;
use App\Models\Company;
use App\Models\CompanySetting;
use App\Models\Subscription;
use PriceFormatter;

/**
 * Абонементы для админа компании
 *
 * Class SubscriptionsCompanyAdminRepository
 * @package App\Repositories
 */
class SubscriptionsCompanyStaffRepository extends SubscriptionsRepository
{
    /**
     * @inheritDoc
     */
    protected function listForSaleByAdmin()
    {
        return Subscription::byCompany($this->company->id)->forStaff()->active()->with(['media'])->orderBy('price')->get();
    }
}

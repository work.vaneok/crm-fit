<?php

namespace App\Rules;

use App\Models\Client;
use App\Models\Subscription;
use App\Repositories\ConfirmSmsCodeRepository;
use Illuminate\Contracts\Validation\Rule;

class SubscriptionOfCompanyRule implements Rule
{
    private $company_id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Subscription::byCompany($this->company_id)->where(['id' => $value])->first() ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Абонемент не относится к этой компании');
    }
}

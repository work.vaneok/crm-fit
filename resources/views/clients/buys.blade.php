<div class="table-responsive">
    <table id="grid-table" class="table  table-hover table-striped">
        <thead>
        <tr>
            <th scope="col">{{ __('Дата') }}</th>
            <th scope="col">{{ __('Операция') }}</th>
            <th scope="col">{{ __('Сумма') }}</th>
            <th scope="col">{{ __('Долг') }}</th>
            <th scope="col">{{ __('Ответственные') }}</th>
            <th scope="col">{{ __('Коментарий') }}</th>
            <th scope="col">{{ __('Статус') }}</th>
            <th scope="col">{{ __('Действия') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr>
                <td>{{$item->formatDateTime('created_at')}}</td>
                <td>{{$item->getOperationName()}}</td>
                <td>{{$item->need_amount}}</td>
                <td>{{$item->current_amount}}</td>
                <td><?php
                    if ($item->createTransaction) {
                        echo __('Создано :', ['?' => $item->createTransaction->user->first_name]);
                    }
                    echo '<br>';
                    if ($item->paidTransaction) {
                        echo __('Оплачено :', ['?' => $item->paidTransaction->user->first_name]);
                    }
                    ?></td>
                <td>{{$item->comment}}</td>
                <td>{{$item->getStatusName()}}</td>
                <td>

                    <?php
                    if($companyClient->balance >= $item->need_amount && $item->needPay()){?>
                    @grid_btn(['route' => route('clients.close-buy', ['buy' => $item->id]),'label' => __('Оплатить'),
                    'class' => 'money-bill'])
                    <?php
                    }

                    ?>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

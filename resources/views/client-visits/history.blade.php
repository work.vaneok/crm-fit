@extends('layouts.dashboard')

@section('title')
    {{ __('Посещения :username', ['username' => $username]) }}
@endsection

@section('content')


    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">{{ __('Дата') }}</th>
                <th scope="col">{{ __('Операция') }}</th>
                <th scope="col">{{ __('Абонемент') }}</th>
                <th scope="col">{{ __('Ответственный') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr class="@if ($item['actionName'] == \App\Constants\VisitActions::VISIT) row-visit @endif">
                    <td>{{ $item['created_at'] }}</td>
                    <td>
                        @if ($item['actionName'] == \App\Constants\VisitActions::VISIT)
                            <i class="fa fa-arrow-right"></i> {{ __('Посещение') }}
                        @else
                            <i class="fa fa-arrow-left"></i> {{ __('Возврат') }}
                        @endif
                    </td>
                    <td>{{ $item['subscription']->title }}</td>
                    <td>
                        {{ $item['createdBy'] }}
                        @if ($item['action_name'] == 'return' && !empty($item['deleted_reason']))
                            <br><i>{{ __('Причина') }}: {{ $item['deleted_reason'] }}</i>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $items ? $items->appends(request()->only('filter'))->links() : '' }}
    </div>
@endsection

@section('actions')

@endsection

@push('css')

@endpush

<?php

namespace App\Presenters;


use App\Models\FirebaseNotification;
use Illuminate\Support\Collection;


class ClientMyNotificationPresenter
{

    public static function present(FirebaseNotification $m): array
    {

        return [
            'title' => $m->title,
            'descriptions' => $m->descriptions,
            'topic_id' => $m->topic_id,
            'created_at' => $m->created_at,
            'updated_at' => $m->updated_at,
            'data' => $m->data,
            'image_url' => $m->image_url
        ];
    }

    public static function presentCollection(Collection $collection): array
    {
        return $collection->map(function (FirebaseNotification $transaction) {
            return self::present($transaction);
        })->all();
    }
}

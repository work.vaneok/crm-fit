<?php

namespace App\Http\Controllers\Actions\ClientVisits;

use App\Components\CrmHelper;
use App\Constants\CompanyStaffRoles;
use App\Http\Controllers\Actions\BaseAction;
use App\Models\ClientVisit;
use App\Models\CompanyClient;
use App\Models\CompanyStaff;
use App\Repositories\DependedOnRole\SubscriptionsRepository;
use Carbon\Carbon;

class ClientVisitsIndexAction extends BaseAction
{
    public $company_id;
    public $company_location_id;

    public $filter_client_id;
    public $filter_subscription_id;
    /*public $filter_location_id;*/
    public $filter_staff_id;
    public $filter_date_from;

    public $filter_date_to;
    public $per_page = 15;

    public $page = 1;
    public $provider;
    public $viewParams;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'company_location_id' => ['integer', 'required'],
            'per_page' => ['integer'],
            'page' => ['integer'],
            'filter_client_id' => ['nullable'],
            'filter_subscription_id' => ['nullable'],
            /*'filter_location_id' => ['nullable'],*/
            'filter_staff_id' => ['nullable'],
            'filter_date_from' => ['nullable'],
            'filter_date_to' => ['nullable'],
        ];
    }

    public function action()
    {
        $subscriptionsRep = app(SubscriptionsRepository::class);

        $this->viewParams['clients'] = ['' => __('поиск по всем клиентам')] + $this->getClientList($this->company_id);
        $this->viewParams['subscriptions'] = ['' => __('поиск по всем абонементам')] + $subscriptionsRep->getListForSearch()->toArray();
        /*$this->viewParams['locations'] = ['' => __('поиск по всем локациям')] + $this->getLocationList($this->company_id);*/
        $this->viewParams['staffs'] = ['' => __('поиск по всем сотрудникам')] + $this->getStaffsList($this->company_id);

        $this->viewParams['items'] = [];
        $this->viewParams['back_url'] = url()->full();

        $this->provider = ClientVisit::query()
            ->where(['company_id' => $this->company_id])
            ->where(['company_location_id' => $this->company_location_id])
            ->with(['location', 'clientSubscription.subscription', 'client', 'createdBy'])
            ->orderByDesc('id');

        if ($this->filter_client_id) {
            $this->provider->where([
                'client_id' => $this->filter_client_id
            ]);
        }

        /*if ($this->filter_location_id) {
            $this->provider->where([
                'company_location_id' => $this->filter_location_id
            ]);
        }*/

        if ($this->filter_staff_id) {
            $this->provider->where([
                'created_by_user_id' => $this->filter_staff_id
            ]);
        }

        /*if ($this->filter_subscription_id) {
            $this->provider->where([
                'client_subscription_id' => $this->filter_subscription_id
            ]);
        }*/

        if ($this->filter_date_from && $this->filter_date_to) {
            $this->provider->whereBetween('created_at', [
                Carbon::parse($this->filter_date_from)->startOfDay()->toDateTimeString(),
                Carbon::parse($this->filter_date_to)->endOfDay()->toDateTimeString()
            ]);
        }

        $this->provider = $this->provider->paginate($this->per_page, ['*'], 'page', $this->page);

        $items = [];

        /** @var ClientVisit $item */
        foreach ($this->provider->items() as $item) {
            $items[$item->id]['id'] = $item->id;

            $items[$item->id]['client']['id'] = $item->client->id;
            $items[$item->id]['client']['first_name'] = $item->client->first_name;
            $items[$item->id]['client']['last_name'] = $item->client->last_name;
            $items[$item->id]['client']['phone'] = $item->client->phone;

            $items[$item->id]['location']['id'] = $item->location->id;
            $items[$item->id]['location']['title'] = $item->location->title;

            $items[$item->id]['clientSubscription']['id'] = $item->clientSubscription->id;
            $items[$item->id]['clientSubscription']['title'] = $item->clientSubscription->subscription->title;

            $items[$item->id]['byUser']['id'] = $item->createdBy->id;
            $items[$item->id]['byUser']['first_name'] = $item->createdBy->first_name;
            $items[$item->id]['byUser']['last_name'] = $item->createdBy->last_name;
            $items[$item->id]['byUser']['phone'] = $item->createdBy->phone;

            $items[$item->id]['key_number'] = $item->key_number ?? '-';
            $items[$item->id]['created_at'] = $item->created_at;
        }

        $this->viewParams['provider'] = $this->provider;
        $this->viewParams['items'] = $items;

    }

    /*private function getLocationList($company_id)
    {
        $response = [];

        $locations = CompanyLocation::query()->where([
            'company_id' => $company_id
        ])->select(['id', 'title'])->get();

        foreach ($locations as $location) {
            $response[$location->id] = $location->title;
        }
        return $response;
    }*/


    private function getClientList($company_id)
    {
        $response = [];

        $clients = CompanyClient::query()
            ->where(['company_id' => $company_id])
            ->with(['client'])
            ->join('clients', 'clients.id', '=', 'company_clients.client_id')
            ->orderByRaw('first_name, last_name')
            ->get();

        foreach ($clients as $client) {
            $response[$client->client_id] = $client->client->first_name . ' ' . $client->client->last_name . ' (' . $client->client->phone . ')';
        }
        return $response;
    }

    public function getStaffsList($company_id)
    {
        $response = [];

        $staffs = CompanyStaff::query()
            ->where(['company_id' => $company_id,])
            ->with(['user'])
            ->get();

        foreach ($staffs as $staff) {
            $role = CompanyStaffRoles::getTitleRoleById($staff->role_in_company);
            $response[$staff->user_id] = $staff->user->first_name . ' ' . $staff->user->last_name . '  ' . $staff->user->phone . ' (' . $role . ')';
        }

        return $response;
    }

}

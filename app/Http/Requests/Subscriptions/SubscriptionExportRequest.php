<?php

namespace App\Http\Requests\Subscriptions;


use App\Http\Requests\Traits\DefaultCompanyLocationIdInRequest;
use Illuminate\Foundation\Http\FormRequest;


class SubscriptionExportRequest extends FormRequest
{
    use DefaultCompanyLocationIdInRequest;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filter.client_id' => ['nullable'],
        ];
    }

}

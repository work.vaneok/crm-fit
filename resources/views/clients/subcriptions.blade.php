<div class="table-responsive">
    <table id="grid-table" class="table  table-hover table-striped">
        <thead>
        <tr>
            <th scope="col">{{ __('Абонемент') }}</th>
            <th scope="col">{{ __('Действителен c/до') }}</th>
            <th scope="col">{{ __('Занятий') }}</th>
            <th scope="col">{{ __('Статус') }}</th>
            <th scope="col">{{ __('Действия') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr class="<?=$item['deleted_at'] ? 'table-danger' : ''?>">
                <td>
                    {{ $item['subscription']['title'] .' #'.$item['subscription_id'] }}
                </td>
                <td>
                    {{ $item['available_from'].' / '.$item['available_to'] }}
                </td>
                <td>
                    {{ $item['quantity_of_occupation']=='-1'?__('Безлемит'): $item['quantity_of_occupation']}}
                </td>
                <td>
                    <?php
                    if ($item['deleted_at']) {
                    ?>
                    <span class="badge badge-danger"><?=__('Удален')?></span>
                    <?php
                    } elseif ($item['active']) {
                    ?>
                    <span class="badge badge-success"><?=__('Активный')?></span>
                    <?
                    }else {
                    ?>
                    <span class="badge badge-secondary"><?=__('Не активный')?></span>
                    <?
                    }

                    ?>
                </td>
                <td>
                    <?php
                    if ($item['deleted_at']) {
                    ?>
                    <a href="<?= route('clients.profile.restore-subscription', ['subscription_id' => $item['subscription_id']])?>" class="btn btn-success btn-sm "
                       role="button" aria-pressed="true"> <i
                            class="fas fa-trash-restore"></i></a>
                    <?

                    } else {
                    ?>
                    <button type="button" class="btn btn-sm btn-primary ajax-buy-button"
                            data-toggle="modal"
                            data-form-url="{{route('clients.profile.update-subscription',
                            [
                                'subscription_id' => $item['subscription_id'],
                            ])}}">
                        <i class="fas fa-edit"></i>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                              style="display: none"></span>
                    </button>
                    <a href="<?= route('clients.profile.delete-subscription', ['subscription_id' => $item['subscription_id']])?>" class="btn btn-danger btn-sm"
                       role="button" aria-pressed="true"> <i
                            class="fas fa-trash"></i></a>
                    <?php
                    }
                    ?>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

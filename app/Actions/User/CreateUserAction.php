<?php
namespace App\Actions\User;

use App\User;
use Hash;
use Str;

class CreateUserAction
{

    /**
     * @param $phone
     * @param $first_name
     * @param $last_name
     * @return User
     * @throws \Exception
     */
    public function execute($phone, $first_name, $last_name)
    {
        return User::firstOrCreate(
            [
                'phone' => $phone,
            ],
            [
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => '',
                'password' => Hash::make(Str::random(4)),
            ]
        );
    }
}

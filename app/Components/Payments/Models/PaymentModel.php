<?php
namespace App\Components\Payments\Models;

class PaymentModel
{
    private $price;
    private $currency;
    private $description;
    private $order_id;
    private $resultUrl;
    private $serverUrl;
    private $companyName;

    public function __construct($companyName, $price, $currency, $description, $order_id, $resultUrl, $serverUrl)
    {
        $this->price = $price;
        $this->currency = $currency;
        $this->description = $description;
        $this->order_id = $order_id;
        $this->resultUrl = $resultUrl;
        $this->serverUrl = $serverUrl;
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getServerUrl()
    {
        return $this->serverUrl;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getGeneratedOrderId()
    {
        return generateOrderId($this->order_id);
    }

    /**
     * @return mixed
     */
    public function getResultUrl()
    {
        return $this->resultUrl;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

}

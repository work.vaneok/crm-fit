<?php

namespace App\Constants;

class CompanyStaffRoles
{
    const OWNER = 1;
    const ADMINISTRATOR = 2;
    const TRAINER = 3;
    const NUTRITIONIST = 4;

    const RoleList = [
        self::OWNER => [
            'title' => 'Владелец'
        ],
        self::ADMINISTRATOR => [
            'title' => 'Администратор'
        ],
        self::TRAINER => [
            'title' => 'Тренер'
        ],
        self::NUTRITIONIST => [
            'title' => 'Диетолог'
        ],
    ];

    public static function getTitleRoleById($id)
    {
        return isset(self::RoleList[$id]) ? self::RoleList[$id]['title'] : '';
    }
}

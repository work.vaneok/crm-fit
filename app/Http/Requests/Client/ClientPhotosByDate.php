<?php

namespace App\Http\Requests\Client;


use App\Helper\ValidatorHelper;
use App\Models\CompanyClient;
use Illuminate\Foundation\Http\FormRequest;


class ClientPhotosByDate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'date' => [
                'required',
                'string'
            ],
            'company_id' => [
                'required',
                'integer',
                ValidatorHelper::createExists(CompanyClient::TableName, CompanyClient::COLUMN_COMPANY_ID)
            ],
            'client_id' => [
                'required',
                'integer',
                ValidatorHelper::createExists(CompanyClient::TableName, CompanyClient::COLUMN_CLIENT_ID)
            ],

        ];
    }


}


<form action="{{ $route }}" method="POST" style="display: inline" data-ajax="true">
    @method('PATCH')
    @csrf

    <button style="margin-left: 5px" type="submit" class="btn btn-sm  {{ $btn ?? '' }} btn-outline-danger delete-btn">  <i class="fa fa-trash"></i>  {{'Уволить!'}}</button>
</form>

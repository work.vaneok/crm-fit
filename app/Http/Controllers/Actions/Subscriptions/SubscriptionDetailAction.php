<?php

namespace App\Http\Controllers\Actions\Subscriptions;

use App\Http\Controllers\Actions\BaseAction;
use App\Models\ClientSubscription;
use App\Models\CompanyClient;
use App\Models\Order;
use App\Models\Subscription;
use View;

class SubscriptionDetailAction extends BaseAction
{
    public $subscription_id;

    public $response;

    public function rules(): array
    {
        return [
            'subscription_id' => ['integer', 'required'],
        ];
    }

    public function action()
    {
        $order = Subscription::findOrFail($this->subscription_id);

        $this->response = [
            'title' => __("Детальная страница абонемента"),
            'content' => View::make('subscriptions.order-detail', [
                    'subscription' => $order->toArray(),
                ]
            )->render()
        ];
    }

}

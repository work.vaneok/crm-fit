<?php
namespace App\Http\Requests\Client;


use App\Rules\PhoneFormatRule;
use Illuminate\Foundation\Http\FormRequest;

class FindClientByPhoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => ['required', new PhoneFormatRule()],
        ];
    }
}

<div class="card shadow" style="height: 160px">
    <div class="card__left">
        <img src="{{asset('images/ticket.svg')}}">
    </div>
    <div class="card__right">
        <div class="card__title">{{$title}}</div>
        <div class="card__items">
            <div class="card__item">
                <i class="icon-label"></i>
                {{$count}}
            </div>
            <div class="card__item total">
                <i class="icon-card"></i>
                {{$price}} грн
            </div>
        </div>
    </div>
</div>

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyLocationsApiCollection extends ResourceCollection
{
    public $collects = CompanyLocationApiResource::class;

}

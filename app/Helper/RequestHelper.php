<?php

namespace App\Helper;


class RequestHelper
{
    const SERVER_RESPONSE = 'response';
    const SERVER_RESPONSE_OK = 'OK';
    const SERVER_RESPONSE_ERROR = 'ERROR';
    const SERVER_VALIDATE_ERROR = 'VALIDATE_ERROR';
    const SERVER_NEED_UPDATE = 'NEED_UPDATE';

    const CLIENT_ERROR_STATUS = 400;
    const NO_CONTENT_STATUS = 204;
    const FORBIDDEN_STATUS = 403;
    const RESOURCE_NOT_FOUND_STATUS = 404;
    const RESOURCE_CREATED_STATUS = 201;
    const SERVER_RESPONSE_OK_CODE = 200;
    const UNAUTHENTICATED_STATUS = 401;
    const SERVER_ERROR = 500;

}

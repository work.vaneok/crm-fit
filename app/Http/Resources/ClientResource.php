<?php

namespace App\Http\Resources;

use App\Components\Globals\ValidityValues;
use App\Components\PriceFormatter;
use App\Models\Client;
use App\Models\ClientSubscription;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this Client */
        return [
            'client' => [
                'id' => $this->id,
                'username' => $this->getUserName(),
            ],
            'subscriptions' => ClientSubscriptionResource::collection($this->activeSubscriptionsV1),
        ];
    }
}

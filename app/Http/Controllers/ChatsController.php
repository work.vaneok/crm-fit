<?php

namespace App\Http\Controllers;

use App\Actions\Chat\ClientGlobalChatAction;
use App\Actions\Chat\SendLikeUserAction;
use App\Components\CrmHelper;
use App\Http\Requests\Chat\ClientGlobalRequest;
use App\Http\Requests\Chat\SendLikeUserRequest;


class ChatsController extends Controller
{


    public function clientGlobalChat(ClientGlobalRequest $request)
    {
        try {
            $action = new ClientGlobalChatAction(array_merge($request->all(), [
                'company_id' => CrmHelper::companyId(),
                'user_id' => $request->user()->id
            ]));
            $action->execute();
            return view('chat.global', [
                'room_id' => $action->resp_chat_room_id,
                'by_id' => $action->resp_chat_by_id,
                'client' => $action->resp_chat_with_client,
                'current_url' => $request->current_url ?? route('clients.index')
            ]);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . ' ' . $exception->getTraceAsString());
            echo '</pre>';
            die;
        }
    }

    public function sendLikeUser(SendLikeUserRequest $request)
    {
        try {
            $action = new SendLikeUserAction(array_merge($request->all(), [
                'user_id' => $request->user()->id,
                'company_id' => CrmHelper::companyId(),

            ]));
            $action->execute();
            return $this->createSuccessResponse();
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . ' ' . $exception->getTraceAsString());
            echo '</pre>';
            die;
        }
    }

}

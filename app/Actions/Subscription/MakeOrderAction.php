<?php
namespace App\Actions\Subscription;

use App\Models\Order;
use Carbon\Carbon;

class MakeOrderAction
{
    /**
     * @param $order_item_type
     * @param $company_id
     * @param $location_id
     * @param $client_id
     * @param $price
     * @param $order_item_id
     * @param $order_item_info
     * @param $payment_source
     * @return Order
     * @throws \Exception
     */
    public function execute($order_item_type, $company_id, $location_id, $client_id, $price, $order_item_id, $order_item_info, $payment_source)
    {
        $order = Order::create([
            'order_item_type' => $order_item_type, //'subscription',
            'company_id' => $company_id, //$data['company_id'],
            'company_location_id' => $location_id, //$data['company_location_id'],
            'client_id' => $client_id, //$data['client_id'],
            'price' => $price, //$subscription->price,
            'order_item_id' => $order_item_id, //$subscription->id,
            'order_item_info' => $order_item_info, //$subscription->toArray(),
            'payment_source' => $payment_source, //$data['payment_source'],

            //'payed_at' => Carbon::now(),
            //'payed_by' => \Auth::id(),
        ]);

        if (!$order) {
            throw new \Exception('Order was not created');
        }

        return $order;
    }
}

<?php

namespace App\Http\Controllers;

use App\Constants\Roles;
use App\DependedGetters\SoldSubscriptionsGetter;
use App\Http\Requests\FilterRequest;
use App\Repositories\DependedOnRole\SubscriptionsRepository;
use App\Repositories\OrderStatusRepository;
use Auth;
use Illuminate\Contracts\Support\Renderable;
use PriceFormatter;

class SoldSubscriptionsController extends Controller
{
    /**
     *
     * @return Renderable
     */
    public function index(FilterRequest $request)
    {
        $items = SoldSubscriptionsGetter::getList();

        /** @var SubscriptionsRepository $subscriptionsRep */
        $subscriptionsRep = app(SubscriptionsRepository::class);

        /** @var OrderStatusRepository $orderStatusRep */
        $orderStatusRep = app(OrderStatusRepository::class);

        $totals['price'] = PriceFormatter::withCurrency($items->getCollection()->sum('price'));

        return view('sold-subscriptions.index', [
            'filters' => [
                'date_range' => true,//Auth::user()->hasRole(Roles::COMPANY_OWNER),
            ],
            'items' => $items,
            'totals' => $totals,
            'subscriptions' => $subscriptionsRep->getListForSearch(),
            'statuses' => $orderStatusRep->getForSelectBox(),
        ]);
    }



}

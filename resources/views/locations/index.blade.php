@extends('layouts.dashboard')

@section('title')
    {{ __('Локации') }}
@endsection

@section('content')

    @filter_form(['fields' => [['name' => 'id', 'label' => 'ID'], ['name' => 'title', 'label' => __('Наименование')]]])

    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">{{ __('Наименование') }}</th>
                <th scope="col">{{ __('Адрес') }}</th>
                <th scope="col">{{ __('Дата создания') }}</th>
                <th scope="col">{{ __('Дата обновления') }}</th>
                <th scope="col">{{ __('Действия') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($locations as $item)
                <tr url="{{route('locations.show',['location' => $item->id])}}">
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->address }}</td>
                    <td>{{ $item->formatDate('created_at') }}</td>
                    <td>{{ $item->formatDate('updated_at') }}</td>
                    <td>
                        @edit(['route' => route('locations.edit',['location' => $item->id, 'backUrl' => url()->full()])])&nbsp;
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $locations ? $locations->appends(request()->only('filter'))->links() : '' }}
    </div>
@endsection

@section('actions')
    @if (Auth::user()->can('create', \App\Models\CompanyLocation::class))

        <li>
            @add(['route' => route('locations.create')])
        </li>
        {{--<div class="btn-toolbar mb-2 mb-md-0">--}}
            {{--@add(['route' => route('locations.create')])--}}
        {{--</div>--}}
    @endif
@endsection

@push('css')

@endpush

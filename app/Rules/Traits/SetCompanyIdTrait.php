<?php
namespace App\Rules\Traits;


trait SetCompanyIdTrait
{
    private $company_id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($company_id)
    {
        $this->company_id = $company_id;
    }

}

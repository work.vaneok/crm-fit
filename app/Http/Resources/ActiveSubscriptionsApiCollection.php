<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ActiveSubscriptionsApiCollection extends ResourceCollection
{
    public $collects = ActiveSubscriptionsApiResource::class;

}

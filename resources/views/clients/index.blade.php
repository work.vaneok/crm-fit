@extends('layouts.dashboard')

@section('title')
    {{ __('Клиенты') }}
    @if (count($dates))
        {{ __('за') }} {{ implode(' - ', $dates) }}
    @endif
@endsection

@section('content')
    <div class="header__filter" style=" margin-bottom: 15px;">
        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#filter"
                style="margin-left: 0;">{{ __('Фильтр') }}</button>
        <div id="filter" class="collapse col-lg-12" style="margin-top: 15px; padding-left: 0px;">
            <form action="" class="grid-filter" method="get" id="filterForm">
                @php($errMessages = [])
                @error('filter.date')
                @php($errMessages[] = $message)
                @enderror
                @error('filter.date_from')
                @php($errMessages[] = $message)
                @enderror
                @error('filter.date_to')
                @php($errMessages[] = $message)
                @enderror

                @if(count($errMessages))
                    <div class="alert alert-danger">{!! implode('<br>', $errMessages) !!}</div>
                @endif
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="filter[username]">{{ __('Имя клиента') }}</label>
                        <input class="form-control " type="text" value="{{get_filter_value('username')}}" name="filter[username]" placeholder="{{ __('Имя клиента') }}">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="filter[phone]">{{  __('Телефон') }}</label>
                        <input class="form-control " type="text" value="{{get_filter_value('phone')}}" name="filter[phone]" placeholder="{{ __('Номер телефона') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="filter[phone]">{{ __('по Дате') }}</label>
                        @if($filters['date_range'])
                            @search_date_range(['name' => 'date'])
                        @else
                            @search_date(['value' => (get_filter_value('date') ??
                            \Carbon\Carbon::now()->format('d.m.Y')), 'name' => 'date'])
                        @endif
                    </div>

                </div>
                <div class="form-row">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary "><i
                                class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                    </div>
                    <div class="form-group">
                        <a href="/{{ Route::current()->uri }}" class="btn btn-danger "><i
                                class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{--<form id="filterForm" action="" class="grid-filter" method="get" >
        @php($errMessages = [])
        @error('filter.date')
        @php($errMessages[] = $message)
        @enderror
        @error('filter.date_from')
        @php($errMessages[] = $message)
        @enderror
        @error('filter.date_to')
        @php($errMessages[] = $message)
        @enderror

        @if(count($errMessages))
            <div class="alert alert-danger">{!! implode('<br>', $errMessages) !!}</div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <input class="form-control form-control-sm" type="text" value="{{get_filter_value('username')}}" name="filter[username]" placeholder="{{ __('Имя клиента') }}">
            </div>

            <div class="col-auto">
                <input class="form-control form-control-sm" type="text" value="{{get_filter_value('phone')}}" name="filter[phone]" placeholder="{{ __('Номер телефона') }}">
            </div>

            <div class="col-auto">
                @if($filters['date_range'])
                    @search_date_range(['name' => 'date'])
                @else
                    @search_date(['value' => (get_filter_value('date') ?? \Carbon\Carbon::now()->format('d.m.Y')), 'name' => 'date'])
                @endif
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                <a href="/{{ Route::current()->uri }}" class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
            </div>
        </div>
    </form>--}}

    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">{{ __('Имя') }}</th>
                <th scope="col">{{ __('Телефон') }}</th>
                <th scope="col">{{ __('Создан') }}</th>
                <th scope="col">{{ __('Последнее посещение') }}</th>
                <th scope="col">{{ __('Действия') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td><a href="{{route('clients.profile',['client' => $item['client_id']])}}">{{ $item['username'] }}</a></td>
                    <td>{{ $item['phone'] }}</td>
                    <td>{{ $item['created_at'] }}</td>
                    <td>{{ $item['last_visited_at'] }}</td>
                    <td>
                        <a href="<?= route('client-visit.history', ['companyClient' => $item['client_id']])?>"
                           class="btn btn btn-sm "
                           role="button" aria-pressed="true"> <i
                                class="fas fa-history"></i></a>

                        <a href="<?= route('sold-subscriptions.index', ['filter[phone]' => $item['phone']])?>"
                           class="btn btn btn-sm "
                           role="button" aria-pressed="true"> <i
                                class="fa fa-shopping-cart"></i></a>

                        <a href="<?= route('chat.client-global-chat', ['client_id'=>$item['client_id'], 'current_url'=>url()->current()])?>"
                           class="btn btn btn-sm "
                           role="button" aria-pressed="true"> <i
                                class="fas fa-comments"></i></a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $items ? $items->appends(request()->only('filter'))->links() : '' }}
    </div>
@endsection
@section('actions')
        <li>
            @add(['route' => route('clients.create')])
        </li>
@endsection

@push('css')

@endpush

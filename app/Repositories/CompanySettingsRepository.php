<?php
namespace App\Repositories;

use App\Components\Globals\CompanyRoleValues;
use App\Components\Globals\ValidityValues;
use App\Models\Company;
use App\Models\CompanySetting;

/**
 * Настройки компании
 *
 * Class CompanySettingsRepository
 * @package App\Repositories
 */
class CompanySettingsRepository
{
    /**
     * @var Company
     */
    private $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getListForForm()
    {
        $settings = collect(config('company_settings'));
        $savedSettings = CompanySetting::byCompany($this->company->id)->get()->mapWithKeys(function ($item) {
            return [
                $item->settings_name => $item->settings_value
            ];
        });

        return $settings->merge($savedSettings);
    }

}

<?php

namespace App\Http\Controllers;

use App\Actions\Subscription\CreateSubscriptionAction;
use App\Actions\Subscription\SaleSubscriptionToClientByStaffAction;
use App\Actions\Subscription\UpdateSubscriptionAction;
use App\Components\CrmHelper;
use App\DependedGetters\SubscriptionsGetter;
use App\Http\Controllers\Actions\Subscriptions\ClientSubscriptionsAction;
use App\Http\Controllers\Actions\Subscriptions\ExportExcelAction;
use App\Http\Controllers\Actions\Subscriptions\OrderDetailAction;
use App\Http\Controllers\Actions\Subscriptions\StaffSubsAction;
use App\Http\Controllers\Actions\Subscriptions\SubscriptionDetailAction;
use App\Http\Controllers\Traits\MediaAssetTrait;
use App\Http\Requests\Subscriptions\ClientSubscriptionsRequest;
use App\Http\Requests\Subscriptions\CreateSubscriptionRequest;
use App\Http\Requests\Subscriptions\OrderDetailRequest;
use App\Http\Requests\Subscriptions\SaleByStaffRequest;
use App\Http\Requests\Subscriptions\StaffSubsRequest;
use App\Http\Requests\Subscriptions\SubscriptionDetailRequest;
use App\Http\Requests\Subscriptions\UpdateSubscriptionRequest;
use App\Models\ClientSubscription;
use App\Models\Subscription;
use App\Repositories\SubscriptionCancellationRepository;
use App\Repositories\ValidityRepository;
use Illuminate\Http\Request;


class SubscriptionsController extends Controller
{
    use MediaAssetTrait;

    /**
     * @var ValidityRepository
     */
    private $validityRep;

    /**
     * @var SubscriptionCancellationRepository
     */
    private $subscriptionCancellationRep;

    public function __construct()
    {
        $this->validityRep = app(ValidityRepository::class);
        $this->subscriptionCancellationRep = app(SubscriptionCancellationRepository::class);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptions = SubscriptionsGetter::getList();

        return view('subscriptions.index', compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subscriptions.create', [
            'validity_values' => $this->validityRep->getForSelectBox(),
            'cancellation_value' => $this->subscriptionCancellationRep->getForSelectBox()
        ]);
    }

    /**
     * @param CreateSubscriptionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\MediaCannotBeDeleted
     */
    public function store(CreateSubscriptionRequest $request)
    {
        $subscription = (new CreateSubscriptionAction())->execute($request->validated());

        $this->addMediaAssets($request, $subscription);

        return $this->redirect(route('subscriptions.index'));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        return view('subscriptions.show', compact('subscription'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        $validity_values = $this->validityRep->getForSelectBox();
        $cancellation_value = $this->subscriptionCancellationRep->getForSelectBox();

        if ($subscription->old_price == 0) {
            $subscription->old_price = null;
        }
        if ($subscription->quantity_of_occupation == -1) {
            $subscription->quantity_of_occupation = null;
        }

        return view('subscriptions.edit', compact('subscription', 'validity_values', 'cancellation_value'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\MediaCannotBeDeleted
     */
    public function update(UpdateSubscriptionRequest $request, Subscription $subscription)
    {
        (new UpdateSubscriptionAction($subscription))->execute($request->validated());
        $this->addMediaAssets($request, $subscription);

        return $this->redirect(route('subscriptions.index'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Subscription $subscription)
    {
        $subscription->delete();

        return redirect()->route('subscriptions.index');
    }


    public function saleByStaff(SaleByStaffRequest $request)
    {
        /** @var ClientSubscription $clientSubscription */

        $clientSubscription = (new SaleSubscriptionToClientByStaffAction())->execute($request->all());
        $backUrl = $request->get('back');

        if ($backUrl) {
            $backUrl = getBackUrl($backUrl, ['barcode' => $clientSubscription->barcode]);
        } else {
            $backUrl = '/';
        }

        return $this->response(['id' => $clientSubscription->id, 'barcode' => $clientSubscription->barcode, 'back' => $backUrl]);
    }


    public function clientSubscriptions(ClientSubscriptionsRequest $request)
    {
        try {
            $action = new ClientSubscriptionsAction([
                'company_id' => CrmHelper::companyId(),
                'company_location_id' => CrmHelper::companyLocationId(),
                'page' => $request->page,
                'filter_client_id' => $request->input('filter.client_id'),
                'filter_date_from' => $request->input('filter.date_from'),
                'filter_date_to' => $request->input('filter.date_to'),
            ]);
            $action->execute();

            return view('subscriptions.clients', $action->viewParams);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . $exception->getTraceAsString());
            echo '</pre>';
            die;

            return redirect()->route('home');
        }
    }

    public function orderDetail(OrderDetailRequest $request)
    {
        try {
            //subscriptions.order-detail
            //subscriptions.subscription-detail
            $action = new OrderDetailAction([
                'order_id' => $request->order_id
            ]);
            $action->execute();
            return $this->createSuccessResponse($action->response);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . ' ' . $exception->getTraceAsString());
            echo '</pre>';
            die;
        }
    }

    public function subscriptionDetail(SubscriptionDetailRequest $request)
    {
        try {
            $action = new SubscriptionDetailAction([
                'subscription_id' => $request->subscription_id
            ]);
            $action->execute();
            return $this->createSuccessResponse($action->response);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . ' ' . $exception->getTraceAsString());
            echo '</pre>';
            die;
        }
    }

    public function export(Request $request)
    {
        $action = new ExportExcelAction([
            'filter_client_id' => $request->input('filter.client_id'),
            'company_id' => CrmHelper::companyId(),
            'company_location_id' => CrmHelper::companyLocationId(),
            'filter_date_from' => $request->input('filter.date_from'),
            'filter_date_to' => $request->input('filter.date_to')
        ]);
        return $action->execute();
    }

    public function actionStaffSubs(StaffSubsRequest $request)
    {
        try {
            $action = new StaffSubsAction([
                'company_id' => CrmHelper::companyId(),
                'user_id' => $request->user()->id,
                'page' => $request->page,
                'filter_client_id' => $request->input('filter.client_id'),
                'filter_date_from' => $request->input('filter.date_from'),
                'filter_date_to' => $request->input('filter.date_to')
            ]);
            $action->execute();
            return view('subscriptions.staff-clients', $action->viewParams);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . $exception->getTraceAsString());
            echo '</pre>';
            die;

            return redirect()->route('home');
        }
    }

}

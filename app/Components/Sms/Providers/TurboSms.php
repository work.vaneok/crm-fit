<?php

namespace App\Components\Sms\Providers;

class TurboSms
{

    private $auth_arr = Array(
        'login' => '',
        'password' => ''
    );
    private $options;
    const soap_gate = 'http://turbosms.in.ua/api/wsdl.html';

    const gate_encoding = 'utf-8';

    private $client_encoding = 'windows-1251';
    private $sender = 'SoapClient';

    private $client;
    public $connected = false;
    public $result_text;
    public $sms_id;

    function __construct($options)
    {
        if (!isset($options['login']) || !isset($options['password']) || !isset($options['sender']) ) {
            throw new \Exception("login OR password OR sender are empty");
        }

        if (!function_exists('iconv')) {
            throw new \Exception('ICONV not installed (http://www.php.net/manual/en/iconv.requirements.php)');
        }
        if (!class_exists('SoapClient')) {
            throw new \Exception('SOAP not installed (http://www.php.net/manual/en/soap.installation.php)');
        }
        $this->options = $options;

    }


    private function connect()
    {
        if ($this->connected) return true;
        $this->client = new \SoapClient (self::soap_gate);
        if (!$this->client) throw new \Exception('Cannot create new Soap client to ' . self::soap_gate);
        $result = $this->client->Auth([
            'login' => $this->options['login'],
            'password' => $this->options['password']
        ]);
        $this->result_text = $this->conv_to_client($result->AuthResult);
        $this->connected = $result->AuthResult == 'Вы успешно авторизировались';
        if (!$this->connected) throw new \Exception('Failed to Auth on Gate: ' . $this->result_text);
        return $this->connected;
    }


    private function conv_to_client($text)
    {
        return $text;
        return iconv(self::gate_encoding, $this->client_encoding, $text);
    }

    private function conv_to_gate($text)
    {
        return $text;
        return iconv($this->client_encoding, self::gate_encoding, $text);
    }

    function bill()
    {
        $result = $this->client->GetCreditBalance();
        return $result->GetCreditBalanceResult;
    }

    function sendSmsUseSoap($phone, $text)
    {
        $this->connect();
        if (!$this->connected) return false;

        $sms = Array(
            'sender' => $this->options['sender'],
            'destination' => $phone,
            'text' => $this->conv_to_gate($text)/*,
			'wappush' => 'http://realt5000.com.ua'*/
        );

        $SendSMSResult = $this->client->SendSMS($sms);

        $this->result_text = $this->conv_to_client(implode('; ', $SendSMSResult->SendSMSResult->ResultArray));

        if ($SendSMSResult->SendSMSResult->ResultArray[0] == 'Сообщения успешно отправлены') {
            $this->sms_id = $SendSMSResult->SendSMSResult->ResultArray[1];

            return [
                'smsId' => $this->sms_id,
                'status' => '1',
                'msg' => 'Отправленно',
                'by' => 'Turbo',
            ];


        } else {
            $this->sms_id = null;
            throw new \Exception('Failed to send sms throw soap gate: ' . $this->result_text . ' (details: http://turbosms.ua/soap.html)');
        }

    }

    public function checkStatusSms($smsId)
    {
        $this->connect();
        if (!$this->connected) return [
            'status' => false
        ];
        $status = $this->client->GetMessageStatus(['MessageId' => $smsId]);

        return [
            'status' => true,
            'msg' => $status->GetMessageStatusResult
        ];


    }

    public function sendSmsUseSql($phone, $text, $time = false)
    {

        try {
            $pdo = new PDO ('mysql:host=' . $this->options['host'] . ';dbname=' . $this->options['dbname'], $this->options['login'], $this->options['password']);
            $pdo->query("SET NAMES utf8;");
            if ($time == false) {
                $stmt = $pdo->prepare("INSERT INTO `:login` (`number`,`message`,`sign`) VALUES (:phone,:text,:sign)");
                $stmt->bindParam(':login', $this->login);
                $stmt->bindParam(':phone', $phone);
                $stmt->bindParam(':text', $text);
                $stmt->bindParam(':sign', $this->sender);
                $stmt->execute();
            } else {
                $stmt = $pdo->prepare("INSERT INTO `:login` (`number`,`message`,`sign`,`send_time`) VALUES (:phone,:text,:sign,:time)");
                $stmt->bindParam(':login', $this->login);
                $stmt->bindParam(':phone', $phone);
                $stmt->bindParam(':text', $text);
                $stmt->bindParam(':time', $time);
                $stmt->bindParam(':sign', $this->sender);
                $stmt->execute();
            }

            return [
                'smsId' => 'Нет',
                'status' => '1',
                'msg' => 'Отправленно',
                'by' => 'Turbo',
            ];


        } catch (PDOException $e) {
            return [
                'status' => '0',
                'msg' => $e->getMessage(),
            ];

        }

    }


}

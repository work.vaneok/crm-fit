<?php
namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait IsActiveScopeTrait
{
    public function scopeActive(Builder $q, $is_active = true)
    {
        $q->where(['is_active' => $is_active]);
    }

    public function getActiveAttribute()
    {
        return $this->is_active == 1;
    }
}

@extends('layouts.dashboard')

@section('content')
    <div class="cash">
        <div class="cash__controls">
            <a href="{{route('clients.create')}}" class="button button-blue">
                Добавить клиента <i class="icon-add-user"></i>
            </a>
            <a href="{{route('sale.subscription')}}" class="button button-green">
                Продать абонемент <i class="icon-cart"></i>
            </a>
            <a href="{{route('client-visit.check')}}" class="button button-yellow">
                Добавить посещение <i class="icon-gym"></i>
            </a>
        </div>
        <div class="cash__info">
            <div>
                <div class="cash__title">
                    {{__('Наличная:')}} <span>{{$cashSubscriptions['total_price']}} грн</span>
                </div>
                <div class="cash__subtitle">Проданных абонементов</div>
                @foreach($cashSubscriptions['items'] as $item)
                    @cash_card(['title' => $item['title'], 'count' => $item['count'], 'price' => $item['price']])
                @endforeach

                {{--@cash_card(['title' => 'Месячные', 'count' => 150, 'price' => '14 500грн'])--}}
                {{--@cash_card(['title' => 'Месячные', 'count' => 150, 'price' => '14 500грн'])--}}
            </div>
            <div>
                <div class="cash__title">
                    {{__('Безналичная:')}} <span>{{$cashlessSubscriptions['total_price']}} грн</span>
                </div>
                <div class="cash__subtitle">Проданных абонементов</div>

                @foreach($cashlessSubscriptions['items'] as $item)
                    @cash_card(['title' => $item['title'], 'count' => $item['count'], 'price' => $item['price']])
                @endforeach

            </div>
        </div>


        <div class="visits shadow active">
            <div class="visits__left">
                <img src="{{asset('images/visits.svg')}}">
                <div class="value"><span>{{$visitsToday}}</span></div>
            </div>

            <div class="visits__right">
                <div class="text">
                    Посещения<br>за текущий день:
                </div>

                <div class="value">
                    <span>{{$visitsToday}}</span> человек
                </div>
            </div>
        </div>
    </div>



    {{--<section class="content-header">--}}
    {{--<h1>--}}
    {{--{{ $header ?? '' }}--}}
    {{--<small> {{ $description ?? '' }} </small>--}}
    {{--</h1>--}}
    {{--</section>--}}
    {{--<section class="content">--}}
    {{--{!! $content !!}--}}
    {{--</section>--}}
@endsection

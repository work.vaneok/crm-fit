<div>
    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">{{ __('ID') }}</th>
                <th scope="col">{{ __('Клиент') }}</th>
                <th scope="col">{{ __('OS') }}</th>
                <th scope="col">{{ __('Version') }}</th>
                <th scope="col">{{ __('Статус') }}</th>
                <th scope="col">{{ __('Ошибка') }}</th>

            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>{{ $item['id'] }}</td>
                    <td>{{  $item['client']['full_name']}}</td>
                    <td>{{  $item['device_os']}}</td>
                    <td>{{  $item['device_os_version']}}</td>
                    <td>{{ $item['status']   }}</td>
                    <td>{{ $item['data'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

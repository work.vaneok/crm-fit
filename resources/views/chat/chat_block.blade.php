<div class="fixed-top d-flex p-1 border-bottom bg-light">
<!--    <div class="p-2">
        <img src="/images/default-avatar.png" class="rounded-circle mr-1"
             alt="Sharon Lessman" width="48" height="48">
    </div>-->
    <div class="p-2">
        <strong>{{$client}}</strong>
        {{--<div>
            <em class="text-muted small">Печатает...</em>
        </div>--}}
    </div>
    <div class="p-2 ml-auto">
        <form action="{{$current_url}}">
            <button type="submit" class="btn btn-danger border btn-lg px-3">
                <i class="fas fa-times"></i>
            </button>
        </form>
    </div>
</div>

<div class="p-4" id="chat-messages" style="margin-top:94px;margin-bottom:54px">
    {{--<div class="d-flex justify-content-start flex-row-reverse py-2">
        <div class="center-block align-self-top">
            <img src="/images/default-avatar.png"
                 class="rounded-circle mr-1" alt="Скульптор Тела (Вы)" width="48" height="48">
        </div>
        <div class="flex-shrink-1 bg-white rounded py-2 px-3 mr-3">
            <div class="font-weight-bold mb-1">Скульптор Тела (Вы)</div>
            Lorem ipsum dolor sit amet, vis erat denique in, dicunt prodesset te vix.
            <div class="text-muted small text-right mb-1">19:05</div>
        </div>
    </div>--}}
    {{--<div class="d-flex justify-content-start flex-row py-2">
        <div class="center-block align-self-top">
            <img src="/images/default-avatar.png"
                 class="rounded-circle mr-1" alt="{{$client}}" width="48" height="48">
        </div>
        <div class="flex-shrink-1 bg-dark text-white rounded py-2 px-3 ml-3">
            <div class="font-weight-bold mb-1">{{$client}}</div>
            Sit meis deleniti eu, pri vidit meliore docendi ut, an eum erat animal commodo.
            <div class="text-muted small text-right mb-1">19:05</div>
        </div>
    </div>--}}
</div>

<div class="fixed-bottom py-2 px-2 bg-light">
    <div class="input-group">
        <div class="flex-grow-1 bd-highlight" contenteditable=true>
            <textarea id="chat-message" rows="1" class="form-control" placeholder="Введите сообщение..."
                      style="min-height: 38px; max-height: 128px"></textarea>
        </div>
        <div class="ml-auto bd-highlight align-self-end">
            <button class="btn btn-primary btn-send-message" style="width:64px;" id="btn-send-message">
                <i class="fas fa-paper-plane"></i>
            </button>
        </div>
    </div>
</div>

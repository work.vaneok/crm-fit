<?php

namespace App\Components\Firebase;

use App\Forms\FirebaseNotificationForm;
use App\Models\ClientDevice;
use App\Models\FirebaseNotification;
use App\Models\FirebaseNotificationReceiver;
use Illuminate\Support\Facades\DB;
use Kreait\Firebase\Messaging\CloudMessage;

class FirebaseFirestoreService
{
    private $firebase;

    public function __construct()
    {
        $this->firebase = app('firebase.firestore')->database();
    }

    public function getFirestoreDb()
    {
        return $this->firebase;
    }

}

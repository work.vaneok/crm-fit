<?php
namespace App\Http\Requests\CompanyNews;

use App\Components\CrmHelper;
use App\Http\Requests\Traits\DefaultCompanyIdInRequest;
use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyNewsRequest extends FormRequest
{
    use DefaultCompanyIdInRequest;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required'],
            'title' => ['required'],
            'description' => ['nullable'],
            'content' => ['required'],
            'image' => ['image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
            'is_show_in_mobile_slider' => ['bool'],
        ];
    }


}

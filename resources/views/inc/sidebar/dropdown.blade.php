<li class="{{__is_active($segment) ? 'active' : ''}}">
    <a href="#homeSubmenu" data-toggle="collapse" class="dropdown-button" aria-expanded="{{__is_active($segment)}}">
        <i class="fas fa-home"></i>
        <span>{{$text}}</span>
        <i class="icon-down"></i>
    </a>
    <ul class="collapse list-unstyled {{__is_active($segment) ? 'show' : ''}}" id="homeSubmenu">
        @foreach($items as $item)
            @sidebar_item($item)
        @endforeach
    </ul>
</li>
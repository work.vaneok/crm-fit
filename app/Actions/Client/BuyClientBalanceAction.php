<?php

namespace App\Actions\Client;

use App\Forms\BuyClientBalanceForm;
use App\Helper\CompanyClientBuyStatusHelper;
use App\Helper\TransactionHelper;
use App\Models\Client;
use App\Models\CompanyClientBuy;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class BuyClientBalanceAction
{
    /**
     * @param $data
     * @return Client|\Illuminate\Database\Eloquent\Model
     */
    public static function execute(BuyClientBalanceForm $form)
    {
        try {
            DB::beginTransaction();
        } catch (\Exception $exception) {
            throw $exception;
        }

        try {

            $transaction = new Transaction([
                Transaction::COLUMN_TYPE_ID => TransactionHelper::TRANSACTION_BUY_TYPE_ID,
                Transaction::COLUMN_TOTAL_AMOUNT => $form->getNeedAmount(),
                Transaction::COLUMN_CREATED_BY_USER_ID => $form->getCreatedBy()
            ]);

            if (!$transaction->save()) {
                throw new \Exception(json_encode($transaction->errors));
            }

            $clintBuy = new CompanyClientBuy([
                CompanyClientBuy::COLUMN_COMPANY_ID => $form->getCompanyId(),
                CompanyClientBuy::COLUMN_CLIENT_ID => $form->getClientId(),
                CompanyClientBuy::COLUMN_COMMENT => $form->getComment(),
                CompanyClientBuy::COLUMN_NEED_AMOUNT => $form->getNeedAmount(),
                CompanyClientBuy::COLUMN_CURRENT_AMOUNT => $form->getNeedAmount(),
                CompanyClientBuy::COLUMN_CREATE_TRANSACTION_ID => $transaction->id,
                CompanyClientBuy::COLUMN_STATUS_ID => CompanyClientBuyStatusHelper::STATUS_NEW_ID,
                CompanyClientBuy::COLUMN_TYPE_ID => CompanyClientBuyStatusHelper::TYPE_BUY_TYPE_ID
            ]);

            if (!$clintBuy->save()) {
                throw new \Exception(json_encode($clintBuy->errors));
            }

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

    }
}

@extends('layouts.dashboard')

@section('title')
    {{ __('Профайл') }}
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->multipart()->route('profile.update', ['user' => $user->id])->put()->fill($user) !!}

        {!! Form::text('first_name', 'Имя') !!}
        {!! Form::text('last_name', 'Фамилия') !!}
        {!! Form::text('phone', 'Телефон')->disabled() !!}

        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit(__('Сохранить изменения')) !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
    <hr>
    <div class="content-m-t">
        <h4>Сбросить пароль</h4>
        {!! Form::open()->multipart()->route('profile.update-password', ['user' => $user->id])->put() !!}

        {!! Form::text('password', 'Пароль')->type('password') !!}
        {!! Form::text('password_confirmation', 'Повтор пароля')->type('password') !!}

        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit(__('Сохранить новый пароль')) !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

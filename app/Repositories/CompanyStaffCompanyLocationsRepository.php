<?php
namespace App\Repositories;

use App\Components\Globals\CompanyRoleValues;
use App\Models\CompanyStaffCompanyLocation;

/**
 * Роли в компании
 *
 * Class CompanyRolesRepository
 * @package App\Repositories
 */
class CompanyStaffCompanyLocationsRepository
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function getForSelectBox()
    {
        return collect(
            CompanyStaffCompanyLocation::get()
        );
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getForSelectBoxForStaff()
    {
        return collect(
            CompanyRoleValues::get()
        );
    }
}

<?php
namespace App\Components\Globals;

class ValidityValues
{
    private static $_values = [];

    public static function get()
    {
        if (empty(static::$_values)) {
            static::$_values = [
                '1' => __('День'),
                '2' => __('Месяц'),
                '3' => __('Год'),
                '4' => __('3 Месяца'),
                '5' => __('6 Месяцов')
            ];
        }

        return static::$_values;
    }

    public static function getByKey($key)
    {
        return static::get()[$key];
    }
}

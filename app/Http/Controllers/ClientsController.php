<?php

namespace App\Http\Controllers;

use App\Actions\Client\BuyClientBalanceAction;
use App\Actions\Client\ClientConfirmSmsAction;
use App\Actions\Client\ClientParameterPhotoHistoryByDayAction;
use App\Actions\Client\ClientPhotoHistoryByDayAction;
use App\Actions\Client\ClientSubscriptionDeleteAction;
use App\Actions\Client\ClientSubscriptionRestoreAction;
use App\Actions\Client\ClientUpdateSubscriptionAction;
use App\Actions\Client\ClientUpdateSubscriptionViewAction;
use App\Actions\Client\CloseBuyAction;
use App\Actions\Client\CreateCompanyClientAction;
use App\Actions\Client\RefillClientBalanceAction;
use App\Components\CrmHelper;
use App\DependedGetters\ClientsGetter;
use App\Forms\BuyClientBalanceForm;
use App\Forms\CloseBuyForm;
use App\Forms\RefillClientBalanceForm;
use App\Http\Controllers\Actions\Clients\ClientExportExcelAction;
use App\Http\Controllers\Actions\Clients\ClientParameterHistoryAction;
use App\Http\Controllers\Actions\Clients\ClientPhotoHistoryAction;
use App\Http\Requests\Client\ClientParameterHistoryRequest;
use App\Http\Requests\Client\ClientParametersHistoryExportRequest;
use App\Http\Requests\Client\ClientPhotoHistoryRequest;
use App\Http\Requests\Client\ClientPhotosByDate;
use App\Http\Requests\Client\ClientRefillRequest;
use App\Http\Requests\Client\ClientSubscriptionDeleteRequest;
use App\Http\Requests\Client\ClientSubscriptionRestoreRequest;
use App\Http\Requests\Client\ClientUpdateSubscriptionRequest;
use App\Http\Requests\Client\ClientUpdateSubscriptionViewRequest;
use App\Http\Requests\Client\CreateClientFromExistRequest;
use App\Http\Requests\Client\CreateClientRequest;
use App\Http\Requests\Client\FindClientByBarcodeRequest;
use App\Http\Requests\Client\FindClientByPhoneRequest;
use App\Http\Resources\ClientResource;
use App\Http\Resources\CompanyClientResource;
use App\Models\Client;
use App\Models\CompanyClientBuy;
use App\Repositories\ClientRepository;
use App\Repositories\CompanyClientBuysRepository;
use App\Repositories\CompanyClientRepository;
use App\Repositories\CompanySettingsRepository;
use App\Repositories\SexRepository;
use App\Rules\PhoneFormatRule;
use App\Transformers\CompanyClientsTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use View;

class ClientsController extends Controller
{
    /**
     * @var SexRepository
     */
    private $sexRepository;
    /**
     * @var ClientRepository
     */
    private $clientRep;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->sexRepository = app(SexRepository::class);
        $this->middleware(function ($request, $next) {
            $this->companySettingsRep = app(CompanySettingsRepository::class);
            $this->clientRep = app(ClientRepository::class);
            return $next($request);
        });
    }

    public function index()
    {
        $dates = [];
        if (get_filter_value('date')) {
            $dates[] = get_filter_value('date');
        }
        if (get_filter_value('date_from')) {
            $dates[] = get_filter_value('date_from');
        }
        if (get_filter_value('date_to')) {
            $dates[] = get_filter_value('date_to');
        }

        $clients = ClientsGetter::getListOrderedByName();

        $clients->getCollection()->transform(new CompanyClientsTransformer());

        return view('clients.index', [
            'dates' => $dates,
            'filters' => [
                'date_range' => true,//Auth::user()->hasRole(Roles::COMPANY_OWNER),
            ],
            'items' => $clients,
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $view = 'clients.create';

        if (request('phone')) {
            $client = $this->clientRep->findByPhone(request('phone'));
            if ($client) {
                $view = 'clients.create_from_exist';
            }
        }

        return view($view, [
            'phone' => request('phone'),
            'client' => $client ?? null,
            'sexValues' => $this->sexRepository->getForSelectBox(),
            'backUrl' => \request('back'),
        ]);
    }

    public function sendConfirmSms(Request $request)
    {
        $validated = $this->validate($request, [
            'phone' => ['required', 'string', new PhoneFormatRule()]
        ]);

        try {
            (new ClientConfirmSmsAction())->execute($validated['phone']);
            return $this->response(['message' => __('Смс с кодом отправлено')]);
        } catch (\Exception $e) {
            Log::error(__FILE__ . ':' . __LINE__ . ' ' . $e->getMessage());
            return $this->response(['errors' => ['phone' => [__('СМС не отправилось')]]], 400);
        }

    }

    public function store(CreateClientRequest $request)
    {
        $client = (new CreateCompanyClientAction())->execute($request->validated());

        if ($client) {
            return redirect(route('sale.subscription') . '?phone=' . $client->phone);
        }
        return redirect(route('sale.subscription'));
    }

    public function storeFromExists(CreateClientFromExistRequest $request)
    {
        $client = (new CreateCompanyClientAction())->execute($request->validated());

        if ($client) {
            return redirect($request->get('back', route('sale.subscription')) . '?phone=' . $client->phone);
        }
    }


    public function findByPhone(FindClientByPhoneRequest $request)
    {
        $client = app(CompanyClientRepository::class)->findByPhone($request->get('phone'));
        if (!$client) {
            return $this->response(['message' => 'Client was not found'], 404);
        }

        return new CompanyClientResource($client);
    }

    public function findByNumber(FindClientByBarcodeRequest $request)
    {
        /** @var Client $client */
        $client = app(CompanyClientRepository::class)->findByBarcode($request->get('number'));
        if (!$client) {
            return $this->response(['code' => 'client_not_found', 'message' => 'Client was not found'], 404);
        }

        if (!count($client->activeSubscriptionsV1)) {
            return $this->response([
                'code' => 'subscription_not_found',
                'message' => 'Subscription was not found',
                'clientData' => [
                    'phone' => $client->phone
                ]
            ], 404);
        }

        return new ClientResource($client);
    }


    public function profile(Client $client)
    {
        $buys = app(CompanyClientBuysRepository::class)->getBuysCompanyClient($client->id, CrmHelper::companyId());
        $total_debt = (string)$buys->sum('current_amount');

        $companyClient = app(CompanyClientRepository::class)->findById($client->id);
        $visits = app(CompanyClientRepository::class)->visitsHistory($companyClient);
        $orders = app(CompanyClientRepository::class)->orders($companyClient);

        $parameters = app(CompanyClientRepository::class)->parameters($companyClient);
        $photos = app(CompanyClientRepository::class)->photos($companyClient);

        $subscriptions = app(CompanyClientRepository::class)->subscriptions($companyClient);

        return view('clients.profile', [
            'companyClient' => $companyClient,
            'client' => $client,
            'buys' => $buys,
            'visits' => $visits,
            'orders' => $orders,
            'total_debt' => $total_debt,
            'photos' => $photos,
            'parameters' => $parameters,
            'subscriptions' => $subscriptions
        ]);
    }

    public function refillBalanceView(Client $client)
    {
        try {
            $company_id = CrmHelper::companyId();
            return $this->createSuccessResponse(
                [
                    'title' => __("Пополнение баланса клиента"),
                    'content' => View::make('clients.refill', [
                            'client' => $client,
                            'company_id' => $company_id
                        ]
                    )->render()
                ]
            );
        } catch (\Exception $exception) {
            return $this->createErrorResponse('');
        }
    }

    public function updateSubscriptionView(ClientUpdateSubscriptionViewRequest $request)
    {
        try {
            $action = new ClientUpdateSubscriptionViewAction(array_merge($request->all(), [
                'company_id' => CrmHelper::companyId(),
            ]));
            $response = $action->execute();
            return $this->createSuccessResponse($response);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . $exception->getTraceAsString());
            echo '</pre>';
            die;

            return $this->createErrorResponse('Ошибка на сервере');
        }
    }

    public function updateSubscription(ClientUpdateSubscriptionRequest $request)
    {
        try {
            $action = new ClientUpdateSubscriptionAction(array_merge($request->all(), [

            ]));
            $action->execute();
            return $this->createSuccessResponse($action->response);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . $exception->getTraceAsString());
            echo '</pre>';
            die;
            return $this->createErrorResponse('Ошибка на сервере');
        }
    }

    public function subscriptionDelete(ClientSubscriptionDeleteRequest $request)
    {
        try {
            $action = new ClientSubscriptionDeleteAction(array_merge($request->all(), [

            ]));
            $action->execute();

            if ($request->input('back_url')) {
                return redirect($request->input('back_url'));
            }

            return redirect()->route('clients.profile', ['client' => $action->resp_client_id]);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage());
            echo '</pre>';
            die;

            return $this->createErrorResponse('Ошибка на сервере');
        }
    }

    public function subscriptionRestore(ClientSubscriptionRestoreRequest $request)
    {
        try {
            $action = new ClientSubscriptionRestoreAction(array_merge($request->all(), [

            ]));
            $action->execute();


            if ($request->input('back_url')) {
                return redirect($request->input('back_url'));
            }
            return redirect()->route('clients.profile', ['client' => $action->resp_client_id]);
        } catch (\Exception $exception) {
            return $this->createErrorResponse('Ошибка на сервере');
        }
    }

    public function refillBalance(ClientRefillRequest $request)
    {
        try {
            $refillForm = new RefillClientBalanceForm();
            $refillForm->setClientId($request->client_id);
            $refillForm->setCompanyId($request->company_id);
            $refillForm->setComment($request->comment);
            $refillForm->setNeedAmount($request->amount);
            $refillForm->setCreatedBy($request->user()->id);

            RefillClientBalanceAction::execute($refillForm);

            return $this->createSuccessResponse([
                'message' => "Баланс успешно пополнен"
            ]);

        } catch (\Exception $exception) {
            return $this->createErrorResponse('Ошибка на сервере');
        }
    }

    public function buyView(Client $client)
    {
        try {
            $company_id = CrmHelper::companyId();
            return $this->createSuccessResponse(
                [
                    'title' => __("Создание платежа"),
                    'content' => View::make('clients.buy', [
                            'client' => $client,
                            'company_id' => $company_id
                        ]
                    )->render()
                ]
            );
        } catch (\Exception $exception) {
            return $this->createErrorResponse('');
        }
    }


    public function buy(ClientRefillRequest $request)
    {
        try {
            $refillForm = new BuyClientBalanceForm();
            $refillForm->setClientId($request->client_id);
            $refillForm->setCompanyId($request->company_id);
            $refillForm->setComment($request->comment);
            $refillForm->setNeedAmount($request->amount);
            $refillForm->setCreatedBy($request->user()->id);

            BuyClientBalanceAction::execute($refillForm);

            return $this->createSuccessResponse([
                'message' => __('Платеж успешно создан')
            ]);

        } catch (\Exception $exception) {
            return $this->createErrorResponse('Ошибка на сервере');
        }
    }

    public function closeBuy(Request $request, CompanyClientBuy $buy)
    {
        try {
            $closeForm = new CloseBuyForm();
            $closeForm->setCreatedBy($request->user()->id);
            $closeForm->setBuyId($buy->id);

            CloseBuyAction::execute($closeForm);

            return redirect()->action(
                'ClientsController@profile', ['client' => $buy->client_id]
            );
        } catch (\Exception $exception) {
            return redirect()->action(
                'ClientsController@profile', ['client' => $buy->client_id]
            );
        }
    }

    public function photoHistoryByDate(ClientPhotosByDate $request)
    {
        try {
            $action = new ClientPhotoHistoryByDayAction(array_merge($request->all(), [
                'company_id' => CrmHelper::companyId(),
            ]));
            $response = $action->execute();
            return $this->createSuccessResponse($response);
        } catch (\Exception $exception) {
            return $this->createErrorResponse('Ошибка на сервере');
        }
    }

    public function parameterPhotoHistory(Request $request)
    {
        try {
            $action = new ClientParameterPhotoHistoryByDayAction(array_merge($request->all(), [
            ]));
            $response = $action->execute();
            return $this->createSuccessResponse($response);
        } catch (\Exception $exception) {
            return $this->createErrorResponse('Ошибка на сервере');
        }
    }

    public function actionParameterHistoryExport(ClientParametersHistoryExportRequest $request)
    {
        try {
            $action = new ClientExportExcelAction([
                'client_id' => $request->input('client_id', null),
                'company_id' => CrmHelper::companyId(),
            ]);
            return $action->execute();
        } catch (\Exception $exception) {
            return $this->createErrorResponse('Ошибка на сервере');
        }
    }

    public function actionParameterHistoryPage(ClientParameterHistoryRequest $request)
    {
        try {
            $action = new ClientParameterHistoryAction([
                'company_id' => CrmHelper::companyId(),
                'client_id' => $request->client_id,
                'page' => $request->page,
            ]);
            $action->execute();
            return view('clients.page-parameters', $action->viewParams);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . $exception->getTraceAsString());
            echo '</pre>';
            die;

            return redirect()->route('home');
        }
    }

    public function actionPhotoHistoryPage(ClientPhotoHistoryRequest $request)
    {
        try {
            $action = new ClientPhotoHistoryAction([
                'company_id' => CrmHelper::companyId(),
                'client_id' => $request->client_id,
                'page' => $request->page,

            ]);
            $action->execute();
            return view('clients.page-photos', $action->viewParams);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . $exception->getTraceAsString());
            echo '</pre>';
            die;

            return redirect()->route('home');
        }
    }

}

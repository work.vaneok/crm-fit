<?php
/**
 * @apiDefine InitGroup Инициализация
 */

/**
 * @apiDefine LoginGroup Авторизация и регистрация
 */

/**
 * @apiDefine MyGroup Моя информация
 */

/**
 * @apiDefine NewsGroup Новости
 */


/**
 * @apiDefine CompanyGroup Компания
 */

/**
 * @apiDefine SliderGroup Слайдер
 */

/**
 * @apiDefine RequestHeader
 *
 * @apiHeader {String} Content-Type Формат отправленных данных, только <code>application/json</code>
 * @apiHeader {String} Accept Формат запрашиваемых данных, только <code>application/json</code>
 * @apiHeader {String} company Текстовый идентификатор компании
 * @apiVersion 0.1.0
 * @apiHeaderExample {json} Пример заголовка
 *     "Accept": "application/json",
 *     "Content-Type": "application/json",
 *     "company": "e97db65f-9fc3-4ba8-983f-b14a3a2f553d"
 *
 */

/**
 * @apiDefine RequestAuthHeader
 *
 * @apiHeader {String} Content-Type Формат отправленных данных, только <code>application/json</code>
 * @apiHeader {String} Accept Формат запрашиваемых данных, только <code>application/json</code>
 * @apiHeader {String} company Текстовый идентификатор компании
 * @apiHeader {String} Authorization Bearer Authorization
 * @apiVersion 0.1.0
 * @apiHeaderExample {json} Пример заголовка
 *     "Accept": "application/json",
 *     "Content-Type": "application/json",
 *     "company": "e97db65f-9fc3-4ba8-983f-b14a3a2f553d",
 *     "Authorization": "Bearer OLraGtiL3uc01uF6JtRIlt96LcerGyE1",
 *
 */

/**
 * @apiDefine ClientInfo
 * @apiSuccess {String} data.token ТОКЕН
 * @apiSuccess {String} data.first_name Имя
 * @apiSuccess {String} data.last_name Фамилия
 * @apiSuccess {String} data.birthday Дата рождения
 * @apiSuccess {String} data.phone Номер телефона
 * @apiSuccess {String} data.email Email
 * @apiSuccess {String} data.barcode Штрих-код
 * @apiSuccess {String} data.sex Пол, -1, 1, 2
 * @apiSuccess {Array} data.active_subscriptions список активных абонементов
 *
 * @apiSuccessExample {json} Успешный ответ
 *     HTTP/1.1 200 OK
 *     {
 *          "data": {
 *              "first_name": "Имя АПИ",
 *              "last_name": "Фамилия АПИ",
 *              "token": "pkJWVAPj7I7zjdZZrXoCgNtcAksWzou1",
 *              "email": "email",
 *              "phone": "380688504700",
 *              "barcode": "30000009",
 *              "sex": 2,
 *              "active_subscriptions": []
 *          }
 *     }
 */

/**
 * @apiDefine UnauthorizedError
 *
 * @apiErrorExample {json} ОШИБКА: Не выполнена авторизация
 *     HTTP/1.1 401 Unauthorized
 *     {
 *          "message": "Unauthenticated."
 *     }
 *
 */

/**
 * @apiDefine NotFoundError
 *
 * @apiErrorExample {json} ОШИБКА: Не найдено
 *     HTTP/1.1 404 Not found
 *
 */

/**
 * @apiDefine SaleStreamBaseErrors
 *
 * @apiErrorExample {json} ОШИБКА: Идентификатор не заполнен
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *       "errors": [
 *           "sid": [
 *               'Поле sid обязательно для заполнения'
 *           ]
 *       ]
 *     }
 *
 * @apiErrorExample {json} ОШИБКА: Поток не найден
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *       "errors": [
 *           "sid": [
 *               'Выбранное значение для sid некорректно'
 *           ]
 *       ]
 *     }
 *
 */

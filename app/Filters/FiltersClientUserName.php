<?php
namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersClientUserName implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->whereHas('client', function (Builder $query) use ($value) {
            $query->where('first_name', 'LIKE', $value . '%')
                ->orWhere('last_name', 'LIKE', $value . '%');
        });
    }
}

@if( empty(Config::get('partial.datepicker')))
    {{ Config::set('partial.datepicker', 1) }}

    @push('js')
        <script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js') }}"></script>
        <script>
            $.fn.datepicker.defaults.language = 'ru';
            $.fn.datepicker.defaults.autoclose = true;
        </script>
    @endpush

    @push('css')
        <link rel="stylesheet" href="{{ asset('assets/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/bootstrap-datepicker/css/bootstrap-datepicker.standalone.min.css') }}" />

        <link rel="stylesheet" href="{{ asset('assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/bootstrap-datepicker/css/bootstrap-datepicker3.standalone.min.css') }}" />
    @endpush
@endif

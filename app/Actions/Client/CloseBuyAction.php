<?php

namespace App\Actions\Client;

use App\Forms\CloseBuyForm;
use App\Helper\CompanyClientBuyStatusHelper;
use App\Helper\TransactionHelper;
use App\Models\Client;
use App\Models\CompanyClient;
use App\Models\CompanyClientBuy;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class CloseBuyAction
{
    /**
     * @param $data
     * @return Client|\Illuminate\Database\Eloquent\Model
     */
    public static function execute(CloseBuyForm $form)
    {
        try {
            DB::beginTransaction();
        } catch (\Exception $exception) {
            throw $exception;
        }

        try {
            $buy = CompanyClientBuy::where(['id' => $form->getBuyId()])->first();

            $companyClient = CompanyClient::where([
                CompanyClient::COLUMN_CLIENT_ID => $buy->client_id,
                CompanyClient::COLUMN_COMPANY_ID => $buy->company_id
            ])->first();

            if ($buy->current_amount > $companyClient->balance) {
                throw new \Exception('$companyClient->balance <=  $buy->needAmount');
            }

            $transaction = new Transaction([
                Transaction::COLUMN_TYPE_ID => TransactionHelper::TRANSACTION_CLOSE_PAYMENT_TYPE_ID,
                Transaction::COLUMN_TOTAL_AMOUNT => $buy->current_amount,
                Transaction::COLUMN_CREATED_BY_USER_ID => $form->getCreatedBy()
            ]);

            if (!$transaction->save()) {
                throw new \Exception(json_encode($transaction->errors));
            }

            $companyClient->balance -= $buy->current_amount;
            $buy->current_amount = 0;
            $buy->paid_transaction_id = $transaction->id;
            $buy->status_id = CompanyClientBuyStatusHelper::STATUS_COMPLETE_ID;

            $companyClient->save();
            $buy->save();

            DB::commit();
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage());
            echo '</pre>';
            die;

            DB::rollBack();
            throw $exception;
        }

    }
}

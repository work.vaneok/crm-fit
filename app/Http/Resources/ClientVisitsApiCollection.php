<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientVisitsApiCollection extends ResourceCollection
{
    public $collects = ClientVisitsApiResource::class;

}

<?php
namespace App\Repositories;

use App\Components\Globals\OrderStatusesValues;

/**
 * Статусы заказов
 *
 * Class OrderStatusRepository
 * @package App\Repositories
 */
class OrderStatusRepository
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function getForSelectBox()
    {
        return collect(
            OrderStatusesValues::get()
        )->map(function ($item) {
            return $item['label'];
        });
    }

    public function getList()
    {
        return collect(
            OrderStatusesValues::get()
        );
    }

    public function getById($id)
    {
        return OrderStatusesValues::getByKey($id);
    }
}

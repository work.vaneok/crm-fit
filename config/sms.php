<?php

return [

    'providers' => [
        'turbosms' => [
            'active' => env('TURBO_SMS_ACTIVE', false),
            'login' => env('TURBO_SMS_LOGIN', ''),
            'password' => env('TURBO_SMS_PASSWORD', ''),
            'sender' => env('TURBO_SMS_SENDER', ''),
        ]
    ],

];

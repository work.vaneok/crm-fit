<?php
namespace App\Models\Traits;

trait BarcodeGeneratorTrait
{
    public static function bootBarcodeGeneratorTrait()
    {
        static::created(function ($model) {
            $model->update(['barcode' => generateBarcode($model->company_id, $model->id)]);
        });
    }
}

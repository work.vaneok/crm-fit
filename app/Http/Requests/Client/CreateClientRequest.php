<?php
namespace App\Http\Requests\Client;


use App\Http\Requests\Traits\DefaultCompanyIdInRequest;
use App\Rules\ClientConfirmSmsCodeRule;
use App\Rules\PhoneFormatRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateClientRequest extends FormRequest
{
    use DefaultCompanyIdInRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required'],
            'phone' => ['required', new PhoneFormatRule(), /*'unique:clients'*/],
            'first_name' => ['required'],
            'last_name' => ['required'],
            'birthday' => ['required', 'date_format:d.m.Y'],
            'sex' => ['required', Rule::in(['1', '2'])],
            'email' => ['nullable', 'email'],
            'sms_code' => ['required', new ClientConfirmSmsCodeRule($this->get('phone'))],
        ];
    }
}

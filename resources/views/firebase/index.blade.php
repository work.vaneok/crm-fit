@extends('layouts.dashboard')

@section('title')
    {{ __('Уведомления Firebase') }}
@endsection

@section('content')
    <div class="header__filter" style=" margin-bottom: 15px;">
        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div class="btn-group mr-2" role="group" aria-label="First group">
                {{-- <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#filter"
                         style="margin-left: 0;">{{ __('Фильтр') }}</button>--}}
                <button type="button" class="btn btn-primary notification-create" data-toggle="modal"
                        data-form-url="{{route('firebase.create-view')}}">
                    {{__('Отправить уведомление всем клиентам',['type' =>\App\Actions\Firebase\FirebaseCreateViewAction::NOTIFICATION_TYPE_SEND_ALL])}}
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                          style="display: none"></span>
                </button>
                <button type="button" class="btn btn-primary notification-create" data-toggle="modal"
                        data-form-url="{{route('firebase.create-view',['type' =>\App\Actions\Firebase\FirebaseCreateViewAction::NOTIFICATION_TYPE_SEND_ONE])}}">
                    {{__('Отправить уведомление одному клиенту')}}
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                          style="display: none"></span>
                </button>
            </div>
        </div>

        <div id="filter" class="collapse col-lg-12" style="margin-top: 15px; padding-left: 0px;">
            <form action="" class="grid-filter" method="get" id="filterForm">
                @php($errMessages = [])
                @error('filter.date')
                @php($errMessages[] = $message)
                @enderror
                @error('filter.date_from')
                @php($errMessages[] = $message)
                @enderror
                @error('filter.date_to')
                @php($errMessages[] = $message)
                @enderror

                @if(count($errMessages))
                    <div class="alert alert-danger">{!! implode('<br>', $errMessages) !!}</div>
                @endif
                <div class="form-row">


                </div>
                <div class="form-row">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary "><i
                                class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                    </div>
                    <div class="form-group">
                        <a href="/{{ Route::current()->uri }}" class="btn btn-danger "><i
                                class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">{{ __('ID') }}</th>
                <th scope="col">{{ __('Заголовок') }}</th>
                <th scope="col">{{ __('Описание') }}</th>
                <th scope="col">{{ __('Создан в') }}</th>
                <th scope="col">{{ __('Действия') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->descriptions   }}</td>
                    <td>{{ $item->created_at }}</td>
                    <td>
                        <div class="btn-group mr-2" role="group" aria-label="First group">
                            <button type="button" class="btn btn-sm btn-primary firebase-detail-view"
                                    data-toggle="modal"
                                    data-form-url="{{route('firebase.detail-view',['id' => $item->id])}}">
                                <i class="fas fa-info"></i>
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                                      style="display: none"></span>
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $items ? $items->appends(request()->only('filter'))->links() : '' }}
    </div>
    @include('inc.ajax-form-modal.form')

@endsection

@push('scripts')
    <script src="{{ asset('js/firebase/index.js') }}"></script>
@endpush

@push('css')
@endpush

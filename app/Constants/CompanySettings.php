<?php
namespace App\Constants;

class CompanySettings
{
    const TIME_TO_RETURN_VISIT = 'company:timeToReturnVisit';
    const WAY_FOR_PAY_LOGIN = 'wayforpay:login';
    const WAY_FOR_PAY_SECRET = 'wayforpay:secret_key';
    const GOOGLE_PLAY = 'google_play';
    const APPLE_STORE = 'apple_store';
    const POLICY_LINK = 'policy';
    const DESCRIPTION = 'description';
    const OFFER_LINK = 'offer';
    const TITLE = 'title';
}

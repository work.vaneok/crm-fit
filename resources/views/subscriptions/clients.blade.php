@extends('layouts.dashboard')

@section('title')
    {{ __('Проданные абонементы') }}
@endsection


@push('scripts')
    <script src="{{ asset('js/subscriptions/clients.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".js-example-basic-single").select2();
        });
    </script>
@endpush

@section('content')

    <div class="header__filter" style=" margin-bottom: 15px;">
        <div class="row">
            <div class="col-1">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#filter"
                            style="margin-left: 0;">{{ __('Фильтр') }}</button>
                    <form action="{{ route('subscriptions.export') }}" accept-charset="UTF-8">
                        @if(Request::has('filter.client_id'))
                        <button type="submit" class="btn btn-primary">{{__('Excel')}}</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="tooltip"  disabled title="{{__('Чтобы активировать возможность импорта нажмите "Фильтр" потом "Фильтровать"')}}">
                                {{__('Excel')}}
                            </button>
                        @endif
                        <input style="display: none" type="text" value="{{get_filter_value('client_id')}}"
                               name="filter[client_id]" placeholder="Id">
                        <input style="display: none" type="text" value="{{get_filter_value('date_from')}}" name="filter[date_from]" placeholder="Id">
                        <input style="display: none" type="text" value="{{get_filter_value('date_to')}}" name="filter[date_to]" placeholder="Id">
                    </form>
                </div>
            </div>

            <div id="filter" class="collapse col-12" style="margin-top: 15px;">
                <form action="" class="grid-filter" method="get" id="filterForm">
                    <div class="form-row">
                        <div class="form-group col-lg-4 col-xl-3">
                            <label for="filter[client_id]"><b>{{ __('Клиент') }}</b></label><br>
                            @search_select2(['name' => 'client_id', 'options' => $clients, 'label' => __('Клиент')])
                        </div>

                        <div class="form-group col-sm-3 col-lg-3 col-xl-2">
                            <label for="filter[client_id]"><b>{{ __('Дата') }}</b></label>
                            @search_date_range(['name' => 'date'])
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary "><i
                                    class="fa fa-filter"></i> {{ __('Фильтровать') }}</button>
                        </div>
                        <div class="form-group">
                            <a href="/{{ Route::current()->uri }}" class="btn btn-danger "><i
                                    class="fa fa-trash-alt"></i> {{ __('Сбросить') }}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">{{ __('Об Клиенте') }}</th>
                <th scope="col">{{ __('Об Абонементе') }}</th>
                <th scope="col">{{ __('Об продаже') }}</th>
                <th scope="col">{{ __('Дата') }}</th>
                <th scope="col">{{ __('Действия') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>
                        <a href="{{route('clients.profile',['client' => $item['client']['id']])}}">  {{$item['client']['first_name'].' '. $item['client']['last_name']}}
                            <i class="fa fa-user" aria-hidden="true"></i></a> <br>
                        <a href="tel:+{{ $item['client']['phone'] }}"> {{ $item['client']['phone'] }} </a>
                    </td>
                    <td>
                        <span><b>№</b>: {{  $item['id'] }}</span>
                        <span><b>{{__('Название')}}</b>: {{  $item['subscription']['title'] }}</span> <br>
                        <span><b>{{__('Действителен')}}</b>: {{  $item['available_date'] }}</span> <br>
                        <span><b>{{__('Занятий')}}</b>: {{  $item['quantity_of_occupation'] }}</span> <br>

                        <?php
                        if ($item['deleted_at']) {
                        ?>
                        <span class="badge badge-danger"><?=__('Удален')?></span>
                        <?php
                        } elseif ($item['active']) {
                        ?>
                        <span class="badge badge-success"><?=__('Активный')?></span>
                        <?
                        }else {
                        ?>
                        <span class="badge badge-secondary"><?=__('Не активный')?></span>
                        <?
                        }

                        ?>
                        {{--    <span> <button type="button" class="btn btn-sm btn-primary ajax-btn-view"
                                           data-toggle="modal"
                                           data-form-url="{{route('subscriptions.subscription-detail',
                                [
                                    'subscription_id' => $item['subscription']['id']
                                ])}}">
                            <i class="fas fa-info-circle"></i>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                                  style="display: none"></span>
                        </button>
                            </span>--}}
                    </td>
                    <td>
                        <span><b>{{__('Цена')}}</b>: {{ $item['order']['amount'] }}</span> <br>
                        <span><b>{{__('Оплата')}}</b>: {{ $item['order']['pay_source'] }} </span> <br>

                        {{-- <span> <button type="button" class="btn btn-sm btn-primary ajax-btn-view"
                                        data-toggle="modal"
                                        data-form-url="{{route('subscriptions.order-detail',
                             [
                                 'order_id' => $item['order']['id']
                             ])}}">
                         <i class="fas fa-info-circle"></i>
                         <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                               style="display: none"></span>
                     </button>
                         </span>
 --}}

                    </td>
                    <td>
                        {{ $item['created_at'] }}
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm btn-primary ajax-btn-form"
                                data-toggle="modal"
                                data-form-url="{{route('clients.profile.update-subscription',
                            [
                                'subscription_id' => $item['id'],
                            ])}}">
                            <i class="fas fa-edit"></i>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"
                                  style="display: none"></span>
                        </button>

                        <?php
                        if ($item['deleted_at']) {
                        ?>
                        <a href="<?= route('clients.profile.restore-subscription', ['subscription_id' => $item['id'], 'back_url' => $back_url])?>"
                           class="btn btn-success btn-sm "
                           role="button" aria-pressed="true"> <i
                                class="fas fa-trash-restore"></i></a>
                        <?
                        }else {
                        ?>
                        <a href="<?= route('clients.profile.delete-subscription', ['subscription_id' => $item['id'], 'back_url' => $back_url])?>"
                           class="btn btn-danger btn-sm"
                           role="button" aria-pressed="true"> <i
                                class="fas fa-trash"></i></a>
                        <?
                        } ?>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $clientSubsModel ? $clientSubsModel->appends(request()->only('filter'))->links() : '' }}

    </div>
    @include('inc.ajax-form-modal.form')

@endsection

@section('actions')

@endsection

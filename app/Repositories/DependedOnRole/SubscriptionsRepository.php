<?php

namespace App\Repositories\DependedOnRole;

use App\Components\Globals\ValidityValues;
use App\Models\Client;
use App\Models\Company;
use App\Models\CompanySetting;
use App\Models\Subscription;
use App\User;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Collection;
use PriceFormatter;

/**
 * Абонементы
 *
 * Class SubscriptionsRepository
 * @package App\Repositories
 */
abstract class SubscriptionsRepository
{
    /**
     * @var Company
     */
    protected $company;

    /**
     * @var User
     */
    protected $user;

    public function __construct(User $user, Company $company)
    {
        $this->company = $company;
        $this->user = $user;
    }

    /**
     * @return Subscription[]|Collection
     */
    abstract protected function listForSaleByAdmin();

    /**
     * @return Subscription[]|Collection
     */
    public function getListForSaleInCrm()
    {
        return $this->listForSaleByAdmin()->mapWithKeys(function ($item) {
            /** @var $item Subscription */
            return [
                $item->id => [
                    'title' => $item->title,
                    'price' => PriceFormatter::withCurrency($item->price),
                    'old_price' => PriceFormatter::withCurrency($item->old_price),
                    'image' => $item->getFirstMedia() ? $item->getFirstMedia()->getUrl('thumb') : false,
                    'quantity_of_occupation' => $item->quantity_of_occupation_text,
                    'validity' => $item->validity,
                    'validityText' => ValidityValues::getByKey($item->validity),
                    'available_to_before_begin_of_use' => $item->available_to_before_begin_of_use,
                ]
            ];
        });
    }

    public function getListForSearch()
    {
        return Subscription::byCompany($this->company->id)->orderBy('validity')->get()->mapWithKeys(function ($item) {
            /** @var $item Subscription */
            return [
                $item->id => $item->title,
            ];
        });
    }

}

<?php
namespace App\Constants;

class StartOfActivity
{
    const FROM_SALE_DATE = 1;
    const FROM_FIRST_VISIT = 2;
}

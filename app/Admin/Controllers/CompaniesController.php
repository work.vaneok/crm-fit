<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\CompanySettingsButton;
use App\Admin\Forms\CompanySettingsForm;
use App\Admin\Forms\NewCompanyOwner;
use App\Models\Company;
use App\Models\CompanyAdminSettings;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CompaniesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Models\Company';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Company());
        //$grid->disableActions();

        $grid->column('id', __('Id'));
        $grid->column('title', __('Title'));
        $grid->column('created_at', __('Created at'));
//        $grid->column('updated_at', __('Updated at'));
        $grid->column('tariff.title', __('Tariff id'));
        $grid->column('available_from', __('Available from'));
        $grid->column('available_to', __('Available to'));

        $grid->actions(function (Grid\Displayers\DropdownActions $actions) {
            $actions->disableEdit();
            $actions->disableView();
            $actions->disableDelete();

            $actions->add(new CompanySettingsButton($actions->getKey()));
        });


//        $grid->column('owner_id', __('Owner id'));
//        $grid->column('description', __('Description'));
//        $grid->column('created_by', __('Created by'));
//        $grid->column('logo_id', __('Logo id'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Company::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('available_from', __('Available from'));
        $show->field('available_to', __('Available to'));
        $show->field('owner_id', __('Owner id'));
        $show->field('title', __('Title'));
        $show->field('description', __('Description'));
        $show->field('created_by', __('Created by'));
        $show->field('logo_id', __('Logo id'));
        $show->field('tariff_id', __('Tariff id'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return NewCompanyOwner
     */
    protected function form()
    {
        return new NewCompanyOwner();
/*
        $form = new Form(new NewCompanyOwner());

        $form->datetime('available_from', __('Доступна с '))->default(date('Y-m-d H:i:s'));
        $form->datetime('available_to', __('Доступна до'))->default(date('Y-m-d H:i:s', strtotime('next year')));
        $form->text('title', __('Название компании'));
        $form->text('first_name', __('Имя собственника'));
        $form->text('last_name', __('Фамилия собственника'));
        $form->text('phone', __('Телефон'));

        return $form;*/
    }

    public function store()
    {
        return $this->form()->store();
    }

    public function settings(Company $company, Content $content)
    {
        return $content->title($company->title)
            ->body($this->settingsForm($company));
    }

    public function storeSettings(Company $company, Content $content)
    {
        return $this->settingsForm($company)->store();
    }

    /**
     * @return CompanySettingsForm
     */
    private function settingsForm(Company $company)
    {
        $form = new CompanySettingsForm();
        $form->setCompany($company);
        return $form;
    }
}

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('assets/components-font-awesome/css/all.min.css')}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />
    @stack('css')
    <script>
        window.translations = {!!  Cache::get('translations')  !!};
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'baseUrl' => url('/'),
            'routes' => collect(\Route::getRoutes())->mapWithKeys(function ($route) { return [$route->getName() => $route->uri()]; })
        ]) !!};
    </script>
</head>
<body>
    <div class="wrapper overflow-hidden">
        @include('_partials.sidebar')

        <div class="main">
            {{--<nav class="navbar navbar-expand-lg navbar-light bg-light header">--}}
                {{--<div class="container-fluid">--}}
                    {{--<button type="button" id="sidebarCollapse" class="btn btn-info">--}}
                        {{--<i class="fas fa-align-left"></i>--}}
                        {{--<span>Toggle Sidebar</span>--}}
                    {{--</button>--}}
                {{--</div>--}}
            {{--</nav>--}}
            @include('_partials.header')

            <div class="container-fluid">
                <div class="content">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <script src="{{mix('js/app.js')}}"></script>
    <script src="{{mix('js/scripts.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous"></script>
    <script>
        $(function () {
            $('body').on('dblclick', 'tr[url]', function () {
                window.location.href = $(this).attr('url');
            });
        });

        function deleteMedia(url, media_id, selector) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {media_id: media_id, _token: '{{csrf_token()}}'},
                success: function (data) {
                    if (data.status === 'ok') {
                        $(selector).empty();
                    }
                }
            });
        }
    </script>

    @stack('js')
    @stack('scripts')
    @yield('after_scripts')
</body>
</html>

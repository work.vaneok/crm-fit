<?php

namespace App\Rules;

use App\Models\Client;
use App\Models\CompanyClient;
use App\Repositories\ConfirmSmsCodeRepository;
use App\Rules\Traits\SetCompanyIdTrait;
use Illuminate\Contracts\Validation\Rule;

class ClientOfCompanyRule implements Rule
{
    use SetCompanyIdTrait;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return CompanyClient::where(['client_id' => $value, 'company_id' => $this->company_id])->first() ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Данный посетитель не является клиентом компании');
    }
}

<?php

namespace App\Forms;

use App\Helper\FirebaseHelper;

class FirebaseNotificationForm
{
    public $devices;
    public $title;
    public $description;
    public $company_id;
    public $media_url;
    public $topic_id;
    public $data;

    public function __construct()
    {

    }

    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    public function getCompanyId(): string
    {
        return $this->company_id;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setDevices(array $devices)
    {
        $this->devices = $devices;
    }

    public function getDevices(): array
    {
        return $this->devices ?? [];
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setMediaUrl(string $media_url)
    {
        $this->media_url = $media_url;
    }

    public function getMediaUrl(): string
    {
        return $this->media_url;
    }

    public function setTopicId(int $topic_id)
    {
        $this->topic_id = $topic_id;
    }

    public function getTopicId(): int
    {
        return $this->topic_id ?? FirebaseHelper::FIREBASE_DEFAULT_TOPIC;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getJSONData()
    {
        return json_encode($this->data);
    }

    public function setDataByTopicID($topic_id = FirebaseHelper::FIREBASE_DEFAULT_TOPIC)
    {
        $this->data = [
            'data_topic_id' => $topic_id,
        ];
    }

    public function getNewsData($news_id)
    {
        return [
            'data_topic_id' => FirebaseHelper::FIREBASE_NEWS_TOPIC,
            'data_topic_pk' => $news_id
        ];
    }

}

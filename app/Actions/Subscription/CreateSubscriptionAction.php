<?php
namespace App\Actions\Subscription;

use App\Models\Subscription;

class CreateSubscriptionAction
{
    /**
     * @param $data
     * @return Subscription|\Illuminate\Database\Eloquent\Model
     */
    public function execute($data)
    {
        return Subscription::create([
                'company_id' => $data['company_id'],
                'title' => $data['title'],
                'price' => $data['price'],
                'old_price' => $data['old_price'] ?? 0,
                'quantity_of_occupation' => $data['quantity_of_occupation'] ?? -1,
                'validity' => $data['validity'],
                'can_be_selected_by_staff' => $data['can_be_selected_by_staff'] ?? 0,
                'can_share' => $data['can_share'] ?? 0,
                'has_trainer' => $data['has_trainer'] ?? 0,
                'is_available_in_mobile_app' => $data['is_available_in_mobile_app'] ?? 0,
                'is_active' => $data['is_active'] ?? 0,
                'available_to_before_begin_of_use' => $data['available_to_before_begin_of_use'],
                'available_from' => $data['available_from'],
                'available_to' => $data['available_to'],
                'can_buy_online' => $data['can_buy_online'] ?? 0,
                'has_nutritionist' => $data['has_nutritionist'] ?? 0,
        ]);
    }
}

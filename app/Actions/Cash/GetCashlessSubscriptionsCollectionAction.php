<?php

namespace App\Actions\Cash;

use App\Constants\PaymentSources;
use App\Repositories\OrderRepository;
use App\Transformers\CashSubscriptionsTransformer;

class GetCashlessSubscriptionsCollectionAction
{
    /**
     * @return array
     */
    public function execute()
    {
        /** @var OrderRepository $orderRep */
        $orderRep = app(OrderRepository::class);

        $collection = $orderRep->getSubscriptionsForCashPerDays(PaymentSources::CARD, 0);

        return [
            'items' => $collection->transform(new CashSubscriptionsTransformer()),
            'total_price' => $collection->sum('price')
        ];
    }
}

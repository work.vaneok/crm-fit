<?php
namespace App\Components;

class PriceFormatter
{
    public function withCurrency($price)
    {
        return $price ? ($price . ' грн.') : '';
    }
}

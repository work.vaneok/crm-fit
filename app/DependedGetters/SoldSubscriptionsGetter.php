<?php

namespace App\DependedGetters;

use App\Components\CrmHelper;
use App\Models\ClientVisit;
use App\Models\CompanyLocation;
use App\Models\Order;
use App\Transformers\SoldSubscriptionsTransformer;
use Carbon\Carbon;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SoldSubscriptionsGetter
{
    static function getList()
    {
        $items = QueryBuilder::for(Order::class)
            ->where([
                'company_id' => CrmHelper::companyId(),
                'company_location_id' => CrmHelper::companyLocationId(),
                'order_item_type' => 'subscription',
            ])
            ->with(['client', 'soldBy', 'subscription'])
            ->allowedFilters([
                AllowedFilter::partial('username', 'client.first_name'),
                AllowedFilter::exact('status', 'status'),
                AllowedFilter::exact('subscription', 'subscription.id'),
                AllowedFilter::scope('date', 'byCreatedAt'),
                AllowedFilter::scope('date_from', 'byCreatedAtFrom'),
                AllowedFilter::scope('date_to', 'byCreatedAtTo'),
                AllowedFilter::scope('phone', 'byPhone'),
            ])
            ->orderBy('id', 'DESC')
            ->paginate(15);
        $items->getCollection()->transform(new SoldSubscriptionsTransformer());
        return $items;
    }
}

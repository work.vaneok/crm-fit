<?php

namespace App\Http\Resources;

use App\Models\ClientSubscription;
use Illuminate\Http\Resources\Json\JsonResource;

class ActiveSubscriptionsApiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var ClientSubscription $this */

        return [
            'id' => $this->id,
            'title' => $this->subscription->title,
            'active_from' => $this->formatDate('available_from'),
            'active_to' => $this->formatDate('available_to'),
            'auto_available_from' => $this->formatDate('available_to_before_begin_of_use'),
            'image' => $this->subscription->getFirstMedia() ? $this->subscription->getFirstMedia()->getFullUrl('thumb') : '',
            'occupations' => $this->quantity_of_occupation, // количество занятий
            //'validity' => $this->validity_text, // Период
        ];
    }
}

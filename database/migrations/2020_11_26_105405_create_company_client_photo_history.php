<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyClientPhotoHistory extends Migration
{
    private $table = 'company_client_photo_history';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id')->comment('Company ID');
            $table->unsignedBigInteger('client_id')->comment('Client ID');
            $table->dateTime('date')->comment('Дата');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign(['company_id'])->on('companies')->references('id');
            $table->foreign(['client_id'])->on('clients')->references('id');
        });
    }

    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropForeign(['company_id']);
            $table->dropForeign(['client_id']);
        });
        Schema::dropIfExists($this->table);
    }
}

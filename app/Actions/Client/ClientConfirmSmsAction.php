<?php
namespace App\Actions\Client;

use App\Repositories\ConfirmSmsCodeRepository;

class ClientConfirmSmsAction
{
    public function execute($phone)
    {
        $code = random_int(1111, 9999);

        app(ConfirmSmsCodeRepository::class)->save($phone, $code);
        \Sms::send($phone, __("Ваш код :code", ['code' => $code]));
    }
}

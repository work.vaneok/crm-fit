<?php

namespace App\Http\Resources;

use App\Components\Globals\ValidityValues;
use App\Components\PriceFormatter;
use App\Models\ClientSubscription;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientSubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this ClientSubscription */
        return [
            'id' => $this->id,
            'title' => $this->subscription->title,
            'price' => \PriceFormatter::withCurrency($this->order->price),
            'available_from' => $this->formatDate('available_from'),
            'available_to' => $this->formatDate('available_to'),
            'validity' => $this->validityText,
            'quantity_of_occupation' => $this->quantity_of_occupation_text, // Осталось занятий
        ];
    }
}

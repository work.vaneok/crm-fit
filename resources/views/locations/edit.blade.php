@extends('layouts.dashboard')

@section('title')
    {{ __('Редактирование локации') }}
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->multipart()->route('locations.update', ['location' => $location->id])->put()->fill($location) !!}

        {!! Form::text('title', 'Наименование') !!}
        {!! Form::textarea('description', 'Описание') !!}
        {!! Form::textarea('address', 'address') !!}
        {!! Form::text('email', 'email')->type('email') !!}
        {!! Form::file('image', __('Изображение')) !!}
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">{{ __('Телефоны') }}</label>
            <div class="col-sm-6">
                <div class="multiple-input-row phone-row">
                    <input type="text" name="phones[]" class="form-control" value="{{ $location->phones[0] ?? '' }}">
                </div>
                <div class="multiple-input-row phone-row">
                    <input type="text" name="phones[]" class="form-control" value="{{ $location->phones[1] ?? '' }}">
                </div>
                <div class="multiple-input-row phone-row">
                    <input type="text" name="phones[]" class="form-control" value="{{ $location->phones[2] ?? '' }}">
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit(__('Сохранить изменения')) !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection

@push('scripts')
    @include('inc.ckeditor')
@endpush


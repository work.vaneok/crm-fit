<?php
namespace App\Actions\User;

use App\Models\Company;
use App\User;
use Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;

use Sms;

class UserResetPasswordAction
{

    /**
     * @param $phone
     * @return User
     * @throws ModelNotFoundException
     */
    public function executeByPhone($phone)
    {
        /** @var User $user */
        $user = User::where(['phone' => $phone])->firstOrFail();
        $newPwd = Str::random(6);

        $user->update(['password' => Hash::make($newPwd)]);

        Sms::send($phone, __('Новый пароль :password', ['password' => $newPwd]));
    }
}

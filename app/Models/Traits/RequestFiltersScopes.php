<?php
namespace App\Models\Traits;


use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Builder;

trait RequestFiltersScopes
{
    public function scopeByPhone(Builder $q, $phone)
    {
        // TODO: change to varchar
        return $q->whereHas('client', function (Builder $q) use ($phone) {
            $q->whereRaw(DB::raw("phone LIKE '%$phone%'"));
        });
    }

    public function scopeByCreatedAt(Builder $q, $date)
    {
        $date = Carbon::createFromFormat('d.m.Y', $date);
        return $q->whereBetween('created_at', [$date->clone()->startOfDay(), $date->clone()->endOfDay()]);
    }

    public function scopeByCreatedAtFrom(Builder $q, $date)
    {
        $date = Carbon::createFromFormat('d.m.Y', $date);
        return $q->where('created_at', '>=', $date->clone()->startOfDay());
    }

    public function scopeByCreatedAtTo(Builder $q, $date)
    {
        $date = Carbon::createFromFormat('d.m.Y', $date);
        return $q->where('created_at', '<=', $date->clone()->endOfDay());
    }

    public function scopeWithClientsByCreatedAtFrom(Builder $q, $date)
    {
        $date = Carbon::createFromFormat('d.m.Y', $date);
        return $q->where('clients.created_at', '>=', $date->clone()->startOfDay());
    }

    public function scopeWithClientsByCreatedAtTo(Builder $q, $date)
    {
        $date = Carbon::createFromFormat('d.m.Y', $date);
        return $q->where('clients.created_at', '<=', $date->clone()->endOfDay());
    }
}

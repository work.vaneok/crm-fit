<?php

namespace App\Http\Controllers\Api\V1;

use App\Actions\Client\ClientSaveLoginAction;
use App\Actions\Client\ClientSmsLoginAction;
use App\Actions\Client\CreateCompanyClientAction;
use App\Components\CrmHelper;
use App\Constants\CacheKeysPrefixes;
use App\Constants\HttpErrors;
use App\DependedGetters\CompanyNewsGetter;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyClientApiResource;
use App\Http\Resources\NewsApiCollection;
use App\Http\Resources\NewsApiResource;
use App\Http\Resources\NewsDetailApiResource;
use App\Models\Client;
use App\Models\CompanyClient;
use App\Models\CompanyNews;
use App\Repositories\CompanyClientRepository;
use App\Repositories\LoginSmsCodeRepository;
use App\Rules\ClientSmsCodeRule;
use App\Rules\PhoneFormatRule;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Validator;

class NewsController extends Controller
{
    /**
     * @apiGroup NewsGroup
     * @apiVersion 0.1.0
     * @apiName NewsRequest
     * @apiUse RequestHeader
     *
     * @api {post} /news Последние новости
     *
     * @apiSuccess {String} data.id ID
     * @apiSuccess {String} data.created_at Дата создания
     * @apiSuccess {String} data.title Заголовок
     * @apiSuccess {String} data.description Описание
     * @apiSuccess {String} data.image Изображение
     * @apiSuccess {Array} links Ссылки для постраничной навигации
     * @apiSuccess {Array} meta Информация о текущей странице
     *
     * @apiSuccessExample {json} Успешный ответ
     *     HTTP/1.1 200 OK
     *     {
     *          "data": [{
     *              "id": "2",
     *              "created_at": "06.02.2020",
     *              "title": "Заголовок",
     *              "description": "Описание",
     *              "image": "<URL>"
     *          }],
     *          "links": {
     *              "first": "http://fitness.org/api/v1/news?page=1",
     *              "last": "http://fitness.org/api/v1/news?page=1",
     *              "prev": null,
     *              "next": null
     *          },
     *          "meta": {
     *              "current_page": 1,
     *              "from": 1,
     *              "last_page": 1,
     *              "path": "http://fitness.org/api/v1/news",
     *              "per_page": 15,
     *              "to": 1,
     *              "total": 1
     *          }
     *      }
     *
     * @apiUse UnauthorizedError
     *
     */
    public function index()
    {
        return new NewsApiCollection(CompanyNewsGetter::getList());
    }

    /**
     * @apiGroup NewsGroup
     * @apiVersion 0.1.0
     * @apiName NewsDetailRequest
     * @apiUse RequestHeader
     *
     * @api {post} /news/2 Новость детально
     *
     * @apiSuccess {String} data.id ID
     * @apiSuccess {String} data.created_at Дата создания
     * @apiSuccess {String} data.title Заголовок
     * @apiSuccess {String} data.description Описание
     * @apiSuccess {String} data.content Содержание новости
     * @apiSuccess {String} data.image Изображение
     *
     * @apiSuccessExample {json} Успешный ответ
     *     HTTP/1.1 200 OK
     *     {
     *          "data": {
     *              "id": "2",
     *              "created_at": "06.02.2020",
     *              "title": "Заголовок",
     *              "description": "Описание",
     *              "content": "Content",
     *              "image": "<URL>"
     *          }
     *      }
     *
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     *
     */
    public function show(CompanyNews $news)
    {
        return new NewsDetailApiResource($news);
    }
}

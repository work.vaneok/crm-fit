<?php
namespace App\Models;

use App\Models\Traits\ByCompanyScopeTrait;
use Illuminate\Database\Eloquent\Model;

class CompanyAdminSettings extends Model
{
    use ByCompanyScopeTrait;

    protected $table = 'company_admin_settings';
    protected $guarded = ['created_at'];

    protected $casts = [
        'settings' => 'array',
    ];

    public function getKeyName()
    {
        return 'company_id';
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}

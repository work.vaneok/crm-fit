<?php
namespace App\Repositories;

use App\Models\Client;
use App\Models\Company;
use App\Models\CompanySetting;

/**
 * Пол
 *
 * Class SexRepository
 * @package App\Repositories
 */
class SexRepository
{

    public function getForSelectBox()
    {
        return [
            '1' => __('М'),
            '2' => __('Ж'),
        ];
    }

}

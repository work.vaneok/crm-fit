<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhoneFormatRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $matches = [];
        preg_match('/^380\d{9}$/i', $value, $matches);
        return isset($matches[0]);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Номер должен начинаться с 380 и иметь формат 380123456789');
    }
}

$(document).ready(function () {
    var ajaxModal = new AjaxFormModal();
    var buttonRefill = new AjaxButton('.ajax-refill-button');
    var buttonBuy = new AjaxButton('.ajax-buy-button');
    var photoHistory = new AjaxButton('.photo-history').setTypeView();

    ajaxModal.addOpenButton(buttonRefill);
    ajaxModal.addOpenButton(buttonBuy);
    ajaxModal.addOpenButton(photoHistory);
    ajaxModal.build();

   /* $(function () {
        $("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");
    });*/
});

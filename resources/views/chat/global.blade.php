@extends('layouts.chat')

@section('title')
    {{ __('Посещения') }}
@endsection

@section('content')
    <div class="p-xl-4">
        @include('chat.chat_block',['chat_with' => $client, 'current_url' => $current_url])
    </div>
@endsection

@section('actions')

@endsection

@push('scripts')
    <script>
        window.chat_id = '<?=$room_id?>';
        window.by_id = '<?=$by_id?>';
    </script>
    <script src="{{mix('js/chat.js')}}"></script>
    <script src="https://rawgit.com/jackmoore/autosize/master/dist/autosize.min.js"></script>
@endpush

<?php
namespace App\Actions\Client;

use App\Models\Client;
use App\Models\CompanyClient;
use Illuminate\Support\Str;

class CreateClientAction
{
    /**
     * @param $data
     * @return Client|\Illuminate\Database\Eloquent\Model
     */
    public function execute($data)
    {
        return Client::create([
            'phone' => $data['phone'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'] ?? '',
            'sex' => $data['sex'] ?? -1,
            'birthday' => $data['birthday'] ?? null,
            'created_by_user_id' => \Auth::id(),
            'password' => \Hash::make(Str::random(6)),
        ]);
    }
}

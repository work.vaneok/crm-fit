<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyLocationApiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'email' => $this->email,
            'image' => $this->getFirstMedia() ? $this->getFirstMedia()->getFullUrl('thumb') : false,
            'address' => $this->address,
            'phones' => collect($this->phones)->filter(function ($item) {
                return !is_null($item);
            }),
        ];
    }
}

<?php

namespace App\Admin\Forms;

use App\Actions\CompanyOwner\CreateCompanyOwnerAction;
use App\Models\Company;
use App\Models\CompanyAdminSettings;
use App\Repositories\TariffRepository;
use App\Rules\PhoneFormatRule;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;

class CompanySettingsForm extends Form
{
    /**
     * The form title.
     *
     * @var string
     */
    public $title = 'Company Settings Form';
    private $id;
    /**
     * @var Company
     */
    private $company;


    /**
     * Handle the form request.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request)
    {
        $company_id = $request->get('company_id');
        $settings = CompanyAdminSettings::findOrNew($company_id);

        $arr = [
            'google_play',
            'apple_store',
            'policy',
            'offer',
            'title',
            'description',
        ];

        $settings->company_id = $company_id;
        $settings->domain = $request->get('domain');
        $settings->settings = $request->only($arr);
        $settings->save();

        admin_success('Processed successfully.');

        return redirect(route('admin.companies.index'));
    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $this->hidden('company_id', 'company_id');

        $set = $this->company && $this->company->adminSettings ? $this->company->adminSettings : null;
        $settings = $set ? $set->settings : [];

        $this->text('domain', 'Domain')
            ->rules(['required', Rule::unique('company_admin_settings')
            ->ignore(\request()->get('company_id'), 'company_id')])
            ->default($set ? $set->domain : '');

        $this->text('title', 'Title')->default($settings['title'] ?? '');

        $this->text('description', 'Описание')->default($settings['description'] ?? '');

        $this->text('google_play', 'Google Play')->default($settings['google_play'] ?? '');
        $this->text('apple_store', 'Apple Store')->default($settings['apple_store'] ?? '');
        $this->text('policy', 'Ссылка на политику конфиденциальности')->default($settings['policy'] ?? '');
        $this->text('offer', 'Ссылка на оферту')->default($settings['offer'] ?? '');

    }

    /**
     * The data of the form.
     *
     * @return array $data
     */
    public function data()
    {
        return [
            'company_id' => \request('company')['id']
        ];
    }

    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
}

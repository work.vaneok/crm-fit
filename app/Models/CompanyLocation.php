<?php

namespace App\Models;

use App\Models\Traits\BarcodeGeneratorTrait;
use App\Models\Traits\DisplayDateFormat;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class CompanyLocation extends Model implements HasMedia
{
    use DisplayDateFormat, HasMediaTrait;

    protected $table = 'company_locations';
    protected $guarded = ['id'];
    protected $casts = [
        'phones' => 'array'
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_MAX, 250, 250);
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function companyStaffCompanyLocation()
    {
        return $this->belongsToMany(CompanyStaffCompanyLocation::class, 'id', 'company_location_id');
    }
}

<?php
namespace App\Listeners;

use App\Events\ClientVisitReturnedEvent;
use App\Models\ClientVisit;
use App\Models\Logs\LogClientVisits;

class LogReturnVisitListener
{
    public function handle(ClientVisitReturnedEvent $event)
    {
        /** @var ClientVisit $visit */
        $visit = ClientVisit::withTrashed()->firstWhere('id', $event->visit_id);
        if (!$visit) {
            \Log::error('Не получил визит для обновления посещений');
            return false;
        }

        LogClientVisits::return($visit);
    }
}

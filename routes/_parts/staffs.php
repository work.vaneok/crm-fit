<?php

use App\Models\CompanyStaff;

Route::group(['middleware' => 'role:' . \App\Constants\Roles::COMPANY_OWNER], function () {
    Route::get('/staffs', 'StaffsController@index')->middleware('can:viewAny, ' . CompanyStaff::class)->name('staffs.index');

    Route::get('/staffs/create', 'StaffsController@create')->middleware('can:create, ' . CompanyStaff::class)->name('staffs.create');
    Route::get('/staffs/edit/{staff}', 'StaffsController@edit')->middleware('can:update,staff')->name('staffs.edit');
    Route::get('/staffs/show/{staff}', 'StaffsController@show')->middleware('can:view,staff')->name('staffs.show');;

    Route::post('/staffs/create', 'StaffsController@store')->middleware('can:create, ' . CompanyStaff::class)->name('staffs.store');
    Route::put('/staffs/edit/{staff}', 'StaffsController@update')->middleware('can:update,staff')->name('staffs.update');

    Route::patch('/staffs/fire/{staff}', 'StaffsController@fireEmployee')->middleware('can:fireEmployee,staff')->name('staffs.fire');
});

<?php
namespace App\Components\Globals;

use App\Constants\OrderStatuses;
use Carbon\Carbon;

/**
 * Список статусов заказов
 * Class OrderStatusesValues
 * @package App\Components\Globals
 */
class OrderStatusesValues
{
    private static $_values = [];

    public static function get()
    {
        if (empty(static::$_values)) {
            static::$_values = [
                '0' => [
                    'label' => __('Создан'),
                ],
                '1' => [
                    'label' => __('Оплачен'),
                ],
                '2' => [
                    'label' => __('Завершен с ошибкой'),
                ],
                '3' => [
                    'label' => __('Отменен'),
                ],
            ];
        }

        return static::$_values;
    }

    public static function getByKey($key)
    {
        return static::get()[$key];
    }
}

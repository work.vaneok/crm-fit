@extends('layouts.dashboard')

@section('title')
    {{ __('Настройки компании') }}
@endsection

@section('content')
    <div class="content-m-t bd-example bd-example-tabs">

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="base-data-tab" data-toggle="tab" href="#base-data" role="tab" aria-controls="base-data" aria-selected="true">{{ __('Основная информация') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="true">{{ __('Настройки') }}</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="base-data" role="tabpanel" aria-labelledby="home-tab">
                {!! Form::open()->multipart()->route('company-settings.update')->put()->fill($company) !!}

                {!! Form::text('title', 'Название') !!}
                {!! Form::textarea('description', 'Описание') !!}
                {!! Form::file('image', __('Изображение')) !!}
                @image_media(['model' => $company, 'form' => true,'key'=>'image'])
                {!! Form::file('image_back', __('Фон')) !!}
                @image_media(['model' => $company, 'form' => true,'key'=>'image_back'])



                <dl class="row">
                    <dt class="col-sm-2">
                        {{ __('Тариф') }}
                    </dt>
                    <dd class="col-sm-10">
                        {{ $company->tariff->title }}
                    </dd>
                </dl>

                <dl class="row">
                    <dt class="col-sm-2">
                        {{ __('Доступна по') }}
                    </dt>
                    <dd class="col-sm-10">
                        {{ $company->formatDate('available_to') }}
                    </dd>
                </dl>

                <div class="form-group row">
                    <div class="offset-2 col-sm-10">
                        {!! Form::submit(__('Сохранить изменения')) !!}
                    </div>
                </div>
                {!!  Form::close() !!}
            </div>
            <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                {!! Form::open()->route('company-settings.update-settings')->put() !!}
                @foreach($settings as $key => $value)
                    {!! Form::text($key, __('settings.' . $key), $value) !!}
                @endforeach
                <div class="form-group row">
                    <div class="offset-2 col-sm-10">
                        {!! Form::submit(__('Сохранить изменения')) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

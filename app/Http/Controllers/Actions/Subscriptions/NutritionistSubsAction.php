<?php

namespace App\Http\Controllers\Actions\Subscriptions;

use App\Http\Controllers\Actions\BaseAction;

class NutritionistSubsAction extends BaseAction
{
    public $company_id;
    public $user_id;

    public $per_page = 15;
    public $page = 1;

    public $viewParams;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'user_id' => ['integer', 'required'],
            'per_page' => ['integer'],
            'page' => ['integer'],
        ];
    }

    public function action()
    {

    }

}

<?php

namespace App\Http\Middleware;

use App\Models\CompanyStaff;
use Closure;

class HasCompanyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        $staff = CompanyStaff::where(['user_id' => $user->id])->first();
        if (!$staff) {
            return redirect('no-companies');
        }

        return $next($request);
    }
}

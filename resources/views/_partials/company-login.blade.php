@if( empty(Config::get('partial.company-login')))
    {{ Config::set('partial.company-login', 1) }}

    @push('js')
        <script src="{{ mix('js/company-login.js') }}"></script>
    @endpush

@endif


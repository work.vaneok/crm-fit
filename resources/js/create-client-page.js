jQuery(function ($) {
    $('#send-sms').on('click', function () {
        axios.post(routes('clients.send-confirm-sms'), $(this).closest('form').serialize()).then(function (response) {
            ajaxSuccess(response.data.message);
        }).catch(ajaxError);
    });
});

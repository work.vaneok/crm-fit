<?php
namespace App\Components\Payments;

class PaymentProviderFactory
{
    private static $class = [];

    /**
     * @param $providerName
     * @return IPaymentProvider
     */
    public static function load($providerName)
    {
        if (!isset(static::$class[$providerName])) {
            $className = __NAMESPACE__ . '\\' . ucfirst($providerName) . 'Provider';
            static::$class[$providerName] = (new $className());
        }

        return static::$class[$providerName];
    }
}

<?php

namespace App\Http\Controllers;

use App\Helper\RequestHelper;
use App\Http\Controllers\Traits\Responses;
use App\Responses\ApiResponse;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Spatie\ArrayToXml\ArrayToXml;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Responses;

    public function redirect($url)
    {
        if (request('backUrl')) {
            return redirect(request('backUrl'));
        }

        return redirect($url);
    }


    final protected function createSuccessResponseEmpty($data)
    {
        return ApiResponse::success($data);
    }

    final protected function createSuccessResponse(array $data = [])
    {

        $data[RequestHelper::SERVER_RESPONSE] = RequestHelper::SERVER_RESPONSE_OK;
        /* $body = json_decode(file_get_contents('php://input'), true);
         $response_type = isset($body['response_type']) ? $body['response_type'] : false;

         if ($response_type == 'xml') {
             $data = ArrayToXml::convert(['data' => $data], '', true, 'UTF-8');

             return ApiResponse::successXml($data);
         }*/
        return ApiResponse::success($data);
    }

    final protected function createErrorResponse($message, $response_status = RequestHelper::SERVER_RESPONSE_ERROR, $response_code = RequestHelper::SERVER_ERROR): ApiResponse
    {
        return ApiResponse::error(
            $message,
            $response_status,
            $response_code
        );
    }
}

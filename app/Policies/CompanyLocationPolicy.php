<?php

namespace App\Policies;

use App\Components\CrmHelper;
use App\Constants\Roles;
use App\Models\CompanyLocation;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyLocationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any company locations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the company location.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyLocation  $location
     * @return mixed
     */
    public function view(User $user, CompanyLocation $location)
    {
        return $location->company->owner_id == $user->id || $location->company->staffs()->where(['user_id' => $user->id])->first();
    }

    /**
     * Determine whether the user can create company locations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole(Roles::COMPANY_OWNER);
    }

    /**
     * Determine whether the user can update the company location.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyLocation  $location
     * @return mixed
     */
    public function update(User $user, CompanyLocation $location)
    {
        return $location->company_id == CrmHelper::companyId() && $user->hasRole(Roles::COMPANY_OWNER);
    }

    /**
     * Determine whether the user can delete the company location.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyLocation  $location
     * @return mixed
     */
    public function delete(User $user, CompanyLocation $location)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the company location.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyLocation  $location
     * @return mixed
     */
    public function restore(User $user, CompanyLocation $location)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the company location.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CompanyLocation  $location
     * @return mixed
     */
    public function forceDelete(User $user, CompanyLocation $location)
    {
        //
    }
}

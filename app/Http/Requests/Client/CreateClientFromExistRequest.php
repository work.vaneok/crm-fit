<?php
namespace App\Http\Requests\Client;


use App\Http\Requests\Traits\DefaultCompanyIdInRequest;
use App\Rules\ClientConfirmSmsCodeRule;
use App\Rules\NoCompanyClientRule;
use App\Rules\PhoneFormatRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateClientFromExistRequest extends FormRequest
{
    use DefaultCompanyIdInRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required'],
            'client_id' => ['required', new NoCompanyClientRule($this->get('company_id'))],
        ];
    }
}

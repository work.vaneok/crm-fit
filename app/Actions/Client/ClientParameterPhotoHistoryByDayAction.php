<?php

namespace App\Actions\Client;


use App\Actions\BaseAction;
use App\Repositories\CompanyClientRepository;
use View;

class ClientParameterPhotoHistoryByDayAction extends BaseAction
{

    public $parameter_id;

    public function rules(): array
    {
        return [
            'parameter_id' => [
                'required',
                'integer',
            ],
        ];
    }

    public function action()
    {
        $photos = app(CompanyClientRepository::class)->parameterPhotosById($this->parameter_id);

        return
            [
                'title' => __("Детальная информация"),
                'content' => View::make('clients.history-by-date', [
                        'photos' => $photos
                    ]
                )->render()
            ];
    }
}

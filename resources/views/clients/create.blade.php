@extends('layouts.dashboard')

@section('title')
    {{ __('Создание клиента') }}
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->multipart()->route('clients.store')->post() !!}
        {!! Form::text('phone', 'Номер телефона', $phone)->placeholder(config('app.form_phone_mask'))->help(__('Номер телефона вводите в формате',['?'=>config('app.form_phone_mask')]))->required() !!}
        {!! Form::text('first_name', 'Имя')->required() !!}
        {!! Form::text('last_name', 'Фамилия')->required() !!}
        {!! Form::text('birthday', 'Дата рождения')->placeholder(config('app.form_date_mask'))->help(__('Дату вводите в формате',['?'=>config('app.form_date_mask')]))->required() !!}
{{--
        {!! Form::date('birthday', 'Дата рождения')->required()->attrs(['class' => 'col-md-4']) !!}
--}}
        {!! Form::select('sex', 'Пол')->required()->options($sexValues)->attrs(['class' => 'col-md-4']) !!}
        {!! Form::text('email', 'Email') !!}
        {!! Form::hidden('back', $backUrl) !!}

        <div class="form-group row send-sms-wrap">
            <div class="offset-2 col-sm-10">
                <button id="send-sms" type="button" class="btn btn-warning">{{ __("Отправить СМС с кодом") }}</button>
            </div>
        </div>

        {!! Form::text('sms_code', __('SMS Код'))->required()->help(__('Действиует 3 минуты'))->attrs(['class' => 'col-md-4']) !!}

        <div class="form-group row submit-btn-wrap">
            <div class="offset-2 col-sm-10">
                {!! Form::submit(__('Сохранить изменения')) !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection


@push('scripts')
    <script src="{{ mix('js/create-client-page.js') }}"></script>
@endpush



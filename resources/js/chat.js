import firebase from 'firebase/app';
import 'firebase/firestore';

class Chat {
    constructor(config) {
        this.chat_id = config.chat_id;
        this.by_id = config.by_id;
        firebase.initializeApp({
            apiKey: "AIzaSyCmSN5FswlLGgJnsFtqCM8oHSw3zsOpQiY",
            authDomain: "abonement-a3353.firebaseapp.com",
            projectId: "abonement-a3353",
            messagingSenderId: "916957942109",
        });
    };

    createLeftMessage(content = '', time = '', user = '') {
        return '<div class="d-flex justify-content-start flex-row py-2">\n' +
            /*'        <div class="center-block align-self-top">\n' +
            '            <img src="/images/default-avatar.png"\n' +
            '                 class="rounded-circle mr-1" alt="' + user + '" width="48" height="48">\n' +
            '        </div>\n' +*/
            '        <div class="flex-shrink-1 bg-dark text-white rounded py-2 px-3 ml-3">\n' +
            '            <div class="font-weight-bold mb-1">' + user + '</div>\n' +
            '            <div style="overflow-wrap: anywhere">' + content + '</div>\n' +
            '            <div class="text-muted small text-right mb-1">' + time + '</div>\n' +
            '        </div>\n' +
            '    </div>'
    };

    createRightMessage(content = '', time = '', user = '') {
        return '<div class="d-flex justify-content-start flex-row-reverse py-2">\n' +
            /*'        <div class="center-block align-self-top">\n' +
            '            <img src="/images/default-avatar.png"\n' +
            '                 class="rounded-circle mr-1" alt="' + user + '" width="48" height="48">\n' +
            '        </div>\n' +*/
            '        <div class="flex-shrink-1 bg-white rounded py-2 px-3 mr-3">\n' +
            '            <div class="font-weight-bold mb-1">' + user + '</div>\n' +
            '            <div style="overflow-wrap: anywhere">' + content + '</div>\n' +
            '            <div class="text-muted small text-right mb-1">' + time + '</div>\n' +
            '        </div>\n' +
            '    </div>'
    }

    timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp * 1000);
        var year = a.getFullYear();
        var month = a.getMonth();
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();
        return date + '/' + month + '/' + year + ' ' + hour + ':' + min + ':' + sec;
    }

    init() {
        const db = firebase.firestore();
        const messagesBlock = jQuery('#chat-messages');
        let self = this;

        db.collection("rooms").doc(this.chat_id).collection("messages")
            .onSnapshot(function (snapshot) {
                snapshot.docChanges().forEach(function (change) {
                    if (change.type === "added") {
                        var data = change.doc.data();
                        var html = 'error';
                        var time = self.timeConverter(data.time);
                        var user = data.by_name;

                        if (data.by_id === self.by_id) {
                            html = self.createRightMessage(data.message, time, user);
                        } else {
                            html = self.createLeftMessage(data.message, time, user);
                        }
                        messagesBlock.append(html);
                    }
                    if (change.type === "modified") {
                        console.log("Modified city: ", change.doc.data());
                        // This is equivalent to child_changed
                    }
                    if (change.type === "removed") {
                        console.log("Removed city: ", change.doc.data());
                        // This is equivalent to child_removed
                    }
                    $('html, body').scrollTop(10000);
                });
            });
    };
}


$(document).ready(function () {
    let chat = new Chat({
        chat_id: window.chat_id,
        by_id: window.by_id,
    });
    chat.init();

    $('#btn-send-message').click(function () {
        let message = $('#chat-message').val();
        console.log(message);
        $.post("/chat/send-like-user", {message: message, chat_id: window.chat_id})
            .done(function (data) {
                $('#chat-message').val('');
                $('#chat-message').animate({height: '38px'}, 500);
            });
    });

    $('#chat-messages').ready(function () {
        let ta = document.getElementById('chat-message');
        autosize(ta);
    });
});



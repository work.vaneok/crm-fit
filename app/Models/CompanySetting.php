<?php

namespace App\Models;

use App\Models\Traits\ByCompanyScopeTrait;
use Illuminate\Database\Eloquent\Model;

class CompanySetting extends Model
{
    use ByCompanyScopeTrait;

    protected $table = 'company_settings';
    protected $guarded = ['id'];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}

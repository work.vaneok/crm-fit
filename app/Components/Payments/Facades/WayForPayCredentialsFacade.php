<?php
namespace App\Components\Payments\Facades;

use Illuminate\Support\Facades\Facade;

class WayForPayCredentialsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'WayForPayCredentials';
    }
}

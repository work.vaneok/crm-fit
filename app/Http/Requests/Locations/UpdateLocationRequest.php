<?php
namespace App\Http\Requests\Locations;

use App\Components\CrmHelper;
use Illuminate\Foundation\Http\FormRequest;

class UpdateLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'address' => ['required'],
            'description' => ['nullable'],
            'email' => ['nullable'],
            'phones' => ['nullable', 'array'],
            'image' => ['image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
        ];
    }

}

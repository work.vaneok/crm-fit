<?php
namespace App\Observers;

use App\Components\CrmHelper;

class CompanyIdObserver
{
    public function creating($model)
    {
        $model->company_id = CrmHelper::company() ? CrmHelper::companyId() : 0;
    }
}

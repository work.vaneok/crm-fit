<?php

namespace App\Responses;


use App\Helper\RequestHelper;
use App\Helper\StatusHelper;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use InvalidArgumentException;

final class ApiResponse extends JsonResponse
{


    public static function error(string $message, $response = RequestHelper::SERVER_RESPONSE_ERROR, $error_code = RequestHelper::SERVER_ERROR): self
    {
        static::assertErrorDataIsValid($response, $message);

        return new static([
            RequestHelper::SERVER_RESPONSE => $response,
            'error' => [
                'message' => $message
            ]
        ], $error_code);
    }

    public static function base($data, $error_code = RequestHelper::SERVER_RESPONSE_OK_CODE): self
    {
        return new static($data, $error_code);
    }

    public static function successXml($content)
    {
        $response = Response::make($content, 200);
        $response->header('Content-Type', 'text/xml');
        return $response;
    }


    public static function success(array $data = []): self
    {
        return new static($data);
    }

    private static function assertErrorDataIsValid(string $code, string $message)
    {
        if (empty($code) || empty($message)) {
            throw new InvalidArgumentException('Error values cannot be empty.');
        }
    }
    /*
        public static function empty(): self
        {
            return new static(null, self::NO_CONTENT_STATUS);
        }

        public static function deleted(): self
        {
            return new static(null, self::NO_CONTENT_STATUS);
        }

        public static function forbidden(string $message): self
        {
            return static::error(StatusHelper::FORBIDDEN, $message)->setStatusCode(self::FORBIDDEN_STATUS);
        }

        public static function unauthenticated(string $message = 'Unauthenticated.'): self
        {
            return static::error(StatusHelper::UNAUTHENTICATED, $message)->setStatusCode(self::UNAUTHENTICATED_STATUS);
        }

        public static function notFound(string $message): self
        {
            return static::error(StatusHelper::NOT_FOUND, $message)->setStatusCode(self::RESOURCE_NOT_FOUND_STATUS);
        }

        private static function assertErrorDataIsValid(string $code, string $message)
        {
            if (empty($code) || empty($message)) {
                throw new InvalidArgumentException('Error values cannot be empty.');
            }
        }

        public static function paginate(LengthAwarePaginator $paginator): self
        {
            $data['status_code'] = StatusHelper::STATUS_CODE_RESPONSE_OK;
            $data['paginate_data'] = $paginator->items();
            return new static([
                'data' => $data,
                'meta' => [
                    'total' => $paginator->total(),
                    'per_page' => $paginator->perPage(),
                    'current_page' => $paginator->currentPage(),
                    'last_page' => $paginator->lastPage(),
                ]
            ]);
        }

        public static function created(array $data): self
        {
            return new static(['data' => $data], self::RESOURCE_CREATED_STATUS);
        }*/
}

<?php

namespace App\Http\Controllers\Actions;

use App\Models\CompanyClientParameterHistory;
use Carbon\Carbon;

class SaveParameterHistoryAction extends BaseAction
{
    public $company_id;
    public $client_id;
    public $weight;
    public $shoulder_volume;
    public $waist;
    public $hips;
    public $biceps_volume;
    public $breast_volume;
    public $comment;
    public $caviar_volume;
    public $leg_volume;
    public $date;


    public $model_id;


    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'client_id' => ['integer', 'required'],
            'weight' => ['required', 'string'],
            'shoulder_volume' => ['required', 'string'],
            'waist' => ['required', 'string'],
            'hips' => ['required', 'string'],
            'biceps_volume' => ['required', 'string'],
            'breast_volume' => ['required', 'string'],
            'leg_volume' => ['required', 'string'],
            'caviar_volume' => ['required', 'string'],
            'comment' => ['string', 'nullable'],
            'date' => ['string', 'required'],
        ];
    }

    public function action()
    {
        $model = new CompanyClientParameterHistory([
            CompanyClientParameterHistory::C_CLIENT_ID => $this->client_id,
            CompanyClientParameterHistory::C_COMPANY_ID => $this->company_id,
            CompanyClientParameterHistory::C_WEIGHT => $this->weight,
            CompanyClientParameterHistory::C_SHOULDER_VOLUME => $this->shoulder_volume,
            CompanyClientParameterHistory::C_WAIST => $this->waist,
            CompanyClientParameterHistory::C_HIPS => $this->hips,
            CompanyClientParameterHistory::C_BICEPS_VOLUME => $this->biceps_volume,
            CompanyClientParameterHistory::C_BREAST_VOLUME => $this->breast_volume,
            CompanyClientParameterHistory::C_LEG_VOLUME => $this->leg_volume,
            CompanyClientParameterHistory::C_CAVIAR_VOLUME => $this->caviar_volume,
            CompanyClientParameterHistory::C_COMMENT => $this->comment,
            CompanyClientParameterHistory::C_DATE => Carbon::parse($this->date)->toDateTimeString(),
        ]);
        if (!$model->save()) {

        }

        if (\Request::has('images')) {
            $images = \Request::file('images');
            foreach ($images as $image) {
                $model->addMedia($image)->toMediaCollection();
            }
        }
        $this->model_id = $model->id;

    }

}

<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 * @package App\Models
 * @property int $id
 * @property int $type_id
 * @property int $created_by_user_id
 * @property int $total_amount
 * @property int $created_at
 */
class Transaction extends Model
{
    const TableName = 'transactions';
    protected $table = self::TableName;
    public $timestamps = false;

    const COLUMN_ID = 'id';
    const COLUMN_TYPE_ID = 'type_id';
    const COLUMN_CREATED_BY_USER_ID = 'created_by_user_id';
    const COLUMN_TOTAL_AMOUNT = 'total_amount';
    const COLUMN_CREATED_AT = 'created_at';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by_user_id', 'id');
    }

}

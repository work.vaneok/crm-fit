<?php
namespace App\Components\Payments;

use App\Components\Payments\Facades\LiqPayFacade;
use App\Components\Payments\Models\PaymentModel;

class LiqPayProvider implements IPaymentProvider
{
    public function getForm(PaymentModel $model)
    {
        //TODO: логика на списание средств
        return LiqPayFacade::cnb_form(array(
            'action'         => 'pay',
            'amount'         => $model->getPrice(),
            'currency'       => $model->getCurrency(),
            'description'    => $model->getDescription(),
            'order_id'       => $model->getGeneratedOrderId(),
            'version'        => '3',
            'language'       => 'ru',
            'result_url'     => $model->getResultUrl(),
            'server_url'     => $model->getServerUrl(),
        ));
    }

    public function successPay($request)
    {
        /*array(
            'action' => 'pay',
            'payment_id' => 1038312558,
            'status' => 'success',
            'version' => 3,
            'type' => 'buy',
            'paytype' => 'card',
            'public_key' => 'sandbox_i70768861756',
            'acq_id' => 414963,
            'order_id' => '6ee34a7d-d4af-4e6f-8dbd-ec7125fb4829',
            'liqpay_order_id' => 'CUN6MW931559568736836943',
            'description' => 'description text',
            'sender_first_name' => 'Павел',
            'sender_last_name' => 'Пожетнов',
            'sender_card_mask2' => '424242*42',
            'sender_card_type' => 'visa',
            'sender_card_country' => 826,
            'ip' => '91.225.111.161',
            'amount' => 100.0,
            'currency' => 'UAH',
            'sender_commission' => 0.0,
            'receiver_commission' => 2.75,
            'agent_commission' => 0.0,
            'amount_debit' => 100.0,
            'amount_credit' => 100.0,
            'commission_debit' => 0.0,
            'commission_credit' => 2.75,
            'currency_debit' => 'UAH',
            'currency_credit' => 'UAH',
            'sender_bonus' => 0.0,
            'amount_bonus' => 0.0,
            'mpi_eci' => '7',
            'is_3ds' => false,
            'language' => 'ru',
            'create_date' => 1559568698706,
            'end_date' => 1559568736928,
            'transaction_id' => 1038312558,
        );*/

        $sign = base64_encode( sha1(
            config('app.liqpay_private_key') .
            $request['data'] .
            config('app.liqpay_private_key')
            , 1 ));

        $params = LiqPayFacade::decode_params($request['data']);

//        file_put_contents('liqpay.log', var_export([request('signature'), $sign, $params], true) . "\n", FILE_APPEND);

        if ($request['signature'] !== $sign) {
            throw new \Exception('SIGNATURE error');
        }

        return [__parse_payment_order_id($params['order_id'])[1], $params, in_array($params['status'], ['success', 'wait_accept'])];
    }
}

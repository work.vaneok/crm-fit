<?php


namespace App\Models\Traits;



use Illuminate\Database\Eloquent\Builder;

trait ByCompanyScopeTrait
{
    public function scopeByCompany(Builder $q, $company_id)
    {
        $q->where(['company_id' => $company_id]);
    }
}

<?php
namespace App\DependedGetters;

use App\Components\CrmHelper;
use App\Models\CompanyLocation;
use App\Models\CompanyNews;
use Spatie\QueryBuilder\QueryBuilder;

class MobileSliderGetter
{
    static function getList()
    {
        $items = QueryBuilder::for(CompanyNews::class)
            ->select(['id', 'created_at', 'title', 'description'])
            ->with(['media'])
            ->where([
                'company_id' => CrmHelper::companyId()
            ])
            ->mobileSlider()
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return $items;
    }
}

<?php
namespace App\Http\Resources;

use App\Models\ClientVisit;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientVisitsApiResource extends JsonResource
{
    public function toArray($request)
    {
        /** @var ClientVisit $this */
        return [
            'date' => $this->formatDateTimeAsDB('created_at'),
            'location' => $this->location->title,
        ];
    }
}

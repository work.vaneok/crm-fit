<?php

namespace App\Http\Controllers;

use App\Actions\CompanyStaff\CreateCompanyStaffAction;
use App\Actions\CompanyStaff\FireOffEmployeeAction;
use App\Actions\CompanyStaff\SetAvailableLocationsAction;
use App\Components\Globals\CompanyRoleValues;
use App\DependedGetters\LocationsGetter;
use App\DependedGetters\StaffGetter;
use App\Http\Controllers\Traits\MediaAssetTrait;
use App\Http\Requests\Staffs\CreateCompanyStaffRequest;
use App\Http\Requests\Staffs\FireEmployeeRequest;
use App\Http\Requests\Staffs\UpdateCompanyStaffRequest;
use App\Models\CompanyStaff;
use App\Models\CompanyStaffCompanyLocation;
use App\Repositories\CompanyRolesRepository;
use App\Repositories\CompanyStaffCompanyLocationsRepository;

class StaffsController extends Controller
{
    use MediaAssetTrait;

    /**
     * @var CompanyRolesRepository
     */
    private $roles;
    private $locations;

    public function __construct()
    {
        $this->roles = app(CompanyRolesRepository::class);
        $this->locations = app(CompanyStaffCompanyLocationsRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = StaffGetter::getList();
        $roles = CompanyRoleValues::get();

        return view('staffs.index', ['staffs' => $staffs, 'roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->roles->getForSelectBoxForStaff();
        $locations = LocationsGetter::getListForStaffs();

        return view('staffs.create', compact('roles', 'locations'));
    }

    /**
     * @param CreateCompanyStaffRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\MediaCannotBeDeleted
     */
    public function store(CreateCompanyStaffRequest $request)
    {
        (new CreateCompanyStaffAction())->execute($request);

        return redirect()->route('staffs.index');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyStaff $staff)
    {
        return view('staffs.show', compact('staff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyStaff $staff)
    {

        $roles = $this->roles->getForSelectBoxForStaff();
        $locations = LocationsGetter::getListForStaffs();;
        $selectedLocations = CompanyStaffCompanyLocation::where('company_staff_id', $staff->id)->get('company_location_id');

        return view('staffs.edit', compact('staff', 'roles', 'locations', 'selectedLocations'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\MediaCannotBeDeleted
     */
    public function update(UpdateCompanyStaffRequest $request, CompanyStaff $staff, CompanyStaffCompanyLocation $companyStaffCompanyLocation)
    {
        $staff->update($request->only('role_in_company', 'trainer_price'));
        $staff->user->update($request->only('first_name', 'last_name', 'birthday', 'phone'));
        (new SetAvailableLocationsAction())->execute($staff->id, $request->get('locations_id'));

        return redirect()->route('staffs.index');
    }

    public function fireEmployee(FireEmployeeRequest $request, CompanyStaff $staff)
    {
        (new FireOffEmployeeAction())->execute($staff, $request->get('reason'));
    }
}

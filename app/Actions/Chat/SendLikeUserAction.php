<?php

namespace App\Actions\Chat;


use App\Actions\BaseAction;
use App\Components\Chat\ChatComponent;

class SendLikeUserAction extends BaseAction
{


    public $user_id;
    public $message;
    public $chat_id;
    public $company_id;


    public function rules(): array
    {
        return [
            'chat_id' => ['required'],
            'message' => ['string', 'required'],
            'user_id' => ['integer', 'required'],
            'company_id' => ['integer', 'required']
        ];
    }

    public function action()
    {
        ChatComponent::addMessageLikeUser($this->chat_id, $this->user_id, $this->message,$this->company_id);
    }

}

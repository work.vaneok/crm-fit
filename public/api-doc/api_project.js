define({
  "name": "Abonement",
  "version": "0.1.0",
  "description": "Описание, как подключиться к api",
  "title": "API",
  "url": "https://abonement.com.ua/api/v1",
  "header": {
    "title": "Интеграция с API",
    "content": ""
  },
  "order": [
    "SmsCodeRequest",
    "TokenRequest",
    "RegisterRequest",
    "MyInfoRequest",
    "ActiveSubscriptionsRequest",
    "CompanySubscriptionsRequest",
    "NewsRequest",
    "NewsDetailRequest"
  ],
  "template": {
    "forceLanguage": "ru",
    "withCompare": true,
    "withGenerator": false
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-03-17T15:41:03.713Z",
    "url": "http://apidocjs.com",
    "version": "0.20.0"
  }
});

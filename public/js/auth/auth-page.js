$(document).ready(function () {
    var ajaxModal = new AjaxFormModal();
    var resetPassword = new AjaxButton('.reset-password');
    ajaxModal.addOpenButton(resetPassword);
    ajaxModal.build();
});

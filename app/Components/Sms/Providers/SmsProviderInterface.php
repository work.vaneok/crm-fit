<?php
namespace App\Components\Sms\Providers;

interface SmsProviderInterface
{
    public function sendSms($phone, $text, $time = false);

    public function checkStatusSms($id);
}

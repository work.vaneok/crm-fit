<?php

namespace App\Rules;

use App\Models\CompanyLocation;
use App\Models\CompanyStaff;
use App\Models\Subscription;
use App\Rules\Traits\SetCompanyIdTrait;
use Auth;
use Illuminate\Contracts\Validation\Rule;

class IsLocationRelatedToCompanyRule implements Rule
{
    use SetCompanyIdTrait;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return CompanyLocation::where([
            'id' => $value,
            'company_id' => $this->company_id
        ])->first() ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Локация не принадлежит компании');
    }
}

<?php
namespace App\Repositories;

use App\Components\Globals\CompanyRoleValues;

/**
 * Роли в компании
 *
 * Class CompanyRolesRepository
 * @package App\Repositories
 */
class CompanyRolesRepository
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function getForSelectBox()
    {
        return collect(
            CompanyRoleValues::get()
        );
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getForSelectBoxForStaff()
    {
        return collect(
            CompanyRoleValues::get()
        );
    }
}

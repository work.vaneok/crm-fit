<li class="nav-item ">
    <a class="nav-link  @if(\Request::segment(1) == $segment)  active @endif" href="{{ $link }}">
        <span class="fa fa-home"></span>
        {{ $name }}
    </a>
</li>

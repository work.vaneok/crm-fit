<?php
namespace App\Http\Requests\Api;

use App\Components\CrmHelper;
use App\Http\Requests\Traits\DefaultCompanyIdInRequest;
use App\Http\Requests\Traits\DefaultCompanyLocationIdInRequest;
use App\Models\Subscription;
use App\Repositories\ValidityRepository;
use App\Rules\ClientOfCompanyRule;
use App\Rules\PhoneFormatRule;
use App\Rules\SubscriptionIsAvailableForMobileSaleRule;
use App\Rules\SubscriptionOfCompanyRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BuySubscriptionRequest extends FormRequest
{
    use DefaultCompanyLocationIdInRequest;

    /**
     * @var Subscription|null
     */
    private $subscription;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required'],
            'company_location_id' => ['required'],
            'subscription_id' => ['required', new SubscriptionOfCompanyRule($this->getCompanyId()), new SubscriptionIsAvailableForMobileSaleRule()],
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $this->subscription = Subscription::find($this->request->get('subscription_id'));
        });
    }

    /**
     * @return Subscription|null
     */
    public function getSubscription(): ?Subscription
    {
        return $this->subscription;
    }

}

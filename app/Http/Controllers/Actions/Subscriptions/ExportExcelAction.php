<?php

namespace App\Http\Controllers\Actions\Subscriptions;

use App\Components\CrmHelper;
use App\Export\CompanyClientExport;
use App\Http\Controllers\Actions\BaseAction;
use App\Models\ClientSubscription;
use App\Models\CompanyLocation;
use App\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ExportExcelAction extends BaseAction
{
    public $company_id;
    public $company_location_id;
    public $filter_client_id;
    public $filter_date_from;
    public $filter_date_to;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'company_location_id' => ['integer', 'required'],
            'filter_client_id' => ['integer', 'nullable'],
        ];
    }

    public function action()
    {
        $subsItem = [];
        $countRows = 0;

        $clientSubsModel = ClientSubscription::query()
            ->where(['company_id' => $this->company_id])
            ->where(['company_location_id' => $this->company_location_id])
            ->with(['user', 'subscription', 'order'])
            ->orderByDesc('id');

        if ($this->filter_client_id) {
            $clientSubsModel->where([
                'client_id' => $this->filter_client_id
            ]);
        }

        if ($this->filter_date_from && $this->filter_date_to) {
            $clientSubsModel->whereBetween('created_at', [
                Carbon::parse($this->filter_date_from)->startOfDay()->toDateTimeString(),
                Carbon::parse($this->filter_date_to)->endOfDay()->toDateTimeString()
            ]);
        }

        $clientSubsModel = $clientSubsModel->get();
        $subsItem[$countRows]['id'] = 'ID';
        $subsItem[$countRows]['available_from'] = 'Активен с';
        $subsItem[$countRows]['available_to'] = 'Активен до';
        $subsItem[$countRows]['quantity_of_occupation'] = 'Количество занятий';
        $subsItem[$countRows]['first_name'] = 'Имя Фамилия';
        $subsItem[$countRows]['phone'] = 'Телефон';
        $subsItem[$countRows]['title'] = 'Абонемент';
        $subsItem[$countRows]['active'] = 'Статус';
        $subsItem[$countRows]['amount'] = 'Цена';
        $subsItem[$countRows]['pay_source'] = 'Тип оплаты';
        $subsItem[$countRows]['created_at'] = 'Куплен/Создан';
        $subsItem[$countRows]['staff'] = 'Создан работником';
        $subsItem[$countRows]['location'] = 'Локация';

        $countRows = 1;
        foreach ($clientSubsModel as $item) {
            if ($item->order && explode(':', $item->order->created_by)[0] == 'clients_api') {
                $staff = '-';
                $location = 'Приложение';
            } else {
                $user = User::find($item->created_by_user_id);
                $staff = ($user) ? $user->first_name . ' ' . $user->last_name : '-';
                $location = CompanyLocation::find($item->company_location_id)->title;
            }

            $subsItem[$countRows]['id'] = $item->id;
            $subsItem[$countRows]['available_from'] = $item->available_from ?? '-';
            $subsItem[$countRows]['available_to'] = $item->available_to ?? '-';
            $subsItem[$countRows]['quantity_of_occupation'] = $item->quantity_of_occupation == -1 ? 'Безлимит' : $item->quantity_of_occupation;
            $subsItem[$countRows]['first_name'] = $item->user->first_name . ' ' . $item->user->last_name;
            $subsItem[$countRows]['phone'] = $item->user->phone . ' ';
            $subsItem[$countRows]['title'] = $item->subscription->title;
            $subsItem[$countRows]['active'] = ($item->deleted_at) ? __('Удалён') : ((($item->available_to < Carbon::now()) && $item->quantity_of_occupation <= 0) ? __('Неактивный') : __('Активный'));
            $subsItem[$countRows]['amount'] = $item->order->price ?? '0';
            $subsItem[$countRows]['pay_source'] = ($item->order && $item->order->payment_source == '1') ? __('Наличный') : (($item->order && $item->order->payment_source == '2') ? __('Безналичный') : '-');
            $subsItem[$countRows]['created_at'] = $item->created_at;
            $subsItem[$countRows]['staff'] = $staff;
            $subsItem[$countRows]['location'] = $location;
            $countRows++;
        }

        $export = new CompanyClientExport($subsItem);
        $filename = 'ПроданныеАбонементы_' . Carbon::now()->toDateString() . '.xlsx';
        return Excel::download($export, $filename);
    }
}

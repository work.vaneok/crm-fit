<?php

namespace App\Actions\Client;


use App\Actions\BaseAction;
use App\Repositories\CompanyClientRepository;
use View;

class ClientPhotoHistoryByDayAction extends BaseAction
{

    public $company_id;
    public $client_id;
    public $date;

    public function rules(): array
    {
        return [
            'date' => [
                'required',
                'string'
            ],
            'company_id' => [
                'required',
                'integer',
            ],
            'client_id' => [
                'required',
                'integer',
            ],
        ];
    }

    public function action()
    {

        $photos = app(CompanyClientRepository::class)->photosByDate($this->client_id, $this->company_id, $this->date);

        return
            [
                'title' => __("Детальная информация"),
                'content' => View::make('clients.history-by-date', [
                        'photos' => $photos
                    ]
                )->render()
            ];
    }
}

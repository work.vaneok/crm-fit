<?php
namespace App\Components\Globals;

use Carbon\Carbon;

/**
 * Периоды аннуляции
 * Class SubscriptionCancellationPeriods
 * @package App\Components\Globals
 */
class SubscriptionCancellationPeriods
{
    private static $_values = [];

    public static function get()
    {
        if (empty(static::$_values)) {
            static::$_values = [
                '1' => [
                    'label' => __('Год'),
                    'code' => 'year',
                    'nextDateFromCurrentFormatted' => Carbon::now()->endOfDay()->addYear()->format('d.m.Y'),
                    'nextDateFromCurrent' => Carbon::now()->endOfDay()->addYear(),
                ],
                '2' => [
                    'label' => __('6 месяцев'),
                    'code' => '6month',
                    'nextDateFromCurrentFormatted' => Carbon::now()->endOfDay()->addMonths(6)->format('d.m.Y'),
                    'nextDateFromCurrent' => Carbon::now()->endOfDay()->addMonths(6),
                ],
                '3' => [
                    'label' => __('1 месяц'),
                    'code' => '1month',
                    'nextDateFromCurrentFormatted' => Carbon::now()->endOfDay()->addMonth()->format('d.m.Y'),
                    'nextDateFromCurrent' => Carbon::now()->endOfDay()->addMonth(),
                ],
                '4' => [
                    'label' => __('Неделя'),
                    'code' => '1week',
                    'nextDateFromCurrentFormatted' => Carbon::now()->endOfDay()->addWeek()->format('d.m.Y'),
                    'nextDateFromCurrent' => Carbon::now()->endOfDay()->addWeek(),
                ],
            ];
        }

        return static::$_values;
    }

    public static function getByKey($key)
    {
        return static::get()[$key];
    }
}

<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->resource('companies', CompaniesController::class, [
        'as' => 'admin'
    ]);
    $router->resource('tariffs', TariffsController::class, [
        'as' => 'admin'
    ]);
    $router->resource('users', UsersController::class, [
        'as' => 'admin'
    ]);
    $router->get('/companies/{company}/settings', 'CompaniesController@settings')->name('admin.company.settings');
    $router->post('/companies/{company}/settings', 'CompaniesController@settingsStore')->name('admin.company.settings-store');
});

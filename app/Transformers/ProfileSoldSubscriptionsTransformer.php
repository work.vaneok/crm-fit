<?php

namespace App\Transformers;

use App\Constants\OrderStatuses;
use App\Repositories\OrderStatusRepository;
use PriceFormatter;
use App\Models\Order;

class ProfileSoldSubscriptionsTransformer
{
    public function __invoke(Order $item)
    {
        /** @var OrderStatusRepository $orderStatusRep */
        $orderStatusRep = app(OrderStatusRepository::class);
        return [
            'client_id' => $item->client_id,
            'client_name' => $item->client->getUserName(true),
            'client_phone' => $item->client->phone,
            'client_subscription_id' => $item->order_item_id,
            'created_at' => $item->formatDateTime('created_at'),
            'title' => $item->order_item_info['title'],
            'price' => $item->price,
            'price_formatted' => PriceFormatter::withCurrency($item->price),
            'soldBy' => $item->soldBy ? $item->soldBy->name : '',
            'pay_source' => (($item->payment_source == '1') ? __('Наличный') : (($item->payment_source == '2') ? __('Безналичный') : '-')),
            'status' => $item->status,
            'statusText' => $orderStatusRep->getById($item->status)['label'],
            'available_from' => $item->subscription ? $item->subscription->formatDate('available_from') : '-',
            'available_to' => $item->subscription ? $item->subscription->formatDate('available_to') : '-',
        ];
    }
}

@extends('layouts.dashboard', ['header' => $header])
@section('content')


    <section class="content-header">
        <h1>
            {{ $header ?? '' }}
            <small> {{ $description ?? '' }} </small>
        </h1>
    </section>
    <section class="content">
        {!! $content !!}
    </section>
@endsection

<?php
namespace App\Components\Sms;

use App\Components\Sms\Events\SmsAfterSendEvent;
use App\Components\Sms\Events\SmsBeforeSendEvent;
use App\Components\Sms\Events\SmsErrorSendEvent;
use App\Components\Sms\Models\SentSms;

class SmsEventSubscriber
{

    public function onBeforeSmsSend(SmsBeforeSendEvent $event) {
        $sms = SentSms::create([
            'internal_id' => $event->getSmsInternalId(),
            'phone' => $event->getPhone(),
            'content' => $event->getMsg(),
        ]);
    }

    public function onAfterSmsSend(SmsAfterSendEvent $event) {

        $sms = SentSms::firstOrCreate([
            'internal_id' => $event->getSmsInternalId(),
        ]);

        $sms->update(
            [
                'phone' => $event->getPhone(),
                'content' => $event->getMsg(),
                'response' => json_encode($event->getResult()),
                'status' => 1,
            ]);
    }

    public function onErrorSmsSend(SmsErrorSendEvent $event) {

        $sms = SentSms::firstOrCreate([
            'internal_id' => $event->getSmsInternalId(),
        ]);

        $sms->update(
            [
                'response' => $event->getMsg(),
            ]);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            SmsBeforeSendEvent::class,
            self::class . '@onBeforeSmsSend'
        );

        $events->listen(
            SmsAfterSendEvent::class,
            self::class . '@onAfterSmsSend'
        );

        $events->listen(
            SmsErrorSendEvent::class,
            self::class . '@onErrorSmsSend'
        );
    }
}

<?php

namespace App\Actions\Client;


use App\Actions\BaseAction;
use App\Constants\CompanyStaffRoles;
use App\Models\ClientSubscription;
use App\Models\Company;
use App\Models\CompanyStaff;
use View;

class ClientUpdateSubscriptionViewAction extends BaseAction
{

    public $subscription_id;


    public function rules(): array
    {
        return [
            'subscription_id' => ['integer', 'required'],
        ];
    }

    public function getTrainers($company_id)
    {
        $response = [];

        $trainers = CompanyStaff::query()->where([
            'company_id' => $company_id,
            'role_in_company' => CompanyStaffRoles::TRAINER
        ])->with([
            'user'
        ])->get();

        foreach ($trainers as $trainer) {
            $response[$trainer->user_id] = $trainer->user->first_name . ' ' . $trainer->user->last_name . ' ( ' . $trainer->user->phone . ')';
        }

        return $response;
    }

    public function getNutritionists($company_id)
    {
        $response = [];

        $nutritionists = CompanyStaff::query()->where([
            'company_id' => $company_id,
            'role_in_company' => CompanyStaffRoles::NUTRITIONIST
        ])->with([
            'user'
        ])->get();

        foreach ($nutritionists as $nutritionist) {
            $response[$nutritionist->user_id] = $nutritionist->user->first_name . ' ' . $nutritionist->user->last_name . ' ( ' . $nutritionist->user->phone . ')';
        }

        return $response;
    }

    public function action()
    {
        $clientSubscriptionModel = ClientSubscription::findOrFail($this->subscription_id);

        $trainers = [
                '' => __('Не выбрано')
            ] + $this->getTrainers($clientSubscriptionModel->company_id);

        $nutritionists = [
                '' => __('Не выбрано')
            ] + $this->getNutritionists($clientSubscriptionModel->company_id);




        return
            [
                'title' => __("Редактирование абонемента клиента"),
                'content' => View::make('clients.update_subscription', [
                        'model' => $clientSubscriptionModel,
                        'trainers' => $trainers,
                        'nutritionists' => $nutritionists
                    ]
                )->render()
            ];
    }
}

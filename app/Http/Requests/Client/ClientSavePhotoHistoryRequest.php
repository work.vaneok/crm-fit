<?php

namespace App\Http\Requests\Client;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ClientSavePhotoHistoryRequest
 * @package App\Http\Requests\Client
 * @property \DateTime $date
 */
class ClientSavePhotoHistoryRequest extends FormRequest
{
    /* */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'date' => [
                'required',
                'string'
            ],
           'images' => ['required', 'array'],
            'images.*' => 'mimes:jpeg,png,jpg,gif'
        ];
    }


}

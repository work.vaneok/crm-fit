<?php
namespace App\Actions\CompanyStaff;

use App\Events\ClientVisitReturnedEvent;
use App\Events\CompanyStaffFiredEvent;
use App\Models\ClientVisit;
use App\Models\CompanyStaff;
use DB;

class FireOffEmployeeAction
{
    public function execute(CompanyStaff $staff, $delete_reason = null)
    {
        DB::transaction(function () use ($staff, $delete_reason) {
            $staff->update(['delete_reason' => $delete_reason]);
            $staff->delete();
        });
        event(new CompanyStaffFiredEvent($staff->id, $staff->user_id, $staff->company_id));
    }
}

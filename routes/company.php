<?php
``
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


use Illuminate\Routing\Router;

Auth::routes(['register' => false, 'reset' => false, 'confirm' => false, 'verify' => false]);

Route::post('/login/sms', 'Auth\LoginController@sms')->name('login.sms')->middleware(['guest']);

Route::get('/login/reset-password-view', 'Auth\LoginController@resetPasswordView')->name('login.reset-password-view')->middleware(['guest']);
Route::post('/login/reset-password', 'Auth\LoginController@resetPassword')->name('login.reset-password')->middleware(['guest']);

Route::group(['middleware' => ['auth']], function (Router $router) {

    Route::get('no-companies', 'NoCompaniesController')->name('no-companies');

    $router->group(['middleware' => ['has-company']], function () {

        Route::get('/dashboard', 'HomeController@dashboard')->name('home');

        /** LOCATIONS */
        $this->register(base_path('routes/_parts/company_locations.php'));

        /** Company news */
        $this->register(base_path('routes/_parts/company_news.php'));

        /** SUBSCRIPTIONS */
        $this->register(base_path('routes/_parts/subscriptions.php'));

        /** STAFFS */
        $this->register(base_path('routes/_parts/staffs.php'));

        /** PROFILE */
        $this->register(base_path('routes/_parts/profile.php'));

        /** Company settings */
        $this->register(base_path('routes/_parts/company_settings.php'));

        /** Sales */
        $this->register(base_path('routes/_parts/sales.php'));

        /** Clients */
        $this->register(base_path('routes/_parts/clients.php'));

        /** Client visit */
        $this->register(base_path('routes/_parts/client-visit.php'));

        /** Client subscriptions */
        $this->register(base_path('routes/_parts/client_subscriptions.php'));

        /** Sold subscriptions */
        $this->register(base_path('routes/_parts/sold_subscriptions.php'));

        /** Firebases */
        $this->register(base_path('routes/_parts/firebase.php'));

        /** Chat */
        $this->register(base_path('routes/_parts/chat.php'));


    });


});

<?php

namespace App\Actions\Client;


use App\Actions\BaseAction;
use App\Components\CrmHelper;
use App\Forms\FirebaseNotificationForm;
use App\Helper\FirebaseHelper;
use App\Models\ClientDevice;
use App\Models\ClientSubscription;
use Carbon\Carbon;
use Fbm;
use GuzzleHttp\Exception\ClientException;

class ClientSubscriptionRestoreAction extends BaseAction
{

    public $subscription_id;


    public $resp_client_id;

    public function rules(): array
    {
        return [
            'subscription_id' => ['integer', 'required'],
        ];
    }

    public function action()
    {
        $clientSubscriptionModel =  ClientSubscription::findOrFail($this->subscription_id);
        $clientSubscriptionModel->deleted_at = null;
        $clientSubscriptionModel->save();
        $this->resp_client_id = $clientSubscriptionModel->client_id;

    }
}

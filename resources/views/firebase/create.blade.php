<div>
    {!! Form::open()->multipart()->route('firebase.create')->post() !!}
    {!! Form::hidden('company_id', $company_id) !!}
    {!! Form::hidden('type', $type) !!}
    {!! Form::text('title',  __('Заголовок'),$title)->required() !!}
    {!! Form::textarea('descriptions', __('Описание'),$description)->required() !!}
    {!! Form::textarea('image_url', __('URL картинки'),$image_url)->required() !!}
    <?php
    if ($type == \App\Actions\Firebase\FirebaseCreateViewAction::NOTIFICATION_TYPE_SEND_ONE) {
        echo Form::select('client_id', __('Клиент'), $clients);
    }
    ?>
    {!!  Form::close() !!}
</div>


<?php

namespace App\Actions\Chat;


use App\Actions\BaseAction;
use App\Components\Chat\ChatComponent;
use App\Components\Firebase\FirebaseService;
use App\Models\Client;
use Firebase\JWT\JWT;
use Google\Cloud\Firestore\V1beta1\FirestoreClient;

class ClientGlobalChatAction extends BaseAction
{

    public $company_id;
    public $client_id;
    public $user_id;

    public $resp_chat_room_id;
    public $resp_chat_by_id;
    public $resp_chat_with_client;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'client_id' => ['integer', 'required'],
            'user_id' => ['integer', 'required'],
        ];
    }

    public function action()
    {
        $this->resp_chat_room_id = ChatComponent::generateRoomIdForGlobalChat($this->company_id, $this->client_id);
        $this->resp_chat_by_id = ChatComponent::generateCompanyUserId($this->user_id);
        $client = Client::findOrFail($this->client_id);
        $this->resp_chat_with_client = $client->first_name . ' ' . $client->last_name . ' (' . $client->phone . ')';


    }

}

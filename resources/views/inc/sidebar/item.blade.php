<li class="{{request()->url() == $url ? 'active' : ''}}">
    <a href="{{$url}}">
        <i class="{{$icon ?? ''}}"></i>
        <span>{!! $text !!}</span>
    </a>
</li>


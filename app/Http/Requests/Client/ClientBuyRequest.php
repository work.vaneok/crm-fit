<?php

namespace App\Http\Requests\Client;


use App\Helper\ValidatorHelper;
use App\Models\CompanyClient;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ClientBuyRequest
 * @package App\Http\Requests\Client
 * @property string $amount
 * @property int $company_id
 * @property int $client_id
 * @property string $comment
 */
class ClientBuyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'amount' => [
                'required',
                'integer'
            ],
            'company_id' => [
                'required',
                'integer',
                ValidatorHelper::createExists(CompanyClient::TableName, CompanyClient::COLUMN_COMPANY_ID)
            ],
            'client_id' => [
                'required',
                'integer',
                ValidatorHelper::createExists(CompanyClient::TableName, CompanyClient::COLUMN_CLIENT_ID)
            ],
            'comment' => [
                'required',
                'string'
            ],
        ];
    }

    public function messages()
    {
        return [
            'amount.required' => __('Поле обязательно'),
            'comment.required' => __('Поле обязательно'),
            'amount.integer' => __('Поле должно быть числом'),
        ];
    }
}

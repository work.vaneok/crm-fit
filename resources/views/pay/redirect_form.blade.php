<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<style>
    * {
        font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
        color: #000;
    }

    input[type="image"] {
        display: none;
    }

    body {
        display: flex;
        align-items: center;
        background: rgba(0, 0, 0, 0.1);
    }
    .text   {
        width: 100%;
        text-align: center;
    }
    form {
        display: none;
    }
</style>
{!! $form !!}
<div class="text">
    {{ __('app.redirect_pay_text') }}
</div>


<script>
    document.forms[0].submit();
</script>

</body>
</html>

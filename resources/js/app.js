    /**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from "vue";

require('./bootstrap');

window.Vue = require('vue');

if (!(Vue.prototype.$route = window.routes)) {
    console.error('routes() is not defined check global.js');
}
if (!(Vue.prototype.$trans = window.trans)) {
    console.error('trans() is not defined check global.js');
}
if (!(Vue.prototype.$trans_choice = window.trans_choice)) {
    console.error('trans_choice() is not defined check global.js');
}

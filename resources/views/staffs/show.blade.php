@extends('layouts.dashboard')

@section('title')
    {{ __('Сотрудник') }}
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                {{ __('Имя') }}
            </dt>
            <dd class="col-sm-10">
                {{ $staff->username }}
            </dd>
        </dl>
        <dl class="row">
            <dt class="col-sm-2">
                {{ __('Телефон') }}
            </dt>
            <dd class="col-sm-10">
                {{ $staff->phone }}
            </dd>
        </dl>
    </div>
@endsection

@section('actions')
    @if (Auth::user()->can('update', $staff))
        <li>
            @edit(['route' => route('staffs.edit', ['staff' => $staff->id])])&nbsp;
        </li>
    @endif
@endsection

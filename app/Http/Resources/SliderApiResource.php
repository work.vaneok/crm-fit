<?php

namespace App\Http\Resources;

use App\Components\Globals\ValidityValues;
use App\Components\PriceFormatter;
use App\Models\Client;
use App\Models\ClientSubscription;
use App\Models\CompanyNews;
use Illuminate\Http\Resources\Json\JsonResource;

class SliderApiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this CompanyNews */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->getFirstMedia()->getFullUrl('thumb'),
        ];
    }
}

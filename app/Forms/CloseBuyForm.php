<?php

namespace App\Forms;


class CloseBuyForm
{

    private $buy_id;
    private $created_by;

    public function __construct()
    {

    }

    public function setCreatedBy($created_by_id)
    {
        $this->created_by = $created_by_id;
    }

    public function getCreatedBy()
    {
        return $this->created_by;
    }

    public function setBuyId(int $buy_id)
    {
        $this->buy_id = $buy_id;
    }

    public function getBuyId()
    {
        return $this->buy_id;
    }




}

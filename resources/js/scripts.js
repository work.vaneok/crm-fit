import * as feather from "feather-icons";

$(document).ready(function () {

    $('#sidebarToggle').click(function () {
       $('.sidebar').toggleClass('open');
    });

    $('.sidebar .dropdown-button').click(function () {

        let dropdown = $(this).next('ul');

        //$('.sidebar').removeClass('collapsed');

        dropdown.one('show.bs.collapse', function () {
            $(this).parent().addClass('active');
        });

        dropdown.one('hide.bs.collapse', function() {
           $(this).parent().removeClass('active');
        });
    });


    $('.visits').click(function () {
       $(this).toggleClass('active');
    });
});
@if(isset($model) && $model->hasMedia($key??''))
    @if(isset($form))
        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                @endif
                <div id="media-image">
                    <img src="{{asset($model->getMedia($key??'')->first()->getUrl($conversionName ?? 'thumb'))}}" alt=""
                         style="max-width: 150px; max-height: 150px;">
                    {{--<br>
                    <a onclick="deleteMedia('{{route('deleteMedia')}}',{{$model->getFirstMedia()->id}}, '#media-image'); return false;"
                       href="#"
                       class="btn btn-danger btn-sm">Удалить</a>--}}
                </div>
                @if(isset($form))

            </div>
        </div>
    @endif
@endif


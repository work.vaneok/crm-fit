@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4">{{ __('У ВАС НЕТ КОМПАНИЙ!') }}</h1>
            <p class="lead">{{ __('Возможно Вы были уволены, или у Вас еще нет созданных компаний') }}</p>
        </div>
    </div>

@endsection

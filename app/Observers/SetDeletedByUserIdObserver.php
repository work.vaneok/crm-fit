<?php
namespace App\Observers;

class SetDeletedByUserIdObserver
{
    public function deleting($model)
    {
        $model->update(['deleted_by' => \Auth::id()]);
    }
}

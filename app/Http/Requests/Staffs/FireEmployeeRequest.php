<?php
namespace App\Http\Requests\Staffs;

use App\Constants\CompanyStaffRoles;
use App\Repositories\CompanyRolesRepository;
use App\Rules\PhoneFormatRule;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class FireEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reason' => ['nullable'],
        ];
    }

}

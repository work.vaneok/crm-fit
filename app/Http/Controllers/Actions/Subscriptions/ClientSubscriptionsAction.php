<?php

namespace App\Http\Controllers\Actions\Subscriptions;

use App\Http\Controllers\Actions\BaseAction;
use App\Models\ClientSubscription;
use App\Models\CompanyClient;
use Carbon\Carbon;

class ClientSubscriptionsAction extends BaseAction
{
    public $company_id;
    public $company_location_id;
    public $filter_client_id;
    public $filter_date_from;
    public $filter_date_to;

    public $per_page = 15;
    public $page = 1;

    public $clientSubsModel;
    public $viewParams;

    public function rules(): array
    {
        return [
            'company_id' => ['integer', 'required'],
            'company_location_id' => ['integer', 'required'],
            'per_page' => ['integer'],
            'page' => ['integer'],
            'filter_client_id' => ['nullable']
        ];
    }

    public function action()
    {
        $this->viewParams['clients'] = ['' => __('поиск по всем клиентам')] + $this->getClientList($this->company_id);
        $this->viewParams['items'] = [];
        $this->viewParams['back_url'] = url()->full();

        $this->clientSubsModel = ClientSubscription::query()
            ->where(['company_id' => $this->company_id])
            ->where(['company_location_id' => $this->company_location_id])
            ->with(['user', 'subscription', 'order'])
            ->orderByDesc('id');

        if ($this->filter_client_id) {
            $this->clientSubsModel
                ->where(['client_id' => $this->filter_client_id]);
        }

        if ($this->filter_date_from && $this->filter_date_to) {
            $this->clientSubsModel->whereBetween('created_at', [
                Carbon::parse($this->filter_date_from)->startOfDay()->toDateTimeString(),
                Carbon::parse($this->filter_date_to)->endOfDay()->toDateTimeString()
            ]);
        }

        $this->clientSubsModel = $this->clientSubsModel->paginate($this->per_page, ['*'], 'page', $this->page);

        $subsItem = [];
        $carbon = Carbon::now();

        foreach ($this->clientSubsModel->items() as $item) {
            $subsItem[$item->id]['id'] = $item->id;
            $subsItem[$item->id]['available_from'] = $item->formatDate('available_from') ?? '-';
            $subsItem[$item->id]['available_to'] = $item->formatDate('available_to') ?? '-';
            $subsItem[$item->id]['available_date'] = $subsItem[$item->id]['available_from'] . ' / ' . $subsItem[$item->id]['available_to'];
            $subsItem[$item->id]['created_at'] = $item->created_at;
            $subsItem[$item->id]['quantity_of_occupation'] = $item->quantity_of_occupation == -1 ? 'Безлимит' : $item->quantity_of_occupation;
            $subsItem[$item->id]['active'] = ($item->available_to > $carbon && $item->quantity_of_occupation != 0 || $item->available_to == null && $item->quantity_of_occupation != 0) ? true : false;
            $subsItem[$item->id]['deleted_at'] = $item->deleted_at;

            $subsItem[$item->id]['client']['first_name'] = $item->user->first_name;
            $subsItem[$item->id]['client']['last_name'] = $item->user->last_name;
            $subsItem[$item->id]['client']['phone'] = $item->user->phone;
            $subsItem[$item->id]['client']['id'] = $item->user->id;

            $subsItem[$item->id]['subscription']['id'] = $item->subscription->id;
            $subsItem[$item->id]['subscription']['title'] = $item->subscription->title;

            $subsItem[$item->id]['order']['id'] = $item->order_id;
            $subsItem[$item->id]['order']['amount'] = $item->order ? $item->order->price : ' - ';
            $subsItem[$item->id]['order']['pay_source'] = $item->order ? ($item->order->payment_source == '1') ? __('Наличный') : (($item->order->payment_source == '2') ? __('Безналичный') : '-') : '-';
        }

        $this->viewParams['clientSubsModel'] = $this->clientSubsModel;
        $this->viewParams['items'] = $subsItem;
    }

    private function getClientList($company_id)
    {
        $response = [];

        $clients = CompanyClient::query()
            ->where(['company_id' => $company_id])
            ->with(['client'])
            ->get();

        foreach ($clients as $client) {
            $response[$client->client_id] = $client->client->first_name . ' ' . $client->client->last_name . ' (' . $client->client->phone . ')';
        }

        return $response;
    }
}
